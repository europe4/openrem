
	Progetto OpenPEC - README.txt di AggiornaLDAP
	Giovanni Faglioni - Nabla2 s.r.l. - 09 Aprile 2004

	AggiornaLDAP v0.2:

	Utility per tenere aggiornato un server ldap
	sotto la basedn "o=postacert" con i dati 
	dell'indice nazionale, senza dover fermare e 
	fare ripartire l'ldap server.
	
	Testato con server openldap 2.0.27
	Basato su ldiff: http://www.xonix.com/ldiff/

	Uso:

	- Installare openldap 2.0.x (openldap 2.1.x o
	2.2.x eseguono dei controlli aggiuntivi sulle
	strutture degli schemi LDAP, che sono incompatibili
	con gli schemi attuali del CNIPA)

	- Guardare in examples/slapd.conf per una conf
	di esempio valida per o=postacert
	(ricordarsi di copiare ind_pec.schema nella
	directory degli schemi...)

	- Creare la directory dove tenere il DB,
	(quella specificata in slapd.conf)

	- Fare partire ldap e provarlo con
	ldapsearch -x -h localhost -b "o=postacert" "(objectclass=*)"
	se risponde, ottimo.

	- Controllare che la dir dei DB sia owned dall'utente
	con cui gira il server ldap. Da noi e'
	chown -R ldap:ldap /var/lib/ldap-postacert
	La meta' delle volte ldap non si popola per via
	di questo problema. :(

	- Installare il modulo perl Net::LDAP dal cpan.
	(ad es. con perl -MCPAN -e shell e poi
	al prompt > install Net::LDAP)
	
	- Creare un utente con cui eseguire gli aggiornamenti
	ldap. Si puo' utilizzare lo stesso utente
	usato per openpec, oppure un utente creato ad hoc,
	ad esempio ldappec.

	- Andare nella home di questo utente, e starrare
	il file AggiornaLDAP-$VERSION.tar.gz
	(Es: AggiornaLDAP-0.2.tar.gz

	- Entrare nella directory cosi' 
	creata, e lanciare il comando ./configure, che
	provvedera' a settare correttamente i paths 
	dei comandi necessari e della directory
	di lavoro nei sorgenti di AggiornaLDAP e ScaricaLDIF.
	Se per caso questo passo dovesse dare errori,
	copiare AggiornaLDAP.dist in AggiornaLDAP,
	copiare ScaricaLDIF.dist in ScaricaLDIF,
	correggere la causa dell'errore e riprovare. 

	- Editare il file AggiornaLDAP.conf, inserendo i
	dati relativi al server LDAP locale che avete
	installato nel punto precedente (hostname, 
	binddn e bindpassword) ed al metodo
	con cui si vuole scaricare l'indice nazionale
	(ldap o https).
	Nota: AggiornaLDAP puo' girare anche su un
	host differente da quello che fa' da server
	ldap.
	
	Testare il tutto,  prima in modalita' Debug:
	./AggiornaLDAP -d 

	NON modifica il server locale, genera solo i files
	con i comandi da dare in pasto a ldapmodify
	(.add, .del e .mod)

	Provare a fare un primo update:
	./AggiornaLDAP

	Aggiorna l'ldap locale, rendendolo identico a quello
	nazionale, scaricando i dati, calcolando le differenze
	ed applicandole al server ldap locale.

	Se questo funziona, si puo' mettere in crontab
	con qualcosa tipo (sottintende una shell bash o sh):

00 00 * * * /path/alla/basedir/AggiornaLDAP 2>&1 > /var/log/openpec/AggiornaLDAP.log

	Questo aggiornera' ogni 24 ore il vostro ldap con
	quello nazionale.
	
	--Gio'

	ScaricaLDIF e' una utility per scaricare l'indice
	nazionale in formato .ldif via protocollo ldap o
	https, viene invocata da AggiornaLDAP ma puo' essere
	lanciata anche manualmente.

