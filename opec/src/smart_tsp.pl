#!/usr/bin/perl
#
# Project       OpenPec
# file name:    smart_tsp.pl
# package:      -
# Version:      2.2.23
#
# DESCRIPTION
# TSP
#
# Developer:
# Fanton Flavio
#
# History [++date++ - ++author++]:
# creation: 18/12/2008 - Fanton Flavio ( flazan@exentrica.it )
# modification:
#
#*  Copyright (C) 2006  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Roma 43 - 57126 Livorno (LI) - Italy
#   via Giuntini, 25 / int. 9 - 56023 Navacchio (PI) - Italy
#   tel. +39 050 754 703 - fax +39 050 754 707
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

use strict;
use MIME::Base64 ();
use File::Basename;
use File::Copy;
use Sys::Syslog;
use POSIX qw ( strftime );
use vars qw( $VERSION );

$VERSION = '2.23';


#--- Configurazione
# accesso TSA
my $CONST_TSA_HOST                      = '<host TSA>';
my $CONST_TSA_USER          = '<user>'; # undef se la TSA non prevede autenticazione
my $CONST_TSA_PSW           = '<password>'; # undef se la TSA non prevede autenticazione
my $tsa_cert;                           # path + nome certificato per la verifica
my $tsa_CAcert                          = '';   # path + nome certificato per la verifica
my $tsa_CApath;                                 # path dei certificati per la verifica

# LOG
my $CONST_LOG_MINLEVEL                  = 5; # da 5 a 1 dove 5 indica il maggiore dettaglio

# PATH file da marcare
# path sorgente (chiave) e destinazione (valore)
my $tsa_pathIN                  = '<dir IN>';
my $tsa_pathOUT                 = '<dir OUT>';
my $tsa_pathtmp                 = '/tmp'; # path tmp

# numero massimo di marche temporali (per sicurezza)
my $LIMIT                   = 100;

# path programmi esterni
my $pathOpenssl4tsa         = '/usr/local/ssl-tsa/bin/openssl';  # path di openssl + path tsa
my $pathWget                = '/usr/bin/wget';  # path wget

# pid
my $pidFileSmartTsp             = '/tmp/smart_tsp.pid';
#---

my $CONST_USER_AGENT        = 'Smart TSP ver. '.$VERSION;
my $CONST_POLLING_WAIT      = 3600;
my $CONST_REQUEST_NAME      = 'request.tsq';
my $CONST_MSGSUCC_VERIFY    = 'Verification: OK';   # messaggio di successo restituito da openssl ts -verify
my $CONST_RESPONSE_EXTENSION= 'tst';

my $LOG_MSGCOSTANT              = "*SMART_TSP $VERSION* "; # costante per syslog

$SIG{INT} = $SIG{TERM} = $SIG{HUP} = $SIG{PIPE} = \&signal_handler;
sub signal_handler {
    unlink $pidFileSmartTsp;
    exit(1)}

#--- parametri linea di comando
my $daemonize = 1;
my($cmd) = lc($ARGV[0]);
if ($cmd =~ /^(start|debug)$/) {
    if(-e $pidFileSmartTsp){
        print "Attenzione: $0 in esecuzione (pid file esistente)\n";
        #exit 1
    }
    print STDOUT "> >>>Starting - $0 - version: $VERSION\n";
    if($cmd eq 'debug'){
        $daemonize = '' ;
        $CONST_LOG_MINLEVEL = 5;
    }else{
        openlog($LOG_MSGCOSTANT, 'cons,pid', 'user') ;
    }

} elsif ($cmd =~ /^stop$/) {
    # recupero il pid
    my($pid);
    if(!open(PID_FILE, $pidFileSmartTsp)){
        print "Impossibile leggere il file $pidFileSmartTsp: $!\n";
        exit(1)}
    while (<PID_FILE>) { chomp; $pid = $1 if /^(\d+)$/ }
    if(!close(PID_FILE)){
        print "Impossibile chiudere il file $pidFileSmartTsp: $!\n";
        exit(1)}
    # signal
    if(!defined($pid)){
        print "PID invalido in $pidFileSmartTsp, impossibile $cmd\n";
        exit(1)}
    if(!kill('HUP',$pid)){
        print "Impossibile HUP $0[$pid]: $!\n";
        exit(1)}
    print STDOUT "> >>>Shutting down - $0 - version: $VERSION\n";
    exit 0;
} else {
    print "Argomento sconosciuto. Sintassi:\n  ".
          "$0 ( start | stop | debug )\n";
    exit 1;
}

$0 = $CONST_USER_AGENT;
$tsa_pathIN = _check_slash($tsa_pathIN);
$tsa_pathOUT = _check_slash($tsa_pathOUT);
$tsa_pathtmp = _check_slash($tsa_pathtmp);

# faccio polling su una dir. appena trovo un file
# lo elaboro e lo sposto sotto un'altra directory
if($daemonize){
        my $pid;
        unless($pid = fork){
            # process child
            while(1){
                &child();
                sleep($CONST_POLLING_WAIT)
            }
                exit(0)
        }
        die "Couldn't fork: $!" unless defined($pid);
        if(open(PID, ">$pidFileSmartTsp")){
            print PID $pid;
            close(PID)}
}else{
    &child()}

# ###########################################
# ###########################################

#
# Esegue fork ed exec catturando STDERR e STDOUT.
# PARAM IN: arr. ref costituente il comando con gli argomenti da eseguire
# PARAM OUT: (exit_code, stderr) in contesto lista o
#      exit_code in contesto scalare
#
sub _exec {
    my (@arg) = @_;
    my($child, $res);

    defined($child = open(OUT, '-|'))
        or return (-1,$!);

    if($child) {
        $res = join('', <OUT>);
        close(OUT) or not $! or return(-1,$!);
    } else {
        select(STDERR); $| = 1;
        select(STDOUT); $| = 1;
        open(STDERR, ">&STDOUT") or return(-1,$!);

        exec(@arg) or return(-1,$!);
    }

    return($?, $res);
}

#
# Scrive il log su syslog o STDOUT
#
# paramIN:   stringa     -   messaggio di log
#            integer     -   livello di log (1-4)
#
# paramOUT:  -
#
sub _writelog {
    my $intLevel    = shift;
    my $strItem     = shift;
    my $strLevel;
    my $prefix;

    # non faccio niente se il livello dell'item e' superiore a minlevel
    return if $CONST_LOG_MINLEVEL < $intLevel;

    unless( $daemonize ){
        $prefix = sprintf("%s %s[%s]: ", strftime("%Y %b %e %H:%M:%S", localtime), $LOG_MSGCOSTANT, $$);
        $strItem = $prefix.$strItem;
        print STDOUT $strItem,"\n";
        return}

    # mapping livellodebug-string - livellodebug-integer
    if($intLevel == 1){
        $strLevel = 'err';
    }elsif($intLevel == 2){
        $strLevel = 'warning';
    }elsif($intLevel == 3){
        $strLevel = 'notice';
    }else{
        $strLevel = 'info';
    }

    syslog($strLevel,$strItem);
}

sub _check_slash ($){
    my $path = shift; $path .= '/' if substr(  $path, -1, 1) ne '/'; $path}

#
# Esegue le seguenti operazioni:
# - recupera tutti i file sotto il path $tsa_pathIN e per ogni file:
# - genera la richiesta (sotto $tsa_pathtmp) per la TSA
# - spedisce la richiesta e salva la risposta (sotto $tsa_pathtmp)
# - verifico la risposta
# - sposto il file e la risposta della TSA sotto $tsa_pathOUT
#
sub child {
    &_writelog(5, "Verifica file pronti per marcatura temporale");
    opendir(DIR, $tsa_pathIN) || die $!;
    my %h;
    my $content;
    while( $content = readdir(DIR) ){
        next if -d "$tsa_pathIN$content";
        my($filename, $directories, $suffix) = fileparse($content, (".$CONST_RESPONSE_EXTENSION"));
        if(exists $h{$filename}){
            delete $h{$filename}
        }else{
            $h{$filename} = $suffix ? 0 : 1
        }
    }
    closedir DIR;

    my @logfiles = grep {  $h{$_}  } keys %h;
    unless(@logfiles){
        &_writelog(1, "Nessun file da marcare");
        return undef;
    }

    my @arguments;
    my($ec, $res);
    my $i;
    foreach (keys %h){
        $i++;
        if( $LIMIT && $i > $LIMIT ){
            &_writelog(0, "Raggiunto limite massimo ($LIMIT) di marcature possibili.\n");
            last}

        &_writelog(2, "Marcatura temporale del file $_");
        # genero la richiesta
        #openssl ts -query -sha1 -data test.txt -cert -out request.tsq
        undef @arguments;
        push @arguments, $pathOpenssl4tsa;
        push @arguments, 'ts';
        push @arguments, '-query';
        push @arguments, '-sha1';
        push @arguments, '-cert';
        push @arguments, '-data';
        push @arguments, $tsa_pathIN.$_;
        push @arguments, '-out';
        push @arguments, $tsa_pathtmp.$CONST_REQUEST_NAME;

        ($ec, $res) = _exec(@arguments);
        chomp($res);
        if ($ec != 0) {
            # errore definitivo per il processo figlio: esco
            &_writelog(0, "Errore definitivo durante la generazione della richiesta: $res");
            exit}
        &_writelog(5, "Richiesta generata ($CONST_REQUEST_NAME)");

        # spedisco la richiesta e salvo la risposta
        undef @arguments;
        push @arguments, $pathWget;
        push @arguments, $CONST_TSA_HOST;
        push @arguments, '--post-file='.$tsa_pathtmp.$CONST_REQUEST_NAME;
        push @arguments, '--header="Content-Type: application/timestamp-query"';
        push @arguments, '--no-check-certificate';
        push @arguments, "--http-user=$CONST_TSA_USER";
        push @arguments, "--http-passwd=$CONST_TSA_PSW";
        push @arguments, "-nv";
        push @arguments, '-O';
        push @arguments, "$tsa_pathIN$_.$CONST_RESPONSE_EXTENSION";

        ($ec, $res) = _exec(@arguments);
        chomp($res);
        if ($ec != 0 ) {
            # errore definitivo per il processo figlio: esco
            &_writelog(0, "Errore durante la comunicazione con la TSA: $res");
            # cancello l'eventuale file corrotto
            unlink "$tsa_pathIN$_.$CONST_RESPONSE_EXTENSION";
            next}
        &_writelog(5, "Risposta dalla TSA ricevuta e salvata ($_.$CONST_RESPONSE_EXTENSION)");

        # verifico la risposta
        # openssl ts -verify -sha1 -data test.txt -in response.tsr -untrusted tsa_ns.crt -CAfile cacert.crt
        undef @arguments;
        push @arguments, $pathOpenssl4tsa;
        push @arguments, 'ts';
        push @arguments, '-verify';
        push @arguments, '-sha1';
        if($tsa_cert){
            push @arguments, '-untrusted';
            push @arguments, $tsa_cert}
        if($tsa_CAcert){
            push @arguments, '-CAfile';
            push @arguments, $tsa_CAcert}
        if($tsa_CApath){
            push @arguments, '-CApath';
            push @arguments, $tsa_CApath}
        push @arguments, '-data';
        push @arguments, $tsa_pathIN.$_;
        push @arguments, '-in';
        push @arguments, "$tsa_pathIN$_.$CONST_RESPONSE_EXTENSION";

        ($ec, $res) = _exec(@arguments);
        chomp($res);
        if ($ec != 0 || $res ne $CONST_MSGSUCC_VERIFY) {
            # errore definitivo per il processo figlio: esco
            &_writelog(0, "Errore durante la verifica: $res");
            unless(unlink "$tsa_pathIN$_.$CONST_RESPONSE_EXTENSION"){
                &_writelog(0, "ATTENZIONE: la marca $tsa_pathIN$_.$CONST_RESPONSE_EXTENSION non e' ".
                              "stata cancellata, provvedere manualmente per ritentare la marcatura di ".
                              "$tsa_pathIN$_");
            }
            next}
        &_writelog(5, "Verifica effettuata con successo");

        # verifica con successo
        # spostare il file di log e la marca temporale (copiare e dopo cancellare)
        # se file o marca sono gia' presenti in destinazione non faccio niente
        if(-e $tsa_pathOUT.$_ or -e "$tsa_pathOUT$_.$CONST_RESPONSE_EXTENSION"){
            &_writelog(0, "Errore durante la copia: file o marca gia' presenti in destinazione");
            next}
        unless( move($tsa_pathIN.$_, $tsa_pathOUT) ){
            &_writelog(0, "Errore durante la copia del file di log '$_' da '$tsa_pathIN' a '$tsa_pathOUT'");
            exit}
        #unless( move($tsa_pathtmp.$CONST_RESPONSE_NAME2, $tsa_pathOUT.$_.'.'.$CONST_RESPONSE_NAME2) ){
        unless( move("$tsa_pathIN$_.$CONST_RESPONSE_EXTENSION", $tsa_pathOUT) ){
            &_writelog(0, "Errore durante la copia del file di log '$_.$CONST_RESPONSE_EXTENSION' da '$tsa_pathIN' a '$tsa_pathOUT'");
            exit}

        &_writelog(0, "Marcato e storicizzato correttamente il file $_");
    }
}
