#!/usr/bin/perl
#
# $Id: opec.pl,v 1.57 2013/03/28 16:59:02 ldivizio Exp $stat
#
# Project       OpenPec
# file name:    opec
#
# DESCRIPTION
# Soluzione Open Source per la Posta Elettronica Certificata
# File di avvio
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   23/01/2004 - Fanton Flavio
#               - corretti bugs vari
#   19/06/2004 - Fanton Flavio
#               - gestione conservativa del ritorno di corePec
#   05/07/2004 - Fanton Flavio
#               - aggiunto sistema di memorizzazione
#         per eliminare il problema della ridondanza
#   04/01/2005 - Fanton Flavio
#               - segnalazione e sentiti
#         ringraziamenti a Stefano Brandimarte [stevens@ced.it]
#       - Aggiornata corePec(): migliorata la gestione del Message-ID.
#   06/09/2005 - Fanton Flavio
#       - Aggiunta la gestione dei segnali con Sys::SigAction
#   17/05/2006 - Fanton Flavio
#       - Aggiunta gestione comandi esterni
#       - Gestione task
#       - Revisione Bookmark
#       - Revisione punto di ricezione
#       - Revisione punto di accesso
#   xx/xx/2006 - Fanton Flavio
#       - obbligatorio il controllo sull'appartenenza della mail al sistema PEC
#   03/07/2006 - Fanton Flavio
#       - Revisione gestione ricevute
#       - Revisione gestione avv. di mancata cons. per virus
#       15/09/2006 - Fanton Flavio
#       - revisione in funzione delle CRL
#       06/10/2006 - Fanton Flavio
#       - aggiunto nel punto di ricezione l'opzione di buttare o generare
#         anomalia per messaggi con header corretto ma con firma non verificata
#       06/11/2006 - Fanton Flavio
#       - corretto bug sul nome del file delle mail con virus
#       01/06/2007 - Fanton Flavio
#       - revisione completa del log legale
#       13/06/2007 - Fanton Flavio
#       - aggiunta risoluzione dominio - gestore
#       14/06/2007 - Fanton Flavio
#       - aggiunta gestione code per RMC tempi max secondo cui viene creato un task
#         per ogni destinatario esterno
#       23/07/2007 - Fanton Flavio
#       - aggiunto il controllo della dimensione del msg al punto di accesso
#       23/07/2007 - Fanton Flavio
#       - portato da 5 a 2 l'errore ESMTP restituito al MTA in caso
#         di dispaccio avviso di rilevazione virus
#       10/10/2007 - Fanton Flavio
#       - corretta la visualizzazione del gestore per le anomalie di msg
#       17/01/2008 - Fanton Flavio
#       - corretto problema sul bookmark in caso di recover dopo il dispaccio
#         locale del DT
#       18/01/2008 - Fanton Flavio
#       - adattata dispatchReport e dispatchReport secondo quando modificato in
#         Opec::Message
#       22/01/2008 - Fanton Flavio
#       - revisione completa bookm
#       22/01/2008 - Fanton Flavio
#       - aggiunto flusso per PA
#       29/01/2008 - Fanton Flavio
#       - aggiunta la cancellazione dei task in caso di ricezione di ricevute di
#         mancata consegna
#       04/01/2008 - Fanton Flavio
#       - adattato alle ultime modifiche di Opec::Sign
#       08/01/2008 - Fanton Flavio
#       - Revisione del log in caso di errore di elaborazione
#       26/03/2008 - Fanton Flavio
#       - aggiunta la possibilita' di impostare da file conf il dominio contenuto
#         nel Message-ID
#       06/04/2009 - Fanton Flavio
#       - inserisco tutti i messaggi il cui sender e' non certificato nel
#         punto di ricezione e tolgo al punto di accesso la gestione
#         dei mess il cui sender e' non certificato
#       09/06/2009 - Fanton Flavio
#       - anticipata la chiamata a validateRecipt con conseguente modifica alla
#         gestione dei bookmark al punto di accesso
#       - aggiunta validateRecipt dove necessario
#       16/07/2010 - Fanton Flavio
#               - esecuzione di $OnErrorExecute in caso di errore terminante di un processo
#       26/08/2010 - Fanton Flavio
#               - bugfix reload netHsm nCipher: adesso Opec::Sign viene inizializzato nella
#         init di ogni processo figlio; questo aggiunge 2 problemi marginali:
#         1. rallentamento di 0.5sec ogni volta che un child viene generato
#         2. una errata conf del modulo (es. nome chiave priv errata) viene
#            individuata durante l'elaborazione della mail e non durante lo
#            start generale
#       10/09/2010 - Fanton Flavio
#               - aggiornato XML in caso di sistema sospeso
#       26/10/2010 - Fanton Flavio
#               - aggiunte le info per la risposta verso MTA configurabile
#       05/11/2010 - Fanton Flavio
#               - introduzione delle mappe
#       22/12/2010 - Fanton Flavio
#               - bugfix su subject con errore nella decodifica
#       03/01/2011 - Fanton Flavio
#               - aggiunto il parametro $preserve_tmp_email per per mantantenere la struttura
#         di lavoro temporanea dell'email in solo in caso d'errore
#   26/05/2011 - Fanton Flavio
#           - aggiunta compatibilita' con log xml
#   03/06/2011 - Fanton Flavio
#           - fix aggiornamento cache dei domini locali
#       26/07/2011 - Fanton Flavio
#               - bugfix emissione ricevuta di non accettazione in modalita' sospesa introdotto
#                 con la chiamata extraInfo
#       26/08/2011 - Fanton Flavio
#               - dettagliato il controllo sul messaggio in ingresso
#       31/01/2012 - Fanton Flavio
#               - Aggiunto il parametro $delivery_NO_certLight che fornisce la possibilita' di eliminare i
#         messaggi in ingresso che non contengono la firma come definita dalle regole tecniche.
#         Controllo piu' leggero di $delivery_NO_cert dove si entra nel
#         merito della firma
#   17/02/2012 - Fanton Flavio
#       - aggiunto controllo tra header e xml allegato: viene verificato se il tipo
#         di messaggio dichiarato nel header è lo stesso di quello in xml
#         Il controllo è particolarmente critico perchè la sola informazione contenuta in header
#         non è sottoscritta dal gestore quindi facilmente attaccabile
#   25/09/2012 - Fanton Flavio
#       - aggiornamento in funzione di Opec::Message::set_size
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#   via Lenin 132/a int. 26 - San Martino Ulmiano
#   56017 San Giuliano Terme (PI) - Italy
#   tel. +39 050 898 111 - fax +39 050 861 200
#   www.ksolutions.it
#
#*  Copyright (C) 2006  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

=head1 NAME

OpenPEC2 - Soluzione Open Source per la Posta Elettronica Certificata

=head1 SYNOPSIS

opec [opzioni] ( start | stop | suspend | debug )

=head1 OPTIONS

=over 8

=item B<--config-file|-c> conf-file

Usa conf-file come file di configurazione.

=item B<--no-check-virus|-n>

Non esegue l'antirvirus.

=item B<--help>

Visualizza questo help ed esce.

=back

=head1 DESCRIPTION

Soluzione Open Source per la Posta Elettronica Certificata.

=head1 AUTHOR

Flavio Fanton <flazan@gmail.com>
Idee, suggerimenti e segnalazioni sono benvenuti.

=cut


package Opec;


use strict;
use vars qw( @ISA );
use Net::Server::PreForkSimple;
use Sys::SigAction qw( set_sig_handler );
use Errno qw( ENOENT );
use DB_File;
use Getopt::Long;
use Pod::Usage;

use Opec::Conf qw( :confvars :const :platform :av_vars :scheduler :log :maps :xml);
use Opec::Util qw( &doLog &doLogXml &prolongTimer &msgId &sanity_check &getDomain
                   &myChomp &enqRMC &delRMC &delRMC12 &delRMC24 &enqSPINLOG &delSPINLOG
                   &splitAddress &getAddresses);
use Opec::Sign;
use Opec::In::Connection;
use Opec::Out::Dispatcher;
use Opec::Command::Dispatcher;
use Opec::Command qw( &deqTask );
use Opec::LdapTool;
use Opec::Lock;
use Opec::Maps::Mapper;
use Opec::Receipt qw( &getAcceptanceReceipt
                      &getGetLoadReceipt
                      &getDeliveryReceipt
                      getNotAcceptanceReceipt
                      &getXml_identificativo
                      &getXml_ricezione
                      &getXml_consegna
                      &getXml_gestore
                      &getNotAcceptanceReceiptVirus
                      &getReceiptVirusFound
                      &getNoDeliveryVirusReceipt
                      &getNotAcceptanceReceiptMaxSize
                      &getNotAcceptanceReceiptMaxRcpt
                      &getNotAcceptanceDLerr);
use Opec::Transport qw( &generateTransport
                        &getTransportFault );
use Opec::Bookmarks qw( &createBookmark
                        &appendBookmark
                        &getBookmark
                        &eraseBookmarks
                        &getTag
                        &lastErr
                        &BIS);
use Opec::Timing qw(&init &sectionTime &extraInfo &report &setMailSize);


@ISA = qw( Net::Server::PreForkSimple );

my $CONST_COMPATIBILITY_SCHEDULER   = '0.02';   # versione compatibile dello scheduler

# Variabili globali al singolo processo
# $inObj                  gestore traffico in ingresso
# $objDispatcher          gestore traffico in uscita
# $objLdap                interfaccia LDAP
# $tempDir                path temporaneo (GLOBALE per poterlo ricliclare)
# $childInvocationCount   contatore di richieste
# $child_task_count       cont. chiamate corePec
# $msgCount               cont. messaggi locali
# $signCode               codice di ritorno dell'init di Opec::Sign
# %hBookMarkTable         gestore bookmark
# $avObj                  gestore antivirus
# $objCommand             interfaccia per l'esecuzione dei task di amministrazione
# $fileDbCache            nome file cache in uso dal processo
# $lockDbCache            mantiene il lock sul file per tutta la vita del processo
# $suspend_mode                           1 se il sistema e' in modalita' sospesa
use vars qw($inObj
            $objDispatcher
            $objLdap
            $tempDir
            $childInvocationCount
            $child_task_count
            $msgCount
            $signCode
            $objSign
            %hBookMarkTable
            $avObj
            $objCommand
            $fileDbCache
            $lockDbCache
            $suspend_mode);

# MAPPING (per comodita') fra le label usate
%hBookMarkTable = ( 'INIT',                 'INIT',
                    'PR_VERIFY',            'PR_VERIFY',
                    'PR_RICPRESAINCARICO',  'PR_RICPRESAINCARICO',
                    'PR_DOCTRANSP',         'PR_DOCTRANSP',
                    'PR_ANOMTRASP',         'PR_ANOMTRASP',
                    'PR_RICAVVCONS',        'PR_RICAVVCONS',
                    'PR_AVIRUS',            'PR_AVIRUS',
                    'PR_AVIRUSFOUND',       'PR_AVIRUSFOUND',
                    #+++
                    'PA_ANOMTRASP',         'PA_ANOMTRASP',
                    'PA_RICACCETTAZIONE',   'PA_RICACCETTAZIONE',
                    'PA_VALIDATERECIPT',    'PA_VALIDATERECIPT',
                    'PA_DOCTRANSP',         'PA_DOCTRANSP',
                    'PA_AVIRUS',            'PA_AVIRUS');


#--- opzioni da riga di comando
my($configFile) = '%%CONFFILE%%'; # path del file di configurazione
my $noCheckVirus = 0;
my $help = 0;
GetOptions(
           'config-file|c=s' => \$configFile,
           'no-check-virus|n' => \$noCheckVirus,
           'help|?' => \$help,
          ) or pod2usage(1);
pod2usage(
          -exitval => 0,
          -verbose => 99,
          -sections => "SYNOPSIS|OPTIONS|AUTHOR",
         ) if $help || !$ARGV[0];


#--- carico il file di conf
Opec::Conf::read_config($configFile);


#--- parametri linea di comando
my($cmd) = lc($ARGV[0]);
if ($cmd =~ /^(start|debug|suspend)$/) {
    if(-e $pidFile){
        print "Attenzione: OpenPec in esecuzione (pid file esistente)\n";
        exit 1}
    if($cmd eq 'debug'){
        $daemonize = '';
        $maxServers = 1;
        $preserve_tmp_email = 1;
    }
    if($cmd eq 'suspend'){
        $suspend_mode = 1;
        print STDOUT "> >>> SUSPEND MODE <<< <\n";
    }
    print STDOUT "> >>>Starting - OpenPEC at $myhostname - version: $VERSION (conf: $configFile)\n"
        if($daemonize);
} elsif ($cmd =~ /^stop$/) {
    # recupero il pid
    if ($pidFile eq ''){
        print "Parametro di configurazione pid_file non definito, ".
                  "impossibile $cmd\n";
            exit(1)}
    my($opecd_pid);
    if(!open(PID_FILE, $pidFile)){
        print "Impossibile leggere il file $pidFile: $!\n";
        exit(1)}
    while (<PID_FILE>) { chomp; $opecd_pid = $1 if /^(\d+)$/ }
    if(!close(PID_FILE)){
        print "Impossibile chiudere il file $pidFile: $!\n";
        exit(1)}
    # signal
    if(!defined($opecd_pid)){
        print "PID invalido in $pidFile, impossibile $cmd\n";
        exit(1)}
    if(!kill('QUIT',$opecd_pid)){
        print "Impossibile QUIT opecd[$opecd_pid]: $!\n";
        exit(1)}
    print STDOUT "> >>>Shutting down - OpenPEC at $myhostname - version: $VERSION\n";
    exit 0;
} else {
    pod2usage(-msg => "Argomento sconosciuto: $ARGV[0]",
              -exitval => 1,
              -verbose => 99,
              -sections => "SYNOPSIS|OPTIONS|AUTHOR");
}

#--- init log
Opec::Log::init($myversion, !$daemonize);   #originale
prolongTimer('INIZIALIZZAZIONE...', 60);
if($suspend_mode){
    doLog(0, "> >>> SUSPEND MODE <<< <");
    doLog(1, "> >>> SUSPEND MODE <<< <");
}

#--- precarico il/i modulo parametrico per gestire
#    il traffico TCP in ingresso
foreach (values %receiveTCP){
    eval "require $_" or
    die "Impossibile caricare $_: $@"}

#--- check server LDAP
eval {$objLdap = Opec::LdapTool->new(1)};
unless(ref($objLdap)){
    $@ && die "$@\n";
    die "Problemi durante la connessione verso il server LDAP\n"
}
$adminPecEmail = $objLdap->getMailReceipt();
undef $objLdap;
die "Impossibile recuperare l'indirizzo di mail di riferimento del fornitore, verificare il parametro \$providerName\n"
    unless($adminPecEmail);
$myDomain = (split (/@/,$adminPecEmail,2) )[1] if !defined $myDomain;

#--- precarico il modulo antivirus
my ( $av_mod ) = split(/:/, $av);
$av_mod = 'Opec::AV::'.$av_mod if($av_mod);
eval "require $av_mod" or die "Impossibile caricare $av_mod: $@\n";
my $objTmp = $av_mod->new;
die "Attenzione il sistema di antivirus sembra down: verificare il parametro \$av o lo stato del server\n"
    unless( ref($objTmp) );
undef $objTmp;

#--- precarico le mappe
my $obj_mapper = Opec::Maps::Mapper->new();
die "Errore di caricamento delle mappe: ".join(", ", @$obj_mapper),"\n"
        if(ref($obj_mapper) eq 'ARRAY');

#--- check modulo Sign
##my $objSignTmp = Opec::Sign->new($objLdap);
##unless(ref($objSignTmp)){
##    doLog(1, $objSignTmp);die "$objSignTmp\n"}
##undef $objSignTmp;

# CRL
print STDERR "WARNING: CRL inesistente, generarla quanto prima\n"
    unless(-e $PATH_CACHE.$CRL_filename);

#--- inizializzazione DB cache
# creo una copia del file dbm per ogni processo
# - Funzionamento -
# A regime esisteranno n copie + 1 del file dbm dove n e' il numero dei processi.
# Ogni processo legge la sua copia mentre una sola viene modificata da
# un utility esterna che la sincronizza con LDAP
# quando il singolo processo si accorge che la sua copia e' obsoleta
# provvede ad aggiornarla ricopiandola; operazione che viene eseguita ad inizio
# elaborazione mail.
# Quest'approccio consente:
# 1. di gestire la concorrenza in modo semplice e sicuro
# 2. di rendere atomico l'aggiornamento del dbm rispetto all'elaborazione della mail
# 3. velocita'
if($localCertDomains =~ /^hash$/i ){
    # pulisco tutti i file cache bloccati
    my $fh;
    opendir(CACHE, $PATH_CACHE) || die "Impossibile accedere a $PATH_CACHE: $!\n";
    while(my $file = readdir(CACHE)){
        next if ( $file !~ /^$DBM_localdomains\..+/ );
        unlink $PATH_CACHE.$file}
    closedir CACHE;

    # duplico i db
    my $path_DBM_localdomains = $PATH_CACHE.$DBM_localdomains;
    $fh = new FileHandle($path_DBM_localdomains, O_CREAT|O_RDWR);
    locksh($fh);
    for(my $i=1; $i<=$maxServers; $i++){
        die "Impossibile creare copie del file cache ($path_DBM_localdomains)\n"
            unless (system ("cp $path_DBM_localdomains $path_DBM_localdomains.$i") == 0 &&
                    chown($daemonUserUid, $daemonUserGid, "$path_DBM_localdomains.$i") == 1);
    }
    unlock($fh);
    $fh->close;
}

#--- configurazione Net::Server
my @port = map { "$_/tcp" }
                (ref $inetSocketPort ? @$inetSocketPort :
                 $inetSocketPort ne '' ? $inetSocketPort : () );
push @port, "$scheduler_sck|unix";



my $server = bless {
    server => {
        # listen on the following sockets (one or more):
        port => \@port,
        # limit socket bind (e.g. to the loopback interface)
        host => ($inetSocketBind eq '' ? '*' : $inetSocketBind),

        max_servers  => $maxServers,  # number of pre-forked children
        max_requests => $maxRequests, # restart child after that many accept's

        user  => $daemonUser,
        group => $daemonGroup,
        pid_file   => $pidFile,
        lock_file  => $lockFile,  # serialization lockfile
      # serialize  => 'flock',     # flock, semaphore, pipe
        background => $daemonize ? 1 : undef,
        setsid     => $daemonize ? 1 : undef,
        chroot     => undef,
        no_close_by_child => 1
    }
}, 'Opec';

#--- avvio servizio
$0 = 'opecd (master)';
alarm(0);  # resetto il timer dell'inizializzazione
doLog(4,"timer azzerato");
$server->run;  # controllo gestito da Net::Server

exit 1; #se arriva qua errore

#
# Flusso nome processo figlio:
# . avvio processo: "opecd (virgin child)"
# . arrivo richiesta: "opecd (child-ready)"
# . richiesta in elaborazione: "opecd (child-<id interno msg>)"
# . richiesta terminata: "opecd (child-ready)"
#
# PERSONALIZZAZIONE
#
sub process_request {
    my($self) = shift;
    my($prop) = $self->{server};
    my($sock) = $prop->{client};
    local $SIG{CHLD} = 'DEFAULT';
    local $SIG{QUIT} = 'IGNORE';

    Opec::Log::request_id(1);
    doLog(1, "Nuova richiesta");
#    doLog(0, "Nuova richiesta");
    init();

    #--- verifico l'esito dell'inizializzazione
    unless(ref($objLdap)){
        doLog(1, $objLdap);die $objLdap}
    unless(ref($objDispatcher)){
        doLog(1, $objDispatcher);die $objDispatcher}
    unless(ref($objSign)){
        doLog(1, $objSign);die "$objSign\n"}
    unless( ref($avObj) ){
        doLog(1, $avObj);die $avObj}
    unless( ref($objCommand) ){
        doLog(1, $objCommand);die $objCommand}

    my $prototmp = $sock->NS_proto;
    my $cmd_nametmp;
    eval {
        # inizializzo il timer con $childTimeout
        my $h = set_sig_handler( 'ALRM' ,sub { die "timed out\n"; } );
        prolongTimer('nuova richiesta - timer reset', $childTimeout);

        my($conn) = Opec::In::Connection->new;
        $conn->proto($sock->NS_proto);
        doLog(2, "Protocollo: ".$sock->NS_proto);

        if($sock->NS_proto eq 'TCP'){
            doLog(2, "Dettagli connessione: ".
                     $prop->{peeraddr}.":".
                     $prop->{sockport}." - ".
                     $receiveTCP{$prop->{sockport}});
            $conn->socket_ip($prop->{sockaddr});
            $conn->socket_port($prop->{sockport});
            $conn->client_ip($prop->{peeraddr});

            if($localCertDomains eq 'hash'){
                unless(-e $fileDbCache){    # prima volta: la copia della cache non e' bloccata
                    # trovo una copia di dbm libera e impsto il lock (che consiste in un rename)
                    opendir(CACHE, $PATH_CACHE) || die "Impossibile aggiornare la cache dbm: $!\n";
                    while(my $file = readdir(CACHE)){
                        next if $file !~ /^$DBM_localdomains\.\d+$/o;

                        $lockDbCache = new FileHandle($PATH_CACHE.$file, O_RDWR);
                        if( lock_u($lockDbCache) ){
                            # lock acquisito
                            # il lock dura quanto $fh_lock_cache quindi fino alla morte del processo
                            $fileDbCache = $PATH_CACHE.$file;
                            last;
                        }
                        undef $lockDbCache;
                    }
                    closedir CACHE;
                    $fileDbCache or die "Impossibile bloccare la copia cache.\n";

                    tie %hLocalCertDomains, 'DB_File', $fileDbCache;
                }

                if(-M $PATH_CACHE.$DBM_localdomains < -M $fileDbCache){    # Aggiornamento dbm
                    untie %hLocalCertDomains;
                    unlock($lockDbCache);
                    undef $lockDbCache;

                    my $fh = new FileHandle($PATH_CACHE.$DBM_localdomains, O_RDWR);
                    locksh($fh);
                    die "Impossibile creare copie del file cache ($PATH_CACHE$DBM_localdomains)\n"
                        unless (system ("cp $PATH_CACHE$DBM_localdomains $fileDbCache") == 0);
                    unlock($fh);
                    $fh->close;

                    $lockDbCache = new FileHandle($fileDbCache, O_RDWR);
                    die "Impossibile acquisire il lock della copia di dbcache" unless(lock_u($lockDbCache));

                    tie %hLocalCertDomains, 'DB_File', $fileDbCache;
                }
            }

            if($inObj && $inObj->objmsg){
                # dalla seconda richiesta (per processo)
                # Opec::In::Smtp vecchia muore e con la nuova
                # cerchiamo di riciclare il path temp.
                $inObj->objmsg->tempdirkeep(1);
                $tempDir = $inObj->objmsg->tempdir}

            $inObj = $receiveTCP{$prop->{sockport}}->new();

            prolongTimer('smtp in');
                $inObj->process_request($sock, $conn, \&corePec, $tempDir, \&report);

        }elsif($sock->NS_proto eq 'UNIX'){
            # SEZIONE SCHEDULER
            doLog(2, "Connessione scheduler");
            local $eol = "\015\012";
            my $res;
            # imposto 60 sec. per l'esecuzione di tutti i comandi
            # !!!VERIFICARE CHE L'ALLARME DELLO SCHEDULER + TEMPO DI ATTESA FRA 2 RICHIESTE
            # SIANO TALI DA SUPERARE QUESTO VALORE - in caso contrario lo scheduler rischia
            # di saturare i processi di opec!!!
            prolongTimer('Richiesta cmd esterni');
            ( defined(send($sock, $Opec::Conf::VERSION.$eol, 0)) &&
              defined(recv($sock, $res, 4096, 0)) )     or die "Errore di trasmissine $!";
                if( _chk_scheduler(myChomp($res)) ){
                    doLog(4, "Scheduler compatibile (ver. ".myChomp($res).")");
                    send($sock, "1$eol", 0) or die "Errore di trasmissine $!";
                    my $result;
                    while(1){
                        defined(recv($sock, $res, 4096, 0)) or die "Errore di trasmissine $!";
                        last if $res eq "$eol.$eol";    # chiusura

                        if($res =~ /^(.+)$eol(.+)$eol$/){   # rivedere l'expressione regolare in modo da non consentire caratteri diversi da $eol (tipo \r ...)
                            my ($cmd_name, $msg_name) = (myChomp($1), myChomp($2));
                            $cmd_nametmp = $cmd_name;
                            my $extObj = deqTask($msg_name);
                            unless( ref($extObj) ){ doLog(0, $extObj); last }

                            $result = $objCommand->cmdExec($cmd_name, $msg_name, $extObj);

                            if(defined $result && $result==1){
                                # ok
                            send($sock, "1$eol", 0) or die "Errore di trasmissine $!";
                                doLog(4, "$cmd_name eseguito")
                            }elsif(defined $result){
                                send($sock, $eol, 0) or die "Errore di trasmissine $!";
                                doLog(4, "Errore durante l'esecuzione del comando $cmd_name: $result")
                            }else{
                                send($sock, $eol, 0) or die "Errore di trasmissine $!";
                                doLog(4, "Errore durante l'esecuzione del comando (time out?)")}
                        }
                    }
                sectionTime('cmd ext');

                }else{
                    # scheduler non compatibile esco
                    send($sock, "0$eol", 0);
                doLog(2, "Scheduler non compatibile (ver. ".myChomp($res).")");
            }
                }else{
                    die "protocollo non supportato: " . $sock->NS_proto."\n"}

    };
    alarm(0);  # resetto il timer
    doLog(5,report());
    doLog(1, "Richiesta chiusa");

    if ($@ ne '') {
        # Errore in eval
        my($msg) = $@ eq "timed out"
                        ? "Il processo supera il tempo a disposizione ($childTimeout secondi), termino"
                        : "TROUBLE?: $@";
        doLog(1, $msg);
        doLog(0, "ERRORE - Errore durante l'elaborazione");

        if(!$preserve_tmp_email && $inObj && $inObj->objmsg){
                $inObj->objmsg->preserve_evidence(0)
        }

        if(defined $OnErrorExecute){
                    doLog(1, "Esecuzione di $OnErrorExecute");
                    doLog(1, "Errore nell'esecuzione del comando $OnErrorExecute, ".
                                 "verificare il parametro \$OnErrorExecute")
                        unless (system ($OnErrorExecute,
                                                        $VERSION,
                                                        $msg) == 0);
        }

        # tag di chiusura
        doLog(0, "*** NEW MAIL - END")
            if( $prototmp eq 'TCP' or ($prototmp eq 'UNIX' and
                ($cmd_nametmp && substr($cmd_nametmp,0,3) eq 'RMC' )  ) );

        msgId(undef);
        die "\n";
    }
    msgId(undef);
}


#
# IN: 1. connessione (obj Open::In::Connection)
#     2. messaggio in ingresso (obj Opec::Message)
# OUT: 1. codice di risposta :
#           1 - ok
#           2 - errore temporaneo (l'MTA puo' ritentare)
#           3 - errore permanente (l'MTA non deve riprovare)
#      2. stringa d'errore del tipo "x.x.x errore in sintesi"
#         (dove x.x.x segue RFC1893 - Enhanced Mail System Status Codes)
#
sub corePec ($$) {
    my ($conn, $msgInfo) = @_;
    my @arrOut;

    $child_task_count++;
    prolongTimer('elaborazione messaggio', $childTimeout);
    sectionTime('smtp in');

    # filtro cortocircuito MTA
    if( !$msgInfo->checkCert() ){
        # mail non coinvolta nel processo di certificazione
            doLog(2, "Filtro opec: mail restituita a MTA");
        @arrOut = $objDispatcher->mailDispatch($msgInfo, 0);
            sectionTime('dispatched filtro MTA');
        return @arrOut}

    my $reciptValue =
        myChomp( $msgInfo->header->get($OPEC_HEADER_KEY_RECEIPT) );
    my $mailDocTraspValue =
        myChomp( $msgInfo->header->get($OPEC_HEADER_KEY_DOCTRASP) );

        setMailSize($msgInfo->mail_size);

    # modalita di sospensione
    if( $suspend_mode ){
        if( defined $reciptValue or defined $mailDocTraspValue ){
            # messaggio in INBOUND
            # buco nero
            doLog(0, "SISTEMA SOSPESO - Eliminazione messaggio");
            return (1, "2.5.0 OK")
        }else{
            # messaggio in
            # emetto un ricevuta di non accettazione per eccezioni formali
            # generazione ricevuta di non accettazione per superamento dimensione
            prolongTimer('sistema sospeso');

            doLog(4, "Check indice delle pubbliche amministrazioni");
            eval{$objLdap->validateRecipt($msgInfo->recips())};
            # in caso di errore LDAP mantengo il messsaggio
            # nella coda MTA per successiva elaborazione
            return (2,"4.4.3 Requested action aborted: error in processing") if($@);

            doLog(0, "SISTEMA SOSPESO - Generazione ricevuta di non accettazione");
            my $arefGetNotAcceptanceReceipt = getNotAcceptanceReceipt($msgInfo,\$msgCount,'sistema sospeso');
            if( !ref($arefGetNotAcceptanceReceipt) ){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                        "Errore durante la generazione dell'avviso di non accettazione");
                return (3, "5.7.7 Request action not taken: error to generate the cert mail");
            }
            sectionTime("generazione $OPEC_MTARESPONSE_TAGS_RDNAC",
                                "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_INBOUND);

            dispatchReportSummary($arefGetNotAcceptanceReceipt);

            $arefGetNotAcceptanceReceipt = $objSign->Sign($arefGetNotAcceptanceReceipt);
            if(!ref($arefGetNotAcceptanceReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                        "Errore durante la firma dell'avviso di non accettazione per ".
                        "superamento dimensione: ".$objSign->error());
                return (3, "5.7.7 Request error to sign formal incorrect receipt")
            }
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDNAC");

            doLog(0, "Dispaccio avviso");
                    @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 2);
                    sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSEND");
            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_SIZE",
                                $arefGetNotAcceptanceReceipt->[0]->mail_size);
            dispatchReport($arefGetNotAcceptanceReceipt);

            return @arrOut;
        }
    }

    if(defined $reciptValue && defined $mailDocTraspValue){
        # errore nell'header
        doLog(0, "Errore: header ambiguo");
        return (3, "5.7.7 Request action not taken: header ambiguous");
    }elsif(defined $reciptValue){
        # ricevute inbound
        # recupero il tipo di ricevuta
        my $receiptName = 'ND';
        if( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_ACCEPT ){
                $receiptName = $OPEC_MTARESPONSE_TAGS_RDAC
        }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_GETLOAD ){
                $receiptName = $OPEC_MTARESPONSE_TAGS_RPC
        }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_DELIVERY ){
                $receiptName = $OPEC_MTARESPONSE_TAGS_RDC
                }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY ){
                        $receiptName = $OPEC_MTARESPONSE_TAGS_RDEC
                }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT ){
                        $receiptName = $OPEC_MTARESPONSE_TAGS_RDNAC
                }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND ){
                        $receiptName = $OPEC_MTARESPONSE_TAGS_RRV
                }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_RMC ){
                        $receiptName = $OPEC_MTARESPONSE_TAGS_RMC
                }

        prolongTimer("Ricevuta in ingresso ($receiptName): verifica");
            doLog(0, "Ricevuta/Avviso inbound");
            $msgInfo->name($reciptValue);

        #$signCode = $objSign->Verify($msgInfo);
        $signCode = $objSign->Verify($msgInfo,$reciptValue);
        sectionTime("verifica ricevuta $receiptName");
            extraInfo(  "$receiptName.$OPEC_MTARESPONSE_Q_DIRECTION",
                                $OPEC_DIRECTION_INBOUND);
        doLog(4, "Verifica: $signCode (".$objSign->error().")");

        if( !defined $signCode or $signCode != 1 ){
            # Ricevuta NON verificata
            if( !$delivery_NO_cert or
                (!$delivery_NO_certLight &&
                $objSign->error() &&
                $objSign->error() eq 'Messaggio senza certificato')){

                doLog(0,"$reciptValue - Esito: Errore durante la verifica della firma".
                        " - Elimino il messaggio");
                                return (1, "2.5.0 OK")
            }
            #return (1, "2.5.0 OK") unless( $delivery_NO_cert );

            # antivirus
            prolongTimer('Scan virus ricevuta');
            my $av_scan_result = $avObj->scan( $msgInfo->mail_text() );
            sectionTime('antivirus');
            if(ref($av_scan_result)){
                # mail con virus
                doLog(0, "Mail con presenza di VIRUS!");
                doLog(0, "Dettagli virus: " . join(', ', @$av_scan_result));
                doLog(0, "Mail eliminata");

                return (1, "2.5.0 OK")
            }elsif($av_scan_result){
                return (2, "4.7.7 Error to scan mail by antivirus")
            }

            # delivero cmq come anomalia di messaggio
            prolongTimer('anomalia di trasporto');
            doLog(0, "Ricevuta/Avviso NON verifica, delivero comunque");

            doLog(4, "Generazione anomalia di trasporto");
            my $objAnomTrasp = getTransportFault($msgInfo, "ricevuta non verificata");
            if( !ref($objAnomTrasp) ){
                doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_ANOM - Esito: ".
                        "Errore durante la generazione della anomalia di messaggio");
                return (2, "4.7.7 Request action not taken: error to generate the cert mail")}

            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_AM",
                                "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_TSSTART");
            extraInfo(  "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_DIRECTION",
                                $OPEC_DIRECTION_INBOUND);

            dispatchReportSummary($objAnomTrasp);

            doLog(4, "Firma anomalia di trasporto");
            $objAnomTrasp = $objSign->Sign($objAnomTrasp);
            if(!ref($objAnomTrasp)){
                doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_ANOM - Esito: ".
                        "Errore durante la firma della anomalia di messaggio - ".
                        $objSign->error());
                return (2, "4.7.7 Request action not taken: error sign")}

            sectionTime("firma $OPEC_MTARESPONSE_TAGS_AM");
            @arrOut = $objDispatcher->mailDispatch($objAnomTrasp, 1);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_AM",
                                "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_TSEND");
            extraInfo(  "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_SIZE",
                                $objAnomTrasp->[0]->mail_size);
            dispatchReport($objAnomTrasp);

            return @arrOut
        }

        dispatchReportSummary($msgInfo);

        # avviso di rilevazione virus informatico
        if( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND ){
            # emetto un avviso di mancata consegna per virus informatico
            prolongTimer('Avviso di mancata consegna per virus informatico');
            doLog(4, 'Ricevuto avviso di rilevazione virus informatico');

            doLog(4, 'Check indice delle pubbliche amministrazioni');
            eval{$objLdap->validateRecipt($msgInfo->recips())};
            # in caso di errore LDAP mantengo il messsaggio
            # nella coda MTA per successiva elaborazione
            return (2,'4.4.3 Requested action aborted: error in processing') if($@);

            doLog(4, 'Emissione avviso di mancata consegna per virus informatico');

            my $arefGetNoDeliveryVirusReceipt = getNoDeliveryVirusReceipt(  $msgInfo, \$msgCount);
            if( !ref($arefGetNoDeliveryVirusReceipt) ){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Esito: ".
                        "Errore durante la generazione dell'avviso di mancata consegna per virus informatici");
                return (2, "4.7.7 Request action not taken: error to generate the cert mail")}

            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RMCV",
                                "$OPEC_MTARESPONSE_TAGS_RMCV.$OPEC_MTARESPONSE_Q_TSSTART");
            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RMCV.$OPEC_MTARESPONSE_Q_DIRECTION",
                                $OPEC_DIRECTION_INBOUND);

            dispatchReportSummary($arefGetNoDeliveryVirusReceipt);

            $arefGetNoDeliveryVirusReceipt = $objSign->Sign($arefGetNoDeliveryVirusReceipt);
            if(!ref($arefGetNoDeliveryVirusReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Esito: ".
                        "Errore durante la firma dell'avviso di mancata consegna per virus informatici - ".
                        $objSign->error());
                return (2, "4.7.7 Error to sign virus receipt - Try later")}

            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RMCV");
            @arrOut = $objDispatcher->mailDispatch($arefGetNoDeliveryVirusReceipt, 1);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RMCV",
                                "$OPEC_MTARESPONSE_TAGS_RMCV.$OPEC_MTARESPONSE_Q_TSEND");
            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RMCV.$OPEC_MTARESPONSE_Q_SIZE",
                                $arefGetNoDeliveryVirusReceipt->[0]->mail_size);
            dispatchReport($arefGetNoDeliveryVirusReceipt);

            # cancellazione task
            # il destinatario della ricevuta e' sempre uno
            if( @arrOut && $arrOut[0]==1 ){
                doLog(4, "Cancellazione task relativi a " . $msgInfo->header->get($OPEC_HEADER_KEY_MSGID));
                my $ric_dest = getXml_consegna($msgInfo);
                my $ric_id   = getXml_identificativo($msgInfo);
                map { delRMC(lc($_).','.$ric_id) } @{$ric_dest} if(defined $ric_dest && defined $ric_id);
                sectionTime('cancellazione task');
            }
        }

            doLog(0, "Ricevuta/Avviso: dispaccio localmente");
            @arrOut = $objDispatcher->mailDispatch($msgInfo, 1);
        sectionTime("delivery ricevuta $receiptName",
                                "$receiptName.$OPEC_MTARESPONSE_Q_TSEND");
            extraInfo(  "$receiptName.$OPEC_MTARESPONSE_Q_SIZE",
                                $msgInfo->mail_size);
        dispatchReport($msgInfo);

        # ricevuta di presa in carico o ricevuta  di avvenuta consegna: elimino il task
        if(@arrOut && $arrOut[0]==1){
            if( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_GETLOAD ){
                doLog(4, "Cancellazione task 12 relativi a " . $msgInfo->header->get($OPEC_HEADER_KEY_MSGID));
                my $ric_dest = getXml_ricezione($msgInfo);
                my $ric_id   = getXml_identificativo($msgInfo);
                map { delRMC12(lc($_).','.$ric_id) } @{$ric_dest} if(defined $ric_dest && defined $ric_id);
                sectionTime('cancellazione task12');
            }elsif( $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_DELIVERY or
                    $reciptValue eq $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY){
                doLog(4, "Cancellazione task 12 e 24 relativi a " . $msgInfo->header->get($OPEC_HEADER_KEY_MSGID));
                my $ric_dest = getXml_consegna($msgInfo);
                my $ric_id   = getXml_identificativo($msgInfo);
                #map { delRMC24(lc($_).','.$ric_id) } @{$ric_dest} if(defined $ric_dest && defined $ric_id);
                map { delRMC(lc($_).','.$ric_id) } @{$ric_dest} if(defined $ric_dest && defined $ric_id);
                sectionTime('cancellazione task24');
            }
        }


    }elsif(defined $mailDocTraspValue || !$msgInfo->sender->certificate()){
        # documento di trasporto
        # PUNTO DI RICEZIONE
        # verifico che il doc di trasporto sia
        # formalmente corretto:
        # . controllo esistenza firma
        # . controllo che la firma sia stata emessa
        #   da un gestore di posta certificata
        # . controllo validita della firma
        prolongTimer('Ricezione DT');
        doLog(0, "Ricezione messaggio");
        $msgInfo->name($mailDocTraspValue || $OPEC_MSG_ORIG_IN);

        dispatchReportSummary($msgInfo);

        if( !$msgInfo->sender->mailAddress ){
            my ($hF) = @{getAddresses ($msgInfo->header->get ('From'))};
            $msgInfo->sender($hF);
        }

        # Flusso BookMark
        # . PR_VERIFY           -> verifica messaggio
        # . PR_RICPRESAINCARICO -> gen./dis. ric. presa in carico
        # . PR_AVIRUS           -> check antivirus
        # . PR_AVIRUSFOUND      -> gen./dis. avv. rilevazione virus
        # . PR_ANOMTRASP        -> gen./dis. anomalia di trasporto
        # . PR_DOCTRANSP        -> dis. doc. di trasporto
        # . PR_RICAVVCONS       -> gen./dis. ric. avvenuta/errore di consegna

            #doLog(0, "Documento di trasporto: elaborazione");
            #+++ CREO/RECUPERO LO STATO DAL BOOKMARK
            my $tmph = $msgInfo->header->dup();
            $tmph->delete($OPEC_HEADER_KEY_MSGID);
        my $bookMark = createBookmark(  $tmph->as_string(),
                                        $OPEC_BIS_PATHNAME);
        undef $tmph;
        doLog(4, "Bookmark: $bookMark - ".BIS());
        $bookMark = getTag($bookMark);
        my $refErrRecips;
        my $av_scan_result;
        my $errRecips_anomtrasp;
        my $refErrRecips_RC;
        #my $objReceiptVirusFoundErr;
        if($bookMark && $hBookMarkTable{'INIT'} ne $bookMark){   # situazione di recover
            # recupero lo stato delle variabili precedenti in funzione dello step
            $signCode = getBookmark($hBookMarkTable{'PR_VERIFY'});

            if( $hBookMarkTable{'PR_VERIFY'} eq $bookMark ){

            }elsif( $hBookMarkTable{'PR_RICPRESAINCARICO'} eq  $bookMark ){

            }elsif( $hBookMarkTable{'PR_AVIRUS'} eq $bookMark ){
                $av_scan_result = getBookmark($hBookMarkTable{'PR_AVIRUS'});

            }elsif( $hBookMarkTable{'PR_AVIRUSFOUND'} eq $bookMark ){
                $av_scan_result = getBookmark($hBookMarkTable{'PR_AVIRUS'});
                #$objReceiptVirusFoundErr = getBookmark($hBookMarkTable{'PR_AVIRUSFOUND'});

            }elsif( $hBookMarkTable{'PR_ANOMTRASP'} eq $bookMark ){
                $av_scan_result = getBookmark($hBookMarkTable{'PR_AVIRUS'});
                $errRecips_anomtrasp = getBookmark($hBookMarkTable{'PR_ANOMTRASP'});

            }elsif( $hBookMarkTable{'PR_DOCTRANSP'} eq $bookMark ){
                $av_scan_result = getBookmark($hBookMarkTable{'PR_AVIRUS'});
                $refErrRecips_RC = getBookmark($hBookMarkTable{'PR_DOCTRANSP'});

            }
            elsif( $hBookMarkTable{'PR_RICAVVCONS'} eq $bookMark ){
                $av_scan_result = getBookmark($hBookMarkTable{'PR_AVIRUS'});
                $refErrRecips_RC = getBookmark($hBookMarkTable{'PR_RICAVVCONS'});
            }
        }

        #+++ blocco VERIFY
        if( !$bookMark || $hBookMarkTable{'INIT'} eq $bookMark ){
                #$signCode = $objSign->Verify($msgInfo);
            $signCode = $objSign->Verify($msgInfo,$mailDocTraspValue);
            sectionTime("verifica $OPEC_MTARESPONSE_TAGS_DT");
            if($signCode == 1){
                my ($headerFrom) = @{getAddresses( $msgInfo->header->get('From') )};
                $signCode = $objLdap->checkDomainProvider(
                                $msgInfo->sender->mailAddress, $headerFrom );
                sectionTime('check domain-provider');
            }
            doLog(4, "Verifica: $signCode (".$objSign->error().")");
            my $strOutSign;
            if($signCode==1){
                $strOutSign = 'Ok';
            }else{
                if($objSign->error()){
                    $strOutSign = $objSign->error()
                }else{
                    $strOutSign = 'ERRORE generico!'
                }
            }
            doLog(0, "Verifica: $strOutSign");

            if( !defined $signCode or $signCode != 1 ){
                    if( !$delivery_NO_cert or
                        (!$delivery_NO_certLight &&
                        $objSign->error() &&
                        $objSign->error() eq 'Messaggio senza certificato')){

                    doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
                            "Errore durante la verifica della firma - ".
                            $objSign->error());
                    eraseBookmarks();
                    return (1, "2.5.0 OK");
                    }
#                unless( $delivery_NO_cert ){
#                    doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
#                            "Errore durante la verifica della firma - ".
#                            $objSign->error());
#                    eraseBookmarks();
#                    return (1, "2.5.0 OK");
#                }
            }
            $bookMark = getTag(appendBookmark($hBookMarkTable{'PR_VERIFY'}, $signCode));
        }

        #+++ blocco RICPRESAINCARICO
        if($signCode == 1){
            # DDT verificato: emetto la RICPRESAINCARICO
            if( !$bookMark || $hBookMarkTable{'PR_VERIFY'} eq $bookMark ){

                # doc. transp. OK
                # GENERO LA RICEVUTA DI PRESA IN CARICO
                # (una per ogni destinatario di mia competenza (locali)
                prolongTimer('Ricevuta di presa in carico');
                my $objGetLoadReceipt = getGetLoadReceipt($msgInfo, \$msgCount);

                if($objGetLoadReceipt){
                    doLog(0, "Generazione ricevuta di presa in carico");
                        sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RPC",
                                                "$OPEC_MTARESPONSE_TAGS_RPC.$OPEC_MTARESPONSE_Q_TSSTART");
                                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RPC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                                        $OPEC_DIRECTION_OUTBOUND);
                    dispatchReportSummary($objGetLoadReceipt);

                    # firmo la ricevuta di presa in carico
                    $objGetLoadReceipt = $objSign->Sign($objGetLoadReceipt);
                    if(!ref($objGetLoadReceipt)){
                        doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_GETLOAD - Esito: ".
                                "Errore durante la firma della ricevuta di presa in carico: ".
                                $objSign->error());
                        return (2, "4.7.7 Request action not taken: error sign")}
                    sectionTime("firma $OPEC_MTARESPONSE_TAGS_RPC");

                    # dispaccio ricevuta di presa in carico
                    # sempre esterna
                    doLog(0, "Dispaccio ricevuta di presa in carico");
                            @arrOut = $objDispatcher->mailDispatch($objGetLoadReceipt, 0);
                    sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RPC",
                                        "$OPEC_MTARESPONSE_TAGS_RPC.$OPEC_MTARESPONSE_Q_TSEND");
                                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RPC.$OPEC_MTARESPONSE_Q_SIZE",
                                                        $objGetLoadReceipt->[0]->mail_size);
                    dispatchReport($objGetLoadReceipt);
                    # non gestisco l'errore sull'invio
                }else{
                    doLog(0, "Ricevuta di presa in carico non generata")
                }
                $bookMark = getTag(appendBookmark($hBookMarkTable{'PR_RICPRESAINCARICO'}));
            }
        }

        #+++ blocco ANTIVIRUS
        if ($noCheckVirus) {
            $bookMark = getTag(appendBookmark($hBookMarkTable{'PR_AVIRUS'}, undef));
            doLog(0, "ATTENZIONE: controllo antivirus disabilitato");
        } elsif (!$bookMark ||
             $hBookMarkTable{'PR_RICPRESAINCARICO'} eq $bookMark ||
             $hBookMarkTable{'PR_VERIFY'} eq $bookMark){
            prolongTimer('Antivirus');
            $av_scan_result = $avObj->scan( $msgInfo->mail_text() );
            sectionTime('antivirus');
            if( ref($av_scan_result) ){
                # MAIL CON VIRUS
                doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
                        "Presenza di VIRUS! (".join(', ', @$av_scan_result).')')
            }elsif($av_scan_result){
                # errore durante la scansione
                # probabilmente potrei proseguire in silenzio
                doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
                        "Errore durante la scansione con antivirus, riprovo piu' tardi");
                return (2, "4.7.7 Error to scan mail by antivirus")
            }else{
                doLog(0, "Check Virus: Ok")
            }
            $bookMark = getTag(appendBookmark($hBookMarkTable{'PR_AVIRUS'}, $av_scan_result));
        }

        # MAIL CON VIRUS
        # msg verificato        -> delivery avviso di rilevazione virus
        # msg non verificato    -> elimino il msg
        # MAIL SENZA VIRUS
        # msg verificato        -> proseguo
        # msg non verificato    -> opz. anomalia msg o elimino
        if(ref($av_scan_result)){
            # MAIL CON VIRUS
            if($signCode == 1){
                # msg verificato:
                # . salvo msg con virus
                # . delivery avviso di rilevazione virus
                prolongTimer('Avviso di rilevazione virus');
                unless( $bookMark && $hBookMarkTable{'PR_AVIRUSFOUND'} eq $bookMark ){
                    if( $msgInfo->save($av_path_mail_virus, BIS().'_'.$msgInfo->sender->mailAddress) ){
                        doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
                            "Mail con virus, errore nel salvataggio, riprovo piu' tardi");
                        return (2, "4.7.7 Error to save VIRUS Mail - Try later");
                    }
                    sectionTime('save msg');
                }
                my $objReceiptVirusFound = getReceiptVirusFound($msgInfo, \$msgCount, join(', ', @$av_scan_result));
                if( scalar @$objReceiptVirusFound < 1 ){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND - Esito: ".
                        "Errore durante la generazione dell'avviso di rilevazione virus");
                    return (2, "4.7.7 Request action not taken: error to generate the cert mail")
                }
                sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RRV",
                                        "$OPEC_MTARESPONSE_TAGS_RRV.$OPEC_MTARESPONSE_Q_TSSTART");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RRV.$OPEC_MTARESPONSE_Q_DIRECTION",
                                                $OPEC_DIRECTION_OUTBOUND);

                dispatchReportSummary($objReceiptVirusFound);
                $objReceiptVirusFound = $objSign->Sign($objReceiptVirusFound);
                if( !ref($objReceiptVirusFound) ){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND - Esito: ".
                        "Errore durante la firma dell'avviso di rilevazione virus, riprovo piu' tardi: ".
                    $objSign->error());
                    return (2, "4.7.7 Request action not taken: error sign")
                }
                sectionTime("firma $OPEC_MTARESPONSE_TAGS_RRV");

                # delivery - sempre esterna
                doLog(0, "Dispaccio avviso di rilevazione virus");
                    @arrOut = $objDispatcher->mailDispatch($objReceiptVirusFound, 0);
                sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RRV",
                                        "$OPEC_MTARESPONSE_TAGS_RRV.$OPEC_MTARESPONSE_Q_TSEND");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RRV.$OPEC_MTARESPONSE_Q_SIZE",
                                                $objReceiptVirusFound->[0]->mail_size);
                dispatchReport($objReceiptVirusFound);

                # se tutte le ric restituiscono un problema temporaneo
                # eseguo il bookm altrimenti erase
                my $ricRes;
                my $i=0;
                foreach(@arrOut){next if($i%2); $ricRes++ if($_ == 0 or $_ == 2)}
                if( $ricRes == scalar @$objReceiptVirusFound ){
                    $bookMark = getTag(appendBookmark($hBookMarkTable{'PR_AVIRUSFOUND'}));
                }else{
                    eraseBookmarks();
                }
                return @arrOut;

            }else{
                # msg non verificato
                # . elimino il msg
                eraseBookmarks();
                return (1, "2.5.0 Request action not taken: VIRUS FOUND!")}
        }else{
            # MAIL SENZA VIRUS
            if($signCode != 1){
                # msg non verificato    -> opz. anomalia msg o elimino
                prolongTimer('Anomalia di trasporto');
                if( $errRecips_anomtrasp ){ # recupero i destinatari precedenti con esito negativo
                    foreach(@$errRecips_anomtrasp){
                        $_->reciptDone(0);
                        $_->reciptRemoteHost('');
                        $_->reciptRemoteResponse('')}
                    $msgInfo->recips( $errRecips_anomtrasp )
                }

                # genero e firma l'anomalia di messaggio
                my $objAnomTrasp = getTransportFault($msgInfo,
                    "la firma digitale del messaggio non risulta attendibile");
                if( !ref($objAnomTrasp) ){
                    doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_ANOM - Esito: ".
                        "Errore durante la generazione della anomalia di messaggio");
                    return (2, "4.7.7 Request action not taken: error to generate the cert mail")
                }
                sectionTime("gen. $OPEC_MTARESPONSE_TAGS_AM",
                                        "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_TSSTART");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_DIRECTION",
                                                $OPEC_DIRECTION_INBOUND);

                dispatchReportSummary($objAnomTrasp);
                $objAnomTrasp = $objSign->Sign($objAnomTrasp);
                if(!ref($objAnomTrasp)){
                    doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_ANOM - Esito: ".
                        "Errore durante la firma della anomalia di messaggio: ".
                        $objSign->error());
                    return (2, "4.7.7 Request action not taken: error sign")
                }
                sectionTime("firma $OPEC_MTARESPONSE_TAGS_AM");

                doLog(0, "Dispaccio anomalia di trasporto");
                @arrOut = $objDispatcher->mailDispatch($objAnomTrasp, 1);
                sectionTime("delivery $OPEC_MTARESPONSE_TAGS_AM",
                                        "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_TSEND");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_AM.$OPEC_MTARESPONSE_Q_SIZE",
                                                $objAnomTrasp->[0]->mail_size);
                dispatchReport($objAnomTrasp);

                # salvo gli eventuali destinatari che hanno
                # generato errori temporanei di delivery
                my @errRecips;
                foreach(@$objAnomTrasp){
                    push @errRecips,
                        grep { $_->reciptDone() == 0 || $_->reciptDone() == 2} @{$_->recips}
                }
                @errRecips ? appendBookmark($hBookMarkTable{'PR_ANOMTRASP'}, \@errRecips) : eraseBookmarks();
                return @arrOut
            }
        }

        #+++ BLOCCO DOC.TRASP.
        if( !$bookMark || $hBookMarkTable{'PR_AVIRUS'} eq $bookMark ){

            # dispaccio localmente il doc di trasporto
            doLog(0, "Dispaccio localmente il documento di trasporto");
            @arrOut = $objDispatcher->mailDispatch($msgInfo, 1);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_DT",
                                "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_INBOUND);
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_SIZE",
                                        $msgInfo->mail_size);

            dispatchReport($msgInfo);

            $bookMark = getTag(appendBookmark($hBookMarkTable{'PR_DOCTRANSP'},$msgInfo->recips()));
        }

        #+++ BLOCCO RICEVUTE DI CONSEGNA/ERRORE DI CONSEGNA
        if( !$bookMark ||
            $hBookMarkTable{'PR_DOCTRANSP'} eq $bookMark ||
            $hBookMarkTable{'PR_RICAVVCONS'} eq $bookMark ){

            prolongTimer('Ricevute di consegna/errore di consegna');

            # caso di recover: recupero gli eventuali destinatari precedenti
            my $recipsDT;
            if($refErrRecips_RC){
                $msgInfo->recips($refErrRecips_RC)
            }else{
                $recipsDT = $msgInfo->recips;
            }

            doLog(0, "Generazione ricevuta di consegna/errore di consegna");
            my $arefGetAcceptReceipt = getDeliveryReceipt($msgInfo,\$msgCount);
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDC",
                                "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_ND);
            dispatchReportSummary($arefGetAcceptReceipt);

            $arefGetAcceptReceipt = $objSign->Sign($arefGetAcceptReceipt);
            if(!ref($arefGetAcceptReceipt)){
                doLog(0,"Esito: Errore durante la firma della ricevuta di consegna".
                        "/errore di consegna: ".$objSign->error());
                return [2, "4.7.7 Request action not taken: error sign"]}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDC");

            # dispaccio delle ricevute di consegna/errore di consegna
            doLog(0, "Dispaccio ricevute di consegna/errore di consegna");
                    @arrOut = $objDispatcher->mailDispatch($arefGetAcceptReceipt, 2);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDC",
                                "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_SIZE",
                                        $arefGetAcceptReceipt->[0]->mail_size);
            dispatchReport($arefGetAcceptReceipt);

            # se tutte le ric restituiscono un problema temporaneo
            # eseguo il bookm altrimenti erase
            my @errRecips;
            foreach(@$arefGetAcceptReceipt){
                push @errRecips, $_->recips->[0]
                    if( $_->recips->[0]->reciptDone() == 0 || $_->recips->[0]->reciptDone() == 2)
            }
            if(@errRecips == @$arefGetAcceptReceipt){
                appendBookmark($hBookMarkTable{'PR_RICAVVCONS'}, $recipsDT);
            }else{
                eraseBookmarks()}
        }
    }else{
        # PUNTO DI ACCESSO
        # sanity check
        # verifiche formali sull'header:
        # . attinenza rfc822
        # . le info dell'envelop devono coincidere
        #   con quelle nell'header
        prolongTimer('Ricezione msg orig');
        doLog(0, "Accettazione messaggio");
        $msgInfo->name($OPEC_MSG_ORIG_IN);
        dispatchReportSummary($msgInfo);

        doLog(4, "sanity_check");
        if( my $sanity_check_out = sanity_check($msgInfo) ){
            # rilevate eccezioni formali
            # AVVISO DI NON ACCETTAZIONE PER ECCEZIONI FORMALI
            # (delivery sempre locale)
            doLog(5, $sanity_check_out);
            doLog(0, "Rilevate eccezioni formali: $sanity_check_out");
            return (3, '5.7.7 Request action not taken: ambiguous header') unless($msgInfo->sender->mailAddress());

            doLog(4, 'Check indice delle pubbliche amministrazioni');
            eval{$objLdap->validateRecipt($msgInfo->recips())};
            # in caso di errore LDAP mantengo il messsaggio
            # nella coda MTA per successiva elaborazione
            return (2,'4.4.3 Requested action aborted: error in processing') if($@);

            prolongTimer('Avviso di non accettazione per ecc. formali');
            my $arefGetNotAcceptanceReceipt = getNotAcceptanceReceipt($msgInfo,\$msgCount,$sanity_check_out);
            if( !ref($arefGetNotAcceptanceReceipt) ){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                            "Errore durante la generazione dell'avviso di non accettazione");
                    return (2, "4.7.7 Request action not taken: error to generate the cert mail")}
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDNAC",
                                "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_INBOUND);

            dispatchReportSummary($arefGetNotAcceptanceReceipt);

            $arefGetNotAcceptanceReceipt = $objSign->Sign($arefGetNotAcceptanceReceipt);
            if(!ref($arefGetNotAcceptanceReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                        "Errore durante la firma dell'avviso di non accettazione per ".
                        "eccezioni formali: ".$objSign->error());
                return (2, "4.7.7 Request error to sign formal incorrect receipt - Try later")}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDNAC");

            doLog(0, "Dispaccio avviso");
            #flazan - 15/04/2007
            #@arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 1);
                    @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 2);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDNAC",
                                "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_SIZE",
                                        $arefGetNotAcceptanceReceipt->[0]->mail_size);
            dispatchReport($arefGetNotAcceptanceReceipt);

            return @arrOut;
        }

                # SPC - uso degli alias
                if( $SPC_ENABLED ){
                        my @rcpt_all = map {$_->mailAddress} @{$msgInfo->recips()};
                        my %rcpt_delete;
                        my %dl_enabled;
                        my %h_rcptTo;
                        my %h_rcptCc;
                        my %DL_used_tmp;
                        my $err;
                        foreach my $rcpt (@rcpt_all){
                                # verifico che l'indirizzo sia un DL
                                unless( $obj_mapper->getData($SPC_ALIAS_LIST_MAP,$rcpt) ){
                                        # l'indirizzo non e' un DL
                                        die $obj_mapper->lastErr() if $obj_mapper->lastErr;
                                        next}

                                # controllo loop DL
                                if( exists $DL_used_tmp{$rcpt} ){
                                        # trovato DL loop
                                        # genero la RdNA
                                        $err = "DL $rcpt non integro - trovato loop";
                        last}

                                # check dominio DL - mittente
                            doLog(2, "SPC: trovato DL $rcpt");
                                my $senderDomain = lc(getDomain($msgInfo->sender->mailAddress));
                                if( $senderDomain ne lc(getDomain($rcpt)) ){
                                        # se il mittente ha un dominio diverso da quello del destinatario
                                        # emetto la RdNA
                                        $err = "dominio mittente diverso dalla DL $rcpt";
                                        last;
                                }

                                # check abilitazione DL
                                if( !%dl_enabled ){
                        my @dl_en = $obj_mapper->getData($SPC_USER_ENABLED,$msgInfo->sender->mailAddress);
                        die $obj_mapper->lastErr() if $obj_mapper->lastErr;

                        if( @dl_en < 1 ){
                                # nessun DL abilitata
                                $err = $msgInfo->sender->mailAddress." non autorizzato ad usare DL";
                                last;
                        }
                        %dl_enabled = map { lc($_) => 1 } @dl_en;
                                }

                if( !exists $dl_enabled{lc($rcpt)} ){
                        # DL non abilitata
                        $err = $msgInfo->sender->mailAddress." non autorizzato ad usare DL $rcpt";
                        last}

                                # esplodo DL
                                $rcpt_delete{$rcpt} = 1;        # DL da eliminare dai destinatari
                                my @rcpt2add = $obj_mapper->getData($SPC_ALIAS_USER_MAP, $rcpt);
                                die $obj_mapper->lastErr() if $obj_mapper->lastErr();
                                if( @rcpt2add <= 0 ){
                                        doLog(2, "SPC: nessun indirizzo trovato per $rcpt");
                                        next}
                                doLog(2, "SPC: $rcpt -> ".join(',',@rcpt2add) );

                                # controllo integrita' DL
                                if( grep {$senderDomain ne lc(getDomain($_))} @rcpt2add ){
                                        # 1 o + membri non hanno lo stesso dominio
                                        # o DL nidificata
                                        # genero la RdNA
                                        # Ai fini dell'accettazione e' necessario essere autorizzati ad
                                        # inviare a tutti i destinatari incluse le liste di distribuzione
                                        #$err = "DL $rcpt non integro";
                                        $err = $msgInfo->sender->mailAddress." non autorizzato ad usare DL $rcpt";
                        last}

                                # indicizzo l'header la prima volta,
                                # servira per aggiornarlo
                                if(!%h_rcptTo and !%h_rcptCc){
                            my $i = 0;
                                    %h_rcptCc = map {lc($_) => ++$i} @{getAddresses( join(',',$msgInfo->header->get('Cc')) )};
                                    $i = 0;
                                    %h_rcptTo = map {lc($_) => ++$i} @{getAddresses( join(',',$msgInfo->header->get('To')) )};
                                }

                                $DL_used_tmp{$rcpt} = 1;
                                push @rcpt_all, @rcpt2add;
                                @h_rcptTo{@rcpt2add} = (1..@rcpt2add) if exists $h_rcptTo{lc($rcpt)};           # basta che la chiave sia positiva
                    @h_rcptCc{@rcpt2add} = (1..@rcpt2add) if exists $h_rcptCc{lc($rcpt)};       # basta che la chiave sia positiva
                        }

                        if( $err ){
                    # generazione RdNA
                    # . dominio mittente diverso da dominio DL
                    # . casella non autorizzata ad usare alias
                    # . alias non integri
                prolongTimer('Avviso di non accettazione per '.$err);
                            doLog(2, "ATTENZIONE: $err");

                    my $arefGetNotAcceptanceDLerr = getNotAcceptanceDLerr($msgInfo,\$msgCount,$err);
                    if( !ref($arefGetNotAcceptanceDLerr) ){
                            doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                                    "Errore durante la generazione dell'avviso di non accettazione");
                            return (2, "4.7.7 Request action not taken: error to generate the cert mail");
                    }
                    sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSSTART");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                                $OPEC_DIRECTION_INBOUND);

                    dispatchReportSummary($arefGetNotAcceptanceDLerr);

                    $arefGetNotAcceptanceDLerr = $objSign->Sign($arefGetNotAcceptanceDLerr);
                    if(!ref($arefGetNotAcceptanceDLerr)){
                        doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                                "Errore durante la firma dell'avviso di non accettazione per ".
                                "utilizzo non autorizzato di alias: ".$objSign->error());
                        return (2, "4.7.7 Request error to sign formal incorrect receipt - Try later")}
                    sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDNAC");

                    doLog(0, "Dispaccio avviso");

                            @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceDLerr, 2);
                    sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSEND");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_SIZE",
                                                $arefGetNotAcceptanceDLerr->[0]->mail_size);
                    dispatchReport($arefGetNotAcceptanceDLerr);
                    return @arrOut;
                        }

                        if(%rcpt_delete){       # se %rcpt_delete != 0 header e envelop sono da aggiornare
                                # allineo l'envelop
                                # @rcpt_all contiene gia' tutti i dest + quelli da aggiungere
                                my @a = grep (!exists $rcpt_delete{$_}, @rcpt_all);
                                $msgInfo->recips(\@a);

                    # allineo l'header
                    $msgInfo->header->delete('Cc');$msgInfo->header->delete('To');
                    map{
                        $msgInfo->header->add('To',$_) if(exists $h_rcptTo{$_});
                        $msgInfo->header->add('Cc',$_) if(exists $h_rcptCc{$_});
                    }@a;
                                $msgInfo->header->combine('To',',');
                                $msgInfo->header->combine('Cc',',');
                        }
                }

            # creo/recupero il bookmark
            # (comincio da qua visto che tutto sommato credo
            # costi + fare createBookmark che sanity_check)
            my $tmph = $msgInfo->header->dup();
            $tmph->delete($OPEC_HEADER_KEY_MSGID);
        my $bookMark = createBookmark(  $tmph->as_string(),
                                        $OPEC_BIS_PATHNAME);
        undef $tmph;
        doLog(4, "Bookmark: $bookMark - ".BIS());
        $bookMark = getTag($bookMark);

        #+++ BLOCCO VALIDATERECIPT
        if( !$bookMark ||
            $bookMark eq $hBookMarkTable{'INIT'}){
            # valido i destinatari tramite LDAP
            # (indice delle pubbliche amministrazioni)
            doLog(4, "Check indice delle pubbliche amministrazioni");
            eval{$objLdap->validateRecipt($msgInfo->recips())};
            # in caso di errore LDAP mantengo il messsaggio
            # nella coda MTA per successiva elaborazione
            return (2,"4.4.3 Requested action aborted: error in processing") if($@);
            $bookMark = getTag(appendBookmark($hBookMarkTable{'PA_VALIDATERECIPT'}, $msgInfo->recips()));
        }else{
            # situazione di recover
            # recupero lo stato di certificazione dei destinatari
            my $tmpRecips = getBookmark($hBookMarkTable{'PA_VALIDATERECIPT'});
            $tmpRecips && $msgInfo->recips($tmpRecips);
        }

        # check antivirus $avObj->scan() :
        # undef    --> mail senza virus
        # arr. ref --> mail con virus (ogni elemento descrive un virus trovato)
        # scalar   --> errore durante la scansione

        #+++ blocco ANTIVIRUS
        if ($noCheckVirus) {
            $bookMark = getTag(appendBookmark($hBookMarkTable{'PA_AVIRUS'}));
            doLog(0, "ATTENZIONE: controllo antivirus disabilitato");
        } elsif (!$bookMark || $bookMark eq $hBookMarkTable{'PA_VALIDATERECIPT'}){
            prolongTimer('Antivirus');
            doLog(0, "Check Virus");
            my $av_scan_result = $avObj->scan( $msgInfo->mail_text() );
            ###my $av_scan_result; # XXX per test virus
            sectionTime('antivirus');
            if(ref($av_scan_result)){
                # mail con virus
                doLog(0, "Mail con presenza di VIRUS!");
                doLog(0, "Dettagli virus: " . join(', ', @$av_scan_result));

                # salvo la mail e restituisco a MTA un messaggio di tipo 5 solo
                # se riesco a salvarla altrimenti di tipo 4 in modo da poter riprovare
                if( $msgInfo->save($av_path_mail_virus, BIS().'_'.$msgInfo->sender->mailAddress) ){
                    return (2, "4.7.7 Error to save VIRUS Mail - Try later")}
                sectionTime('save msg');

                # spedisco l'avviso
                my $arefGetNotAcceptanceReceiptVirusReceipt = getNotAcceptanceReceiptVirus( $msgInfo,
                                                                                            \$msgCount,
                                                                                            join(',',@$av_scan_result) );
                if( !ref($arefGetNotAcceptanceReceiptVirusReceipt) ){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                            "Errore durante la generazione dell'avviso di non accettazione per virus informatici");
                    return (2, "4.7.7 Request action not taken: error to generate the cert mail")}
                sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSSTART");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                                $OPEC_DIRECTION_INBOUND);

                dispatchReportSummary($arefGetNotAcceptanceReceiptVirusReceipt);

                $arefGetNotAcceptanceReceiptVirusReceipt = $objSign->Sign($arefGetNotAcceptanceReceiptVirusReceipt);
                if(!ref($arefGetNotAcceptanceReceiptVirusReceipt)){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                            "Errore durante la firma dell'avviso di non accettazione per ".
                            "virus informatici: ".$objSign->error());
                    return (2, "4.7.7 Error to sign virus receipt - Try later")}
                sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDNAC");

                doLog(0, "Dispaccio avviso");
                    @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceiptVirusReceipt, 1);
                    sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSEND");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_SIZE",
                                                $arefGetNotAcceptanceReceiptVirusReceipt->[0]->mail_size);
                dispatchReport($arefGetNotAcceptanceReceiptVirusReceipt);

                eraseBookmarks() if(@arrOut && $arrOut[0] != 2);
                return @arrOut;
            }elsif($av_scan_result){
                return (2, "4.7.7 Error to scan mail by antivirus")}
            $bookMark = getTag(appendBookmark($hBookMarkTable{'PA_AVIRUS'}));
        }

        # MODALITA PUBBLICA AMMINISTRAZIONE
        # Viene gestita raggruppando i domini in 3 categorie:
        # 1. PA (1)
        # 2. altra PA il cui servizio e' fornito da PA (2)
        # 3. privato (0/undef)
        if($PAmode){
            doLog(4, "check_PA");
            my $senderDom = lc(getDomain($msgInfo->sender->mailAddress()));

            if( $senderDom && !$hLocalCertDomains{$senderDom} ){
                # se almeno uno dei destinatari e' non PA, emetto la
                # RICEVUTA DI NON ACCETTAZIONE
                my @recipsPA = grep { $hLocalCertDomains{lc(getDomain($_->mailAddress()))} == 1 }
                    @{$msgInfo->recips};

                if( @recipsPA != @{$msgInfo->recips} ){
                    # ric di non accettazione
                    prolongTimer('Avviso di non accettazione - flusso PA');
                    doLog(0, "Rilevate eccezioni formali");
                    my $arefGetNotAcceptanceReceipt = getNotAcceptanceReceipt(
                        $msgInfo,\$msgCount,'presenza di destinatari non consentiti');
                    if( !ref($arefGetNotAcceptanceReceipt) ){
                            doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                                    "Errore durante la generazione dell'avviso di non accettazione");
                            return (2, "4.7.7 Request action not taken: error to generate the cert mail")}
                    sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSSTART");
                                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                                        $OPEC_DIRECTION_INBOUND);

                    dispatchReportSummary($arefGetNotAcceptanceReceipt);

                    $arefGetNotAcceptanceReceipt = $objSign->Sign($arefGetNotAcceptanceReceipt);
                    if(!ref($arefGetNotAcceptanceReceipt)){
                        doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                                "Errore durante la firma dell'avviso di non accettazione per ".
                                "eccezioni formali: ".$objSign->error());
                        return (2, "4.7.7 Request error to sign formal incorrect receipt - Try later")}
                    sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDNAC");

                    doLog(0, "Dispaccio avviso");
                            @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 2);
                    sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDNAC",
                                        "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_TSEND");
                                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDNAC.$OPEC_MTARESPONSE_Q_SIZE",
                                                        $arefGetNotAcceptanceReceipt->[0]->mail_size);
                    dispatchReport($arefGetNotAcceptanceReceipt);

                    return @arrOut;
                }
            }
        }

        # Controllo dimensione messaggio e numero di destinatari:
        doLog(4, "check message size");
                my $realMsgSize = $msgInfo->mail_size;
                my $realRcptNum = scalar @{$msgInfo->recips()};

        # controllo della DIMENSIONE msg in ingresso
        my $esmtpMaxMsgSize_value = $esmtpMaxMsgSize;
                if($MAPS_LIST{ESMPTMAXMSGSIZE}){
                        $esmtpMaxMsgSize_value = $obj_mapper->getData($esmtpMaxMsgSize,$msgInfo->sender->mailAddress);
                        doLog(2, 'MAP SMPTMAXMSGRCPT ERR: '.$obj_mapper->lastErr) if $obj_mapper->lastErr;
                }

                if( $esmtpMaxMsgSize_value && (($realMsgSize * $realRcptNum) > $esmtpMaxMsgSize_value) ){
                    prolongTimer('Avviso di non accettazione per superamento dimensione messaggio');
                    doLog(2, "ATTENZIONE: dimensione del messaggio ".
                             "($realMsgSize bytes * $realRcptNum rcpt) oltre il limite impostato ".
                             "($esmtpMaxMsgSize_value bytes)");
                    doLog(0, "Rilevato superamento dimensione messaggio");

            # generazione ricevuta di non accettazione per superamento dimensione
            my $arefGetNotAcceptanceReceipt = getNotAcceptanceReceiptMaxSize(
                $msgInfo,\$msgCount,'superamento dimensione massima consentita');
            if( !ref($arefGetNotAcceptanceReceipt) ){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                            "Errore durante la generazione dell'avviso di non accettazione");
                    return (2, "4.7.7 Request action not taken: error to generate the cert mail");
            }
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RMC",
                                "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_INBOUND);

            dispatchReportSummary($arefGetNotAcceptanceReceipt);

            $arefGetNotAcceptanceReceipt = $objSign->Sign($arefGetNotAcceptanceReceipt);
            if(!ref($arefGetNotAcceptanceReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                        "Errore durante la firma dell'avviso di non accettazione per ".
                        "superamento dimensione: ".$objSign->error());
                return (2, "4.7.7 Request error to sign formal incorrect receipt - Try later")}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RMC");

            doLog(0, "Dispaccio avviso");
            #flazan - 15/04/2007
            #@arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 1);
                    @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 2);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RMC",
                                "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_SIZE",
                                        $arefGetNotAcceptanceReceipt->[0]->mail_size);
            dispatchReport($arefGetNotAcceptanceReceipt);

            return @arrOut;
                }

        # controllo sul NUMERO dei DESTINATARI msg in ingresso
        my $smtpMaxMsgRcpt_value = $smtpMaxMsgRcpt;
                if($MAPS_LIST{SMPTMAXMSGRCPT}){
                        $smtpMaxMsgRcpt_value = $obj_mapper->getData($smtpMaxMsgRcpt,$msgInfo->sender->mailAddress);
                        doLog(2, 'MAP SMPTMAXMSGRCPT ERR: '.$obj_mapper->lastErr) if $obj_mapper->lastErr;
                }
                if( $smtpMaxMsgRcpt_value && ($realRcptNum > $smtpMaxMsgRcpt_value) ){
                    prolongTimer('Avviso di non accettazione per superamento limite numero destinatari');
                    doLog(2, "ATTENZIONE: numero dei destinatari ".
                             "($realRcptNum) oltre il limite impostato ".
                             "($smtpMaxMsgRcpt_value)");
                    doLog(0, "Rilevato superamento limite numero destinatari");

            # generazione ricevuta di non accettazione per superamento limite numero destinatari
            my $arefGetNotAcceptanceReceipt = getNotAcceptanceReceiptMaxRcpt(
                $msgInfo,\$msgCount,'superamento numero destinatari consentiti');
            if( !ref($arefGetNotAcceptanceReceipt) ){
                    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                            "Errore durante la generazione dell'avviso di non accettazione");
                    return (2, "4.7.7 Request action not taken: error to generate the cert mail")}
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RMC",
                                "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_INBOUND);
            dispatchReportSummary($arefGetNotAcceptanceReceipt);

            $arefGetNotAcceptanceReceipt = $objSign->Sign($arefGetNotAcceptanceReceipt);
            if(!ref($arefGetNotAcceptanceReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT - Esito: ".
                        "Errore durante la firma dell'avviso di non accettazione per ".
                        "superamento dimensione: ".$objSign->error());
                return (2, "4.7.7 Request error to sign formal incorrect receipt - Try later")}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RMC");

            doLog(0, "Dispaccio avviso");
                    @arrOut = $objDispatcher->mailDispatch($arefGetNotAcceptanceReceipt, 2);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RMC",
                                "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RMC.$OPEC_MTARESPONSE_Q_SIZE",
                                        $arefGetNotAcceptanceReceipt->[0]->mail_size);
            dispatchReport($arefGetNotAcceptanceReceipt);

            return @arrOut;
                }

#        #+++ BLOCCO VALIDATERECIPT
#        if( !$bookMark ||
#            $bookMark eq $hBookMarkTable{'PA_AVIRUS'}){
#            # valido i destinatari tramite LDAP
#            # (indice delle pubbliche amministrazioni)
#            doLog(4, "Check indice delle pubbliche amministrazioni");
#            eval{$objLdap->validateRecipt($msgInfo->recips())};
#            # in caso di errore LDAP mantengo il messsaggio
#            # nella coda MTA per successiva elaborazione
#            return (2,"4.4.3 Requested action aborted: error in processing") if($@);
#            $bookMark = getTag(appendBookmark($hBookMarkTable{'PA_VALIDATERECIPT'}, $msgInfo->recips()));
#        }

        #+++ BLOCCO RICACCETTAZIONE
        if( !$bookMark ||
            $bookMark eq $hBookMarkTable{'PA_AVIRUS'}){
            prolongTimer('Ricevuta di accettazione');
#            if(!$msgInfo->recips()){
#                my $tmpRecips = getBookmark($hBookMarkTable{'PA_VALIDATERECIPT'});
#                $tmpRecips && $msgInfo->recips($tmpRecips)}

            # genero la ricevuta di accettazione
            doLog(0, "Generazione ricevuta di accettazione");
            my $objGetAcceptReceipt = getAcceptanceReceipt($msgInfo,\$msgCount);
            if( !ref($objGetAcceptReceipt) ){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ACCEPT - Esito: ".
                        "Errore durante la generazione della ricevuta di accettazione");
                return (2, "4.7.7 Request action not taken: error to generate the cert mail")}
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDAC",
                                "$OPEC_MTARESPONSE_TAGS_RDAC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDAC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_INBOUND);

            dispatchReportSummary($objGetAcceptReceipt);

            # firmo la ricevuta di accettazione
            $objGetAcceptReceipt = $objSign->Sign($objGetAcceptReceipt);
            if(!ref($objGetAcceptReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ACCEPT - Esito: ".
                        "Errore durante la firma della ricevuta di accettazione - ".
                        $objSign->error());
                return (2, "4.7.7 Request action not taken: error sign")}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDAC");

            # spedisco la ricevuta di accettazione
            # delivery sempre locale cert ad 1 dest.
            doLog(0, "Dispaccio ricevuta di accettazione");
            @arrOut = $objDispatcher->mailDispatch($objGetAcceptReceipt, 1);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDAC",
                                "$OPEC_MTARESPONSE_TAGS_RDAC.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDAC.$OPEC_MTARESPONSE_Q_SIZE",
                                        $objGetAcceptReceipt->[0]->mail_size);
            dispatchReport($objGetAcceptReceipt);

            # se la ricevuta di accettazione non viene
            # deliverata con successo esco
            my $deliv = shift @arrOut;
            return ($deliv, shift @arrOut) if($deliv != 1); # errore

            $bookMark = getTag(appendBookmark($hBookMarkTable{'PA_RICACCETTAZIONE'}));
        }

        #+++ BLOCCO DOC.TRASP.
        #+++ BLOCCO RICAVVCONS
        my $objDocTrasp;
        if( !$bookMark ||
            $bookMark eq $hBookMarkTable{'PA_RICACCETTAZIONE'} ||
            $bookMark eq $hBookMarkTable{'PA_DOCTRANSP'} ||
            $bookMark eq $hBookMarkTable{'PA_RICAVVCONS'}){

            # genera il doc di trasporto
            prolongTimer('Documento di trasporto');
            doLog(0, "Generazione documento di trasporto");
            $objDocTrasp = generateTransport($msgInfo);
            if( !ref($objDocTrasp) ){
                doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
                        "Errore durante la generazione del doc. trasp.");
                return (2, "4.7.7 Request action not taken: error to generate the cert mail")}
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_DT",
                                "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $msgInfo->direction);

            dispatchReportSummary($objDocTrasp);

            # firmo il doc
            $objDocTrasp = $objSign->Sign($objDocTrasp);
            if(!ref($objDocTrasp)){
                doLog(0,"$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP - Esito: ".
                        "Errore durante la firma del doc. trasp. - ".
                        $objSign->error());
                return [2, "4.7.7 Request action not taken: error sign"]}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_DT");

            if($bookMark && $bookMark eq $hBookMarkTable{'PA_DOCTRANSP'}){
                my $tmpRecips = getBookmark($hBookMarkTable{'PA_DOCTRANSP'});
                $tmpRecips && $objDocTrasp->[0]->recips($tmpRecips)
            }else{
                # Dispaccio documento di trasporto
                doLog(0, "Dispaccio documento di trasporto");
                @arrOut = $objDispatcher->mailDispatch($objDocTrasp, 2);
                sectionTime("delivery $OPEC_MTARESPONSE_TAGS_DT",
                                        "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_TSEND");
                            extraInfo(  "$OPEC_MTARESPONSE_TAGS_DT.$OPEC_MTARESPONSE_Q_SIZE",
                                                $objDocTrasp->[0]->mail_size);
                dispatchReport($objDocTrasp);

                # se c'e' stata almeno 1 spedizione verso fornitori (certificati) esterni avvenuta con successo
                # devo creare il task, no altrimenti
                my $task_counter = 0;
                foreach ( grep {$_->reciptDone() and
                                $_->reciptDone() == 1 and
                                $_->certificate() and
                                !exists $hLocalCertDomains{lc(substr($_->mailAddress, index($_->mailAddress, '@')+1))}} @{$objDocTrasp->[0]->recips()} ){

                    enqRMC( lc($_->mailAddress()).','.$objDocTrasp->[0]->msgid_new(), $objDocTrasp->[0]->getCopyLight($_->mailAddress()) ) ?
                        doLog(0, 'Errore accodamento task') :
                        doLog(0, 'Task RMC accodato per '.$_->mailAddress());
                }

                # salvo gli eventuali destinatari che hanno
                # generato errori temporanei di delivery
                appendBookmark( $hBookMarkTable{'PA_DOCTRANSP'}, $objDocTrasp->[0]->recips);
            }

            if($bookMark && $bookMark eq $hBookMarkTable{'PA_RICAVVCONS'}){
                prolongTimer('Ricevute di consegna/errore di consegna');
                my $refErrRecips = getBookmark($hBookMarkTable{'PA_RICAVVCONS'});
                foreach(@$refErrRecips){
                    $_->reciptDone(0);
                    $_->reciptRemoteHost('');
                    $_->reciptRemoteResponse('');
                }
                $refErrRecips && $msgInfo->recips($refErrRecips)
            }

            # se l'utente e' locale devo generare eventualmente
            # le ricevute di consegna/errore di consegna
            my $arefGetAcceptReceipt = getDeliveryReceipt($objDocTrasp->[0],\$msgCount);

            if(!defined $arefGetAcceptReceipt->[0]){
                eraseBookmarks();
                return (1, "2.5.0 OK")}
            sectionTime("gen. $OPEC_MTARESPONSE_TAGS_RDC",
                                "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_TSSTART");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_DIRECTION",
                                        $OPEC_DIRECTION_ND);


            dispatchReportSummary($arefGetAcceptReceipt);
            my $OPEC_HEADER_VALUE_tmp = myChomp( $arefGetAcceptReceipt->[0]->header->get($OPEC_HEADER_KEY_RECEIPT) );

            # firmo la ricevuta di consegna
            doLog(0, "Generazione ricevuta di consegna/errore di consegna");
            $arefGetAcceptReceipt = $objSign->Sign($arefGetAcceptReceipt);
            if(!ref($arefGetAcceptReceipt)){
                doLog(0,"$OPEC_HEADER_VALUE_tmp - Esito: ".
                        "Errore durante la firma della ricevuta di consegna/errore di consegna - ".
                        $objSign->error());
                return [2, "4.7.7 Request action not taken: error sign"]}
            sectionTime("firma $OPEC_MTARESPONSE_TAGS_RDC");

            # dispaccio delle ricevute di consegna/errore di consegna
            doLog(0, "Dispaccio ricevute di consegna/errore di consegna");
            @arrOut = $objDispatcher->mailDispatch($arefGetAcceptReceipt, 1);
            sectionTime("delivery $OPEC_MTARESPONSE_TAGS_RDC",
                                "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_TSEND");
                    extraInfo(  "$OPEC_MTARESPONSE_TAGS_RDC.$OPEC_MTARESPONSE_Q_SIZE",
                                        $arefGetAcceptReceipt->[0]->mail_size);
            dispatchReport($arefGetAcceptReceipt);

            # salvo gli eventuali destinatari che hanno
            # generato errori temporanei di delivery
            $arefGetAcceptReceipt = [$arefGetAcceptReceipt]
                if(ref($arefGetAcceptReceipt) ne 'ARRAY');
            my @errRecips;
            foreach(@$arefGetAcceptReceipt){
                push @errRecips, grep { $_->reciptDone() == 0 ||
                                        $_->reciptDone() == 2}
                                 @{$_->recips}}
            if(@errRecips == @$arefGetAcceptReceipt){
                appendBookmark($hBookMarkTable{'PA_RICAVVCONS'}, \@errRecips);
            }else{
                eraseBookmarks()
            }
        #}
        }
    }

    @arrOut;
}

#
# Log legale degli estremi del/i messaggi forniti in ingresso
#
# IN: obj Opec::Message o a ref di Opec::Message
#
# OUT: -
#
sub dispatchReportSummary ($) {
    my $objRef = shift;

    return if(!ref($objRef));
    $objRef = [$objRef] if(ref($objRef) ne 'ARRAY');

    my $direction;
    my $i;
    my $gest;
    my $err_est;
    my $dest_dt;
    my $identificativo;
    foreach my $obj (@$objRef){
        $i++;
        $gest    = $obj->get_xml_data('dati#gestore-emittente') || 'Non fornito';
        $err_est = $obj->get_xml_data('dati#errore-esteso');
        $dest_dt = $obj->get_xml_data('dati#consegna');
        $identificativo = $obj->get_xml_data('dati#identificativo');

        doLog(0,$obj->name.".$i - Direzione: ".$obj->direction);
        doLog(0,$obj->name.".$i - Sender: ".$obj->sender->mailAddress);
        map { doLog(0,$obj->name.".$i - Rcpt: ".$_->mailAddress) } @{$obj->recips()};
        doLog(0,$obj->name.".$i - Subject: ".$obj->h_subject_utf8);
        doLog(0,$obj->name.".$i - Message-ID: ". &myChomp($obj->mime_entity->head->get($OPEC_HEADER_KEY_MSGID)));
        doLog(0,$obj->name.".$i - Message-ID-rif: ". &myChomp($obj->mime_entity->head->get($OPEC_HEADER_KEY_MSGID_RIF)));
        if ($identificativo) {
            doLog(0,$obj->name.".$i - Identificativo: " .
                  '<' . &myChomp($identificativo) . '>');
        }
        doLog(0,$obj->name.".$i - Gestore: $gest");
        doLog(0,$obj->name.".$i - Errore-esteso: $err_est") if defined $err_est;
        doLog(0,$obj->name.".$i - Consegna-DT: $dest_dt") if defined $dest_dt;
        doLog(0,$obj->name.".$i - Dimensione: " . $obj->mail_size) if defined $obj->mail_size;
        if ( $ATTACH_DO_LOG ) {
          my $attachs_ref = $obj->attachs();
          for my $attach_name ( keys %$attachs_ref ) {
            my $attach_info = $attachs_ref->{$attach_name};
            if ( $ATTACH_CHECKSUM ) {
              doLog(0,$obj->name.".$i - Allegato: $attach_name (size: @$attach_info[0], digest: @$attach_info[1])");
            } else {
              doLog(0,$obj->name.".$i - Allegato: $attach_name (size: @$attach_info[0])");
            }
          }
        }
    }
}

sub dispatchReport {
    my $objRef = shift;

    return if(!ref($objRef));
    $objRef = [$objRef] if(ref($objRef) ne 'ARRAY');

    doLog(0, "Resoconto dispaccio:");
    my $i;
    foreach my $obj(@{$objRef}){
        $i++;
        foreach(@{$obj->recips()}){
            doLog(0, $obj->name.".$i - Esito: ".$_->mailAddress.' - '.$_->reciptRemoteResponse)}
            # log xml
            doLogXml($obj);
            doLog(0,$obj->name.".$i - Dimensione: " . $obj->mail_size) if defined $obj->mail_size;
    }

}


# ###
### user customizable Net::Server hook
sub child_init_hook {
    my($self) = shift;
    local $SIG{CHLD} = 'DEFAULT';
    $0 = 'opecd (virgin child-ready)';
    $msgCount = 1;
    $ENV{'HOME'} = $MYHOME;

    # imposta il task per la rotazione dei log
    # qui sono sicuro di avere i task dell'utente giusto e
    # se succede qualcosa vengono ricreati
    defined $SYSTEM_MINTIME ? enqSPINLOG(0) : delSPINLOG(0);
    defined $LEGAL_MINTIME  ? enqSPINLOG(1) : delSPINLOG(1);
    defined $XML_MINTIME    ? enqSPINLOG(2) : delSPINLOG(2);

    # istanzio l'obj Opec::LdapTool
    # (che inizializza la connessione verso LDAP)
    # non appena carico il processo e per
    # ogni richiesta riciclo l'oggetto
    eval {$objLdap = new Opec::LdapTool};
    $objLdap = $@ if $@;

    # istanzio il dispatcher:
    # gestisce il traffico in uscita
    $objDispatcher = new Opec::Out::Dispatcher($obj_mapper);

    # inizializzo Opec::Sign
    $objSign = Opec::Sign->new($objLdap);

    # inizializzo il modulo di interfaccia per antivirus
    $avObj = $av_mod->new();

    # inizializzo il modulo per l'esecuzione dei task di amministrazione
    $objCommand = new Opec::Command::Dispatcher;

}

### user customizable Net::Server hook
sub post_accept_hook {
    my($self) = shift;
    local $SIG{CHLD} = 'DEFAULT';
    $childInvocationCount++; # contatore di richieste
    $0 = 'opecd (child-ready)';     # modifico il nome del processo
}

sub child_finish_hook {
    my($self) = shift;

    local $SIG{CHLD} = 'DEFAULT';
    undef $inObj;

    # sblocco la copia cache
    untie %hLocalCertDomains;
    undef $lockDbCache;
}

sub _chk_scheduler ($) {
    my $scheduler_ver = shift;
    return 0 if($scheduler_ver !~ /^\d+\.?\d+$/);
    $scheduler_ver >= $CONST_COMPATIBILITY_SCHEDULER ? 1 :0
}
