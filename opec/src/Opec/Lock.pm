#
# $Id: Lock.pm,v 1.15 2011/06/03 18:02:02 flazan Exp $
#
# Project       OpenPec
# file name:    Lock.pm
# package:      Opec::Lock
#
# DESCRIPTION
# Gestione della concorrenza su file
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#
package Opec::Lock;

use strict;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    @EXPORT = qw(&lock &lock_u &locksh &locksh_u &unlock);
}
use Fcntl qw(:flock);

use subs @EXPORT;


sub locksh_u($) {
    my $file = shift;
    return flock($file, LOCK_NB|LOCK_SH) ? 1 : undef;
}

sub lock_u($) {
    my $file = shift;
    return flock($file, LOCK_NB|LOCK_EX) ? 1 : undef;
}

sub locksh($) {
    my $file = shift;
    flock($file, LOCK_SH) or die "Can't lock $file: $!";
}

sub lock($) {
    my $file = shift;
    flock($file, LOCK_EX) or die "Can't lock $file: $!";
    seek($file, 0, 2) or die "Can't position $file to its tail: $!";
}

sub unlock($) {
    my $file = shift;
    flock($file, LOCK_UN) or die "Can't unlock $file: $!";
}

1;

__END__

=head1 NAME

Opec::Lock - Gestione della concorrenza su file

=head1 SYNOPSIS

    use Opec::Lock;

    lock('prova.txt');
    unlock('prova.txt');




=head1 DESCRIPTION

C<Opec::Lock> permette di bloccare o sbloccare l'accesso in scrittura ad un file


=head1 FUNCTIONS

=over 4

=item lock ( STRING )

Blocca il file STRING. Restituisce C<die> in caso di errore

    lock ( 'prova.txt' );

=back

=item unlock ( STRING)

Sblocca il file STRING. Restituisce un C<die> in caso di errore

    unlock ( 'prova.txt' );

=head1 AUTHOR

Fanton Flavio, Luca Manganelli

=cut
