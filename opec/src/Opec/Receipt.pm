#
# $Id: Receipt.pm,v 1.48 2013/03/28 16:59:03 ldivizio Exp $
#
# Project       OpenPec
# file name:    Receipt.pm
# package:      Opec::Receipt
#
# DESCRIPTION
# Generatore di ricevute
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   23/04/2004
#     - risolti vari bug soprattutto nelle ricevute di avvenuta consegna
#   05/07/2004
#     - risolto bug su Ricevute di Consegna/Errore di Consegna anomale.
#       Segnalato da stefano.zanarini@yacme.com
#   21/10/2004 - Fanton Flavio - segnalazione e soluzione Zuin
#       Aggiornamento timezone
#   16/03/2006 - Umberto Ferrara
#     - aggiunta ricevuta di non accettazione
#   20/03/2006 - Umberto Ferrara
#     - aggiunte ricevute nuove regole tecniche
#   15/09/2006 - Flavio Fanton
#     - Revisione avvisi virus
#   23/07/2007 - Flavio Fanton
#     - aggiunta getNotAcceptanceReceiptMaxSize per la generazione della
#       ricevuta di non accettzione per superamento dimensione max
#       del messaggio in ingresso
#   23/07/2007 - Flavio Fanton
#     - forzata codifica a 7-bit per il body delle ricevute (_getReceipt)
#   22/01/2008 - Flavio Fanton
#     - modificata la ricevuta di non accettazione in modo che prenda
#       la stringa d'errore dall'esterno
#   04/01/2008 - Fanton Flavio
#     - adattato alle ultime modifiche di Opec::Sign
#   15/12/2008 - Flavio Fanton
#     - bugfix su caratteri speciali con codifica diversa da iso-8859-1
#   29/05/2009 - Luca Di Vizio
#     - bugfix per caratteri che utilizzano code page
#   09/06/2009 - Flavio Fanton
#     - eliminate tutte le validateRecipt data la modifica su opec
#   01/02/2010 - Fanton Flavio
#       - Corretto bugfix sui destinatari multipli
#   17/09/2010 - Fanton Flavio
#       - aggiornato il pattern per il match di utente inesistente verso LDA
#   20/12/2010 - Flavio Fanton
#         - sincronizzati istanti temporali tra xml e body
#     - bugfix su attributo tipo del tag destinatari
#   22/12/2010 - Fanton Flavio
#         - bugfix su subject con errore nella decodifica
#   23/12/2010 - Fanton Flavio
#         - bugfix su nome file errato in ricevuta breve su utenti locali
#       segnalazione HP-EDS
#   15/02/2011 - Fanton Flavio
#         - bugfix su getXml_err segnalato da DigitPA
#       28/03/2011 - Fanton Flavio
#               - aggiunta gestione di $KEY_TEMPLATE_RCPTTO_SHORT che si riferisce ai
#         destinatari senza il dettaglio certificato/non certificato
#       30/05/2011 - Fanton Flavio
#               - aggiunto il display name al sender
#       30/05/2011 - Fanton Flavio
#               - aggiunto template html
#       26/07/2011 - Fanton Flavio
#               - sostituita la lista dei destinatari attuali con quelli originali
#         nel body della ricevuta in carico
#       22/08/2011 - Fanton Flavio
#         - sostituita la chiamata a getCertInfo_rmc con getCertInfo_rmc12 o getCertInfo_rmc24
#       13/10/2011 - Fanton Flavio
#     - gestione di TS header = TS body = TS xml = istante di creazione dell'oggetto che rappresenta il messaggio
#       oppure  TS header = istante di ricezione del messaggio che lo ha generato e
#               TS body = TS xml = istante di creazione dell'oggetto che rappresenta il messaggio
#   10/04/2012 - Luca Di Vizio
#     - bugfix su template della ricevuta di accettazione
#       26/04/2012 - Fanton Flavio
#     - fix coerenza TS Header, TS Body, TS XML
#   17/08/2012 - Fanton Flavio, Luca Di Vizio
#     - Bugfix ricevuta di consegna breve
#   20/03/2013 - Fanton Flavio, Lorenzo Gaggini
#     - Aggiunto controllo log di getGetLoadReceipt
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2011  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

package Opec::Receipt;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    %EXPORT_TAGS = ();
    @EXPORT = ();
    @EXPORT_OK = qw( &getAcceptanceReceipt &getGetLoadReceipt
                     &getDeliveryReceipt &getNotAcceptanceReceipt
                     &getNotAcceptanceReceiptVirus &getReceiptVirusFound
                     &getNotAcceptanceReceiptMaxSize
                     &getNotAcceptanceReceiptMaxRcpt
                     &getNoDeliveryVirusReceipt
                     &getXml_identificativo
                     &getXml_destinatari
                     &getXml_ricezione
                     &getXml_consegna
                     &getXml_gestore
                     &getXml_risposte
                     &getRMC12
                     &getRMC24
                     &getNotAcceptanceDLerr );
}

use subs @EXPORT_OK;


use strict;
use Encode qw( encode );
use MIME::Words qw( encode_mimeword );
use Opec::Conf qw ( :confvars :const :template :platform );
use Opec::Util qw (&actualDateTime &actualDate &actualTime &myChomp
                   &headerMsgId &splitAddress &getAddresses &timezone);
use Opec::Message;
use Opec::Xml qw(&getCertInfo_accept &getCertInfo_getload
                 &getCertInfo_delivery &getCertInfo_err_delivery
                 &getCertInfo_notaccept &getCertInfo_notaccept_virus
                 &getCertInfo_virusfound &getCertInfo_errdelivery_virus
                 &getCertInfo_rmc12
                 &getCertInfo_rmc24);

use Mail::Header;
use MIME::Tools;
use MIME::Body;
use MIME::Entity;
use Opec::Sign;
use File::Basename;

my %OPEC_XML_CACHE = ();
my @OPEC_HEADER_MIME_VERSION    = ('MIME-Version', '1.0');
my $CONST_MSG_FILENAME          = 'msg.txt';

# obj contenente i dati da fondere con i template
my %myFields;
    $myFields{$KEY_TEMPLATE_DATA}       = '';
    $myFields{$KEY_TEMPLATE_ORA}        = '';
    $myFields{$KEY_TEMPLATE_ZONA}       = '';
    $myFields{$KEY_TEMPLATE_SUBJECT}    = '';
    $myFields{$KEY_TEMPLATE_MAILFROM}   = '';
    $myFields{$KEY_TEMPLATE_RCPTTO}     = '';
    $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = '';
    $myFields{$KEY_TEMPLATE_MSGID}      = '';
    $myFields{$KEY_TEMPLATE_ERR}        = '';
    $myFields{$KEY_TEMPLATE_VIRUS}      = '';

#
# Creo un numero di ricevute uguale al numero
# di destinatari locali certificati
#
# IN: rif. obj Opec::Message
#     (messaggio originale in ingresso)
# OUT: . rif. arr. di obj Opec::Message
#        (ricevute di presa in carico)
#      . undef in caso di incoerenza
#
sub getGetLoadReceipt ($$) {
    my ($originalMsg,$recivedCount) = @_;

    #--- filtro
    # se il gestore di posta del mittente e' locale
    # non viene prodotta alcuna ricevuta
    my $orig_sender = $originalMsg->sender->mailAddress;
    my $orig_sender_domain;
    $orig_sender_domain = substr($orig_sender_domain,1)
            if($orig_sender_domain = (splitAddress($orig_sender))[1]);
    return if(exists $hLocalCertDomains{lc($orig_sender_domain)});

    # lista destinatari di competenza
    my @myRcpt = grep { exists $hLocalCertDomains{ lc(substr(
                (splitAddress($_->mailAddress))[1],1)) } }
                @{$originalMsg->recips()};

    my @msgOut;
    my @msgData;
    my $ldap_silent_tmp = $Opec::objLdap->{silent};
    foreach( @myRcpt ){
        @msgData = ( $originalMsg,
                     $recivedCount,
                     undef,
                     [$_->mailAddress],
                     1,
                     undef,
                     $OPEC_HEADER_VALUE_RECEIPT_GETLOAD);
        push @msgOut, _getRecipt(@msgData);
        $Opec::objLdap->{silent} = 1;
    }
    $Opec::objLdap->{silent} = $ldap_silent_tmp;

    \@msgOut;

}

#
# Generatore di ricevuta di accettazione
# La ricevuta e' relativa al mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
# OUT:1.arr ref ricevute di avvenuta/errore di consegna
#     (Opec::Message)
#
sub getAcceptanceReceipt ($$)
    {push @_, undef, undef, 0, undef, $OPEC_HEADER_VALUE_RECEIPT_ACCEPT; _getRecipt(@_)}


#
# Generatore di ricevuta di non accettazione per eccezioni formali
# La ricevuta e' relativa al mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
# OUT:1.arr ref ricevute di non accettazione
#     (Opec::Message)
#
sub getNotAcceptanceReceipt ($$$)
    {push @_, undef, 4, undef, $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT;
    _getRecipt(@_)}


#
# Generatore di ricevuta di non accettazione per virus
# La ricevuta e' relativa al mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
#     3. descrizione del virus
# OUT:1.arr ref ricevute di non accettazione
#     (Opec::Message)
#
sub getNotAcceptanceReceiptVirus ($$$)
    {push @_, undef, 5, undef, $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT; _getRecipt(@_)}


#
# Generatore di avviso rilevazione virus
# La ricevuta e' relativa al gestore mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
#     3. descrizione del virus
# OUT:1.arr ref di ricevute di rilevazione virus
#     (Opec::Message)
#
sub getReceiptVirusFound ($$$){
    my ($originalMsg, $recivedCount, $errMsr) = @_;
    my @msgData;
    my @msgOut;
    # lista destinatari di competenza
        my @myRcpt = grep { exists $hLocalCertDomains{ lc(substr(
                        (splitAddress($_->mailAddress))[1],1)) } }
                        @{$originalMsg->recips()};

    foreach( @myRcpt ){
        @msgData = ( $originalMsg,
                     $recivedCount,
                     $errMsr,
                     [$_->mailAddress],
                     6,
                     undef,
                     $OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND);
        push @msgOut, _getRecipt(@msgData);
    }

    \@msgOut;
}


#
# Generatore di avviso di mancata consegna per virus
# La ricevuta e' relativa al mittente originale
#
# IN: 1. messaggio (ricevuta di rilevazione
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
#
# OUT:1.arr ref ricevuta di mancata consegna per virus
#     (Opec::Message)
#
sub getNoDeliveryVirusReceipt ($$)  {
    my ($originalMsg, $recivedCount) = @_;
    my @msgData;
    my @msgOut;

    my $err_desc = getXml_err($originalMsg);
    my $aref_dest= getXml_consegna($originalMsg) || return;

    foreach( @$aref_dest ){
        @msgData = ( $originalMsg,
                     $recivedCount,
                     $err_desc,
                     [$_],
                     7,
                     undef,
                     $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY);
        push @msgOut, _getRecipt(@msgData);
    }

    \@msgOut;
}

#
# Generatore di ricevuta di avvenuta/errrore di consegna
# La ricevuta e' relativa ad ogni destinatario locale
# e si differenzia per dest. primari (To) e secondari (Cc)
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
#
# OUT:1.arr ref ricevute di avvenuta/errore di consegna
#     (Opec::Message)
#
sub getDeliveryReceipt ($$) {
    my ($trasportMsg,$recivedCount) = @_;
    my $trasportMsgHead = $trasportMsg->header;
    my @arrReceipt;
    my $receiptType = lc(myChomp($trasportMsgHead->get($OPEC_HEADER_KEY_RECEIPT_TYPE)));

    #--- recupero i destinatari dall'header tenendo separati
    # i primari dai secondari
    my @arrRcptCc = @{getAddresses( join(',',$trasportMsgHead->get('Cc')) )};
    my @arrRcptTo = @{getAddresses( join(',',$trasportMsgHead->get('To')) )};

    # l'envelop e i campi To e Cc del doc. di trasp. sono
    # gli stessi del msg originale

    #--- creo un hash rcpt locali -> To/Cc
    # mi servira' per stabilire se allegare o meno
    # il messaggio originale.
    # Analizzo prima Cc per gestire il caso di ambiguita'
    my %hRcptHeader;
    foreach ( @arrRcptCc ){
        $hRcptHeader{$_} = 'Cc'
            if( exists $hLocalCertDomains{
            lc(substr((splitAddress($_))[1],1)) } )}
    foreach ( @arrRcptTo ){
        $hRcptHeader{$_} = 'To'
            if( exists $hLocalCertDomains{
            lc(substr((splitAddress($_))[1],1)) } )}
    return [] if(!%hRcptHeader);

    #--- per ogni destinatario
    #--- creo un nuovo messaggio avente:
    #--- . recips: il solo destinatario oggetto del ciclo
    #--- . sender: il sender del msg di trasporto (SERVE?)
    #--- . Subject: quello originale
        my $orig_sender = $trasportMsg->sender->mailAddress;
    my $orig_msgid = $trasportMsg->msgid_orig();
    my $new_msgid = $trasportMsg->msgid_new();


        my @allRcpt = grep { exists $hLocalCertDomains{ lc(substr(
                        (splitAddress($_->mailAddress))[1],1)) } }
                        @{$trasportMsg->recips()};
        my $orig_rcpt = getXml_destinatari($trasportMsg) ||
                        [map {[$_->mailAddress,$_->certificate]} @{$trasportMsg->recips()}];


    my $orig_mime_entity;
    foreach my $objRcpt (@allRcpt){
        my $objCopy = new Opec::Message($trasportMsg->tempdir());
        $objCopy->recips([$objRcpt]);
        $objCopy->sender($orig_sender);
        $objCopy->header($trasportMsg->header);

        $objCopy->msgid_new( $new_msgid );
        $objCopy->msgid_orig( $orig_msgid );

        # recupero l'obj Opec::Message::EnvelopRecipt
        # per verificare l'esito della trasmissione
        my $receipt;
        if($objRcpt->reciptDone() == 1){
            # spedito correttamente
            # genero la ricevuta di consegna

                        # specializzazione in base al tipo di ricevuta  di consegna
                        # (completa, breve e sintetica) a seconda di quanto indicato
                        # nell' header del msg di trasporto
                        if ($receiptType eq $OPEC_HEADER_VALUE_RECEIPT_TYPE_SHORT) {    # generazione ricevuta di avvenuta consegna BREVE
                                if($hRcptHeader{$objRcpt->mailAddress()} eq 'To'){
                            # allego il messaggio originale
                            # + gli hash degli allegati
                                if(!$orig_mime_entity){
                                        my @orig_mime_parts = $trasportMsg->mime_entity->parts;
                                        ($orig_mime_entity)=grep {_keep_part($_,$orig_msgid)} @orig_mime_parts;

                                        if(!$orig_mime_entity){
                                                foreach (@orig_mime_parts){
                                                        my @tmp_orig_mime_entity = $_->parts;
                                                        ($orig_mime_entity)=grep {_keep_part($_,$orig_msgid)}
                                                                                                @tmp_orig_mime_entity;
                                                        last if $orig_mime_entity
                                                }
                                        }
                            }

                                # recupero header e body del messaggio
                                my $orig_mime_entity_hash = _formatHash($orig_mime_entity, $trasportMsg->tempdir());
                                $objCopy->mime_entity($orig_mime_entity_hash);
                    $receipt = _getRecipt($objCopy, $recivedCount, undef, $orig_rcpt, 9, 1,
                                                                          $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
                }else{
                                        $receipt = _getRecipt($objCopy, $recivedCount, undef, $orig_rcpt, 9,undef,
                                                                                  $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
                }

            }elsif ($receiptType eq $OPEC_HEADER_VALUE_RECEIPT_TYPE_SYNTHETIC) {    # generazione ricevuta di avvenuta consegna SINTETICA
                                        # non viene allegato niente
                    $receipt = _getRecipt($objCopy, $recivedCount, undef, $orig_rcpt, 8, 1,
                                                                                $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);

                    }else{  # generazione ricevuta di avvenuta consegna COMPLETA
                                if($hRcptHeader{$objRcpt->mailAddress()} eq 'To'){
                                        # allego il msg originale:
                                        # recupero il messaggio originale dagli allegati (solo per gli utenti in "to"
                                        # (nome allegato postacert.eml e tipo message/rfc822)
                                        # e' necessaria anche una verifica sul msgid?

                                        if(!$orig_mime_entity){
                                            my @orig_mime_parts;
                        # ###
                                            if( $trasportMsg->msgid_new ){
                            @orig_mime_parts = $trasportMsg->mime_entity->parts;
                                        }else{
                            my $parser = new MIME::Parser;
                            $parser->extract_nested_messages(0);
                            $parser->output_dir($trasportMsg->tempdir());
                            $parser->output_to_core(0);
                            $parser->tmp_to_core(0);

                            my $tmp_entity = $parser->parse_open( $trasportMsg->tempfiledir );
                                                @orig_mime_parts = $tmp_entity->parts;
                                    }
                        # ###
                                        ($orig_mime_entity)=grep {_keep_part($_,$orig_msgid)} @orig_mime_parts;
                                        if(!$orig_mime_entity){
                                                foreach (@orig_mime_parts){
                                                        my @tmp_orig_mime_entity = $_->parts;
                                                        ($orig_mime_entity)=grep {_keep_part($_,$orig_msgid)}
                                                                                                @tmp_orig_mime_entity;
                                                        last if $orig_mime_entity
                                                }
                                        }
                                }
                                        $objCopy->mime_entity($orig_mime_entity);

                                        $receipt = _getRecipt($objCopy, $recivedCount, undef, $orig_rcpt, 2, 1,
                                                                                $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
                                }else{
                                        $receipt = _getRecipt($objCopy, $recivedCount, undef, $orig_rcpt, 2,undef,
                                                                                $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
                }
                    }

        }else{
            # errore di spedizione
            # genero l'avviso di mancata consegna
            my $errType;
            if( $objRcpt->reciptRemoteResponse &&
                $objRcpt->reciptRemoteResponse =~ /(user|mailbox)\s+unknown|doesn\'t exist/io){
                $errType = $OPEC_CERTINFO_ERR_VALUES_NODEST
            }else{
                $errType = $OPEC_CERTINFO_ERR_VALUES_ALTRO;
            }

            $receipt = _getRecipt(  $objCopy, $recivedCount, $errType, $orig_rcpt, 3, undef,
                                    $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY);
        }
        push @arrReceipt, $receipt;
    }

    \@arrReceipt;
}

#
# Generatore di
# Ricevuta di mancata consegna per superamento dei tempi massimi previsti
# delle 12 ore
#
# IN: 1. messaggio (ricevuta di rilevazione
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
#
# OUT:1. ref RMC12
#     (Opec::Message)
#
sub getRMC12
    {push @_, undef, undef, 10, undef, $OPEC_HEADER_VALUE_RECEIPT_RMC; _getRecipt(@_)}



#
# Generatore di
# Ricevuta di mancata consegna per superamento dei tempi massimi previsti
# delle 24 ore
#
# IN: 1. messaggio (ricevuta di rilevazione
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
#
# OUT:1. ref RMC24
#     (Opec::Message)
#
sub getRMC24
    {push @_, undef, undef, 11, undef, $OPEC_HEADER_VALUE_RECEIPT_RMC; _getRecipt(@_)}


#
# Generatore di ricevuta di non accettazione per superamento dimensione max consentita
# La ricevuta e' relativa al mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
# OUT:1.arr ref ricevute di non accettazione
#     (Opec::Message)
#
sub getNotAcceptanceReceiptMaxSize ($$$)
    {push @_, undef, 12, undef, $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT;
    _getRecipt(@_)}

#
# Generatore di ricevuta di non accettazione per superamento limite max num rcpt
# La ricevuta e' relativa al mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
# OUT:1.arr ref ricevute di non accettazione
#     (Opec::Message)
#
sub getNotAcceptanceReceiptMaxRcpt ($$$)
    {push @_, undef, 13, undef, $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT;
    _getRecipt(@_)}

#
# Generatore di ricevuta di non accettazione errori con DL
# La ricevuta e' relativa al mittente
#
# IN: 1. documento di trasporto
#     (Opec::Message)
#     2. intero per generare il msgid della ricevuta
# OUT:1.arr ref ricevute di non accettazione
#     (Opec::Message)
#
sub getNotAcceptanceDLerr ($$$)
    {push @_, undef, 14, undef, $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT;
    _getRecipt(@_)}

#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'risposte'
#
# IN: 1. Opec::Message
# OUT: arr ref contenente gli indirizzi contenuti nel tag 'risposte',
# undef altrimenti
#
sub getXml_risposte ($) {
    my $objMsg   = shift;
    my $xmlFile;

    return undef unless($xmlFile = _checkXml($objMsg));

    open(XML, $xmlFile) || return undef;
    my @content;
    my $bool;
    while(<XML>){
        if(!$bool){
            $bool = 1 if /\s*\<\s*$OPEC_XML_TAG_INTESTAZIONE\s*\>/so;
            next}

        if(/\s*\<\s*$OPEC_XML_TAG_RISPOSTE\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_RISPOSTE\s*\>/so){
            my $tmp = $1;
            $tmp =~ s/\s//gxs;
            push @content, $tmp;
            next}

        last if /\s*\<\s*\/\s*$OPEC_XML_TAG_INTESTAZIONE\s*\>/so;
    }
    close(XML);

    return @content ? \@content : undef;
}

#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'identificativo'
#
# IN: 1. Opec::Message
# OUT: stringa dell'identificativo, undef altrimenti
#
sub getXml_identificativo ($) {
    my $objMsg   = shift;
    my $xmlFile;

    return undef unless($xmlFile = _checkXml($objMsg));

    open(XML, $xmlFile) || return undef;
    my $ident;
    my $boolStart = 1;
    my $boolEnd   = 0;
    while(<XML>){
        if($boolStart && /\s*\<\s*$OPEC_XML_TAG_IDENTIFICATIVO\s*\>/so){
            $ident = $';
            $boolEnd = 1; undef $boolStart;
            # verifico che sia tutto su una stessa linea
            if(/\s*\<\s*$OPEC_XML_TAG_IDENTIFICATIVO\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_IDENTIFICATIVO\s*\>/so){
                $ident = $1;
                last}
            next;
        }

        if($boolEnd){
            if(/\s*\<\s*\/\s*$OPEC_XML_TAG_IDENTIFICATIVO\s*\>/so){
                $ident .= $`;
                last;
            }
            $ident .= $_;
        }
    }
    close(XML);

    $ident =~ s/\015|\012|\n|<|>|\s//gxs if(defined $ident);

    $ident
}

#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'destinatari'
#
# IN: 1. Opec::Message
# OUT: arr ref contenente i [destinatario,tipo] del forward path, undef altrimenti
#
sub getXml_destinatari ($) {
    my $objMsg   = shift;
    my $xmlFile;

    # verifico che l'obj message non sia stato creato nella sessione:
    # in questo caso alcuni elementi dell'obj mime sono incore
    foreach($objMsg->mime_entity->parts){
        return undef if( ref($_->bodyhandle) eq 'MIME::Body::InCore' );
    }

    return undef unless($xmlFile = _checkXml($objMsg));

    open(XML, $xmlFile) || return undef;
    my @dest;
    my $bool;
    while(<XML>){
        if(!$bool){
            $bool = 1 if /\s*\<\s*$OPEC_XML_TAG_INTESTAZIONE\s*\>/so;
            next}

        if(/\s*\<\s*$OPEC_XML_TAG_RCPT\s*tipo\s*=\"?(certificato|esterno)\"?\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_RCPT\s*\>/iso){
            my $tipo = $1;
            my $tmp  = $2; $tmp  =~ s/\s//gxs;
            $tipo = ($tipo =~ /certificato/io) ? 1 : 0;

            push @dest, [$tmp,$tipo];
            next}

        last if(/\s*\<\s*\/\s*$OPEC_XML_TAG_INTESTAZIONE\s*\>/so);
    }
    close(XML);

    return @dest ? \@dest : undef;
}


#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'ricezione'
#
# IN: 1. Opec::Message
# OUT: arr ref contenente gli indirizzi contenuti nel tag 'ricezione',
# undef altrimenti
#
sub getXml_ricezione ($) {
    my $objMsg   = shift;
    my $xmlFile;

    return undef unless($xmlFile = _checkXml($objMsg));

    open(XML, $xmlFile) || return undef;
    my @dest;
    my $bool;
    while(<XML>){
        if(!$bool){
            $bool = 1 if /\s*\<\s*$OPEC_XML_TAG_DATI\s*\>/so;
            next}

        if(/\s*\<\s*$OPEC_XML_TAG_RICEZIONE\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_RICEZIONE\s*\>/so){
            my $tmp = $1;
            $tmp =~ s/\s//gxs;
            push @dest, $tmp;
            next}

        last if(/\s*\<\s*\/\s*$OPEC_XML_TAG_DATI\s*\>/so);
    }
    close(XML);

    return @dest ? \@dest : undef;
}

#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'gestore-emittente'
#
# IN: 1. Opec::Message
# OUT: stringa del tag, undef altrimenti
#
sub getXml_gestore ($) {
    my $objMsg   = shift;
    my $xmlFile;

    return undef unless($xmlFile = _checkXml($objMsg));

    open(XML, $xmlFile) || return undef;
    my $ident;
    my $boolStart = 1;
    my $boolEnd   = 0;
    while(<XML>){
        if($boolStart && /\s*\<\s*$OPEC_XML_TAG_GESTORE\s*\>/so){
            $ident = $';
            $boolEnd = 1; undef $boolStart;
            # verifico che sia tutto su una stessa linea
            if(/\s*\<\s*$OPEC_XML_TAG_GESTORE\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_GESTORE\s*\>/so){
                $ident = $1;
                last}
            next;
        }

        if($boolEnd){
            if(/\s*\<\s*\/\s*$OPEC_XML_TAG_GESTORE\s*\>/so){
                $ident .= $`;
                last;
            }
            $ident .= $_;
        }
    }
    close(XML);

    $ident =~ s/\015|\012|\n|<|>|\s//gxs if(defined $ident);

    $ident
}


#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'consegna'
#
# IN: 1. Opec::Message
# OUT: arr ref contenente gli indirizzi contenuti nel tag 'consegna',
# undef altrimenti
#
sub getXml_consegna ($) {
    my $objMsg   = shift;
    my $xmlFile;

    return undef unless($xmlFile = _checkXml($objMsg));

    open(XML, $xmlFile) || return undef;
    my @dest;
    my $bool;
    while(<XML>){
        if(!$bool){
            $bool = 1 if(/\s*\<\s*$OPEC_XML_TAG_DATI\s*\>/so);
            next}

        if(/\s*\<\s*$OPEC_XML_TAG_CONSEGNA\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_CONSEGNA\s*\>/so){
            my $tmp = $1;
            $tmp =~ s/\s//gxs;
            push @dest, $tmp;
            next}

        last if(/\s*\<\s*\/\s*$OPEC_XML_TAG_DATI\s*\>/so);
    }
    close(XML);

    return @dest ? \@dest : undef;
}


#
# Dato un oggetto Opec::Message (tipo ricevuta validata) restituisce
# il valore del tag 'identificativo'
#
# IN: 1. Opec::Message
# OUT: stringa dell'identificativo, undef altrimenti
#
sub getXml_err ($) {
    my $objMsg   = shift;
    my $xmlFile;

    return unless($xmlFile = _checkXml($objMsg));

        my $xml_str;
    {
        local $/=undef;
        open FILE, $xmlFile or return;
        binmode FILE;
        $xml_str = <FILE>;
        close FILE;
    }

        $xml_str =~ /\s*\<\s*$OPEC_XML_TAG_ERRESTESO\s*\>(.+)\<\s*\/\s*$OPEC_XML_TAG_ERRESTESO\s*\>/so;
    return if !defined $1;

    my $err = $1;
    $err =~ s/$eol|$eolcp//g;

        return $err;
}

#
# funzione privata
# Dato un oggetto Opec::Message, restituisce il nome del file xml che contiene i dati
# di certificazione.
# Questo metodo si rende fondamentale dal momento in cui un messaggio puo' contenere
# + allegati con lo stesso nome; dobbiamo quindi prevenire il caso in cui un fornitore
# alleghi oltre al file daticert.xml un altro file con lo stesso nome ma con contenuto
# inaspettato.
#
# IN: 1. Opec::Message
# OUT: restituisce il nome del file che rappresenta l'allegato xml certificato, undef altrimenti
#
sub _checkXml ($) {
    my $objMsg   = shift;
    return undef if(!$objMsg || ref($objMsg) ne 'Opec::Message');

    chdir($objMsg->tempdir) || return undef;
    return $OPEC_XML_FILE if exists $OPEC_XML_CACHE{$OPEC_XML_FILE};
    $objMsg->mime_entity;

    # devo verificare che il file $OPEC_XML_FILE sia quello che mi aspetto
    open(XML, $OPEC_XML_FILE) || return undef;
    my $check;
    while(<XML>){
        if(/\s*\<\s*postacert\s*tipo\s*=/so){
            $check = $OPEC_XML_FILE;
            last}
    }
    close(XML);

    $check
}


#
# funzione privata
# Generatore di ricevute generico
# IN: 1. Messaggio ricevuto in ingresso (obj. Opec::Message)
#     2. contatore - per generare un eventuale ID
#     3. eventuale descrizione del virus trovato sulla mail (per ricevute di non
#        accettazione per virus, rilevazione virus e mancata consegna per virus)
#     4. eventuale array destinatari msg originale (solo per ricevuta di mancata
#         consegna per virus)
#     3. tipo di ricevuta da creare:
#        0 - ric. ACCETTAZIONE
#        1 - ric. PRESA IN CARICO
#        2 - ric. AVVENUTA CONSEGNA
#        3 - ric. ERRORE DI CONSEGNA
#        4 - avv. NON ACCETTAZIONE
#        5 - avv. NON ACCETTAZIONE PER VIRUS
#        6 - avv. RILEVAZIONE VIRUS
#        7 - avv. MANCATA CONSEGNA PER VIRUS
#        8 - ric. AVVENUTA CONSEGNA SINTETICA
#        9 - ric. AVVENUTA CONSEGNA BREVE
#       10 - ric. MANCATA CONSEGNA 12
#       11 - ric. MANCATA CONSEGNA 24
#       12 - ric. NON ACCETTAZIONE PER SUPERAMENTO DIMENSIONE
#       13 - ric. NON ACCETTAZIONE PER SUPERAMENTO LIMITE NUM RCPT
#     4. eventuale messaggio da allegare (obj. Opec::Message)
# OUT: ricevuta richiesta (obj. Opec::Message)
#
sub _getRecipt ($$;$$$$$) {
    my $originalMsg     = shift;
    my $recivedCount    = shift;
    my $errDesc         = shift;
    my $origArrTo       = shift;
    my $recType         = shift;
    my $attachOrigMsg   = shift;
    my $recName         = shift;

    my $originalMsgHead = $originalMsg->header;
    # nuova ricevuta
    # mantengo mime entity in memoria ad esclusione
    # di ric. di avv. consegna in presenza di allegato
        my $receiptMsg          = Opec::Message->new($originalMsg->tempdir,$recName);
        my $receiptMsgMimeHead  = new MIME::Head;

        #--- genero l'HEADER comune
        $receiptMsgMimeHead->add ($OPEC_HEADER_MIME_VERSION[0], $OPEC_HEADER_MIME_VERSION[1]);
#    if($OPEC_TS_HEADERXMLBODY_ISSAME){
#       # TS Header = TS Body = TS XML =
#       # timestamp di creazione dell'oggetto che rappresenta il messaggio
#               $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));
#    }else{
#       # TS Header = istante di ricezione messaggio e
#       # TS Body = TS XML = timestamp di creazione dell'oggetto che rappresenta il messaggio
#       $receiptMsgMimeHead->add ('Date', actualDateTime($originalMsg->rx_time));
#    }

        if(defined $adminPecEmail_RDS){
                $receiptMsgMimeHead->add ('From', "$adminPecEmail_RDS <$adminPecEmail>");
        }else{
                $receiptMsgMimeHead->add ('From', $adminPecEmail);
        }

    $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID,'<'.headerMsgId($recivedCount).'>');

    #--- specializzazione per ricevuta
    my $body;
    my $body_html;
    my $xml;
        my @arrTo;
        my $orig_subject = $originalMsg->h_subject_utf8;
    my $orig_sender  = $originalMsg->sender->mailAddress;

        if($recType == 0){      # ACCETTAZIONE
            # TS Header = TS Body = TS XML = data di effettiva accettazione
        $receiptMsgMimeHead->add ('Date', actualDateTime($originalMsg->rx_time));

            # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                    $OPEC_HEADER_VALUE_RECEIPT_ACCEPT);
            $receiptMsgMimeHead->add ('To', $orig_sender);
            if(defined $originalMsg->msgid_orig()){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        '<'.$originalMsg->msgid_orig().'>')}
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_ACCEPT.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_ACCEPT.$orig_subject);
            push @arrTo, $orig_sender;
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO}     = join ("\n",
            map(    $_->certificate?$_->mailAddress.' ("posta certificata")':
                    $_->mailAddress.' ("posta ordinaria")',
                    @{$originalMsg->recips}));
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT}     = join ("\n",
            map(    $_->mailAddress,
                    @{$originalMsg->recips}));
        $body = $TEMPLATE_RECEIPT_ACCEPT;
        $body_html = $TEMPLATE_RECEIPT_ACCEPT_HTML;
                $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($originalMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($originalMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ZONA}    = timezone($originalMsg->rx_time);

                # xml
            #$xml = getCertInfo_accept($receiptMsg->rx_time, $originalMsg);
            $xml = getCertInfo_accept($originalMsg->rx_time, $originalMsg);

        }elsif($recType == 1){  # PRESA IN CARICO
            # TS Header = TS Body = TS XML = data di effettiva accettazione
        $receiptMsgMimeHead->add ('Date', actualDateTime($originalMsg->rx_time));

            # mittente: gestore di riferimento
            # del mittente della mail originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                    $OPEC_HEADER_VALUE_RECEIPT_GETLOAD);
        my $rifMailAddSender =
                $Opec::objLdap->getMailReceipt($originalMsg->sender);
        $receiptMsgMimeHead->add ('To', $rifMailAddSender);
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
            push @arrTo, $rifMailAddSender;
            # il subj del doc trasp e' del tipo:
            # POSTA CERTIFICATA: <testo>
            $orig_subject = substr( $orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP) );
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_GETLOAD.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_GETLOAD.$orig_subject);
#        # body: destinatari
#        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
#        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n", @$origArrTo);
#        $body = $TEMPLATE_RECEIPT_GETLOAD;
#        $body_html = $TEMPLATE_RECEIPT_GETLOAD_HTML;

        # devo recuperare l'header del msg originale
        # per poter costruire la parte dell'instestazione del file xml
                my @orig_mime_parts = $originalMsg->mime_entity->parts;
                my ($orig_mime_entity)=grep {_keep_part($_)} @orig_mime_parts;
                if(!$orig_mime_entity){
                        foreach (@orig_mime_parts){
                                my @tmp_orig_mime_entity = $_->parts;
                                ($orig_mime_entity)=grep {_keep_part($_)}
                                                                        @tmp_orig_mime_entity;
                                last if $orig_mime_entity
                        }
                }
        my $objOrig = Opec::Message->new($originalMsg->tempdir,$recName);
        $objOrig->mime_entity($orig_mime_entity->parts);
        $objOrig->sender(@{getAddresses($objOrig->header->get('From'))});
        my @arrRcpt = @{getAddresses(join(',',$objOrig->header->get('Cc')))};
        push @arrRcpt,@{getAddresses(join(',',$objOrig->header->get('To')))};
        eval{$Opec::objLdap->validateRecipt($objOrig->recips(\@arrRcpt))};

        # body: destinatari originali
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = join ("\n",
            map(    $_->mailAddress,
                    @{$objOrig->recips}));
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n",
            map(    $_->certificate?$_->mailAddress.' "posta certificata"':
                    $_->mailAddress.' "posta ordinaria"',
                    @{$objOrig->recips}));
        $body = $TEMPLATE_RECEIPT_GETLOAD;
        $body_html = $TEMPLATE_RECEIPT_GETLOAD_HTML;
                $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($originalMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($originalMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ZONA}    = timezone($originalMsg->rx_time);

        $xml = getCertInfo_getload(     $originalMsg->rx_time,
                                                                $originalMsg,
                                                                $objOrig,
                                                                $origArrTo);

        }elsif($recType == 2){  # AVVENUTA CONSEGNA COMPLETA
            # TS Header = TS Body = TS XML =
            # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

            # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                    $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
            $receiptMsgMimeHead->add ('To', $orig_sender);

        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
            push @arrTo, $orig_sender;
            # il subj del doc trasp e' del tipo:
            # POSTA CERTIFICATA: <testo>
            $orig_subject = substr($orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP));
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY.$orig_subject);
        # body (il destinatario e' sempre uno)
        my $tmpRcpt = $originalMsg->recips->[0];
        $myFields{$KEY_TEMPLATE_RCPTTO} = $tmpRcpt->mailAddress .
                                        ' "posta certificata"';
                $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = $tmpRcpt->mailAddress;
            $body = $TEMPLATE_RECEIPT_DELIVERY;
            $body_html = $TEMPLATE_RECEIPT_DELIVERY_HTML;
                $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
            $xml = getCertInfo_delivery( $receiptMsg->rx_time, $originalMsg, $origArrTo );

        }elsif($recType == 3){  # AVV. DI MANCATA CONSEGNA
            # TS Header = TS Body = TS XML =
            # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

            # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                    $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY);
            $receiptMsgMimeHead->add ('To', $orig_sender);
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
            push @arrTo, $orig_sender;
        if( $orig_subject =~ /^$OPEC_HEADER_SUBJECT_DOCTRASP/){
            $orig_subject = substr(
            $orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP));
        }
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY.$orig_subject);
        # body (il destinatario e' sempre uno)
        my $tmpRcpt = $originalMsg->recips->[0];
        $myFields{$KEY_TEMPLATE_RCPTTO} = $tmpRcpt->mailAddress .
                                        ' "posta certificata"';
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = $tmpRcpt->mailAddress;
        $myFields{$KEY_TEMPLATE_ERR} = $tmpRcpt->reciptRemoteResponse();
            $body = $TEMPLATE_RECEIPT_ERR_DELIVERY;
            $body_html = $TEMPLATE_RECEIPT_ERR_DELIVERY_HTML;
                $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
            $xml = getCertInfo_err_delivery($receiptMsg->rx_time,
                                                                        $originalMsg,
                                        $origArrTo,
                                        $errDesc,
                                            $tmpRcpt->reciptRemoteResponse());


    }elsif($recType == 4){  # NON ACCETTAZIONE PER ECCEZIONI FORMALI
            # TS Header = TS Body = TS XML =
            # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
            $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( $originalMsg->msgid_orig() ){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, '<'.$originalMsg->msgid_orig().'>');
        }elsif($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        }
        push @arrTo, $orig_sender;
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject);
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO}     = join ("\n",
            map(    $_->certificate?$_->mailAddress.' "posta certificata"':
                    $_->mailAddress.' "posta ordinaria"',
                    @{$originalMsg->recips}));
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT}= join ("\n",
            map(    $_->mailAddress,
                    @{$originalMsg->recips}));

        $myFields{$KEY_TEMPLATE_ERR} = $errDesc;
        $body = $TEMPLATE_RECEIPT_NOTACCEPT;
        $body_html = $TEMPLATE_RECEIPT_NOTACCEPT_HTML;
                $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
                $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_notaccept($receiptMsg->rx_time, $originalMsg,undef,$errDesc);

    }elsif($recType == 5){  # NON ACCETTAZIONE PER VIRUS
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                   $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT);
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_SICUREZZA,
                   $OPEC_HEADER_VALUE_SICUREZZA_ERR);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( $originalMsg->msgid_orig() ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        '<'.$originalMsg->msgid_orig().'>')}
        push @arrTo, $orig_sender;
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT_VIRUS.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT_VIRUS.$orig_subject);
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n",
            map( $_->mailAddress,@{$originalMsg->recips}));
        $myFields{$KEY_TEMPLATE_VIRUS} = $errDesc;
        $body = $TEMPLATE_RECEIPT_NOTACCEPT_VIRUS;
        $body_html = $TEMPLATE_RECEIPT_NOTACCEPT_VIRUS_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_notaccept_virus($receiptMsg->rx_time,
                                                                           $originalMsg,
                                                   $OPEC_CERTINFO_ERR_VALUES_VIRUS,
                                           $errDesc);

    }elsif($recType == 6){  # RILEVAZIONE VIRUS
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # header (mittente originale)
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                   $OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND);
        $receiptMsgMimeHead->add ($OPEC_HEADER_XSENDER,
                   $orig_sender);
        my $rifMailAddSender =
            $Opec::objLdap->getMailReceipt($originalMsg->sender);
        $receiptMsgMimeHead->add ('To', $rifMailAddSender);
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        push @arrTo, $rifMailAddSender;
        # recupero il subject originale
        $orig_subject = substr($orig_subject, length($OPEC_HEADER_SUBJECT_DOCTRASP) );
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_VIRUSFOUND.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_VIRUSFOUND.$orig_subject);
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n", @$origArrTo);
        $myFields{$KEY_TEMPLATE_VIRUS} = $errDesc;
        $body = $TEMPLATE_RECEIPT_VIRUSFOUND;
        $body_html = $TEMPLATE_RECEIPT_VIRUSFOUND_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_virusfound($receiptMsg->rx_time,
                                                                  $originalMsg,
                                      $OPEC_CERTINFO_ERR_VALUES_ALTRO,
                                      $errDesc,
                                      getXml_destinatari($originalMsg),
                                      $origArrTo);

        }elsif($recType == 7){  # MANCATA CONSEGNA PER VIRUS
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # header
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                            $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY);
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_SICUREZZA,
                            $OPEC_HEADER_VALUE_SICUREZZA_ERR);
        # recupero il mittente originale
        $orig_sender = myChomp($originalMsgHead->get($OPEC_HEADER_XSENDER));
        $receiptMsgMimeHead->add ('To', $orig_sender);
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
            push @arrTo, $orig_sender;
        # recupero il subject originale
        $orig_subject = substr($orig_subject, length($OPEC_HEADER_SUBJECT_RECEIPT_VIRUSFOUND));
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY_VIRUS.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY_VIRUS.$orig_subject);
        # body
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n", @$origArrTo);
        $myFields{$KEY_TEMPLATE_VIRUS} =  $errDesc;
        $originalMsg->msgid_new(getXml_identificativo($originalMsg));
        $body = $TEMPLATE_RECEIPT_ERRDELIVERY_VIRUS;
        $body_html = $TEMPLATE_RECEIPT_ERRDELIVERY_VIRUS_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_errdelivery_virus(   $receiptMsg->rx_time,
                                                                                        $originalMsg,
                                                $OPEC_CERTINFO_ERR_VALUES_VIRUS,
                                                $errDesc,
                                                getXml_destinatari($originalMsg),
                                                $origArrTo,
                                                getXml_risposte($originalMsg));


        }elsif($recType == 8){  # AVVENUTA CONSEGNA SINTETICA
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
        push @arrTo, $orig_sender;
        # il subj del doc trasp e' del tipo:
        # POSTA CERTIFICATA: <testo>
        $orig_subject = substr(
        $orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP));
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY.$orig_subject);
        # body (il destinatario e' sempre uno)
        my $tmpRcpt = $originalMsg->recips->[0];
        $myFields{$KEY_TEMPLATE_RCPTTO}     =
        $tmpRcpt->mailAddress . ' "posta certificata"';
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = $tmpRcpt->mailAddress;
        $body = $TEMPLATE_SYNTHETIC_RECEIPT_DELIVERY;
        $body_html = $TEMPLATE_SYNTHETIC_RECEIPT_DELIVERY_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_delivery($receiptMsg->rx_time,
                                                                $originalMsg,
                                                                $origArrTo );

        }elsif($recType == 9){  # AVVENUTA CONSEGNA BREVE
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
                $OPEC_HEADER_VALUE_RECEIPT_DELIVERY);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
        push @arrTo, $orig_sender;
        # il subj del doc trasp e' del tipo:
        # POSTA CERTIFICATA: <testo>
        $orig_subject = substr(
        $orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP));
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY.$orig_subject);
        # body (il destinatario e' sempre uno)
        my $tmpRcpt = $originalMsg->recips->[0];
        $myFields{$KEY_TEMPLATE_RCPTTO}     =
        $tmpRcpt->mailAddress . ' "posta certificata"';
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = $tmpRcpt->mailAddress;
        $body = $TEMPLATE_SHORT_RECEIPT_DELIVERY;
        $body_html = $TEMPLATE_SHORT_RECEIPT_DELIVERY_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_delivery($receiptMsg->rx_time,
                                                                $originalMsg,
                                                                $origArrTo );

        }elsif($recType == 10){  # RMC 12
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT, $OPEC_HEADER_VALUE_RECEIPT_RMC);
        $receiptMsgMimeHead->add ('To', $orig_sender); push @arrTo, $orig_sender;
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
        $orig_subject = substr( $orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP) );
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_RMC.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_RMC.$orig_subject);
        # body
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n", map( $_->mailAddress,@{$originalMsg->recips} ));
        $body = $TEMPLATE_RECEIPT_RMC12;
        $body_html = $TEMPLATE_RECEIPT_RMC12_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

        # xml
        $xml = getCertInfo_rmc12($receiptMsg->rx_time, $originalMsg);

        }elsif($recType == 11){  # RMC 24
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT, $OPEC_HEADER_VALUE_RECEIPT_RMC);
        $receiptMsgMimeHead->add ('To', $orig_sender); push @arrTo, $orig_sender;
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $receiptMsgMimeHead->add (  $OPEC_HEADER_KEY_MSGID_RIF,
                                        $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF))}
        $orig_subject = substr( $orig_subject,length($OPEC_HEADER_SUBJECT_DOCTRASP) );
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_RMC.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_RMC.$orig_subject);
        # body
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n", map( $_->mailAddress,@{$originalMsg->recips} ));
        $body = $TEMPLATE_RECEIPT_RMC24;
        $body_html = $TEMPLATE_RECEIPT_RMC24_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

                # xml
        $xml = getCertInfo_rmc24($receiptMsg->rx_time, $originalMsg);

    }elsif($recType == 12){  # NON ACCETTAZIONE PER SUPERAMENTO DIMENSIONE
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
            $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( $originalMsg->msgid_orig() ){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, '<'.$originalMsg->msgid_orig().'>');
        }elsif($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        }
        push @arrTo, $orig_sender;
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject);
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n",
            map( $_->mailAddress,@{$originalMsg->recips}));
        $myFields{$KEY_TEMPLATE_ERR} = $errDesc;
        $body = $TEMPLATE_RECEIPT_NOTACCEPT;
        $body_html = $TEMPLATE_RECEIPT_NOTACCEPT_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

                # xml
        $xml = getCertInfo_notaccept(   $receiptMsg->rx_time,
                                                                        $originalMsg,
                                                                        undef,
                                                                        $errDesc);
    }elsif($recType == 13){  # NON ACCETTAZIONE PER SUPERAMENTO LIMITE NUM RCPT
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
            $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( $originalMsg->msgid_orig() ){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, '<'.$originalMsg->msgid_orig().'>');
        }elsif($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        }
        push @arrTo, $orig_sender;
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject);
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n",
            map( $_->mailAddress,@{$originalMsg->recips}));
        $myFields{$KEY_TEMPLATE_ERR} = $errDesc;
        $body = $TEMPLATE_RECEIPT_NOTACCEPT;
        $body_html = $TEMPLATE_RECEIPT_NOTACCEPT_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

                # xml
        $xml = getCertInfo_notaccept(   $receiptMsg->rx_time,
                                                                        $originalMsg,
                                                                        undef,
                                                                        $errDesc);
    }elsif($recType == 14){  # SPC - NON ACCETTAZIONE PER ERRORE CON DL
        # TS Header = TS Body = TS XML =
        # timestamp di creazione dell'oggetto che rappresenta il messaggio
        $receiptMsgMimeHead->add ('Date', actualDateTime($receiptMsg->rx_time));

        # mittente posta originale
        $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_RECEIPT,
            $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT);
        $receiptMsgMimeHead->add ('To', $orig_sender);
        if( $originalMsg->msgid_orig() ){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, '<'.$originalMsg->msgid_orig().'>');
        }elsif($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)){
            $receiptMsgMimeHead->add ($OPEC_HEADER_KEY_MSGID_RIF, $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        }
        push @arrTo, $orig_sender;
        $receiptMsgMimeHead->add (  'Subject',
                                    ($orig_subject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT.$orig_subject);
        # body: destinatari
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} = join ("\n",
            map( $_->mailAddress,@{$originalMsg->recips}));
        $myFields{$KEY_TEMPLATE_ERR} = $errDesc;
        $body = $TEMPLATE_RECEIPT_NOTACCEPT_DL_ERR;
        $body_html = $TEMPLATE_RECEIPT_NOTACCEPT_DL_ERR_HTML;
        $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
        $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);

                # xml
        $xml = getCertInfo_notaccept(   $receiptMsg->rx_time,
                                                                        $originalMsg,
                                                                        undef,
                                                                        $errDesc);
    }

    #--- genero l'envelop
    $receiptMsg->sender($adminPecEmail);
        $receiptMsg->recips(\@arrTo);

        #--- genero il BODY
#       $myFields{$KEY_TEMPLATE_DATA}    = actualDate ($receiptMsg->rx_time);
#       $myFields{$KEY_TEMPLATE_ORA}     = actualTime ($receiptMsg->rx_time);
#       $myFields{$KEY_TEMPLATE_ZONA}    = timezone($receiptMsg->rx_time);
    $myFields{$KEY_TEMPLATE_SUBJECT} = encode("iso-8859-1",$orig_subject);
    $myFields{$KEY_TEMPLATE_MAILFROM}= $orig_sender;
    if($originalMsg->msgid_new()){
        $myFields{$KEY_TEMPLATE_MSGID}   = $originalMsg->msgid_new()
    }else{
        $myFields{$KEY_TEMPLATE_MSGID}   = $originalMsg->msgid_orig()}

    # fondo i template con i dati
    $body =~ s/\[(\w+)\]/$myFields{lc($1)}/gm;
    $body_html =~ s/\[(\w+)\]/$myFields{lc($1)}/gm if $body_html;

        #$receiptMsg->mime_entity->bodyhandle ( MIME::Body::Scalar->new($body) );
        $receiptMsgMimeHead->replace('X-Mailer', undef);

        if($body_html){
            $receiptMsg->mime_entity->build( %{$receiptMsgMimeHead->header_hashref()},
                                             Type => 'multipart/mixed');

            my $top_alternative = $receiptMsg->mime_entity->attach(Type => 'multipart/alternative');
            $top_alternative->preamble([]);

            $top_alternative->attach(Type     => 'text/plain',
                                     Encoding => 'quoted-printable',
                                     Data     => $body,
                                     Charset  => 'iso-8859-1');

            $top_alternative->attach(Type     => 'text/html',
                                     Encoding => 'quoted-printable',
                                     Data     => $body_html,
                                     Charset  => 'iso-8859-1');
        }else{
            $receiptMsg->mime_entity->build( %{$receiptMsgMimeHead->header_hashref()},
                                             Type     => 'text/plain',
                                             Encoding => 'quoted-printable',
                                             Data     => $body,
                                             Charset  => 'iso-8859-1');
        }
        $receiptMsg->mime_entity->preamble([]);

        # inserisco l'attachment XML nel MIME::Entity
        # il metodo attach cambia il msg in multipart
        utf8::encode($xml);
        $receiptMsg->xml_local($xml);
        $receiptMsg->mime_entity->attach( Data => $xml,
                          Type => 'application/xml',
                          Encoding => 'base64',
                              Filename => 'daticert.xml');

    # in caso di ricevuta di consegna a destinatario
    # primario (o ambiguo) aggiungo il messaggio originario
        $receiptMsg->mime_entity->add_part($originalMsg->mime_entity)
        if( ($recType == 2 or $recType == 9) and $attachOrigMsg);

    $receiptMsg
}


#
#
#
sub _keep_part {
    my $objEntity   = shift;
    my $msgId       = shift;
    my $objEntityHead = $objEntity->head;
    my $out = 0;

    #--- analizzo l'header
    # 1. verifico che Content-Type deve essere message/rfc822
    # 2. se Content-Disposition esiste verifico che contenga filename = postacert.eml
    # 3. altrimenti verifico che Content-Type contenga name = postacert.eml
    my $attrContentType = $objEntityHead->get('Content-Type');
    my $attrContentDisposition = $objEntityHead->get('Content-Disposition');

    if($attrContentType){
        my ($ctFirst, $ctSecond) = split /;/, $attrContentType;
        return 0 if lc($ctFirst) ne 'message/rfc822';

        if($attrContentDisposition){
            my $cdSecond = (split /;/, $attrContentDisposition)[1];
            if($cdSecond){
                $cdSecond =~ /\s*(\S+\b)\s*=\s*\"?(\S+\b)\"?\s*/s;
                $out=1 if(lc($1) eq 'filename' && $2 eq 'postacert.eml')}}
        if(!$out && $ctSecond){
            $ctSecond=~ /\s*(\S+\b)\s*=\s*\"?(\S+\b)\"?\s*/s;
            $out=1 if(lc($1) eq 'name' && $2 eq 'postacert.eml')}
    }

    $out
}

#
# Dato un'obj MIME::Entity restituisce ancora un MIME::Entity che rispetta
# le specifiche richieste per la ricevuta di avvenuta consegna breve; ossia:
# * se il messagio e' di tipo MIME
#   . tutti gli allegati vengono sostituiti con il relativo hash
# * se il messaggio e' di tipo S/MIME
#   . multipart/signed: inalterati le componenti di firma (il certificato allegato)
#     mentre sostituisco i file del messaggio originale (incapsulato nella struttura S/MIME)
#     con i corrispondenti hash.
#   . application/x-pkcs7-mime o application/pkcs7-mime: lascio la mail inalterata
# * NOTA:
#   allegati di tipo message/rfc822 vengono completamente sostituiti con il relativo hash
#
# IN: 1. MIME::Entity del messaggio originale
#     2. [path temporaneo utilizzato dal parser del mime tool]
#
# OUT: MIME::Entity formattato secondo le richieste della ric. di cons. breve
#      undef in caso di errori
#
sub _formatHash {
    my $origEntity = shift;
    my $tempDir    = shift;

    my @partsOrig;
    my $tmp_entity;
    if( defined $origEntity->bodyhandle ){
        my $parser     = new MIME::Parser;
        $parser->extract_nested_messages(0);
        if($tempDir){
            $parser->output_under($tempDir);
            $parser->output_to_core(0);
            $parser->tmp_to_core(0);
        }else{
            $parser->output_to_core(1);
            $parser->tmp_to_core(1);
            $parser->use_inner_files(1)
        }

        eval{
            $tmp_entity = $parser->parse_open( $origEntity->bodyhandle->path )};
        return if($@ or !$tmp_entity);
        @partsOrig = $tmp_entity->parts;
    }else{
        @partsOrig = $origEntity->parts;
        $tmp_entity = $partsOrig[0]->dup;
        @partsOrig = $partsOrig[0]->parts;
    }

    # ritorno l'entity fornita in ingresso se quest'ultima non e' multipart,
    # non ha quindi allegati
    return $origEntity if $#partsOrig < 1;

    # qui il msg originale e' scomposto in parti
    # decido se S/MIME o MIME
    my @id_attach;
    my @id_noattach;
    if( $tmp_entity->effective_type =~ /multipart\/signed/si){
        # S/MIME application/pkcs7-signature o x-application/pkcs7-signature

        # l'entity e' formata da 2 sotto entity:
        # 1. contiene i dati di firma da aggiungere senza modificare
        # 2. contiene i dati da cui ricavare l'hash
        my $id_signed;
        my $id_nosigned;
        my $attrContentType_second = (split /;/, $partsOrig[0]->get('Content-Type'))[1];
        if( $attrContentType_second =~ /^\s*application\/pkcs7\-signature|application\/x\-pkcs7\-signature\s*/mi){
            $id_signed = 0;
            $id_nosigned = 1;
        }else{
            $id_signed = 1;
            $id_nosigned = 0;
        }
        my @parts_nosigned = $partsOrig[$id_nosigned]->parts;

        # scorro le entity del msg orig per stabilire quali sono gli attachment da cui l'hash
        # ed il body
        my $bool_attach2hash;
        my $boolbody = 1;
        for(my $i=0; $i<@parts_nosigned; $i++){
            undef $bool_attach2hash;

            my ($attrContentType_first, $attrContentType_second) =
                (split /;/, $parts_nosigned[$i]->get('Content-Type'));
            my $attrContentDisposition_second =
                (split /;/, $parts_nosigned[$i]->get('Content-Disposition'))[1];

            $bool_attach2hash = 1 if(  $attrContentType_second =~ /^(\s|$eol|$eolcp)*name\s*=.+/im );
            $bool_attach2hash = 1 if(  !$bool_attach2hash and
                                       $attrContentDisposition_second =~ /^(\s|$eol|$eolcp)*filename\s*=.+/im );
            $bool_attach2hash = 1 if(  !$bool_attach2hash and
                                       $attrContentType_first =~ /^\s*message\/rfc822\s*/mi );
            # se esiste una entity criptata application/pkcs7-mime o application/x-pkcs7-mime
            # restituisco l'entity in ingresso
            return $origEntity    if(  !$bool_attach2hash and
                                       $attrContentType_second =~ /^\s*application\/pkcs7\-mime|application\/x\-pkcs7\-mime\s*/mi);

            if($bool_attach2hash){
                # allegato to hash
                push @id_attach, $i
            }elsif($boolbody){
                # body - mantengo inalterato
                undef $boolbody;
                push @id_noattach, $i
            }
        }

        # se non ci sono attach. da sostituire
        # ritorno l'entity orig
        return $origEntity unless(@id_attach);

        # cancello tutte le entity dal mime principale che non mi interessano
        $tmp_entity->parts($id_nosigned)->parts( [@parts_nosigned[@id_noattach]] );

        # creo e aggancio gli attach con i corrispondenti hash all'entity della mail originale
        foreach (@id_attach){
            my $attach_path_file;
            if($parts_nosigned[$_]->bodyhandle) {
               $attach_path_file = $parts_nosigned[$_]->bodyhandle->path;
            }elsif($parts_nosigned[$_]->head->recommended_filename){
               $attach_path_file = $tempDir.'/'.$parts_nosigned[$_]->head->recommended_filename;
            }
            next unless $attach_path_file;
            my $strHash = Opec::Sign::getHash($attach_path_file);
            die ("Errore durante la generazione del hash\n") unless ($strHash);
            open(HASH, ">", $attach_path_file.'.hash') or die $!; print HASH $strHash; close(HASH);

            $tmp_entity->parts($id_nosigned)->add_part(MIME::Entity->build(Path => $attach_path_file.'.hash',
                                                                           Type => 'text/plain',
                                                                           Encoding => '7bit',
                                                                           Filename => basename($attach_path_file).'.hash',
                                                                           'X-Mailer' => undef));
        }

        $origEntity->parts([]) if(!defined $origEntity->bodyhandle);    # sbianco il multipart $origEntity

    }else{
        # scorro le entity del msg orig per stabilire quali sono gli attachment da cui l'hash
        # ed il body
        my $bool_attach2hash;
        my $boolbody = 1;
        for(my $i=0; $i<@partsOrig; $i++){
            undef $bool_attach2hash;

            my ($attrContentType_first, $attrContentType_second) =
                (split /;/, $partsOrig[$i]->get('Content-Type'));
            my $attrContentDisposition_second =
                (split /;/, $partsOrig[$i]->get('Content-Disposition'))[1];

            $bool_attach2hash = 1 if(  $attrContentType_second =~ /^(\s|$eol|$eolcp)*name\s*=.+/im );
            $bool_attach2hash = 1 if(  !$bool_attach2hash and
                                       $attrContentDisposition_second =~ /^(\s|$eol|$eolcp)*filename\s*=.+/im );
            $bool_attach2hash = 1 if(  !$bool_attach2hash and
                                       $attrContentType_first =~ /^\s*message\/rfc822\s*/mi );
            # se esiste una entity criptata application/pkcs7-mime o application/x-pkcs7-mime
            # restituisco l'entity in ingresso
            return $origEntity    if(  !$bool_attach2hash and
                                       $attrContentType_second =~ /^\s*application\/pkcs7\-mime|application\/x\-pkcs7\-mime\s*/mi);

            if($bool_attach2hash){
                # allegato to hash
                push @id_attach, $i
            }elsif($boolbody){
                # body - mantengo inalterato
                undef $boolbody;
                push @id_noattach, $i
            }
        }

        # cancello tutte le entity dal mime principale che non mi interessano
        $tmp_entity->parts( [@partsOrig[@id_noattach]] );

        # creo e aggancio gli attach con i corrispondenti hash all'entity della mail originale
        foreach (@id_attach){
                my $attach_path_file;
                if($partsOrig[$_]->bodyhandle){
                    $attach_path_file = $partsOrig[$_]->bodyhandle->path;
                }elsif($partsOrig[$_]->head->recommended_filename){
                    $attach_path_file = $tempDir.'/'.$partsOrig[$_]->head->recommended_filename;
                }
                next unless $attach_path_file;
            my $strHash = Opec::Sign::getHash($attach_path_file);
            die ("Errore durante la generazione del hash\n") unless ($strHash);
            open(HASH, ">", $attach_path_file.'.hash') or die $!; print HASH $strHash; close(HASH);

            $tmp_entity->add_part(MIME::Entity->build(Path => $attach_path_file.'.hash',
                                                      Type => 'text/plain',
                                                      Encoding => '7bit',
                                                      Filename => basename($attach_path_file).'.hash',
                                                      'X-Mailer' => undef));
        }

        $origEntity->parts([]) if(!defined $origEntity->bodyhandle);    # sbianco il multipart $origEntity

    }

    # sostituisco all'entity in ingresso l'entity della mail originale opportunamente modificata
    $origEntity->bodyhandle( MIME::Body::Scalar->new($tmp_entity->stringify) );

    return $origEntity
}



1;

__END__

=head1 NAME

Opec::Receipt - Generatore di ricevute

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
