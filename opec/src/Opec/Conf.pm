#
# $Id: Conf.pm,v 1.62 2012/08/23 13:54:24 ldivizio Exp $
#
# Project       OpenPec
# file name:    Conf.pm
# package:      Opec::Conf
#
# DESCRIPTION
# Gestione della configurazione
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#        23/04/2004
#       - Aggiunti controlli a $MYHOME
#   21/10/2004 - Fanton Flavio - segnalazione e soluzione Zuin
#       - Aggiornamento timezone
#   20/3/2006 - Umberto Ferrara
#       - Aggiunte costanti per ricevute nuove regole tecniche
#   21/3/2006 - Fanto Flavio
#       - Aggiunte costanti per smart_tsp.pl
#   26/4/2006 - Fanto Flavio
#       - Aggiunte costanti per scheduler.pl
#   17/05/2006 - Fanto Flavio
#       - Aggiunte costanti per rotate
#   18/05/2006 - Fanto Flavio
#       - Aggiunti controlli sulla dimensione minima del file di log per la rotazione:
#               se il valore esiste non deve essere minore di 1000000B (1MB)
#               se non esiste non viene fatta la rotazione in base al criterio della dimensione
#   23/05/2006 - Fanto Flavio
#       - Rivisti parametri di configurazione con l'obbiettivo di lasciare all'amministratore
#               meno parametri possibile da impostare
#   04/07/2006 - Fanto Flavio
#       - Aggiornati i controlli
#   21/09/2006 - Fanton Flavio
#       - rivista gestione mail di servizio del MTA
#   06/11/2006 - Fanton Flavio
#       - aggiunte le variabili $add_certs, $CApath e validazione relativa
#   04/02/2007 - Fanton Flavio
#       - gestione di $OPEC_DEFAULT_EMAIL_SENDER_MTA come rif. array
#   13/06/2007 - Fanton Flavio
#       - aggiunti costanti di tag xml
#   27/12/2007 - Fanton Flavio
#       - tolti i controlli sui parametri SSL, tutti demandati a Opec::Sign
#   22/01/2008 - Fanton Flavio
#       - aggiunto parametro $PAmode per la modalita' di funzionamento
#               'pubblica amministrazione'
#   14/03/2008 - Fanton Flavio
#       - modificato valore di inizializz di myDomain per consentire a opec
#               di assegnargli automatic il dominio di riferimento del gestore o
#               di impostarlo da file di conf
#   12/05/2009 - Flavio Fanton
#       - aggiunta lista di campi da copiare dal msg-originale al DT
#   09/06/2009 - Flavio Fanton
#       - aggiunto parametro $CACHE_ldap per attivare la cache LDAP
#       30/09/2009 - Fanton Flavio
#       - aggiunto parametro di config providerUnit
#       30/09/2009 - Fanton Flavio
#       - XXX aggiunta cache ldap
#       16/07/2010 - Fanton Flavio
#               - aggiunto il parametro $OnErrorExecute
#       27/09/2010 - Fanton Flavio (thx a EDS-HP)
#               - modificato header From del DT
#                       da 'Per conto di '
#                       a  'Per conto di: '
#       26/10/2010 - Fanton Flavio
#               - aggiunte le info per la risposta verso MTA configurabile
#       05/11/2010 - Fanton Flavio
#               - introduzione delle mappe
#       03/01/2011 - Fanton Flavio
#               - aggiunto il parametro $preserve_tmp_email per per mantantenere la struttura
#         di lavoro temporanea dell'email in solo in caso d'errore
#       26/01/2011 - Fanton Flavio
#               - aggiunto controlli con SPC abilitato
#       28/03/2011 - Fanton Flavio
#               - aggiunta la costante $KEY_TEMPLATE_RCPTTO_SHORT che si riferisce ai
#         destinatari senza il dettaglio certificato/non certificato
#       30/05/2011 - Fanton Flavio
#               - aggiunto il display name al sender ($adminPecEmail_RDS)
#       30/05/2011 - Fanton Flavio
#               - aggiunto template html
#       10/06/2011 - Fanton Flavio
#               - aggiunta $CONST_REM_TIMEOUT: scadenza task REMainder rimasti orfani
#       04/07/2011 - Fanton Flavio
#               - aggiunta $OPEC_XML_RMC_ERRSTR: errore-esteso nel caso di RMC
#       22/08/2011 - Fanton Flavio
#               - sostituito $OPEC_XML_RMC_ERRSTR con $OPEC_XML_RMC_ERRSTR12 e
#         $OPEC_XML_RMC_ERRSTR24 per fornire una descrizione dell'errore esteso
#         piu' precisa
#       13/10/2011 - Fanton Flavio
#         -  aggiunto $OPEC_TS_HEADERXMLBODY_ISSAME
#       07/11/2011 - Fanton Flavio
#         -  aggiunto $SYSTEM_DO_LOG_TASK abilitare la stampa nel log di sistema delle operazioni
#        sui task
#       26/04/2012 - Fanton Flavio
#         -  tolto $OPEC_TS_HEADERXMLBODY_ISSAME
#       27/04/2012 - Fanton Flavio
#         -  aggiunto il parametro $checkHeaderXml per attivare la verifica
#        tra il tipo messaggio dichiarato nell'header e quello nel xml
#
#*  Copyright (C) 2011  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org  - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

package Opec::Conf;


use strict;
use DB_File;
use FileHandle;
use Opec::Lock qw( &lock &locksh &unlock );

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $VERSION);
    $VERSION = '%%VERSION%%';
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = ();
    %EXPORT_TAGS = (
        'confvars' => [qw(
            $idServer $providerName $providerUnit $adminPecEmail %hLocalCertDomains $localCertDomains $DBM_localdomains
            $delivery_NO_cert
            $delivery_NO_certLight
            $esmtpMaxMsgSize
            $smtpMaxMsgRcpt
            $adminName $myDomain
        $MYHOME $TEMPBASE $TEMPSHARED
        $PIDPATH $LOCKPATH $PATH_CERTS $PATH_CACHE
        $daemonize $daemonUser $daemonGroup
        $maxServers $maxRequests $childTimeout
        $inetSocketPort $inetSocketBind
        $pidFile $lockFile
        $forward_canonical $forward_pec %receiveTCP
        $localpart_is_case_sensitive
        $localhost_name
        $ldap_host $ldap_port $ldap_user $ldap_psw $ldap_basedn $ldap_localfilter
        $ldap2_host $ldap2_port $ldap2_user $ldap2_psw
        $daemonUserUid $daemonUserGid
        $CRL_filename
        $PAmode
        $CACHE_ldap
        $OnErrorExecute
        $preserve_tmp_email
        $adminPecEmail_RDS
        )],
        'log' => [qw(
        $LOGPATH $LOGPATH_STORY $LOGPATH_STAMP_TMP
        $SYSTEM_DO_LOG $SYSTEM_SYSLOG_LEVEL $SYSTEM_LOGFILE $SYSTEM_LOGFILE_LEVEL $SYSTEM_DO_LOG_TASK
        $LEGAL_DO_LOG $LEGAL_SYSLOG_LEVEL $LEGAL_LOGFILE
        $SYSTEM_MINTIME $SYSTEM_MAXSIZE $LEGAL_MINTIME $LEGAL_MAXSIZE
        $LOGPATH_STAMP_DIRNAME $LOGPATH_STAMP_PATHNAME
        $XML_DO_LOG $XML_SYSLOG_LEVEL $XML_LOGFILE $XML_MINTIME $XML_MAXSIZE $XML_TRIMWS $XML_PREFIXOFF $XML_ADD_MSGIDHEADER
        $ATTACH_DO_LOG $ATTACH_CHECKSUM
        )],
        'ssl' => [qw(
            $openSSLPath
        $engine
        $lib_engine
        $filePrivateKey
        $certFile
        $openSSLDigest
        $CApath
        $add_certs
        $noverify
        $checkHeaderXml
        )],
        'platform' => [qw(
        $eol $eolcp $myversion $myhostname $VERSION
        )],
        'const'    => [qw(
        $OPEC_DEFAULT_EMAIL_SENDER_MTA
        $OPEC_HEADER_KEY_MSGID
        $OPEC_HEADER_KEY_MSGID_RIF

        $OPEC_HEADER_KEY_RECEIPT
        $OPEC_MSG_ORIG_IN
        $OPEC_HEADER_VALUE_RECEIPT_ACCEPT
        $OPEC_HEADER_VALUE_RECEIPT_GETLOAD
        $OPEC_HEADER_VALUE_RECEIPT_DELIVERY
        $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY
        $OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT
        $OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND
        $OPEC_HEADER_VALUE_RECEIPT_RMC

        $OPEC_HEADER_KEY_DOCTRASP
        $OPEC_HEADER_VALUE_DOCTRANSP_ANOM
        $OPEC_HEADER_VALUE_DOCTRANSP_TRANSP

        $OPEC_HEADER_SUBJECT_DOCTRASP
        $OPEC_HEADER_SUBJECT_ANOM
        $OPEC_HEADER_SUBJECT_RECEIPT_ACCEPT
        $OPEC_HEADER_SUBJECT_RECEIPT_GETLOAD
        $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY
        $OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY
        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT
        $OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT_VIRUS
        $OPEC_HEADER_SUBJECT_RECEIPT_VIRUSFOUND
        $OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY_VIRUS
        $OPEC_HEADER_SUBJECT_RECEIPT_RMC

        $OPEC_HEADER_KEY_SICUREZZA
        $OPEC_HEADER_VALUE_SICUREZZA_ERR

        $OPEC_HEADER_XSENDER

        $OPEC_HEADER_KEY_RECEIPT_TYPE
        $OPEC_HEADER_VALUE_RECEIPT_TYPE_COMPLETE
        $OPEC_HEADER_VALUE_RECEIPT_TYPE_SYNTHETIC
        $OPEC_HEADER_VALUE_RECEIPT_TYPE_SHORT

        $OPEC_HEADER_XREF_MSGID

        $OPEC_LDAP_SEARCH_DOMAIN
        $OPEC_LDAP_SEARCH_PROVIDERNAME
        $OPEC_LDAP_SEARCH_PROVIDERUNIT
        $OPEC_LDAP_SEARCH_MAILRECEIPT
        $OPEC_LDAP_SEARCH_CERTIFICATE
        $OPEC_LDAP_SEARCH_CERTIFICATEHASH

        $OPEC_BIS_DIRNAME
        $OPEC_BIS_PATHNAME

        $OPEC_QUEUE_DIRNAME
        $OPEC_QUEUE_PATHNAME

        $OPEC_POSTACERT_FILE
        $OPEC_SMIME_FILE

        $OPEC_XML_FILE
        $OPEC_XML_TAG_IDENTIFICATIVO
        $OPEC_XML_TAG_RCPT
        $OPEC_XML_TAG_ERRESTESO
        $OPEC_XML_TAG_INTESTAZIONE
        $OPEC_XML_TAG_RICEZIONE
        $OPEC_XML_TAG_CONSEGNA
        $OPEC_XML_TAG_GESTORE
        $OPEC_XML_TAG_DATI
        $OPEC_XML_TAG_RISPOSTE
        $OPEC_XML_TAG_DIMENSIONE
        $OPEC_XML_RMC_ERRSTR12
        $OPEC_XML_RMC_ERRSTR24

        %OPEC_XML_ADD_MAILHEADER

        $OPEC_PREFROM_DOCTRANSP

        @OPEC_MAILHEADER_WHITELIST

                $OPEC_MTARESPONSE_TAGS_ID
                $OPEC_MTARESPONSE_TAGS_DT
                $OPEC_MTARESPONSE_TAGS_AM
                $OPEC_MTARESPONSE_TAGS_RDAC
                $OPEC_MTARESPONSE_TAGS_RPC
                $OPEC_MTARESPONSE_TAGS_RDNAC
                $OPEC_MTARESPONSE_TAGS_RRV
                $OPEC_MTARESPONSE_TAGS_RMCV
                $OPEC_MTARESPONSE_TAGS_RDC
                $OPEC_MTARESPONSE_TAGS_RDEC
                $OPEC_MTARESPONSE_TAGS_RMC

                $OPEC_MTARESPONSE_Q_NAME
                $OPEC_MTARESPONSE_Q_DIRECTION
                $OPEC_MTARESPONSE_Q_SIZE
                $OPEC_MTARESPONSE_Q_TSSTART
                $OPEC_MTARESPONSE_Q_TSEND

                %OPEC_MTARESPONSE_TAGS

                $OPEC_DIRECTION_OUTBOUND
                $OPEC_DIRECTION_INBOUND
                $OPEC_DIRECTION_MIXED
                $OPEC_DIRECTION_ND

                $SPC_ENABLED

                @OPEC_HEADERTOFLOAT
        )],
        'template'  => [qw(
                $TEMPLATE_MTARESPONSE

        $TEMPLATE_RECEIPT_ACCEPT
        $TEMPLATE_RECEIPT_ACCEPT_HTML
        $TEMPLATE_RECEIPT_GETLOAD
        $TEMPLATE_RECEIPT_GETLOAD_HTML
        $TEMPLATE_RECEIPT_DELIVERY
        $TEMPLATE_RECEIPT_DELIVERY_HTML
        $TEMPLATE_RECEIPT_ERR_DELIVERY
        $TEMPLATE_RECEIPT_ERR_DELIVERY_HTML
        $TEMPLATE_RECEIPT_NOTACCEPT
        $TEMPLATE_RECEIPT_NOTACCEPT_HTML
        $TEMPLATE_RECEIPT_NOTACCEPT_VIRUS
        $TEMPLATE_RECEIPT_NOTACCEPT_VIRUS_HTML
        $TEMPLATE_RECEIPT_VIRUSFOUND
        $TEMPLATE_RECEIPT_VIRUSFOUND_HTML
        $TEMPLATE_RECEIPT_ERRDELIVERY_VIRUS
        $TEMPLATE_RECEIPT_ERRDELIVERY_VIRUS_HTML
        $TEMPLATE_SHORT_RECEIPT_DELIVERY
        $TEMPLATE_SHORT_RECEIPT_DELIVERY_HTML
        $TEMPLATE_SYNTHETIC_RECEIPT_DELIVERY
        $TEMPLATE_SYNTHETIC_RECEIPT_DELIVERY_HTML
        $TEMPLATE_RECEIPT_RMC12
        $TEMPLATE_RECEIPT_RMC12_HTML
        $TEMPLATE_RECEIPT_RMC24
        $TEMPLATE_RECEIPT_RMC24_HTML
        $TEMPLATE_RECEIPT_NOTACCEPT_DL_ERR
        $TEMPLATE_RECEIPT_NOTACCEPT_DL_ERR_HTML

        $TEMPLATE_DOCTRASP
        $TEMPLATE_DOCTRASP_HTML
        $TEMPLATE_DOCTRANSP_ANOM
        $TEMPLATE_DOCTRANSP_ANOM_HTML

        $KEY_TEMPLATE_DATA
        $KEY_TEMPLATE_ORA
        $KEY_TEMPLATE_ZONA
        $KEY_TEMPLATE_SUBJECT
        $KEY_TEMPLATE_MAILFROM
        $KEY_TEMPLATE_RCPTTO
        $KEY_TEMPLATE_RCPTTO_SHORT
        $KEY_TEMPLATE_MSGID
        $KEY_TEMPLATE_ERR
        $KEY_TEMPLATE_VIRUS

        $OPEC_CERTINFO_TYPE_ACCEPT
        $OPEC_CERTINFO_TYPE_GETLOAD
        $OPEC_CERTINFO_TYPE_DELIVERY
        $OPEC_CERTINFO_TYPE_TRASP
        $OPEC_CERTINFO_TYPE_ERR_DELIVERY
        $OPEC_CERTINFO_TYPE_NOTACCEPT
        $OPEC_CERTINFO_TYPE_VIRUS
        $OPEC_CERTINFO_TYPE_RMC

        $OPEC_CERTINFO_ERR_VALUES_NESSUNO
        $OPEC_CERTINFO_ERR_VALUES_NODEST
        $OPEC_CERTINFO_ERR_VALUES_NODOMINIO
        $OPEC_CERTINFO_ERR_VALUES_ALTRO
        $OPEC_CERTINFO_ERR_VALUES_VIRUS
        )],
        'response'              => [qw(
                $SANITY_CHECK_DEFAULT
                $SANITY_CHECK_NO_FROM
                $SANITY_CHECK_NO_TO
                $SANITY_CHECK_NO_RFC822
                $SANITY_CHECK_NO_RPATH
                $SANITY_CHECK_DIS_RPATH_FROM
                $SANITY_CHECK_NO_HEADER_DEST
                $SANITY_CHECK_NO_FPATH
                $SANITY_CHECK_BCC
                $SANITY_CHECK_DIS_FPATH_TOCC
        )],
        'av_vars'  => [qw(
        $av_path_mail_virus
            $av
            $av_params
            $av_disabled
        )],
        'tsa'  => [qw(
            $pidFileSmartTsp
            $tsa_host
            $tsa_service
        $tsa_cert
        $tsa_CAcert
        @OPEC_TSA_TRANSPORTS
        )],
        'scheduler'  => [qw(
        $OPEC_QUEUE_DIR_TMP
        $OPEC_QUEUE_DIR_OPT
        $OPEC_QUEUE_DIR_WORK

        $CONST_CMD_RMC12
        $CONST_CMD_RMC24
        $CONST_CMD_SPINLOG_SYSTEM
        $CONST_CMD_SPINLOG_LEGAL
        $CONST_CMD_SPINLOG_XML

            $pidFileScheduler
        $scheduler_sck

        $CONST_REM_TIMEOUT
        )],
        'maps'          => [qw(
                %MAPS_DEFINITION
                %MAPS
                %MAPS_LIST

                $SPC_ALIAS_USER_MAP
                $SPC_ALIAS_LIST_MAP
                $SPC_USER_ENABLED

                $forward_pec
                $forward_canonical
                $esmtpMaxMsgSize
                $smtpMaxMsgRcpt
        )],
        'xml'           => [qw(
                $XML_ROOTNODE_NAME
                $XML_SENDER_PATH
                $XML_RCPT_PATH

                $XML_OPENPEC_TAG_PREFIX
        )]
    );
    Exporter::export_tags qw(   confvars
                                                        platform
                                                        const
                                                        log
                                                        template
                                                        response
                                                        ssl
                                                        av_vars
                                                        tsa
                                                        scheduler
                                                        maps
                                                        xml);
} # BEGIN

use POSIX qw(uname);
use Errno qw(ENOENT);

use vars @EXPORT;

#--- IMPOSTO I VALORI DI DEFAULT

# id del server (per il campo Message-ID)
$idServer = 1;

# stabilisce se inoltrare al destinatario locale mail
# che hanno fallito la verifica della firma
# true per deliverale, false altrimenti
$delivery_NO_cert = 1;

# stabilisce se inoltrare al destinatario locale mail
# che non contengono la firma come definita dalle regole tecniche.
# Controllo piu' leggero di $delivery_NO_cert dove si entra nel
# merito della firma
# true per deliverale, false altrimenti
$delivery_NO_certLight = 1;

# Attibuto LDAP providerName del fornitore di PEC
$providerName = 'EXEntrica S.r.l.';
$ldap_basedn  = 'o=postacert';
$localCertDomains = 'hash';

# nome gestore
$adminName = 'EXEntrica S.r.l.';

# SSL
$engine                 = 'openssl';
$openSSLPath    = '/usr/bin/openssl';
$openSSLDigest  = 'sha1';
$noverify = 1;
$checkHeaderXml;

$myversion = 'opec'.$VERSION; $myversion =~ s/\.//g;

$eol    = "\n";
$eolcp  = "\015\012";

# path default
$MYHOME             = '/home/dopec/MYHOME';
$LOGPATH_STAMP_DIRNAME = 'logstamp';

$myDomain   = undef;   # va a comporre il message-id secondo specifiche
                       # tipicamente il dominio del gestore del servizio

# 2 cose:
# 1. indica a Net::Server se avviarsi in modalita' daemon (1) oppure no (0 - undef)
# 2. se loggare cmq su STDERR oppure seguire le indicazioni del file di conf
$daemonize = 1;

# Net::Server settings
$maxServers   = 2;      # number of pre-forked children
$maxRequests  = 10;     # retire a child after that many accepts
$childTimeout = 8*60;   # abort child if it does not complete each task in n sec

# log di sistema
$SYSTEM_DO_LOG          = 1;    # default value: syslog
$SYSTEM_SYSLOG_LEVEL    = "mail.info";
$SYSTEM_LOGFILE         = "opec.log";
$SYSTEM_LOGFILE_LEVEL   = 5;    # (defaults to 5)
$SYSTEM_MINTIME         = undef;
$SYSTEM_MAXSIZE         = undef;
$SYSTEM_DO_LOG_TASK     = 0;

# log legale
$LEGAL_DO_LOG          = 1;    # default value: syslog
$LEGAL_SYSLOG_LEVEL    = "mail.info";
$LEGAL_LOGFILE         = "opec.log";
$LEGAL_MINTIME         = undef;
$LEGAL_MAXSIZE         = undef;

# log xml
$XML_DO_LOG            = 1;    # default value: syslog
$XML_SYSLOG_LEVEL      = "mail.info";
$XML_LOGFILE           = "opec_xml.log";
$XML_MINTIME           = undef;
$XML_MAXSIZE           = undef;
$XML_ADD_MSGIDHEADER   = 0;

# log allegati
$ATTACH_DO_LOG         = 0;
$ATTACH_CHECKSUM       = 0;

#
# Receiving mail related
$forward_canonical  = 'SMTP:127.0.0.1:10025';
$forward_pec        = '127.0.0.1:10025';
%receiveTCP         = (10024, 'SMTP');
$inetSocketPort     = keys %receiveTCP;
$inetSocketBind     = '127.0.0.1';

# antivirus
$av = 'Clamav:localhost:3310';
$av_params = [];

# $myhostname is used by SMTP server module in the initial SMTP welcome line,
# in inserted 'Received:' lines, Message-ID in notifications, log entries, ...
$myhostname = (uname)[1];

# $localhost_name is the name of THIS host running opecd
# (typically 'localhost'). It is used in HELO SMTP/LMTP command
# when reinjecting mail back to MTA via SMTP for final delivery.
$localhost_name = 'localhost';

# Affects matching of localpart of e-mail addresses (left of '@')
# in lookups: true = case sensitive, false = case insensitive
$localpart_is_case_sensitive = 0;

# nome file Berkeley DB contenente i domini locali
$DBM_localdomains = 'localdomain.db';
# nome file Berkeley DB contenente la CRL
$CRL_filename = 'crl.db';

# true mantengo il path di lavoro temporaneo + contenuto (solo in caso d'errore),
# false lo elimino comunque
$preserve_tmp_email = 0;

# COSTANTI
$OPEC_HEADER_KEY_MSGID                  = 'Message-ID';
$OPEC_HEADER_KEY_MSGID_RIF              = 'X-Riferimento-Message-ID';

$OPEC_HEADER_KEY_RECEIPT                = 'X-Ricevuta';

$OPEC_MSG_ORIG_IN                       = 'msg-originale';
$OPEC_HEADER_VALUE_RECEIPT_ACCEPT       = 'accettazione';
$OPEC_HEADER_VALUE_RECEIPT_GETLOAD      = 'presa-in-carico';
$OPEC_HEADER_VALUE_RECEIPT_DELIVERY     = 'avvenuta-consegna';
$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY  = 'errore-consegna';
$OPEC_HEADER_VALUE_RECEIPT_NOTACCEPT    = 'non-accettazione';
$OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND   = 'rilevazione-virus';
$OPEC_HEADER_VALUE_RECEIPT_RMC          = 'preavviso-errore-consegna';

$OPEC_HEADER_KEY_DOCTRASP               = 'X-Trasporto';
$OPEC_HEADER_VALUE_DOCTRANSP_ANOM       = 'errore';
$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP     = 'posta-certificata';

$OPEC_HEADER_SUBJECT_DOCTRASP                   = 'POSTA CERTIFICATA: ';
$OPEC_HEADER_SUBJECT_ANOM                       = 'ANOMALIA MESSAGGIO: ';
$OPEC_HEADER_SUBJECT_RECEIPT_ACCEPT             = 'ACCETTAZIONE: ';
$OPEC_HEADER_SUBJECT_RECEIPT_GETLOAD            = 'PRESA IN CARICO: ';
$OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY           = 'CONSEGNA: ';
$OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY        = 'AVVISO DI MANCATA CONSEGNA: ';
$OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT          = 'AVVISO DI NON ACCETTAZIONE: ';
$OPEC_HEADER_SUBJECT_RECEIPT_NOTACCEPT_VIRUS    = 'AVVISO DI NON ACCETTAZIONE PER VIRUS: ';
$OPEC_HEADER_SUBJECT_RECEIPT_VIRUSFOUND                 = 'PROBLEMA DI SICUREZZA: ';
$OPEC_HEADER_SUBJECT_RECEIPT_ERRDELIVERY_VIRUS  = 'AVVISO DI MANCATA CONSEGNA PER VIRUS: ';
$OPEC_HEADER_SUBJECT_RECEIPT_RMC                = 'AVVISO DI MANCATA CONSEGNA PER SUP. TEMPO MASSIMO: ';

$OPEC_HEADER_KEY_SICUREZZA                  = 'X-VerificaSicurezza';
$OPEC_HEADER_VALUE_SICUREZZA_ERR            = 'errore';

$OPEC_HEADER_XSENDER                        = 'X-Mittente';

$OPEC_HEADER_KEY_RECEIPT_TYPE               = 'X-TipoRicevuta';
$OPEC_HEADER_VALUE_RECEIPT_TYPE_COMPLETE    = 'completa';
$OPEC_HEADER_VALUE_RECEIPT_TYPE_SYNTHETIC   = 'sintetica';
$OPEC_HEADER_VALUE_RECEIPT_TYPE_SHORT       = 'breve';

$OPEC_HEADER_XREF_MSGID                     = 'X-Riferimento-Message-ID';

$OPEC_PREFROM_DOCTRANSP                     = 'Per conto di: ';

# costanti per allegati
$OPEC_POSTACERT_FILE                        = 'postacert.eml';
$OPEC_SMIME_FILE                            = 'smime.p7s';

# costanti per i dati di certificazione (XML)
$OPEC_XML_FILE                          = 'daticert.xml';
$OPEC_XML_TAG_IDENTIFICATIVO            = 'identificativo';
$OPEC_XML_TAG_RCPT                      = 'destinatari';
$OPEC_XML_TAG_RICEZIONE                 = 'ricezione';
$OPEC_XML_TAG_CONSEGNA                  = 'consegna';
$OPEC_XML_TAG_ERRESTESO                 = 'errore-esteso';
$OPEC_XML_TAG_INTESTAZIONE              = 'intestazione';
$OPEC_XML_TAG_DATI                      = 'dati';
$OPEC_XML_TAG_GESTORE                   = 'gestore-emittente';
$OPEC_XML_TAG_RISPOSTE                  = 'risposte';
$OPEC_XML_TAG_DIMENSIONE                = 'dimensione';  # Usato nel log xml.
$OPEC_XML_RMC_ERRSTR12                  = 'Messaggio non consegnato entro le 12 ore successive all\'invio';
$OPEC_XML_RMC_ERRSTR24                  = 'Messaggio non consegnato entro le 24 ore successive all\'invio';

$OPEC_CERTINFO_TYPE_ACCEPT              = 'accettazione';
$OPEC_CERTINFO_TYPE_GETLOAD             = 'presa-in-carico';
$OPEC_CERTINFO_TYPE_DELIVERY            = 'avvenuta-consegna';
$OPEC_CERTINFO_TYPE_TRASP               = 'posta-certificata';
$OPEC_CERTINFO_TYPE_ERR_DELIVERY        = 'errore-consegna';
$OPEC_CERTINFO_TYPE_NOTACCEPT           = 'non-accettazione';
$OPEC_CERTINFO_TYPE_VIRUS               = 'rilevazione-virus';
$OPEC_CERTINFO_TYPE_RMC                 = 'preavviso-errore-consegna';

$OPEC_CERTINFO_ERR_VALUES_NESSUNO       = 'nessuno';
$OPEC_CERTINFO_ERR_VALUES_NODEST        = 'no-dest';
$OPEC_CERTINFO_ERR_VALUES_NODOMINIO     = 'no-dominio';
$OPEC_CERTINFO_ERR_VALUES_ALTRO         = 'altro';
$OPEC_CERTINFO_ERR_VALUES_VIRUS         = 'virus';

# costanti template
$KEY_TEMPLATE_DATA              = 'data';
$KEY_TEMPLATE_ORA               = 'ora';
$KEY_TEMPLATE_ZONA              = 'zona';
$KEY_TEMPLATE_SUBJECT           = 'subject';
$KEY_TEMPLATE_MAILFROM          = 'mailfrom';
$KEY_TEMPLATE_RCPTTO            = 'rcptto';
$KEY_TEMPLATE_RCPTTO_SHORT      = 'rcpttoshort';
$KEY_TEMPLATE_MSGID             = 'msgid';
$KEY_TEMPLATE_ERR               = 'errore';
$KEY_TEMPLATE_VIRUS             = 'virus';

# response
$SANITY_CHECK_DEFAULT           = 'header non corretto';
$SANITY_CHECK_NO_FROM           = 'messaggio senza mittente';
$SANITY_CHECK_NO_TO             = 'messaggio senza destinatario primario';
$SANITY_CHECK_NO_RFC822                 = ' non rfc822 compliant';
$SANITY_CHECK_NO_RPATH          = 'envelop senza mittente';
$SANITY_CHECK_DIS_RPATH_FROM    = 'mittente del messaggio non affidabile';
$SANITY_CHECK_NO_HEADER_DEST    = 'header senza destinatari';
$SANITY_CHECK_NO_FPATH          = 'envelop senza destinatari';
$SANITY_CHECK_BCC               = 'presenza di destinatari in copia nascosta';
$SANITY_CHECK_DIS_FPATH_TOCC    = ': destinatario non presente in To o Cc';


# LDAP ATTRIBUTES: chiavi di ricerca
$OPEC_LDAP_SEARCH_DOMAIN                = 'managedDomains';
$OPEC_LDAP_SEARCH_PROVIDERNAME          = 'providerName';
$OPEC_LDAP_SEARCH_PROVIDERUNIT          = 'providerUnit';
$OPEC_LDAP_SEARCH_MAILRECEIPT           = 'mailReceipt';
$OPEC_LDAP_SEARCH_CERTIFICATE           = 'providerCertificate';
$OPEC_LDAP_SEARCH_CERTIFICATEHASH       = 'providerCertificateHash';

$CACHE_ldap = 1;    # 1 abilita la cache ldap, 0/undef disabilita

# BIS
$OPEC_BIS_DIRNAME       = 'BIS';

# SCHEDULER
$OPEC_QUEUE_DIRNAME     = 'QUEUE';
$OPEC_QUEUE_DIR_TMP     = 'tmp';
$OPEC_QUEUE_DIR_OPT     = 'opt';
$OPEC_QUEUE_DIR_WORK    = 'work';

$CONST_CMD_RMC12                = 'RMC12';
$CONST_CMD_RMC24                = 'RMC24';
$CONST_CMD_SPINLOG_LEGAL        = 'SPINLOG_LEGAL';
$CONST_CMD_SPINLOG_SYSTEM       = 'SPINLOG_SYSTEM';
$CONST_CMD_SPINLOG_XML          = 'SPINLOG_XML';

$CONST_REM_TIMEOUT              = 60*60*4;      # 4h, scadenza task REM orfani

# lista header che vengono copiati
# dal messaggio originale al DT
@OPEC_MAILHEADER_WHITELIST = ('X-Priority','X-Forwarded','Disposition-Notification-To');

# definizione nome TAG per la risposta al MTA
$OPEC_MTARESPONSE_TAGS_ID       = 'ID';
$OPEC_MTARESPONSE_TAGS_DT       = 'DT';
$OPEC_MTARESPONSE_TAGS_AM       = 'AM';
$OPEC_MTARESPONSE_TAGS_RDAC     = 'RdAC';
$OPEC_MTARESPONSE_TAGS_RPC      = 'RpC';
$OPEC_MTARESPONSE_TAGS_RDNAC    = 'RdNAC';
$OPEC_MTARESPONSE_TAGS_RRV      = 'RRV';
$OPEC_MTARESPONSE_TAGS_RDC      = 'RdC';
$OPEC_MTARESPONSE_TAGS_RDEC     = 'RdEC';
$OPEC_MTARESPONSE_TAGS_RMC      = 'RMC';

$OPEC_MTARESPONSE_Q_NAME        = 'name';
$OPEC_MTARESPONSE_Q_DIRECTION   = 'direction';
$OPEC_MTARESPONSE_Q_SIZE        = 'size';
$OPEC_MTARESPONSE_Q_TSSTART     = 'ts_start';
$OPEC_MTARESPONSE_Q_TSEND       = 'ts_end';

$OPEC_DIRECTION_OUTBOUND        = 'OUTBOUND';
$OPEC_DIRECTION_INBOUND                 = 'INBOUND';
$OPEC_DIRECTION_MIXED           = 'MIXED';
$OPEC_DIRECTION_ND              = 'ND';

$scheduler_sck          = '/tmp/opec_scheduler.sck';

# mappe implementate
# chiave: nome mappa (utilizzabile da file di conf)
# valore: modulo che la implementa
my %MAPS_DEFINITION = ( MYSQL   => 'Opec::Maps::MAPS_MYSQL',
                        LDAP    => 'Opec::Maps::MAPS_LDAP',
                        STATIC  => 'Opec::Maps::MAPS_STATIC',
                        DEFAULT => 'Opec::Maps::MAPS_DEFAULT');

# XML costants
$XML_ROOTNODE_NAME      = 'postacert';
$XML_SENDER_PATH        = 'intestazione#mittente';
$XML_RCPT_PATH          = 'intestazione#destinatari';
$XML_OPENPEC_TAG_PREFIX = 'openpec-';

#
# Esegue il file di configurazione
# IN: path del file di configurazione
# OUT: -
#
#*** PARAMETRI CONSIGLIATI
# $idServer             - id istanza di OpenPEC (fondamentale per installazioni con + istanze)
# $adminName            - nome gestore
# $maxServers           - numero di processi contemporanei
# $daemonUser           - utente di esecuzione
# $daemonGroup          - gruppo di esecuzione
# $MYHOME               - path home
# $TEMPBASE             - path per il salvataggio temporaneo delle mail in arrivo
# $TEMPSHARED           - path condiviso fra le istanze di OpenPEC
# %receiveTCP           - metodo di ricezione delle mail
# $ldap_host            - host primario ldap
# $ldap_port            - porta host primario ldap
# $ldap_user            - utente per l'accesso ldap primario
# $ldap_psw             - passwor per l'accesso ldap primario
# $ldap_basedn          - basedn per l'accesso ldap primario
# $ldap2_host           - host secondario ldap
# $ldap2_port           - porta host secondario ldap
# $ldap2_user           - utente per l'accesso ldap secondario
# $ldap2_psw            - passwor per l'accesso ldap secondario
# $openSSLPath          - path OpenSSL
# $filePrivateKey       - nome file chiave per la firma delle mail
# $certFile             - nome file certificato da allegare alle mail firmate
# $esmtpMaxMsgSize      - dimensione max mail in ingresso (minimo 30MB per normativa)
# $smtpMaxMsgRcpt       - numero di destinatari massimo del mesg orig accettabile (minimo 50 da normativa)
# $tsa_cert             - nome file certificato della TSA utilizzato da smart_tsp
# $tsa_CAcert           - nome file certificato di root della TSA utilizzato da smart_tsp
# $tsa_host             - host della TSA
# @OPEC_TSA_TRANSPORTS  - metodo di comunicazione con la TSA
# $SYSTEM_MAXSIZE       - limite dimensionale per la rotazione del log di sistema
# $LEGAL_MAXSIZE        - limite dimensionale per la rotazione del log legale
# $SYSTEM_DO_LOG        - modalita' log di sistema
# $SYSTEM_SYSLOG_LEVEL  - livello syslog del log di sistema: utilizzato si e' stato scelto syslog per il log di sistema
# $SYSTEM_LOGFILE       - nome file di log: utilizzato si e' stato scelto il file per il log di sistema
# $SYSTEM_LOGFILE_LEVEL - livello log di sistema: utilizzato si e' stato scelto il file per il log di sistema
# $SYSTEM_MINTIME       - soglia temporale in secondi per la rotazione del log di sistema; undef per disattivarla
# $SYSTEM_MAXSIZE       - soglia dimensionale in Byte per la rotazione del log di sistema; undef per disattivarla
# $SYSTEM_DO_LOG_TASK   - se true abilita la stampa delle operazioni sui task nel log di sistema
# $LEGAL_DO_LOG         - modalita' log legale
# $LEGAL_SYSLOG_LEVEL   - livello syslog del log legale: utilizzato si e' stato scelto syslog per il log legale
# $LEGAL_LOGFILE        - nome file di log: utilizzato si e' stato scelto il file per il log legale
# $LEGAL_MINTIME        - soglia temporale in secondi per la rotazione del log legale; undef per disattivarla
# $LEGAL_MAXSIZE        - soglia dimensionale in Byte per la rotazione del log legale; undef per disattivarla
# $XML_DO_LOG           - livello syslog del log xml utilizzato
# $XML_SYSLOG_LEVEL     - livello syslog del log XML
# $XML_LOGFILE          - nome file di log
# $XML_MINTIME          - soglia temporale in secondi per la rotazione del log xml; undef per disattivarla
# $XML_MAXSIZE          - soglia dimensionale in Byte per la rotazione del log xml; undef per disattivarla
# $ATTACH_DO_LOG        - abilita log degli allegati
# $ATTACH_CHECKSUM      - abilita il checksum (sha1) degli allegati
# $forward_canonical    - metodo di delivery della posta convenzionale
# $forward_pec          - server di delivery LMTP della posta certificata
# %receiveTCP           - metodo di ricezione della posta
# $inetSocketBind       - se impostato limita la ricezione su un particolare ip
# $av                   - definizione modulo di interfaccia antivirus
# $av_params            - eventuale array di parametri da passare all'antivirus
# $av_disabled                  - consente di disabilitare la valutazione dell'esito dell'antivirus, utile per test
# $preserve_tmp_email   - per mantantenere la struttura di lavoro temporanea dell'email - solo in caso d'errore

#
#*** PARAMETRI OPZIONALI
# $LOCKPATH                 - path contenente tutti i file di lock
# $PIDPATH                  - path contenente tutti i file pid
# $LOGPATH                  - path contenente la struttura di log
# $SYSTEM_LOGFILE           - file di log di sistema
# $LEGAL_LOGFILE            - file di log legale
# $LOGPATH_STORY            - contiene i log ruotati pronti per essere marcati
# $LOGPATH_STAMP_TMP        - path temporaneo utilizzato da smart_tsp per marcare i file di log
# $LOGPATH_STAMP_PATHNAME   - file di log marcati temporalmente in attesa di essere salvati definitivamente
# $PATH_CERTS               - contiene tutti i certificati e chiavi private
# $av_path_mail_virus       - mail risultate positive alla verifica dell'antivirus
# $OPEC_BIS_PATHNAME        - path condiviso bookmark
# $OPEC_QUEUE_PATHNAME      - path condiviso per la gestione dei task
# $pidFile                  - pid file dell'istanza di OpenPEC
# $pidFileSmartTsp          - pid file di smart_tsp
# $pidFileScheduler         - pid file scheduler
# $lockFile                 - lock file dell'istanza di OpenPEC
# $OPEC_QUEUE_DIRNAME       - nome dir contenente la struttura dei task
# $OPEC_QUEUE_DIR_TMP       - nome dir temporanea della struttura dei task
# $OPEC_QUEUE_DIR_OPT       - nome dir di attesa  della struttura dei task
# $OPEC_QUEUE_DIR_WORK      - nome dir di lavoro della struttura dei task
# $scheduler_sck            - nome completo file socket per comunicazione con scheduler
# $OPEC_BIS_DIRNAME         - nome dir contenente i bookmark
# $maxRequests              - numero di richieste dopo le quali il processo muore
# $childTimeout             - tempo max in secondi consentito all'elaborazione di una mail
# $LOGPATH_STAMP_DIRNAME    - nome dir file di log marcati
# $adminPecEmail_RDS            - Display name ricevute certificate
# %OPEC_XML_ADD_MAILHEADER      - Per aggiungere valori dell'header nel log xml
# @OPEC_HEADERTOFLOAT           - Elenco degli header del messaggio originale da copiare nel DT/AM

sub read_config ($) {
    my($configFile) = @_;
    my($msg);
    my($errn) = stat($configFile) ? 0 : 0+$!;
    if ($errn == ENOENT) { $msg = "inesistente" }
    elsif ($errn)        { $msg = "inaccessible: $!" }
    elsif (! -f _)       { $msg = "non regolare" }
    elsif (! -r _)       { $msg = "non accessibile in lettura" }
    die "File di configurazione $configFile $msg\n"
        if (defined $msg);

    do $configFile;

    die "Errore nel file di configurazione $configFile: $@\n" if ($@ ne '');

    # check exists for user and group
    die "L'utente $daemonUser non e' definito sul sistema\n"
        if(!($daemonUserUid = getpwnam($daemonUser)));
    die "Il gruppo $daemonGroup non e' definito sul sistema\n"
        if(!($daemonUserGid = getgrnam($daemonGroup)));

    #
    # STRUTTURA LOCALE (rispetto alla home):
    # /tmp          - $TEMPBASE             (path locale mail temporanee)
    # /log          - $LOGPATH              (contiene il file di log online)
    # /log/log_story - $LOGPATH_STORY        (contiene i log ruotati pronti per essere marcati)
    # /log/logtmp   - $LOGPATH_STAMP_TMP    (path temporaneo utilizzato da smart_tsp per marcare i file di log)
    # /certs        - $PATH_CERTS           (contiene tutti i certificati e chiavi private)
    # /cachedb      - $PATH_CACHE           (opzionale - contiene i dbm dei domini locali 1 per ogni processo + il master)
    # /VIRUS_MAIL   - $av_path_mail_virus   (mail risultate positive alla verifica dell'antivirus)
    #
    die "La variabile $MYHOME non esiste nel file di configurazione!\n"
        if (!defined $MYHOME || $MYHOME eq '');
    die "\$MYHOME = '$MYHOME' e' una directory inesistente\n"
        if ( !-d $MYHOME );

    unless($TEMPBASE){$TEMPBASE = "$MYHOME/tmp"}
    if( !-d $TEMPBASE ){
        unless( mkdir($TEMPBASE) &&
                chown($daemonUserUid, $daemonUserGid, $TEMPBASE) ){
            die "\$TEMPBASE = '$TEMPBASE' impossibile creare la directory: $!\n"
        }
    }

    unless($TEMPSHARED){$TEMPSHARED = $TEMPBASE}
    if( !-d $TEMPSHARED ){
        unless( mkdir($TEMPSHARED) &&
                chown($daemonUserUid, $daemonUserGid, $TEMPSHARED) ){
            die "\$TEMPSHARED = '$TEMPSHARED' impossibile creare la directory: $!\n"
        }
    }

    $MYHOME = _check_slash($MYHOME);

    $LOCKPATH = $MYHOME.'lock' unless($LOCKPATH);   # imposto il valore di default se non impostato esternamente
    if ( !-d $LOCKPATH ){
        unless( mkdir($LOCKPATH) &&
                chown($daemonUserUid, $daemonUserGid, $LOCKPATH) ){
            die "\$LOCKPATH = '$LOCKPATH' impossibile creare la directory: $!\n"
        }
    }

    $PIDPATH = $MYHOME.'pid' unless($PIDPATH);
    if ( !-d $PIDPATH ){
        unless( mkdir($PIDPATH) &&
                chown($daemonUserUid, $daemonUserGid, $PIDPATH) ){
            die "\$PIDPATH = '$PIDPATH' impossibile creare la directory: $!\n"
        }
    }

    $LOGPATH = $MYHOME.'log' unless($LOGPATH);
    if ( !-d $LOGPATH ){
        unless( mkdir($LOGPATH) &&
                chown($daemonUserUid, $daemonUserGid, $LOGPATH) ){
            die "\$LOGPATH = '$LOGPATH' impossibile creare la directory: $!\n"
        }
    }

    $LOGPATH = _check_slash($LOGPATH);
    $SYSTEM_LOGFILE = $LOGPATH.$SYSTEM_LOGFILE;
    $LEGAL_LOGFILE  = $LOGPATH.$LEGAL_LOGFILE;
    $XML_LOGFILE    = $LOGPATH.$XML_LOGFILE;
#    die "Non e' possibile impostare il file di log di sistema uguale al file di log legale, " .
#        "verificare il file di configurazione\n" if($SYSTEM_LOGFILE eq $LEGAL_LOGFILE);

    $LOGPATH_STORY = $LOGPATH.'log_story' unless($LOGPATH_STORY);
    $LOGPATH_STORY = _check_slash($LOGPATH_STORY);
    if ( !-d $LOGPATH_STORY ){
        unless( mkdir($LOGPATH_STORY) &&
                chown($daemonUserUid, $daemonUserGid, $LOGPATH_STORY) ){
            die "\$LOGPATH_STORY = '$LOGPATH_STORY' impossibile creare la directory: $!\n"
        }
    }

    $LOGPATH_STAMP_TMP = $LOGPATH.'log_tmp' unless($LOGPATH_STAMP_TMP);
    $LOGPATH_STAMP_TMP = _check_slash($LOGPATH_STAMP_TMP);
    if ( !-d $LOGPATH_STAMP_TMP ){
        unless( mkdir($LOGPATH_STAMP_TMP) &&
                chown($daemonUserUid, $daemonUserGid, $LOGPATH_STAMP_TMP) ){
            die "\$LOGPATH_STAMP_TMP = '$LOGPATH_STAMP_TMP' impossibile creare la directory: $!\n"
        }
    }

    $PATH_CERTS = $MYHOME.'certs' unless($PATH_CERTS);
    $PATH_CERTS = _check_slash($PATH_CERTS);
    if ( !-d $PATH_CERTS ){
        unless( mkdir($PATH_CERTS) &&
                chown($daemonUserUid, $daemonUserGid, $PATH_CERTS) ){
            die "\$PATH_CERTS = '$PATH_CERTS' impossibile creare la directory: $!\n"
        }
    }



    $av_path_mail_virus = $MYHOME.'VIRUS_MAIL' unless($av_path_mail_virus);
    $av_path_mail_virus = _check_slash($av_path_mail_virus);
    if ( !-d $av_path_mail_virus ){
        unless( mkdir($av_path_mail_virus) &&
                chown($daemonUserUid, $daemonUserGid, $av_path_mail_virus) ){
            die "\$av_path_mail_virus = '$av_path_mail_virus' impossibile creare la directory: $!\n"
        }
    }

    #
    # STRUTTURA CONDIVISA
    # /shared           - $TEMPSHARED               (path condiviso fra le istanze di OpenPEC)
    # /shared/logstamp  - $LOGPATH_STAMP_PATHNAME   (file di log marcati temporalmente in attesa di essere salvati definitivamente)
    # /shared/BIS       - $OPEC_BIS_PATHNAME        (path condiviso bookmark)
    # /shared/queue     - $OPEC_QUEUE_PATHNAME      (path condiviso per la gestione dei task)
    #
    die "\$TEMPSHARED = '$TEMPSHARED' e' una directory inesistente\n"
        if ( !-d $TEMPSHARED );
    $TEMPSHARED = _check_slash($TEMPSHARED);

    $OPEC_BIS_PATHNAME = $TEMPSHARED.$OPEC_BIS_DIRNAME unless($OPEC_BIS_PATHNAME);
    if ( !-d $OPEC_BIS_PATHNAME ){
        unless( mkdir($OPEC_BIS_PATHNAME) &&
                chown($daemonUserUid, $daemonUserGid, $OPEC_BIS_PATHNAME) ){
            die "\$OPEC_BIS_PATHNAME = '$OPEC_BIS_PATHNAME' e' una directory inesistente\n"
        }
    }

    $OPEC_QUEUE_PATHNAME = $TEMPSHARED.$OPEC_QUEUE_DIRNAME unless($OPEC_QUEUE_PATHNAME);
    if ( !-d $OPEC_QUEUE_PATHNAME ){
        unless( mkdir($OPEC_QUEUE_PATHNAME) &&
                chown($daemonUserUid, $daemonUserGid, $OPEC_QUEUE_PATHNAME) ){
            die "\$OPEC_QUEUE_PATHNAME = '$OPEC_QUEUE_PATHNAME' e' una directory inesistente\n"
        }
    }

    $OPEC_QUEUE_PATHNAME = $TEMPSHARED.$OPEC_QUEUE_DIRNAME unless($OPEC_QUEUE_PATHNAME);
    my $queue_pathid = _check_slash($OPEC_QUEUE_PATHNAME).$idServer.'/';
    if ( !-d $queue_pathid ){
        unless( mkdir($queue_pathid) &&
                mkdir($queue_pathid.$OPEC_QUEUE_DIR_TMP) &&
                mkdir($queue_pathid.$OPEC_QUEUE_DIR_OPT) &&
                mkdir($queue_pathid.$OPEC_QUEUE_DIR_WORK) &&
                chown($daemonUserUid, $daemonUserGid, $queue_pathid) &&
                chown($daemonUserUid, $daemonUserGid, $queue_pathid.$OPEC_QUEUE_DIR_TMP) &&
                chown($daemonUserUid, $daemonUserGid, $queue_pathid.$OPEC_QUEUE_DIR_OPT) &&
                chown($daemonUserUid, $daemonUserGid, $queue_pathid.$OPEC_QUEUE_DIR_WORK)){
            die "Impossibile creare $queue_pathid o una sua sotto directory\n"
        }
    }

    $LOGPATH_STAMP_PATHNAME = $TEMPSHARED.$LOGPATH_STAMP_DIRNAME unless($LOGPATH_STAMP_PATHNAME);
    $LOGPATH_STAMP_PATHNAME = _check_slash($LOGPATH_STAMP_PATHNAME);
    if ( !-d $LOGPATH_STAMP_PATHNAME ){
        unless( mkdir($LOGPATH_STAMP_PATHNAME) &&
                chown($daemonUserUid, $daemonUserGid, $LOGPATH_STAMP_PATHNAME) ){
            die "\$LOGPATH_STAMP_PATHNAME = '$LOGPATH_STAMP_PATHNAME' e' una directory inesistente\n"
        }
    }

    if($maxServers !~ /\d+/){
        die "Errore nel file di configurazione $configFile: verificare il parametro \$maxServers\n"}
    if($maxRequests !~ /\d+/){
        die "Errore nel file di configurazione $configFile: verificare il parametro \$maxRequests\n"}
    if($childTimeout !~ /\d+/){
        die "Errore nel file di configurazione $configFile: verificare il parametro \$childTimeout\n"}

    if(!$SYSTEM_DO_LOG && $SYSTEM_LOGFILE_LEVEL !~ /[1-5]/){
        die "Errore nel file di configurazione $configFile: verificare il parametro \$SYSTEM_LOGFILE_LEVEL\n"}

    #
    $PIDPATH    = _check_slash($PIDPATH);
    $LOCKPATH   = _check_slash($LOCKPATH);
    $pidFile    = $pidFile ? $PIDPATH.$pidFile : $PIDPATH.'opec.pid';
    $lockFile   = $lockFile ? $LOCKPATH.$lockFile : $LOCKPATH.'opec.lock';

    my @arrTmp = keys %receiveTCP;
    die "Errore nel file di configurazione $configFile: ".
        "verificare il parametro \$receiveTCP\n"
        unless(@arrTmp);

    foreach (@arrTmp) {
        die "Errore nel file di configurazione $configFile: ".
            "verificare il parametro \$receiveTCP\n"
            unless(/\d+/);
        $receiveTCP{$_} = 'Opec::In::'.$receiveTCP{$_};
    }
    $inetSocketPort     = \@arrTmp;

    if($OPEC_DEFAULT_EMAIL_SENDER_MTA && !ref($OPEC_DEFAULT_EMAIL_SENDER_MTA)){
        $OPEC_DEFAULT_EMAIL_SENDER_MTA = [$OPEC_DEFAULT_EMAIL_SENDER_MTA]
    }

    # check parametri di connessione LDAP
    die "Errore nel file di configurazione $configFile: ".
        "host LDAP non impostato.\n"
        if($ldap_host !~ /.+/);
    if(!$ldap_port){
        $ldap_port = 389
    }elsif($ldap_port !~ /[0-9]+/){
        die "Errore nel file di configurazione $configFile: ".
            "porta del host LDAP errata.\n"}
    # check parametri di connessione LDAP2
    if($ldap2_host){
        if(!$ldap2_port){
            $ldap_port = 389
        }elsif($ldap2_port !~ /[0-9]+/){
            die "Errore nel file di configurazione $configFile: ".
                "porta del host2 LDAP errata.\n"}}

    if(defined $providerUnit){
        $ldap_localfilter = "(&($OPEC_LDAP_SEARCH_PROVIDERNAME=$providerName)($OPEC_LDAP_SEARCH_PROVIDERUNIT=$providerUnit))";
    }else{
        $ldap_localfilter = "(&($OPEC_LDAP_SEARCH_PROVIDERNAME=$providerName)(!($OPEC_LDAP_SEARCH_PROVIDERUNIT=*)))";
    }

    #$esmtpMaxMsgSize = 48234496 if(!defined $esmtpMaxMsgSize or $esmtpMaxMsgSize !~ /\d+/);
    $esmtpMaxMsgSize = 49000000 if(!defined $esmtpMaxMsgSize or $esmtpMaxMsgSize !~ /\d+/); # per soddisfare i test di interop
    $smtpMaxMsgRcpt = 50 if(!defined $smtpMaxMsgRcpt or $smtpMaxMsgRcpt !~ /\d+/);

    # check parametri SSL
    die "Errore nel file di configurazione $configFile: ".
        "verificare il parametro \$openSSLPath\n"
        unless(-x $openSSLPath);

    if(!$engine or $engine eq 'openssl' or $engine eq 'ERACOM'){
        $filePrivateKey = $filePrivateKey ? $PATH_CERTS.$filePrivateKey : $PATH_CERTS.'privkey.pem'}
    $certFile = $certFile ? $PATH_CERTS.$certFile : $PATH_CERTS.'cert.cert';

    $PATH_CACHE = $MYHOME.'cachedb' unless($PATH_CACHE);
    if ( !-d $PATH_CACHE ){
        unless( mkdir($PATH_CACHE) &&
                chown($daemonUserUid, $daemonUserGid, $PATH_CACHE) ){
            die "\$PATH_CACHE = '$PATH_CACHE' e' una directory inesistente\n"
        }
    }
    $PATH_CACHE = _check_slash($PATH_CACHE);

    if(defined $CApath){
        die "\$CApath = '$CApath' e' una directory inesistente\n"
            unless(-d $CApath);
        $CApath = _check_slash($CApath);
        $noverify = 0;
    }else{
        $CApath = $PATH_CERTS;
    }

    if($add_certs){
        $add_certs = [$add_certs] unless(ref($add_certs));
        foreach (@$add_certs){
            die "\$add_certs: verificare il parametro\n" unless(-r $_);
        }
    }

        # trasformo l'array dei local in hash per ricerche veloci
        # Tabella dei domini locali
        # Le possibilita' sono:
        # 1. rif. arr.  => contiene direttamente i domini locali
        # 2. hash       => DBM da caricare avente il formato
    #                  <nomedominio> 1
        if(ref($localCertDomains) eq 'ARRAY'){       # 1. rif. arr. => contiene direttamente i domini locali
            %hLocalCertDomains = map { lc($_) => 1 } @$localCertDomains;
        }elsif(ref($localCertDomains) eq 'HASH' && $PAmode){    # 2. configurazione per PA
            %hLocalCertDomains = %$localCertDomains;
    }elsif($localCertDomains =~ /^hash$/i ){     # 3. hash => DBM da caricare avente il formato
            # creo una copia del file dbm per ogni processo
            # - Funzionamento -
            # A regime esisteranno n copie + 1 del file dbm dove n e' il numero dei processi.
            # Ogni processo legge la sua copia mentre una sola viene modificata da
            # un utility esterna che la sincronizza con LDAP
            # quando il singolo processo si accorge che la sua copia e' obsoleta
            # provvede ad aggiornarla ricopiandola; operazione che viene eseguita ad inizio
            # elaborazione mail.
            # Quest'approccio consente:
            # 1. di gestire la concorrenza in modo semplice e sicuro
            # 2. di rendere atomico l'aggiornamento del dbm rispetto all'elaborazione della mail
            # 3. velocita'
        die "Errore nel file di configurazione $configFile: ".
            "verificare il parametro \$DBM_localdomains\n"
            unless($DBM_localdomains);

        my $path_DBM_localdomains = $PATH_CACHE.$DBM_localdomains;
        die "File cache ($DBM_localdomains) inesistente, generarlo o impostare manualmente i domini locali\n"
            unless(-e $path_DBM_localdomains);
    }else{
        die "Errore nel file di configurazione $configFile: ".
            'parametro $localCertDomains non corretto'."\n"
    }

    # queue
    $pidFileScheduler = $pidFileScheduler ? $PIDPATH.$pidFileScheduler : $PIDPATH.'scheduler.pid';

    # controlli sui parametri di log
    # soglia minima: 1MB
    $SYSTEM_MAXSIZE = 1000000 if($SYSTEM_MAXSIZE && $SYSTEM_MAXSIZE =~ /^\d$/ && $SYSTEM_MAXSIZE < 1000000);
    $LEGAL_MAXSIZE  = 1000000 if($SYSTEM_MAXSIZE && $SYSTEM_MAXSIZE =~ /^\d$/ && $SYSTEM_MAXSIZE < 1000000);
    # soglia minima: 1h
    $SYSTEM_MINTIME = 3600 if($SYSTEM_MINTIME && $SYSTEM_MINTIME =~ /^\d$/ && $SYSTEM_MINTIME < 3600);
    $LEGAL_MINTIME  = 3600 if($LEGAL_MINTIME && $LEGAL_MINTIME =~ /^\d$/ && $LEGAL_MINTIME < 3600);

    # check $OnErrorExecute
    if(defined $OnErrorExecute){
        die "Errore nel file di configurazione $configFile: ".
                "impossibile eseguire $OnErrorExecute, verificare il parametro \$OnErrorExecute\n"
                if(!-X $OnErrorExecute);
    }

    # controllo sul formato di $TEMPLATE_MTARESPONSE
    if( defined $TEMPLATE_MTARESPONSE ){
    }

    # indicizzo le mappe
    # %MAPS conterra' la risoluzione tra indice e definizione mappa
    # ed il parametro di conf della mappa viene sostituito con l'indice
    my $idgm;
    if($SPC_ENABLED){
            if(ref($SPC_ALIAS_USER_MAP) eq 'HASH' && exists $SPC_ALIAS_USER_MAP->{'TypeMap'} ){
                die "ERRORE: la mappa \$SPC_ALIAS_USER_MAP ".$SPC_ALIAS_USER_MAP->{'TypeMap'}.
                    " risulta sconusciuta, verificare il file di configurazione\n"
                        if !exists $MAPS_DEFINITION{$SPC_ALIAS_USER_MAP->{'TypeMap'}};

                        $SPC_ALIAS_USER_MAP->{'TypeMap'} = $MAPS_DEFINITION{$SPC_ALIAS_USER_MAP->{'TypeMap'}};
                        $MAPS{++$idgm} = $SPC_ALIAS_USER_MAP;
                        $SPC_ALIAS_USER_MAP  = $idgm;
            }else{
                die "ERRORE: con \$SPC_ENABLED abilitato e' necessario definire correttamente ".
                    "\$SPC_ALIAS_USER_MAP\n"
            }
            if( ref($SPC_ALIAS_LIST_MAP) eq 'HASH' && exists $SPC_ALIAS_LIST_MAP->{'TypeMap'} ){
                die "ERRORE: la mappa del parametro \$SPC_ALIAS_LIST_MAP ".$SPC_ALIAS_LIST_MAP->{'TypeMap'}.
                    " risulta sconusciuta, verificare il file di configurazione\n"
                        if !exists $MAPS_DEFINITION{$SPC_ALIAS_LIST_MAP->{'TypeMap'}};

                        $SPC_ALIAS_LIST_MAP->{'TypeMap'} = $MAPS_DEFINITION{$SPC_ALIAS_LIST_MAP->{'TypeMap'}};
                        $MAPS{++$idgm} = $SPC_ALIAS_LIST_MAP;
                        $SPC_ALIAS_LIST_MAP  = $idgm;
            }else{
                die "ERRORE: con \$SPC_ENABLED abilitato e' necessario definire correttamente ".
                    "\$SPC_ALIAS_LIST_MAP\n"
            }
            if( ref($SPC_USER_ENABLED) eq 'HASH' && exists $SPC_USER_ENABLED->{'TypeMap'} ){
                die "ERRORE: la mappa del parametro \$SPC_USER_ENABLED ".$SPC_USER_ENABLED->{'TypeMap'}.
                    " risulta sconusciuta, verificare il file di configurazione\n"
                        if !exists $MAPS_DEFINITION{$SPC_USER_ENABLED->{'TypeMap'}};

                        $SPC_USER_ENABLED->{'TypeMap'} = $MAPS_DEFINITION{$SPC_USER_ENABLED->{'TypeMap'}};
                        $MAPS{++$idgm} = $SPC_USER_ENABLED;
                        $SPC_USER_ENABLED  = $idgm;
            }else{
                die "ERRORE: con \$SPC_ENABLED abilitato e' necessario definire correttamente ".
                    "\$SPC_ALIAS_LIST_MAP\n"
            }
    }

    #forward_canonical
    $forward_canonical = [$forward_canonical]
        if( ref($forward_canonical) eq 'HASH' && exists $forward_canonical->{'TypeMap'} );
    if( ref($forward_canonical) eq 'ARRAY' ){
        for(my $i=0; $i<@$forward_canonical; $i++ ){
           die "ERRORE: la mappa del parametro \$forward_canonical ".$forward_canonical->[$i]->{'TypeMap'}.
               " risulta sconusciuta, verificare il file di configurazione\n"
                 if !exists $MAPS_DEFINITION{$forward_canonical->[$i]->{'TypeMap'}};

           $forward_canonical->[$i]->{'TypeMap'} = $MAPS_DEFINITION{$forward_canonical->[$i]->{'TypeMap'}};
           $MAPS{++$idgm} = $forward_canonical->[$i];
           $forward_canonical->[$i] = $idgm;
        }
        $MAPS_LIST{FORWARD_CANONICAL} = 1;
        $forward_canonical = $forward_canonical->[0] if @$forward_canonical == 1;
    }



    #forward_pec
    $forward_pec = [$forward_pec]
        if( ref($forward_pec) eq 'HASH' && exists $forward_pec->{'TypeMap'} );
    if( ref($forward_pec) eq 'ARRAY' ){
        for(my $i=0; $i<@$forward_pec; $i++ ){
           die "ERRORE: la mappa del parametro \$forward_pec ".$forward_pec->[$i]->{'TypeMap'}.
               " risulta sconusciuta, verificare il file di configurazione\n"
                 if !exists $MAPS_DEFINITION{$forward_pec->[$i]->{'TypeMap'}};

           $forward_pec->[$i]->{'TypeMap'} = $MAPS_DEFINITION{$forward_pec->[$i]->{'TypeMap'}};
           $MAPS{++$idgm} = $forward_pec->[$i];
           $forward_pec->[$i] = $idgm;
        }
                $MAPS_LIST{FORWARD_PEC} = 1;
        $forward_pec = $forward_pec->[0] if @$forward_pec == 1;
    }

    #esmtpMaxMsgSize
    $esmtpMaxMsgSize = [$esmtpMaxMsgSize]
        if( ref($esmtpMaxMsgSize) eq 'HASH' && exists $esmtpMaxMsgSize->{'TypeMap'} );
    if( ref($esmtpMaxMsgSize) eq 'ARRAY' ){
        for(my $i=0; $i<@$esmtpMaxMsgSize; $i++ ){
           die "ERRORE: la mappa del parametro \$esmtpMaxMsgSize ".$esmtpMaxMsgSize->[$i]->{'TypeMap'}.
               " risulta sconusciuta, verificare il file di configurazione\n"
                 if !exists $MAPS_DEFINITION{$esmtpMaxMsgSize->[$i]->{'TypeMap'}};

           $esmtpMaxMsgSize->[$i]->{'TypeMap'} = $MAPS_DEFINITION{$esmtpMaxMsgSize->[$i]->{'TypeMap'}};
           $MAPS{++$idgm} = $esmtpMaxMsgSize->[$i];
           $esmtpMaxMsgSize->[$i] = $idgm;
        }
        $MAPS_LIST{ESMPTMAXMSGSIZE} = 1;
        $esmtpMaxMsgSize = $esmtpMaxMsgSize->[0] if @$esmtpMaxMsgSize == 1;
    }

    #smtpMaxMsgRcpt
    $smtpMaxMsgRcpt = [$smtpMaxMsgRcpt]
        if( ref($smtpMaxMsgRcpt) eq 'HASH' && exists $smtpMaxMsgRcpt->{'TypeMap'} );
    if( ref($smtpMaxMsgRcpt) eq 'ARRAY' ){
        for(my $i=0; $i<@$smtpMaxMsgRcpt; $i++ ){
           die "ERRORE: la mappa del parametro \$smtpMaxMsgRcpt ".$smtpMaxMsgRcpt->[$i]->{'TypeMap'}.
               " risulta sconusciuta, verificare il file di configurazione\n"
                 if !exists $MAPS_DEFINITION{$smtpMaxMsgRcpt->[$i]->{'TypeMap'}};

           $smtpMaxMsgRcpt->[$i]->{'TypeMap'} = $MAPS_DEFINITION{$smtpMaxMsgRcpt->[$i]->{'TypeMap'}};
           $MAPS{++$idgm} = $smtpMaxMsgRcpt->[$i];
           $smtpMaxMsgRcpt->[$i] = $idgm;
        }
        $MAPS_LIST{SMPTMAXMSGRCPT} = 1;
        $smtpMaxMsgRcpt = $smtpMaxMsgRcpt->[0] if @$smtpMaxMsgRcpt == 1;
    }


    if( defined $adminPecEmail_RDS &&
        $adminPecEmail_RDS =~ /\P{IsASCII}/ ){
                die "ERRORE: il parametro \$adminPecEmail_RDS deve contenere solo caratteri ASCII\n"
    }

}


#
# Restituisce il path in ingresso con lo slash finale
# IN: path
# OUT: path con slash
#
sub _check_slash ($){
    my $path = shift; $path .= '/' if substr(  $path, -1, 1) ne '/'; $path}

1;
#

__END__

=head1 NAME

Opec::Conf - Gestione della configurazione

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
