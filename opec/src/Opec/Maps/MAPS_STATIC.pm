#
# $Id: MAPS_STATIC.pm,v 1.1 2012/08/17 16:21:23 flazan Exp $
#
# Project
# file name:    MAPS_STATIC.pm
# package:      Opec::Maps::MAPS_STATIC
#
# DESCRIPTION
# Implementazione della mappa statica - con valori inseriti contestualmente
#
# Parametri della mappa - i campi con * sono obbligatori:
#
#          (*dsn   => 'database1=<database>,host1=<hostname>,port1=<port>;database2=<database>,host2=<hostname>,port2=<port>',
#               user  => '',
#               pass  => '',
#               *query => '',
#               ttl   => 3600);
#
#
# Autore: Luca Di Vizio (divizio@exentrica.it)
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 17/08/2012 - Luca Di Vizio
# modification:
#
#*  Copyright (C) 2011  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 25 / int. 30 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

package Opec::Maps::MAPS_STATIC;

use strict;

my $MAP_NAME    = 'STATIC';


# metodo di classe
# COSTRUTTORE
#  IN: -
#
#  OUT:  rif. oggetto se ok, stringa d'errore
#        altrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

    $self->{_idlm}              = 0;    # indice locale mappe
    $self->{_lasterr}   = '';   # ultimo errore, da consultare subito dopo la chiamata di un metodo

    $self;
}

#
# metodo d'istanza
# Indicizza e valida le mappe restituendo l'id
#
#  IN: 1. ref hash dei parametri della mappa
#          2. eventuale indice della mappa
#
# OUT:  id mappa se ok, undef altrimenti (avvalorato)
#
sub addMap {
        my $self = shift;
        my $opt          = shift;
        my $idlm_candidate = shift;
    $self->{_lasterr} = '';

    if( ref($opt) ne 'HASH' or
        ref($opt->{value}) ne 'HASH' or
        !$opt->{query}){

        $self->{_lasterr} = "ERRORE: verificare la configurazione delle mappe $MAP_NAME";
        return
        }

        my $idlm = ++$self->{_idlm};
        if( defined $idlm_candidate ){
                if(exists $self->{$idlm_candidate}){
                        $self->{_lasterr} = "mappa gia' presente";
                        return}
                $idlm = $idlm_candidate
        }

        $self->{$idlm} = $opt;

        $idlm;
}

#
# DATO il paramentro esegue la query specificata
# e restituisce un array di elementi in un contesto lista
# mentre il primo elemento nel contesto scalare
#
# PARAM IN:     1. id mappa
#               2. parametro da utilizzare come clausola - indirizzo email
#
# PARAM OUT:    contesto arr: lista elementi
#                               contesto scal: elemento ritornato dal hash
#                               undef in caso d'errore
#
sub getValue {
    my $self    = shift;
    my $idlm            = shift;
    my $p       = shift;
    $self->{_lasterr} = '';

        if ( !defined $idlm or
                 $idlm !~ /^\d$/ or
                 !exists $self->{$idlm} ){
                $self->{_lasterr} =     'ID mappa non fornito o mappa non inizializzata';
                return}
        if ( !defined $p ){
                $self->{_lasterr} =     'Parametro in input non definito';
                return}

        my $query = $self->{$idlm}->{query};
        my ($localpart,$domain) = split('@',$p,2);
        my %h = ('%s' => $p,
                         '%u' => $localpart,
                         '%d' => $domain);

        # se la query contiene uno dei parametri corrispondente ad un
        # valore nullo, la query non viene eseguita e ritorna undef - errore
        if(!defined $h{'%u'}){
                 if ( index($query,'%u') != -1 ){
                        $self->{_lasterr} =     'parametro %u richiesto ma non definito';
                        return}
        }
        if(!defined $h{'%d'}){
                if( index($query,'%d') != -1 ){
                        $self->{_lasterr} =     'parametro %d richiesto ma non definito';
                        return}
        }

        $query =~ s/(%[sud])/$h{lc($1)}/gm;

    return wantarray ? ($self->{$idlm}->{value}->{$query}) : $self->{$idlm}->{value}->{$query};
}

#
# Metodo di istanza
# Restituisce l'ultimo errore
#
# IN: -
# OUT: $self->{_lasterr}
#
sub lastErr
  { my($self)=shift; $self->{_lasterr} }




1;


__END__

=head1 NAME

Opec::Maps::STATIC - Implementazione della mappa statica

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>
Luca Di Vizio <divizio@exentrica.it>

=cut
