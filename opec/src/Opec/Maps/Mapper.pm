#
# $Id: Mapper.pm,v 1.8 2012/08/23 08:52:55 ldivizio Exp $
#
# Project
# file name:    Mapper.pm
# package:      Opec::Maps::Mapper
#
# DESCRIPTION
# Livello di astrazione che fornisce un unico punto di accesso alle mappe
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 04/01/2011 - Fanton Flavio
# modification:
#   20/01/2011 - Fanton Flavio - aggiunto il motodo mapExists
#
#*  Copyright (C) 2011  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 25 / int. 30 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#


package Opec::Maps::Mapper;

use strict;
use Opec::Conf qw( :maps );


#
# Metodo di classe
# COSTRUTTORE
#
#  IN: -
#
#  OUT:  rif. oggetto Opec::Maps::Mapper, rif array alrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

        #--- precarico i moduli che realizzano le mappe
        # carico solo le mappe utilizzate
        my @err2loadMaps;
        my %h_tmp;
        $self->{_h_name} = {};  # mappa nome interno - riferimento oggetto
        $self->{_data_cache} = []; # cache dei dati; array di refh dove l'indice dell'array e' l'id della mappa
        $self->{_last_id} = undef;
        foreach my $idgm (keys %MAPS){

                if(!exists $h_tmp{$MAPS{$idgm}->{'TypeMap'}}){
                        unless( eval 'require '.$MAPS{$idgm}->{'TypeMap'} ){
                                push @err2loadMaps, $idgm." ($@)";
                                next}
                        $self->{_h_name}->{$idgm} =     $MAPS{$idgm}->{'TypeMap'}->new;
                        $h_tmp{$MAPS{$idgm}->{'TypeMap'}} = $idgm;
                }else{
                        $self->{_h_name}->{$idgm} = $self->{_h_name}->{$h_tmp{$MAPS{$idgm}->{'TypeMap'}}};
                }

                my $idlm = $self->{_h_name}->{$idgm}->addMap($MAPS{$idgm}->{'Options'},$idgm);
                if( !defined $idlm or $idlm != $idgm){
                        push @err2loadMaps, $self->{_h_name}->{$idgm}->lastErr();
                        next}

                $self->{_data_cache}->[$idgm] = {};
        }
        return \@err2loadMaps if @err2loadMaps;

    $self;
}

#
# Metodo di istanza
# getData
#
#  IN:  id mappa  - intero
#       parametro - stringa
#
#  OUT:  rif. array se sono stati trovati elementi, undef alrimenti
#
sub _getDataSingleMap {
    my $self = shift;
    my $idgm = shift || return;
    my $par  = shift;

    return if !exists  $self->{_h_name}->{$idgm};
    $self->{_last_id} = $idgm;

    if( exists $self->{_data_cache}->[$idgm]->{$par} and
        ((time - $self->{_data_cache}->[$idgm]->{$par}->[1]) <
        $self->{_h_name}->{$idgm}->{$idgm}->{ttl} ) ){

        if(wantarray){
            return if( !ref $self->{_data_cache}->[$idgm]->{$par}->[0] ); # XXX
            return @{$self->{_data_cache}->[$idgm]->{$par}->[0]}}

        return $self->{_data_cache}->[$idgm]->{$par}->[0]
    }

    $self->{_data_cache}->[$idgm]->{$par}->[1] = time;
    return $self->{_data_cache}->[$idgm]->{$par}->[0] =
           $self->{_h_name}->{$idgm}->getValue($idgm,$par)
           if !wantarray;

    my @tmp = $self->{_h_name}->{$idgm}->getValue($idgm,$par);
    $self->{_data_cache}->[$idgm]->{$par}->[0] = \@tmp;

    return @tmp;
}

#
# Metodo di istanza
# getData
#
#  IN:  id mappa  - intero
#               parametro - stringa
#
#  OUT:  rif. array se sono stati trovati elementi, undef alrimenti
#
sub getData {
    my $self = shift;
    my $idgm = shift || return;
    my $par  = shift;
    my $out;
    my @outArr;

    return if !defined $par;

    if(ref($idgm) eq 'ARRAY'){
        # sono state definite piu' mappe
        # da consultare in esclalation
        # dalla prima all'ultima
        foreach my $idMap (@$idgm){
            if(wantarray){
                @outArr = $self->_getDataSingleMap($idMap, $par);
                return @outArr if (@outArr);
            }else{
                $out = $self->_getDataSingleMap($idMap, $par);
                return $out if defined $out;
            }
        }
    }else{
                if(wantarray){
                @outArr = $self->_getDataSingleMap($idgm, $par);
                return @outArr;
                }else{
                        return $self->_getDataSingleMap($idgm, $par);
                }
   }
}


#
# Metodo di istanza
# Restituisce true se la mappa esiste false altrimenti
#
# IN: id mappa
# OUT: boolean
#
sub mapExists {
        my($self)=shift;
        my $idgm = shift || return;

        exists $self->{_h_name}->{$idgm}
}


#
# Metodo di istanza
# Restituisce l'ultimo errore, undef altrimenti
#
# IN: -
# OUT: $self->{_lasterr}
#
sub lastErr {
        my($self)=shift;
        my $idgm  = shift || $self->{_last_id};

        return if !defined $idgm;
        return $self->{_h_name}->{$idgm}->lastErr();
}


1;


__END__

=head1 NAME

Opec::Maps::Mapper - Livello di astrazione che fornisce un unico punto di accesso alle mappe

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
