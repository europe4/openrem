#
# $Id: MAPS_LDAP.pm,v 1.1 2011/02/14 16:19:00 flazan Exp $
#
# Project
# file name:    MAPS_LDAP.pm
# package:      Opec::Maps::MAPS_LDAP
#
# DESCRIPTION
# Implementazione della mappa LDAP
#
# Parametri della mappa - i campi con * sono obbligatori:
#
#          (*dsn   => 'host=<hostname1>,port=<port1>;host=<hostname2>,port=<port2>',
#               user  => '',
#               pass  => '',
#               *query => '',
#               ttl   => 3600);
#
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 04/11/2010 - Fanton Flavio
# modification:
#
#*  Copyright (C) 2011  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 25 / int. 30 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

package Opec::Maps::MAPS_LDAP;

use strict;

use Net::LDAP;

my $timeout_connection = 3600;      # tempo in secondi oltre il quale viene ritentata
                                    # la connessione verso il primo server
                                    # se necessario
my $max_server          = 5;                            # numero massimo di server possibili
my $MAP_NAME    = 'LDAP';

#
# metodo d'istanza privato
# VALIDA LA CONNESSIONE
# SE CASO LA RICREA CHIAMANDO  _connection
#
# paramIN:   1. id mappa
#                        2. booleano: true se forzo una riconnessione, false altrimenti
#
# paramOUT:  rif. obj se ok, stringa d'errore altrimenti
#
sub _getConnection {
    my $self    = shift;
        my $idlm        = shift;
        my $reconn      = shift;

    if( !$reconn && defined $self->{$idlm}->{_dsn_connected_idx} ){
        # la connessione esiste
                # la restituisco se e' sul primario oppure se e' su un secondario e non e' scaduta
            return $self->{_cache_con}->{$self->{$idlm}->{urlsf}->[$self->{$idlm}->{_dsn_connected_idx}]}->{ldaph}
                    if( !$self->{$idlm}->{_dsn_connected_idx} or
                        (time - $self->{$idlm}->{'_lastconnection'} <= $timeout_connection) );
    }

        # se arriviamo qua
        # . la connessione e' su un secondario scaduta
        # . la connessione non esiste
        # . richiesta riconnessione
        delete $self->{_cache_con}->{$self->{$idlm}->{urlsf}->[$self->{$idlm}->{_dsn_connected_idx}]}
                if $reconn; # cancello l'eventuale cache

        my $last_err;
        for (my $i=0; $i<@{$self->{$idlm}->{urlsf}}; $i++){
                my $urlsf = $self->{$idlm}->{urlsf}->[$i];
                if( exists $self->{_cache_con}->{$urlsf} ){
                        # la connessione e' nella cache
                        # la restituisco
                        $self->{$idlm}->{_dsn_connected_idx} = $i;
                        return $self->{_cache_con}->{$urlsf}->{ldaph};
                }else{
                        # la connessione non esiste
                        # la creo
                        my $ldaph = $self->_connection($idlm,$i);
                        if(ref $ldaph){
                                # connessione ok
                                $self->{$idlm}->{_dsn_connected_idx} = $i;
                                return $ldaph
                        }
                        $last_err .= $ldaph.";";
                }
                $i++;
        }

        # se arrivo qua sono in errore
        return substr($last_err,0,length($last_err)-1 );
}

#
# metodo d'istanza privato
# Effettua la connessione e restituisce l'handle al db
# aggiornando la cache delle connessioni
#
# IN:   1. id mappa
#               2. id connessione
#
# OUT: rif. Obj se ok, stringa d'errore altrimenti
#
sub _connection {
    my $self    = shift;
    my $idlm    = shift;
    my $id_conn         = shift;

        # se la connessione esiste mi sconnetto ed elimino la cache
        my $urlsf = $self->{$idlm}->{urlsf}->[$id_conn];
        if( exists $self->{_cache_con}->{$urlsf} ){
                $self->{_cache_con}->{$urlsf}->{ldaph}->unbind;
                delete $self->{_cache_con}->{$urlsf};
        }

        my $ldaph =
           $self->{_cache_con}->{$urlsf}->{ldaph} = Net::LDAP->new($self->{$idlm}->{urls}->[$id_conn],
                                                                                                   timeout => 5);
        return "Mappa $MAP_NAME: $@" if($@ || !$ldaph);

        # se l'utente nn esiste si effettua il bind anonimo
    my $objLdapMsg;
    if($self->{$idlm}->{bind_dn}){
                $objLdapMsg = $self->{$idlm}->{bind_dn} eq '__anonymous__'?
                                                $ldaph->bind():
                                        $ldaph->bind( $self->{$idlm}->{bind_dn},
                                              password => $self->{$idlm}->{bind_pw},
                                              version  => $self->{$idlm}->{version});
            return "Mappa $MAP_NAME: ".$objLdapMsg->code.' - '.
                $objLdapMsg->error_name.' - ' .$objLdapMsg->error_text
                if($objLdapMsg->code);
    }

        # OK
        $self->{_cache_con}->{$urlsf}->{lts} = time;

        return $ldaph
}

# metodo di classe
# COSTRUTTORE
#  IN: -
#
#  OUT:  rif. oggetto se ok, stringa d'errore
#        altrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

        # cache connessioni
        # una connessione e' individuata dal dsn format ed e' un hash del tipo
        # ldaph => handle connessione;
        # lts => epoch connessione;
    $self->{_cache_con} = {};

    $self->{_idlm}              = 0;    # indice locale mappe
    $self->{_lasterr}   = '';   # ultimo errore, da consultare subito dopo la chiamata di un metodo

    $self;
}

#
# metodo d'istanza
# Indicizza e valida le mappe restituendo l'id
#
#  IN: 1. ref hash dei parametri della mappa, tipo
#                       url   => '<url1***>|<url2***>', # ldap://localhost:389 default
#       ****bind_dn   => 'uid=mypec,dc=your,dc=com',
#               bind_pw   => 'secret',
#               *search_base => '',
#           query_filter => '',
#                       *attrib => '',
#                       version => 3, # 2 default
#                       scope => 'sub|base|one', # sub default
#                       sizelimit => 0,         # num max entry ritornato dal server, 0 per nessun limite
#                       result_format => '', # formatta il/i dati ottenuti
#                       ttl    => 3600
#
#                * i campi contrassegnati con * sono obbligatori
#        ** %s e' il parametro del contesto, tipicamento l'indirizzo email
#                *** urlx e' del tipo [<protocollo>://<host>[:<porta>]]
#               **** se bind_dn e' __anonymous__ bind anomino, se non definito o false
#                        il bind non viene eseguito
#
#          2. eventuale indice della mappa
#
# OUT:  id mappa se ok, undef altrimenti (avvalorato)
#
sub addMap {
        my $self = shift;
        my $opt          = shift;
        my $idlm_candidate = shift;
    $self->{_lasterr} = '';

    if( ref($opt) ne 'HASH' or
        !$opt->{search_base} or
        !$opt->{attrib}){

        $self->{_lasterr} = "Mappa $MAP_NAME: errore di configurazione";
        return
        }
        $opt->{scope}                   ||= 'sub';
        $opt->{version}                 ||= 2;
        $opt->{sizelimit}       ||= 0;

        my $idlm = ++$self->{_idlm};
        if( defined $idlm_candidate ){
                if(exists $self->{$idlm_candidate}){
                        $self->{_lasterr} = "Mappa $MAP_NAME: errore durante l'indicizzazione delle mappe";
                        return}
                $idlm = $idlm_candidate
        }

        # url => '<url1***>|<url2***>'
        # *** urlx e' del tipo [<protocollo>://<host>[:<porta>]]
        $opt->{'url'} ||= 'ldap://localhost:389';
        my @urls_tmp = split(/\|/,$opt->{'url'},$max_server);
        $self->{$idlm} = $opt;
        $self->{$idlm}->{bind_dn} ||= '';
        $self->{$idlm}->{bind_pw} ||= '';
        $self->{$idlm}->{urls} = [];
        $self->{$idlm}->{urlsf}= [];

        # check formato dsn
        foreach (@urls_tmp){
                my ($proto,$host,$port) = split(/:/,$_,3);
                $proto = lc($proto);
                if( ($proto ne 'ldap' and $proto ne 'ldaps') ){
                        $self->{_lasterr} = "Mappa $MAP_NAME: errore di configurazione, verificare il protocollo del parametro url";
                        return}

                if($host !~ /^\/\/[0-9a-zA-Z.\-_]+$/){
                        $self->{_lasterr} = "Mappa $MAP_NAME: errore di configurazione, verificare l'host del parametro url";
                        return}

                if(defined $port){
                        if($port !~ /^\d+$/){
                                $self->{_lasterr} = "Mappa $MAP_NAME: errore di configurazione, verificare la porta del parametro url";
                                return
                        }
                }else{
                        $port = 389;
                }

                push @{$self->{$idlm}->{urls}}, "$proto:$host:$port";
                push @{$self->{$idlm}->{urlsf}},_dsn_k_format(  "$proto:$host:$port",
                                                                                                                $self->{$idlm}->{bind_dn},
                                                                                                                $self->{$idlm}->{bind_pw});
        }

    $self->{$idlm}->{_dsn_connected_idx} = undef;# indice della URL a cui siamo connessi con successo

        $idlm;
}

#
# DATO il paramentro esegue la query specificata
# e restituisce un array di elementi in un contesto lista
# mentre il primo elemento nel contesto scalare
#
# PARAM IN:     1. id mappa
#               2. parametro da utilizzare come clausola nella query - indirizzo email
#
# PARAM OUT:    contesto arr: lista elementi
#                               contesto scal: primo elemento ritornato dalla query o undef se non ci sono elementi
#                               undef in caso d'errore
#
sub getValue {
    my $self    = shift;
    my $idlm            = shift;
    my $p       = shift;
    $self->{_lasterr} = '';

        if ( !defined $idlm or
                 $idlm !~ /^\d$/ or
                 !exists $self->{$idlm} ){
                $self->{_lasterr} =     "Mappa $MAP_NAME: ID mappa non fornito o mappa non inizializzata";
                return}
        if ( !defined $p ){
                $self->{_lasterr} =     "Mappa $MAP_NAME: parametro in input non definito";
                return}

        my $search_base = $self->{$idlm}->{search_base};
        my $filter      = $self->{$idlm}->{query_filter};
        my ($localpart,$domain) = split('@',$p,2);
        my %h = ('%s' => $p,
                         '%u' => $localpart,
                         '%d' => $domain);

        # se la query contiene uno dei parametri corrispondente ad un
        # valore nullo, la query non viene eseguita e ritorna undef - errore
        if(!defined $h{'%u'}){
                 if ( index($search_base,'%u') != -1 ){
                        $self->{_lasterr} =     "Mappa $MAP_NAME: parametro %u richiesto ma non definito";
                        return}
        }
        if(!defined $h{'%d'}){
                if( index($search_base,'%d') != -1 ){
                        $self->{_lasterr} =     "Mappa $MAP_NAME: parametro %d richiesto ma non definito";
                        return}
        }
        $search_base =~ s/(%[sud])/$h{lc($1)}/gm;

        if($filter){
                if(!defined $h{'%u'}){
                         if ( index($filter,'%u') != -1 ){
                                $self->{_lasterr} =     "Mappa $MAP_NAME: parametro %u richiesto ma non definito";
                                return}
                }
                if(!defined $h{'%d'}){
                        if( index($filter,'%d') != -1 ){
                                $self->{_lasterr} =     "Mappa $MAP_NAME: parametro %d richiesto ma non definito";
                                return}
                }
                $filter =~ s/(%[sud])/$h{lc($1)}/gm;
        }

        my $first_try = 1;
        while(1){
                my $ldaph = $first_try?$self->_getConnection($idlm):$self->_getConnection($idlm,1);
                if(!ref($ldaph)){
                        $self->{_lasterr} = $ldaph;
                        return}

                my $ldp_msg =   $ldaph->search(         base     =>  $search_base,
                                                                                scope    =>  $self->{$idlm}->{scope},
                                                                                sizelimit=>  $self->{$idlm}->{sizelimit},
                                                                                attrs    => [$self->{$idlm}->{attrib}],
                                                                                filter   =>  $filter);

            if($ldp_msg->code && $first_try){$first_try = 0;next}
            if($ldp_msg->code){
                $self->{_lasterr} =     "Mappa $MAP_NAME: errore nell'interrogazione LDAP - ".$ldp_msg->code.
                                                ' - '.$ldp_msg->error_name.' - '.$ldp_msg->error_text;
                return}
                if(wantarray){
                        my @a_entries = $ldp_msg->entries();
                        return if @a_entries == 0;

                        my @tmp;
                        foreach (@a_entries) {push @tmp, $_->get_value($self->{$idlm}->{attrib})}
                        return $self->_result_format($idlm, \@tmp);
                }else{
                        my $out;
                        if( $out = $ldp_msg->shift_entry() ){
                                ($out) = $out->get_value($self->{$idlm}->{attrib});
                        }
                        return $self->_result_format($idlm,$out)
                }
        }

}

#
# Metodo di istanza
# Restituisce l'ultimo errore
#
# IN: -
# OUT: $self->{_lasterr}
#
sub lastErr
  { my($self)=shift; $self->{_lasterr} }

#
# Metodo di classe
#
# Formatta una stringa o un'array di stringhe
# La formattazione avviene secondo un template che puo' contenere
# %s = stringa intera
# %u parte a sx della @
# %d parte a dx della @
#
# Esempio di template 'LMTP:%s:24'
# se la stringa di ingresso e' 'localhost'
# otterremo 'LMTP:localhost:24'
#
#
# PARAM IN:     1. template con token %s %d %u
#                               2. stringa o array di stringhe da risolvere
#
# PARAM OUT:    stringa/array risolto se in ingresso e' stata fornita una stringa/array
#                               con token, undef in caso di errore o di nessun elemento in ingresso
#               quanto in ingresso se il template non contiene token
#
#
sub _result_format {
        my $self  = shift;
        my $idlm  = shift;
    my $in    = shift;
    my $templ = $self->{$idlm}->{result_format};
    return shift if( $templ !~ /%[sud]/ ); # se il template non contiene token restituisco il risultato
    my $want_lst = 1;

    if(!ref($in)){$in = [$in];$want_lst = 0}

    my @out = map {
        my ($localpart,$domain) = split('@',$_,2);
        my %h = ('%s' => $_,
                 '%u' => $localpart,
                 '%d' => $domain);
        local $_ = $templ;
        s/(%[sud])/$h{lc($1)}/gm;
        $_
    } @$in;

    $want_lst ? @out: shift @out;
}

#
# Metodo di classe
#
# Formatta l'url di connessione in modo che possa essere
# utilizzata come chiave all'interno di un hash per mantenere
# una cache delle connessioni
#
# L'url e' del tipo ldaps://1.2.3.4:666 ossia
# <protocollo>://<host>:<porta>
# dove <protocollo> puo' essere ldap o ldaps
#
# trasformo quanto fornito nel formato costante seguente:
# <protocollo>;<host>;<porta>;<bind_dn>;<bind_pw>
# dove
#
# PARAM IN:     stringa
#
# PARAM OUT:    stringa formattata, undef in caso d'errore
#
sub _dsn_k_format {
        my $url = shift || return;
        my $u = shift;
        my $p = shift;

        my($proto,$host,$port) = split(/:/,$url,3);
        $proto= substr($proto,index($proto,'//')+1);

        "$proto;$host;$port;$u;$p"
}





1;


__END__

=head1 NAME

Opec::Maps::DB - Implementazione della mappa MySql

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
