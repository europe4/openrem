#
# $Id: MAPS_MYSQL.pm,v 1.7 2012/08/23 08:52:55 ldivizio Exp $
#
# Project
# file name:    MAPS_MYSQL.pm
# package:      Opec::Maps::MAPS_MYSQL
#
# DESCRIPTION
# Implementazione della mappa MySql
#
# Parametri della mappa - i campi con * sono obbligatori:
#
#          (*dsn   => 'database1=<database>,host1=<hostname>,port1=<port>;database2=<database>,host2=<hostname>,port2=<port>',
#               user  => '',
#               pass  => '',
#               *query => '',
#               ttl   => 3600);
#
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 04/11/2010 - Fanton Flavio
# modification:
#
#*  Copyright (C) 2011  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 25 / int. 30 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

package Opec::Maps::MAPS_MYSQL;

use strict;

use DBI;

my $timeout_connection = 3600;      # tempo in secondi oltre il quale viene ritentata
                                    # la connessione verso il primo server
                                    # se necessario
my $max_server          = 5;                            # numero massimo di server possibili
my $MAP_NAME    = 'MYSQL';
my $dsn_base    = 'DBI:mysql:';


#
# metodo d'istanza privato
# VALIDA LA CONNESSIONE
# SE CASO LA RICREA CHIAMANDO  _connection
#
# paramIN:   1. id mappa
#                        2. booleano: true se forzo una riconnessione, false altrimenti
#
# paramOUT:  rif. obj se ok, stringa d'errore altrimenti
#
sub _getConnection {
    my $self    = shift;
        my $idlm                = shift;
        my $reconn      = shift;

    if( !$reconn && defined $self->{$idlm}->{_dsn_connected_idx} ){
        # la connessione esiste
                # la restituisco se e' sul primario oppure se e' su un secondario e non e' scaduta
            return $self->{_cache_con}->{$self->{$idlm}->{dsnf}->[$self->{$idlm}->{_dsn_connected_idx}]}->{dbh}
                    if( !$self->{$idlm}->{_dsn_connected_idx} or
                        (time - $self->{$idlm}->{'_lastconnection'} <= $timeout_connection) );
    }

        # se arriviamo qua
        # . la connessione e' su un secondario scaduta
        # . la connessione non esiste
        # . richiesta riconnessione
        delete $self->{_cache_con}->{$self->{$idlm}->{dsnf}->[$self->{$idlm}->{_dsn_connected_idx}]}
                if $reconn; # cancello l'eventuale cache

        my $last_err;
        for (my $i=0; $i<@{$self->{$idlm}->{dsnf}}; $i++){
                my $dsnf = $self->{$idlm}->{dsnf}->[$i];
                if( exists $self->{_cache_con}->{$dsnf} ){
                        # la connessione e' nella cache
                        # la restituisco
                        $self->{$idlm}->{_dsn_connected_idx} = $i;
                        return $self->{_cache_con}->{$dsnf}->{dbh};
                }else{
                        # la connessione non esiste
                        # la creo
                        my $dbh = $self->_connection($idlm,$i);

                        if(ref $dbh){
                                # connessione ok
                                $self->{$idlm}->{_dsn_connected_idx} = $i;
                                return $dbh
                        }
                        $last_err .= $dbh.";";
                }
                $i++;
        }

        # se arrivo qua sono in errore
        return substr($last_err,0,length($last_err)-1 );
}

#
# metodo d'istanza privato
# Effettua la connessione e restituisce l'handle al db
# aggiornando la cache delle connessioni
#
# IN:   1. dsn
#               2. user
#               3. pass
#
# OUT: rif. Obj se ok, stringa d'errore altrimenti
#
sub _connection {
    my $self    = shift;
    my $idlm    = shift;
    my $id_conn         = shift;

        # se la connessione esiste mi sconnetto ed elimino la cache
        my $dsnf = $self->{$idlm}->{dsnf}->[$id_conn];
        if( exists $self->{_cache_con}->{$dsnf} ){
                $self->{_cache_con}->{$dsnf}->{dbh}->disconnect;
                delete $self->{_cache_con}->{$dsnf};
        }

        my $dbh;
        eval{
                $self->{_cache_con}->{$dsnf}->{dbh} =
                $dbh = DBI->connect($dsn_base.$self->{$idlm}->{dsn}->[$id_conn],
                                                        $self->{$idlm}->{user},
                                                        $self->{$idlm}->{pass},
                                                        { RaiseError => 1,
                                                          PrintError => 0,
                                                          mysql_auto_reconnect => 1});
        };

        return $@ || $DBI::errstr if( $@ or $DBI::errstr );

        # OK
        $self->{_cache_con}->{$dsnf}->{lts} = time;

        return $dbh
}

# metodo di classe
# COSTRUTTORE
#  IN: -
#
#  OUT:  rif. oggetto se ok, stringa d'errore
#        altrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

        # cache connessioni
        # una connessione e' individuata dal dsn format ed e' un hash del tipo
        # dbh => handle connessione;
        # lts => epoch connessione;
    $self->{_cache_con} = {};

    $self->{_idlm}              = 0;    # indice locale mappe
    $self->{_lasterr}   = '';   # ultimo errore, da consultare subito dopo la chiamata di un metodo

    $self;
}

#
# metodo d'istanza
# Indicizza e valida le mappe restituendo l'id
#
#  IN: 1. ref hash dei parametri della mappa, tipo
#                       *dsn   => 'database=<database>;host=<hostname>;port1=<port>|database=<database>;host2=<hostname>;port=<port>',
#               user   => 'nomeutente',
#               pass   => 'mypass',
#               *query => 'select campo1 as param1 from MIATABELLA where campo2="%s"'
#                       ttl    => 3600
#
#                * i campi contrassegnati con * sono obbligatori
#        ** %s e' il parametro del contesto, tipicamento l'indirizzo email
#
#          2. eventuale indice della mappa
#
# OUT:  id mappa se ok, undef altrimenti (avvalorato)
#
sub addMap {
        my $self = shift;
        my $opt          = shift;
        my $idlm_candidate = shift;
    $self->{_lasterr} = '';

    if( ref($opt) ne 'HASH' or
        !$opt->{dsn} or
        !$opt->{query}){

        $self->{_lasterr} = "ERRORE: verificare la configurazione delle mappe $MAP_NAME";
        return
        }

        my $idlm = ++$self->{_idlm};
        if( defined $idlm_candidate ){
                if(exists $self->{$idlm_candidate}){
                        $self->{_lasterr} = "mappa gia' presente";
                        return}
                $idlm = $idlm_candidate
        }

        my @dsn_tmp = split(/\|/,$opt->{'dsn'},$max_server);
        $self->{$idlm} = $opt;
        $self->{$idlm}->{dsn} = [];
        $self->{$idlm}->{dsnf}= [];

        # check formato dsn
        foreach (@dsn_tmp){
                my ($db,$host,$port) = split /;/;
                if($db !~ /^database=/){
                        $self->{_lasterr} = "ERRORE: verificare la configurazione delle mappe $MAP_NAME";
                        return}

                if($host){
                        if($host !~ /^host=/){
                                $self->{_lasterr} = "ERRORE: verificare la configurazione delle mappe $MAP_NAME";
                                return
                        }
                }else{
                        $host = 'host=localhost';
                }

                if($port){
                        if($port !~ /^port=\d+$/){
                                $self->{_lasterr} = "ERRORE: verificare la configurazione delle mappe $MAP_NAME";
                                return
                        }
                }else{
                        $port = 'port=3306';
                }

                push @{$self->{$idlm}->{dsn}}, "$db;$host;$port";
                push @{$self->{$idlm}->{dsnf}},_dsn_k_format(   "$db;$host;$port",
                                                                                                                $self->{$idlm}->{user},
                                                                                                                $self->{$idlm}->{pass});
        }

#       $self->{$idlm}->{user} = undef if !exists $self->{$idlm}->{user};
#       $self->{$idlm}->{pass} = undef if !exists $self->{$idlm}->{pass};
    $self->{$idlm}->{_dsn_connected_idx} = undef;       # indice del DNS del server a cui siamo connessi con successo

        $idlm;
}

#
# DATO il paramentro esegue la query specificata
# e restituisce un array di elementi in un contesto lista
# mentre il primo elemento nel contesto scalare
#
# PARAM IN:     1. id mappa
#               2. parametro da utilizzare come clausola nella query - indirizzo email
#
# PARAM OUT:    contesto arr: lista elementi
#                               contesto scal: primo elemento ritornato dalla query o undef se non ci sono elementi
#                               undef in caso d'errore
#
sub getValue {
    my $self    = shift;
    my $idlm            = shift;
    my $p       = shift;
    $self->{_lasterr} = '';

        if ( !defined $idlm or
                 $idlm !~ /^\d$/ or
                 !exists $self->{$idlm} ){
                $self->{_lasterr} =     'ID mappa non fornito o mappa non inizializzata';
                return}
        if ( !defined $p ){
                $self->{_lasterr} =     'Parametro in input non definito';
                return}

        my $query = $self->{$idlm}->{query};
        my ($localpart,$domain) = split('@',$p,2);
        my %h = ('%s' => $p,
                         '%u' => $localpart,
                         '%d' => $domain);

        # se la query contiene uno dei parametri corrispondente ad un
        # valore nullo, la query non viene eseguita e ritorna undef - errore
        if(!defined $h{'%u'}){
                 if ( index($query,'%u') != -1 ){
                        $self->{_lasterr} =     'parametro %u richiesto ma non definito';
                        return}
        }
        if(!defined $h{'%d'}){
                if( index($query,'%d') != -1 ){
                        $self->{_lasterr} =     'parametro %d richiesto ma non definito';
                        return}
        }

        $query =~ s/(%[sud])/$h{lc($1)}/gm;

        my $first_try = 1;
        my $out;
        while(1){
                my $dbh = $first_try?$self->_getConnection($idlm):$self->_getConnection($idlm,1);
                if(!ref($dbh)){
                        $self->{_lasterr} = $dbh;
                        return}

            my $st = $dbh->prepare( $query );
            if(!$st && $first_try){$first_try = 0;next}
            if(!$st){
                $self->{_lasterr} =     'errore durante l\'esecuzione della query '.$query;
                return}

                $st->execute;
                $out = wantarray ? $st->fetchall_arrayref([0]): $st->fetchrow_array();

                if($st->err && $first_try){$first_try = 0;next}
                if($st->err){
                        $self->{_lasterr} =     'errore durante il recupero dati: '.$st->err;
                        return}

            $st->finish;
            last;
        }

    return wantarray ? map {$_->[0]} @$out : $out;
}

#
# Metodo di istanza
# Restituisce l'ultimo errore
#
# IN: -
# OUT: $self->{_lasterr}
#
sub lastErr
  { my($self)=shift; $self->{_lasterr} }

#
# Metodo di classe
#
# Formatta il dsn in modo che possa essere utilizzato
# come chiave all'interno di un hash per mantenere
# una cache delle connessioni
#
# il DSN e' del tipo
# database=$database;host=$hostname;port=$port
# dove l'unico elemento obbligatorio e' database
#
# trasformo quanto fornito nel formato costante
# seguente
# $database;$hostname;$port;$username;$pass
# dove
# $database e' obbligatorio quindi non e' toccato
# $port e' 3306 se non fornita
# $hostname e' case insensitive quindi viene trasformato in lowercase
#                       e impostato a localhost se non fornito
# $username niente se non fornito
# $pass niente se non fornito
#
# PARAM IN:     stringa
#
# PARAM OUT:    stringa formattata, undef in caso d'errore
#
sub _dsn_k_format {
        my $dsn = shift || return;
        my $u = shift;
        my $p = shift;

        my($db,$host,$port) = split(/;/,$dsn,3);
        $db   = substr($db,index($db,'=')+1);
        $host = substr($host,index($host,'=')+1);
        $port = substr($port,index($port,'=')+1);

        "$db;$host;$port;$u;$p"
}





1;


__END__

=head1 NAME

Opec::Maps::DB - Implementazione della mappa MySql

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
