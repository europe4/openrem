#
# $Id: Clamav.pm,v 1.12 2011/02/14 16:19:52 flazan Exp $
#
# Project       OpenPec
# file name:    Clamav.pm
# package:      Opec::AV
#
# DESCRIPTION
# Interfaccia per l'accesso all'antivirus ClamAV.
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 03/08/2005 - Fanton Flavio | flazan
# modification:
# 25/07/2006 - Fanton Flavio
#   . aggiunta la scansione tramite socket unix via stream
# 25/01/2008 - Fanton Flavio
#   . eliminata la gestione dei timer
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#
package Opec::AV::Clamav;

use IO::Socket;
use Opec::Conf qw(:av_vars);
use Opec::Util qw( &prolongTimer );
use strict;
use warnings;


# COMANDI clamd
my $CMD_CLAMD_PING      = 'PING';
my $CMD_CLAMD_VERSION   = 'VERSION';
my $CMD_CLAMD_STREAM    = 'STREAM';
my $CMD_CLAMD_RELOAD    = 'RELOAD';
my $CMD_CLAMD_SCAN      = 'SCAN';
my $CMD_CLAMD_CONTSCAN  = 'CONTSCAN';

my $STREAM_BLOCK_SIZE   = 4096;
my $TIMEOUT_CONNECTION  = 1000; #3;
my $TIMEOUT_OPERATION   = 1000; #5;

# proprieta private
# $self->{'_conn_type'} - tipo di connessione: tcp | unix
# $self->{'_remote_host'}
# $self->{'_remote_port'}
# $self->{'_local_socket'}

# metodo di classe
# COSTRUTTORE
#  IN: -
#  OUT:  rif. oggetto se ok, stringa d'errore
#        altrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

    my ($mod, $destination, $port ) = split /:/, $av;

    # solo controlli veloci
    # controlli di sintassi e semantica sono fatti in Config.pm
    if($port){
        $self->{'_conn_type'} = 'tcp';
        $self->{'_remote_host'} = $destination;
        $self->{'_remote_port'} = $port;
    }elsif(-S $destination){
        $self->{'_conn_type'} = 'unix';
        $self->{'_local_socket'} = $destination;
    }else{
        return "ClamAV Interface: impossibile stabilire il tipo di socket"}

    # reload
    my $out = $self->_command(1,$CMD_CLAMD_RELOAD);
    return $out if(!ref($out));

    $self;
}


# metodo di istanza
# Scansiona il file fornendo il path
#
#  IN: path assoluto del file | handle del file
#  OUT: undef    --> mail senza virus
#       arr. ref --> mail con virus (ogni elemento descrive un virus trovato)
#       scalar   --> errore durante la scansione
#
sub scan {
    my $self        = shift;
    my $obj2scan    = shift;

    if(ref($obj2scan) && $self->{'_conn_type'} eq 'tcp'){
        # TCP
        if(ref($obj2scan)){
            # $obj2scan (deve) rappresenta un obj che fornisca i metodi seek e read
            $self->_scan_stream($obj2scan)
        }else{
            # $obj2scan rappresenta il path del doc da scansionare
            my $h = IO::File->new($obj2scan);
            $self->_scan_stream($h);
        }
    }elsif(-r $obj2scan){
        # UNIX
        if(ref($obj2scan)){
            # $obj2scan (deve) rappresenta un obj che fornisca i metodi seek e read
            $self->_scan_stream($obj2scan)
        }else{
            # $obj2scan rappresenta il path del doc da scansionare
            $self->_scan_file($obj2scan)
        }
    }else{
        return "Tipo di socket inconsistente ($self->{'_conn_type'})."
    }

}

# metodo di istanza
# Verifica il server Clamd e' in esecuzione
#
#  IN: -
#  OUT: 1 server in esecuzione, 0 server down
#
sub ping {
    my $self = shift;

    my $out = $self->_command(1,$CMD_CLAMD_PING);
    (ref($out) && $out->[0] =~ /PONG/) ? 1 : 0
}

# metodo d'istanza privato
# Scansiona il file dato il path assoluto
#
#  IN: path del file
#  OUT: arr ref se ok, stringa d'errore altrimenti
#
sub _scan_file {
    my $self    = shift;
    my $file    = shift;

    my $out = $self->_command(0,$CMD_CLAMD_CONTSCAN,[$file]);
    return $out if(!ref($out));

    # restituisco la risposta formattata
    $self->_res_format($out)
}

# metodo d'istanza privato
# Scansiona il file dato l'handle (solo per connessioni tipo TCP)
#
#  IN: handle dei dati da scansionare
#  OUT: arr ref se ok, string d'errore altrimeti
#
sub _scan_stream {
    my $self    = shift;
    my $handle  = shift;

    $handle->seek(0,0);

    # recupero la connessione la connection
    # richiedo la funzione,
    # recupero la porta e
    # creo la connessione sulla nuova porta
#    my $connMaster = $self->_getConnection_tcp();
    my $connMaster = $self->_getConnection();
    return $connMaster if(!ref($connMaster));

    print $connMaster $CMD_CLAMD_STREAM;
    my $stream_port = $connMaster->getline();

    if($stream_port =~ /^PORT\s(\d+)$/){
        $stream_port = $1;
    }else{
        return "ClamAV Interface: errore durante l'inizializzazione dello stream di dati"}
    my $connStream = $self->_getConnection_tcp( 0, $self->{'_remote_host'}||'localhost',
                                                   $stream_port);
    return $connStream if(!ref($connStream));

    # leggo dal handle in ingresso,
    # scrivo sul socket appena creato
    # chiudo i socket
    # restituisco la risposta formattata
    my $block;
    while($handle->read($block, $STREAM_BLOCK_SIZE)){
        print $connStream $block}
    $connStream->close;

    my $responseMaster = $connMaster->getline();
    $connMaster->close();

    $self->_res_format([$responseMaster])
}


### metodi privati

#
# metodo d'istanza privato
# Effettua la connessione
#
# IN: - reload timer (0 si, 1 altrimenti)
#     - (prende tutto da configurazione)
# OUT: Obj IO::Socket se ok, stringa d'errore altrimenti
#
sub _getConnection () {
    my $self    = shift;
    my $reload_timer = shift;
    my $conn_generic;

    if($self->{'_conn_type'} eq 'tcp'){
        # connessione TCP
        $conn_generic = $self->_getConnection_tcp($reload_timer);
    }elsif($self->{'_conn_type'} eq 'unix'){
        # connessione unix
        $conn_generic = $self->_getConnection_unix($reload_timer);
    }else{
        return "Impossibile rilevare il tipo di socket verso ClamAV"}
}


#
# metodo d'istanza privato
# Effettua la connessione TCP
#
# IN: - reload timer (0 si, 1 altrimenti)
#     - nome host (default $self->{'_remote_host'})
#     - porta     (default $self->{'_remote_port'})
# OUT: Obj IO::Socket se ok, stringa d'errore altrimenti
#
sub _getConnection_tcp {
    my $self        = shift;
    my $reload_timer= shift;
    my $host        = shift || $self->{'_remote_host'};
    my $port        = shift || $self->{'_remote_port'};
    my $conn_tcp;
    my $remaining_time;

    ($remaining_time) = alarm(0) if(!$reload_timer);  # sospendo la gestione del timer
    $conn_tcp = IO::Socket::INET->new(  PeerAddr => $host,
                                        PeerPort => $port,
                                        Proto    => "tcp",
                                        Type     => SOCK_STREAM,
                                        Timeout  => 10);
    return "ClamAV Interface: impossibile connettersi a clamd ($host:$port)."
        if(!$conn_tcp);

    prolongTimer('avirus-connect_sckinet', $remaining_time) if(!$reload_timer);  # riprendo la gestione del timer

    $conn_tcp->autoflush(1);

    $conn_tcp
}


#
# metodo d'istanza privato
# Effettua la connessione via local socket
#
# IN: - reload timer (0 si, 1 altrimenti)
#     - path socket unix se non fornito utilizza $self->{'_local_socket'}
# OUT: Obj IO::Socket se ok, stringa d'errore altrimenti
#
sub _getConnection_unix {
    my $self        = shift;
    my $reload_timer= shift;
    my $local_socket= shift || $self->{'_local_socket'};
    my $conn_unix;

    my($remaining_time) = alarm(0) if(!$reload_timer);  # sospendo la gestione del timer
    $conn_unix = IO::Socket::UNIX->new( Peer => $local_socket,
                                        Timeout  => 10 );

    return "ClamAV Interface: impossibile connettersi via local socket ($local_socket)."
        if(!$conn_unix);

    prolongTimer('avirus-connect_sckunix', $remaining_time) if(!$reload_timer);  # riprendo la gestione del timer

    $conn_unix->autoflush(1);

    $conn_unix
}



#
# metodo d'istanza privato
# Scrive il comando dato in ingresso sul socket e
# restituisce il risultato
#
# IN: - reload timer (0 si, 1 altrimenti)
#     - nome comando, arr. ref. di parametri aggiuntivi
# OUT: quanto restituito dal socket come ref array o stringa d'errore altrimenti
#
sub _command {
    my $self    = shift;
    my $reload_timer = shift;
    my $cmd     = shift;
    my $ref_param   = shift;

    my $conn = $self->_getConnection($reload_timer);
    return $conn if(!ref($conn));

    if($ref_param){
        my $param   = join(',',@$ref_param);
        $conn->print("$cmd,$param");
    }else{
        $conn->print($cmd)}

    my @data = <$conn>;

    $conn->close();
    \@data
}

#
# metodo d'istanza privato
# Formatta la risposta di ClamAV
#
# IN: - risposta di ClamAV come array ref
# OUT: undef    --> mail senza virus
#      arr. ref --> mail con virus (ogni elemento descrive un virus trovato)
#      scalar   --> errore durante la scansione
#
sub _res_format {
    my $self = shift;
    my $res_clamav = shift;

    return "ClamAV Interface: valore di ritorno inaspettato di ClamAV"
        if(!$res_clamav or !ref($res_clamav));

    # mail ok
    return undef if($res_clamav->[0] =~ /.+: OK$/o);

    # mail con virus
    my @res_clamav_formatted = map  { substr( $_, index($_,' ')) }
                                    @$res_clamav;
    chomp(@res_clamav_formatted);
    return \@res_clamav_formatted;
}


1;


__END__

=head1 NAME

Opec::AV::Clamav - Interfaccia antivirus per ClamAV

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
