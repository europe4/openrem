#
# $Id: RMC.pm,v 1.15 2011/10/13 17:26:56 flazan Exp $
#
# Project
# file name:    RMC.pm
# package:      Opec::Command::RMC
#
# DESCRIPTION
# Gestione della ricevuta di mancata consegna per scadenza dei termini.
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 26/04/2006 - Fanton Flavio
# modification:
#
# 01/06/2007 - Fanton Flavio
#       . aggiunto log legale
# 14/06/2007 - Fanton Flavio
#       . aggiunta gestione una ricevuta per ogni rcpt
# 04/01/2008 - Fanton Flavio
#       . adattato alle ultime modifiche di Opec::Sign
# 15/12/2008 - Flavio Fanton
#       . bugfix su caratteri speciali con codifica diversa da iso-8859-1
#
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

package Opec::Command::RMC;


use strict;
use Opec::Conf qw( :const :confvars );
use Opec::Sign;
use Opec::Util qw( &doLog &myChomp &msgId &checkRFC2822 &doLogXml );
use Opec::Receipt qw(   &getRMC12
                        &getRMC24);

my $seq = 0;

#
# Metodo di default richiamato dallo scheduler
#
# PARAM IN:     1. nome comando
#               2. nome messaggio
#               3. parametro da passare
#
# PARAM OUT:    1 se ok, stringa d'errore o undef altrimenti
#
sub command ($$;$){
    my $cmd     = shift || return "Comando non definito";
    my $task    = shift;
    my $param   = shift;
    my $objDispatcher;

    return 'Nessun oggetto Message da processare - verificare il contenuto delle code'
        if(!ref($param));

        msgId(sprintf(  "%05d.%02d.%s",
                        $$,
                        $Opec::childInvocationCount,
                        ++$seq));
    doLog(0, "*** NEW MAIL - START");

#    my ($ric_rcpt) = split /,/,$task,2;
#    if($ric_rcpt && checkRFC2822($ric_rcpt)){
#        $param->recips([$ric_rcpt])
#    }

    my $objReceipt;
    if($cmd eq 'RMC12'){
        doLog(0, 'Generazione Ricevuta di mancata consegna - 12');
        $objReceipt = getRMC12($param, 0);
    }elsif($cmd eq 'RMC24'){
        doLog(0, 'Generazione Ricevuta di mancata consegna - 24');
        $objReceipt = getRMC24($param, 0);
    }else{
        return "Comando non previsto";
    }

    my $err_est = $objReceipt->get_xml_data('dati#errore-esteso');
    my $dest_dt = $objReceipt->get_xml_data('dati#consegna');
    my $identificativo = $objReceipt->get_xml_data('dati#identificativo');

    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Direzione: OUTBOUND");
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Sender: ".$objReceipt->sender->mailAddress);
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Rcpt: ".($objReceipt->recips)->[0]->mailAddress);
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Subject: ".&myChomp($objReceipt->header->get ('Subject')));
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Message-ID: ".&myChomp($objReceipt->header->get($OPEC_HEADER_KEY_MSGID)));
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Message-ID-rif: ".&myChomp($objReceipt->header->get($OPEC_HEADER_KEY_MSGID_RIF)));
    if ($identificativo) {
        doLog(0, $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY .
              ' - Identificativo: <' . &myChomp($identificativo) . '>');
    }
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Gestore: $adminName");
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Errore-esteso: $err_est") if defined $err_est;
    doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Consegna-DT: $dest_dt") if defined $dest_dt;

    my $objSign = Opec::Sign->new();
    unless(ref($objSign)){
        doLog(1, $objSign);die "$objSign\n"}

    $objReceipt = $objSign->Sign($objReceipt);

    if(!ref($objReceipt)){
        doLog(0,"$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Esito: ".
                "Errore durante la firma della ricevuta di mancata consegna: ".
                $objSign->error().' - 2');
        return $objSign->error()}

    $objDispatcher = new Opec::Out::Dispatcher || die $objDispatcher;
    $objDispatcher->mailDispatch($objReceipt, 1);
    dispatchReport($objReceipt, "$OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY - Esito: ");
    doLog(0, "*** NEW MAIL - END");

    return 1
}

sub dispatchReport {
    my $objRef = shift;
    my $pre = shift||'';

    return if(!ref($objRef));
    $objRef = [$objRef] if(ref($objRef) ne 'ARRAY');

    doLog(0, "Resoconto dispaccio:");
    foreach my $obj(@{$objRef}){
        foreach(@{$obj->recips()}){
            doLog(0, $pre.$_->mailAddress.' - '.$_->reciptRemoteResponse)}
    }

    # log xml
    doLogXml($objRef);
}


1;

__END__

=head1 NAME

Opec::Command::RMC

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
