#
# $Id: SPINLOG_XML.pm,v 1.2 2012/08/16 16:39:54 flazan Exp $
#
# Project
# file name:    SPINLOG.pm
# package:      Opec::Command::SPINLOG_XML
#
# DESCRIPTION
# Gestione della rotazione log XML con criterio temporale
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 26/08/2011 - Fanton Flavio
# modification:
#       16/08/2012 - Fanton Flavio/Luca Di Vizio
#               - bugfix: i processi che avevano servito le chiamate dallo scheduler non
#         loggavano piu' su file il log legale e di sistema in caso di logxml abilitato
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

package Opec::Command::SPINLOG_XML;


use strict;
use Opec::Conf qw( $OPEC_QUEUE_PATHNAME $OPEC_QUEUE_DIR_OPT $XML_MINTIME $idServer );
use Opec::Log qw( &init &rotate );

#
# Metodo di default richiamato dallo scheduler
#
# PARAM IN:     1. nome comando
#               2. nome task
#               3. parametro da passare
#
# PARAM OUT:    1 se ok e se non viene effettuata alcuna rotazione, stringa d'errore o undef altrimenti
#
sub command ($$;$){
    my $cmd     = shift;
    my $task    = shift;
    my $param   = shift;
    my $out     = 1;

    # verifico la scadenza basandomi sul mtime del file
    my $mtime = (stat "$OPEC_QUEUE_PATHNAME/$idServer/$OPEC_QUEUE_DIR_OPT/$task")[9];   # mtime
    return "Impossibile accedere al task" unless defined $mtime;
    #my $mtime = 1;
    if( time > $mtime+$XML_MINTIME ){
        # task scaduto
        # ruotare e aggiornare mtime
        init('',undef,1);
        my $out = rotate(2);
        if(defined $out && $out==1){
            $mtime = time;
            $out = utime $mtime, $mtime, "$OPEC_QUEUE_PATHNAME/$idServer/$OPEC_QUEUE_DIR_OPT/$task";
                return "Impossibile aggiornare il modification time del task ".
                "$OPEC_QUEUE_PATHNAME/$idServer/$OPEC_QUEUE_DIR_OPT/$task: $!"
                if($out != 1);
         }
    }

    $out;
}



1;

__END__

=head1 NAME

Opec::Command::SPINLOG_XML - Task per la rotazione del log XML con criterio temporale

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
