#
# $Id: Dispatcher.pm,v 1.10 2011/10/13 17:26:56 flazan Exp $
#
# Project
# file name:    Dispatcher.pm
# package:      Opec::Command::Dispatcher
#
# DESCRIPTION
# Livello di astrazione per l'esecuzione degli ordini del comandati via socket UNIX
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 26/04/2006 - Fanton Flavio
# modification:
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

package Opec::Command::Dispatcher;


# ### verificare
use strict;

my $CONST_DEFAULTMET = 'command';
my %mod2load = (    RMC12           =>  'Opec::Command::RMC',
                    RMC24           =>  'Opec::Command::RMC',
                    SPINLOG_LEGAL   =>  'Opec::Command::SPINLOG_LEGAL',
                    SPINLOG_SYSTEM  =>  'Opec::Command::SPINLOG_SYSTEM',
                    SPINLOG_XML                 =>      'Opec::Command::SPINLOG_XML');

#
# COSTRUTTORE
# Precarico tutti i moduli utilizzabili
#  IN:  -
#  OUT:  rif. oggetto se ok, stringa d'errore altrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);
        my @err2load;

    foreach (values %mod2load) {
        eval "require $_";
        push @err2load, $_ if $@}

    @err2load ? "Errore di caricamento dei moduli: ".join(", ", @err2load) : $self;
}


#
# Metodo per eseguire il comando ricevuto dallo scheduler
#
# PARAM IN:     1. nome comando
#               2. nome messaggio
#               3. parametro da passare
#
# PARAM OUT:    1 se ok, stringa d'errore o undef altrimenti
#
sub cmdExec ($$$) {
    my $self    = shift;
    my $cmd     = shift;
    my $task    = shift;
    my $param   = shift;
    my $out;

    exists $mod2load{uc($cmd)} || return "Comando $cmd non definito";

    my $str = $mod2load{uc($cmd)} . '::' . $CONST_DEFAULTMET . '($cmd, $task, $param)';

    $out = eval $str;

}


1;


__END__

=head1 NAME

Opec::Command::Dispatcher - Livello di astrazione per l'interfaccia di comunicazione verso l'esterno

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
