#
# $Id: Sign.pm,v 1.29 2013/03/28 16:59:03 ldivizio Exp $
#
# Project       OpenPec
# file name:    Sign.pm
# package:      Opec::Sign
#
# DESCRIPTION
# Gestione firme
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
# 23/04/2004 - Fanton Flavio
#   - Risolto il bug sulla firma e verifica
#   - Ottimizzato il codice
# 11/09/2006 - Fanton Flavio
#   - Risolto bug sulla verifica di mail firmate
# 15/09/2006 - Fanton Flavio
#   - aggiunta gestione CRL
# 04/10/2006 - Fanton Flavio
#   - Aggiunto supporto per interfaccia API openssl verso HSM
# 06/11/2006 - Fanton Flavio
#   - Aggiunto supporto per l'inclusione di + certificati durante il processo di firma
#     e path per i certificati trusted
# 23/07/2007 - Fanton Flavio
#   - Aggiornata la Sign: resituisce 0 se l'oggetto in ingresso e' vuoto
# 16/01/2008 - Fanton Flavio
#   - inserimento del modulo Crypt::SMimeEngine che permette di caricare una sola volta
#     la chiave privata nel modulo HSM e di chiamare direttamente le api di openssl
#     senza chiamate esterne
# 25/01/2008 - Fanton Flavio
#   - eliminata la gestione dei timer
# 04/02/2008 - Fanton Flavio
#   - il modulo diventa ad oggetti perche' ogni processo di openpec abbia una
#     propria istanza di Crypt::SMimeEngine
# 14/03/2008 - Fanton Flavio
#   - corretto valore di ritorno di _verify in caso di chiamata verso
#     Crypt::SMimeEngine::verify con errore
# 24/02/2011 - Fanton Flavio
#   - adattamento per ERACOM
# 17/02/2012 - Fanton Flavio
#   - aggiunto controllo tra header e xml allegato: viene verificato se il tipo
#     di messaggio dichiarato nel header è lo stesso di quello in xml
#     Il controllo è particolarmente critico perchè la soa informazione contenuta in header
#     non è sottoscritta dal gestore quindi facilmente attaccabile
#
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#
package Opec::Sign;


use MIME::Base64;
use Crypt::SMimeEngine qw (&digest &init &sign &verify &getFingerprint &getCertInfo &getErrStr);
use Opec::Conf qw (:ssl :confvars :xml);
use Opec::Lock;
use strict;
use warnings;
use IO::File;
use Time::Local;


# costanti
my $prefix = 'tmpcerts';
my %MONTHS = (  'Jan'=>0,
                'Feb'=>1,
                'Mar'=>2,
                'Apr'=>3,
                'May'=>4,
                'Jun'=>5,
                'Jul'=>6,
                'Aug'=>7,
                'Sep'=>8,
                'Oct'=>9,
                'Nov'=>10,
                'Dec'=>11);
my $fileEmailTmp            = 'emailtmp.out';
my $fileEmailSignedNoHeader = 'emailsignednoheader.out';
my $fileEmailSigned         = 'emailsigned.signed_';  # verra' appeso un contatore
my $fileCertSigner          = 'certsigner_'; # verra' appeso un contatore l'id del processo


#
# Metodo di classe
# COSTRUTTORE
# Verifica:
# . la correttezza del path di openssl
# . la presenza del certificato locale
# . la presenza della chiave privata
#
# IN:   ref. obj a Opec::LdapTool
#
# OUT:  rif. oggetto se ok, stringa d'errore altrimenti
#
sub new {
        my $proto       = shift;
        my $hLdap       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

    $self->{hldap}    = $hLdap || '';
    $self->{errstr}   = '';

    return "Errore nel file di configurazione: controllare il parametro \$certFile\n"
        if (!defined $certFile || !(-r $certFile));

    if($engine && $engine ne 'openssl' && $engine ne 'ERACOM' ){
        return "Errore nel file di configurazione: ".
            "chiave privata non impostata; verificare il parametro \$filePrivateKey\n"
            unless($filePrivateKey);
        return "Errore nel file di configurazione: ".
            "libreria engine non esistente o non leggibile, verificare il parametro \$lib_engine\n"
            if( !-r $lib_engine );
    }else{
        return "Errore nel file di configurazione: controllare il parametro \$filePrivateKey"
            if (!defined $filePrivateKey || !(-r $filePrivateKey));
    }

    if($engine && $engine eq 'ERACOM' ){
        $ENV{'PKCS_SLOT_0_PIN'} = $lib_engine;
    }

    # inizializzo il modulo
    my $tmp_engine     = $engine;
    my $tmp_lib_engine = $lib_engine;
    if($engine && $engine eq 'ERACOM'){
        $tmp_engine = 'openssl';
        undef $tmp_lib_engine}
    if( init($CApath, $certFile, $filePrivateKey, $add_certs, $tmp_engine, $tmp_lib_engine) ){
        return "Errore durante il caricamento dell'engine $tmp_engine: ".getErrStr()."\n";
    }

    $self;
}


# Metodo di istanza
# Firma Opec::Message
# parameters:
#   Opec::Message/arr. ref. Opec::Message
# returns:
#   arr. ref. di Opec::Message firmati o 0 in caso di errore
#
sub Sign {
    my $self = shift;
    my $msgs = shift;
    $self->{errstr} = '';

    if( !$msgs ){
        $self->{errstr} = 'Niente da firmare';
        return 0}

    $msgs = [$msgs] if(ref($msgs) ne 'ARRAY');

    my @tmp = @$msgs;
    for (my $i = 0; $i <= $#tmp; $i++) {
        $tmp[$i] = $self->_sign ($tmp[$i],$i);
        return 0 if ($tmp[$i] == 0)}

    return \@tmp;
}


# Metodo di classe
# Esegue fork ed exec catturando STDERR e STDOUT.
# PARAM IN: arr. ref costituente il comando con gli argomenti da eseguire
# PARAM OUT: (exit_code, stderr) in contesto lista o
#      exit_code in contesto scalare
#
sub _exec {
    my (@arg) = @_;
    my($child, $res);

    defined($child = open(OUT, '-|'))
        or return (-1,$!);

    if($child) {
        $res = join('', <OUT>);
        close(OUT) or not $! or return(-1,$!);
    } else {
        select(STDERR); $| = 1;
        select(STDOUT); $| = 1;
        open(STDERR, ">&STDOUT") or die "Impossibile redirigere STDERR su STDOUT";

        exec(@arg) or die "Impossibile eseguire exec";
    }

    return($?, $res);
}

# Metodo di istanza
# Firma Opec::Message
# parameters:
#   Opec::Message - Original Message
# returns:
#   Opec::Message, oppure 0 in caso di errore avvalorando $self->{errstr}
#                  con la stringa d'errore adeguata
#
sub _sign {
    my $self = shift;
    my $originalMsg = shift;
    my $counter     = shift;

    $self->{errstr} = '';

    # per la gestione dei file temporanei (da fare)
    if (!$originalMsg->{tempdir}) {
        $self->{errstr} = 'Directory temporanea per la mail da firmare non definita';
        return 0}
    mkdir $originalMsg->{tempdir} unless(-d $originalMsg->{tempdir});

    # $body e' la mail da 'passare' a openssl
    my $body = $originalMsg -> mime_entity;

    # $copy e' una copia dello header originale
    my $copy = $body -> head -> dup;

    # rimuovo i campi dello header che non mi servono per la firma
    my @arr = $body -> head -> tags;
    foreach (@arr) {
        if ($_ !~ /^MIME/ && $_ !~ /^Content/) {
                $body -> head -> delete ($_);
        } else {
            $copy -> delete ($_)}}

    # scrivo sul file la mail da firmare (decurtata dei campi dello header)
    if (open FHL, "> ".$originalMsg->{tempdir}.'/'.$fileEmailTmp) {
        $body -> print (\*FHL);
        close FHL;
    } else {
        $self->{errstr} = 'Impossibile creare il file contenente la mail da firmare';
        return 0}

    if($engine && $engine eq 'ERACOM'){
        my @arguments;

        push @arguments, $openSSLPath;
        push @arguments, 'smime';
        push @arguments, '-sign';
        push @arguments, '-engine';
        push @arguments, $engine;
        push @arguments, '-signer';
        push @arguments, $certFile;
        push @arguments, '-inkey';
        push @arguments, $filePrivateKey;
        push @arguments, '-in';
        push @arguments, $originalMsg->{tempdir}.'/'.$fileEmailTmp;
        push @arguments, '-out';
        push @arguments, $originalMsg->{tempdir}.'/'.$fileEmailSignedNoHeader;

        my ($ec, $res) = _exec (@arguments);

        unless(defined($ec) && $ec == 0) {
            $self->{errstr} = "Errore durante la firma (engine $engine): $ec - $res";
            return 0}
    }else{
        if( Crypt::SMimeEngine::sign($originalMsg->{tempdir}.'/'.$fileEmailTmp,
                                     $originalMsg->{tempdir}.'/'.$fileEmailSignedNoHeader) ){
            $self->{errstr} =       "Errore durante la firma (engine $engine): ".getErrStr();
            return 0
        }
    }

    # aggiungo in cima l'header originale alla mail firmata su un nuovo file
    unless(open FHL, "> ".$originalMsg->{tempdir}.'/'.$fileEmailSigned.$counter){
        $self->{errstr} = 'Impossibile creare il file '.$fileEmailSigned.$counter;
        return 0}
    $copy -> print (\*FHL);

    unless(open FHI, $originalMsg->{tempdir}.'/'.$fileEmailSignedNoHeader){
        $self->{errstr} = "Impossibile creare il file $fileEmailSignedNoHeader";
        return 0}
    while(<FHI>){print FHL $_}
    close FHI;
    close FHL;

    my $fh = new IO::File "< ".$originalMsg->{tempdir}.'/'.$fileEmailSigned.$counter;
    unless($fh) {
        $self->{errstr} = 'Impossibile aprire il file contenente la mail firmata';
        return 0}

    $originalMsg -> mail_text ($fh);
    $originalMsg -> header_clean();

    $originalMsg;
}

# Metodo di istanza
# Controlla la mail ed il certificato
# parameters:
#   Opec::Message - Messaggio con certificato
#       string opz. - tipo messaggio che mi devo aspettare
#            (valore dell'attributo del header X-Ricevuta o X-Trasporto)
# returns:
#   1 - successo / altrimenti - errore
#
sub Verify {
    my $self = shift;
    my $originalMsg = shift;
    my $msgType     = shift;
    my $mimeEntity  = $originalMsg -> mime_entity;
    my $retcode;
    $self->{errstr} = '';

    # FLUSSO
    # . verifica presenza del certificato
    # . verifica OSSL
    # . verifica scadenza
    # . check CRL
    # . verifica email
    # . check presenza del certificato su indice
    # . opz. check tipo messaggio (l'header deve essere coerente con daticert.xml)

    # controllo l'esistenza dell'allegato MIME::Entity
    my @orig_mime_parts = $mimeEntity->parts;
    my @arrSign = grep { _keep_part($_) } @orig_mime_parts;
    if($#arrSign != 0){
        $self->{errstr} = 'Messaggio senza certificato';
        return 0}

    # verifica openssl
    my $certSigner = $self->_verify($originalMsg);
    return 0 if( !defined $certSigner );

    # controllo che il certificato del signer sia presente sull'indice
    my $hashMailCert = $self->getCertificateHash( $certSigner );
    return 0 unless( $hashMailCert );

    my @arr;
    eval {@arr = @{$self->{hldap}->getCertificateFromHash($hashMailCert)}};
    if($@ or !@arr){
        my ($email) = @{Opec::Util::getAddresses($mimeEntity->head->get('From'))};
        my $domain = Opec::Util::getDomain($email);
        $self->{errstr} = "Nessun certificato trovato per $domain";
        return 0;}

    if($checkHeaderXml && $msgType){
        my $msgTypeXml = $originalMsg->get_xml_data($XML_ROOTNODE_NAME.'.'.'tipo');
        if(!$msgTypeXml){
                $self->{errstr} = "daticert.xml non presente o malformato";
            return 0;}
        if(lc($msgTypeXml) ne lc($msgType)){
                $self->{errstr} = "Il messaggio risulta malformato: header e xml non coerenti - header = $msgType e xml = $msgTypeXml";
                return 0;}
    }

    return 1;
}

# Metodo di istanza
# controlla la validita' del certificato e restituisce l'email
# Parametri:
#   Stringa - nome file del certificato
# Return value:
#   Indirizzo email oppure 0 in caso d'errore avvalorando $self->{errstr}
#                   con la stringa d'errore adeguata
#
sub getCertificateEmail {
    my $self = shift;
    my $certFile = shift;
    $self->{errstr} = '';

    my $hr = getCertInfo($certFile);

    if(!ref($hr) or ref($hr) ne 'HASH' ){
        $self->{errstr} = getErrStr();
        return 0
    }

    my $email = $hr->{'v3_email'};
    my $notBefore = $hr->{'startdate'};
    my $notAfter = $hr->{'enddate'};
    my $cert_serial = $hr->{'serial'};

    if( !Opec::Util::checkRFC2822($email) ){
        $self->{errstr} = 'Formato email del certificato non corretto: certificato non valido';
        return 0
    }

    $notBefore     =~ /^(\w+)\s+(\d+)\s+(\d+):(\d+):(\d+)\s+(\d+)/;
    my $notBeforeSecs = timegm ($5, $4, $3, $2, $MONTHS{$1}, $6);

    $notAfter      =~ /^(\w+)\s+(\d+)\s+(\d+):(\d+):(\d+)\s+(\d+)/;
    my $notAfterSecs  = timegm ($5, $4, $3, $2, $MONTHS{$1}, $6);

    if (!($notBeforeSecs < time && time < $notAfterSecs)) {
        $self->{errstr} = 'Certificato scaduto';
        return 0}

    # check CRL
    my %hCrl;
    my $fh = new FileHandle($PATH_CACHE.$CRL_filename);
    if( ref($fh) ){
        locksh($fh);
        tie %hCrl, 'DB_File', $PATH_CACHE.$CRL_filename;
        if(defined $hCrl{$cert_serial} && time > $hCrl{$cert_serial}) {
            $self->{errstr} = "Certificato revocato";
            return 0}
        untie %hCrl;
        unlock($fh);
        $fh->close;
        undef %hCrl;
    }

    $email;
}

# Metodo di classe
# Restituisce l'hash del file fornito in ingresso
# parameters:
#   1. path completo di nome file
# returns:
#   hash se ok, oppure 0/undef in caso di errore
#
sub getHash {
    my $msg = shift;
    my $out;

    unless(defined $msg) {
        return 0;
    }

    $out = digest($msg, $openSSLDigest);

    unless(defined($out)) {
        return 0;
    }

   return $out;
}

# Metodo di istanza
# Restituisce l'hash del certificato (path file) fornito in ingresso
# parameters:
#   1. path completo di nome file del certificato
# returns:
#   hash se ok, oppure 0/undef in caso di errore avvalorando $self->{errstr}
#               con la stringa d'errore adeguata
#
sub getCertificateHash {
    my $self = shift;
    my $msg = shift;
    my $out;
    $self->{errstr} = '';

    unless(defined $msg){
        $self->{errstr} = 'Nessun parametro fornito per la generazione del hash del file';
        return 0}

    $out = getFingerprint($msg, $openSSLDigest);

    unless( defined($out) ) {
        $self->{errstr} = 'Errore durante la generazione del hash del file: '. $msg;
        return 0}

    $self->{errstr} = 'HASH generato con successo';
    $out =~ s/://go;
    return $out;
}


# Metodo di istanza
# Controlla il cerificato
# parameters:
#   Opec::Message - Messaggio con certificato
# returns:
#   path file certificato del signer ok, oppure undef in caso di errore avvalorando $self->{errstr}
#         con la stringa d'errore adeguata
#
sub _verify {
    my $self = shift;
    my $originalMsg = shift;
    my $mimeEntity = $originalMsg -> mime_entity;
    my $cert_signer = $originalMsg->{tempdir}.'/'.$fileCertSigner.$$;
    $self->{errstr} = '';

    my $filename = $originalMsg->tempfiledir;
    if (!$filename) {
        $self->{errstr} = 'Opec::Message->tempfiledir null!';
        return}

        if( Crypt::SMimeEngine::verify($filename, $cert_signer, $noverify) ){
        $self->{errstr} =   "Errore durante la verifica (engine $engine): ".getErrStr();
            return}

    # controllo email del certificato con quella dell'oggetto SMIME
    my $issuerEmail = $self->getCertificateEmail($cert_signer);
    if (!$issuerEmail) {
        # $self->{errstr} settato da getCertificateEmail
        return}

    my $from = Opec::Util::getAddresses($mimeEntity->head->get('From'))->[0];
    if ($issuerEmail ne $from) {
        $self->{errstr} = 'L\'indirizzo email del certificato '.
                          'non corrisponde al mittente della mail';
        return}


    $self->{errstr} = 'Messaggio verificato con successo';
    return $cert_signer;
}

sub error {
    my $self = shift;
    return $self->{errstr};
}


# Metodo di classe
# utilizzata da _checkSign per verificare la presenza della firma
# come componente del messaggio
#
sub _keep_part {
    my $objEntity   = shift;
    my $objEntityHead = $objEntity->head;
    my $out = 0;

    #--- analizzo l'header
    # 0. verifico che Content-Transfer-Encoding=base64
    # 1. verifico che Content-Type debba essere application/x-pkcs7-signature
    # 2. se Content-Disposition esiste verifico che contenga filename = smime.p7s
    # 3. altrimenti verifico che Content-Type contenga name = postacert.eml
    my $attrContentType = Opec::Util::myChomp ($objEntityHead->get('Content-Type'));
    my $attrContentDisposition = Opec::Util::myChomp ($objEntityHead->get('Content-Disposition'));
    my $attrContentTransfertEnc = Opec::Util::myChomp ($objEntityHead->get('Content-Transfer-Encoding'));

    if( $attrContentTransfertEnc &&
        $attrContentTransfertEnc eq 'base64' &&
        $attrContentType){
        my ($ctFirst, $ctSecond) = split /;/, $attrContentType;
        return 0 if(lc($ctFirst) ne 'application/pkcs7-signature' &&
                    lc($ctFirst) ne 'application/x-pkcs7-signature');

        if($attrContentDisposition){
            my $cdSecond = (split /;/, $attrContentDisposition)[1];
            if($cdSecond){
                $cdSecond =~ /\s*(\S+\b)\s*=\s*\"?(\S+\b)\"?\s*/s;
                $out=1 if(lc($1) eq 'filename' && $2 eq 'smime.p7s')}}
        if(!$out && $ctSecond){
            $ctSecond=~ /\s*(\S+\b)\s*=\s*\"?(\S+\b)\"?\s*/s;
            $out=1 if(lc($1) eq 'name' && $2 eq 'smime.p7s')}
    }

    $out
}



1;

__END__

=head1 NAME

Opec::Sign - Gestione dei messaggi SMIME

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
