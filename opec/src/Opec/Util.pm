#
# $Id: Util.pm,v 1.40 2012/04/18 13:18:17 ldivizio Exp $
#
# Project       OpenPec
# file name:    Util.pm
# package:      Opec::Util
#
# DESCRIPTION
# Modulo di utilita'
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification: 20/10/2004 - Fanton Flavio
#               actualDate affetto da mese-1; segnalato da Zuin
#   21/10/2004 - Fanton Flavio - segnalazione e soluzione Zuin
#       Aggiornamento timezone
#   04/01/2005 - Fanton Flavio - segnalazione e sentiti
#                ringraziamenti a Stefano Brandimarte [stevens@ced.it]
#       Aggiornata headerMsgId() in modo che il Message-ID sia
#       racchiuso fra <> (rfc 2822).
#   21/09/2005 - Fanton Flavio
#       aggiunta gestione mail di servizio del MTA
#   04/02/2007 - Fanton Flavio
#       aggiornata sanity_check per gestire liste di indirizzi di servizio
#   24/02/2007 - Fanton Flavio
#       exportata la funz getDomain
#   24/02/2007 - Fanton Flavio
#       exportata la funz checkRFC2822
#   09/11/2007 - Fanton Flavio
#       Corretto bugfix sul timezone
#   01/02/2010 - Fanton Flavio
#       Bugfix sui destinatari multipli
#   20/12/2010 - Fanton Flavio
#         - revisione di actualDate, actualTime, actualDateTime e timezone
#   26/05/2011 - Fanton Flavio
#         - aggiunta compatibilita' con log xml
#   09/06/2011 - Fanton Flavio
#               - sostituito delTask con delTaskSafe nei metodi delRMC*
#   23/06/2011 - Fanton Flavio
#               - corretto @WDAYS
#       26/08/2011 - Fanton Flavio
#               - dettagliato la sanity_check
#       29/10/2011 - Fanton Flavio
#               - bugfix su doLogXml in caso di anomalia di messaggio
#       - bugfix su xmlTreeToString: formato log xml errato e sostituzione
#         < e >
#       07/11/2011 - Fanton Flavio
#               - aggiunto il controllo su esistenza destinatario primario (segnalazione HP)
#
#    18/04/2012 - Luca Di Vizio
#       - Bugfix su timezone (estratto dal package Time::Zone)
#
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#
package Opec::Util;


use strict;
use Mail::Address;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    %EXPORT_TAGS = ();
    @EXPORT = ();
    @EXPORT_OK = qw( &doLog &doLogXml &msgId &headerMsgId &prolongTimer &splitAddress
                     &unquoteRfc2821Local &sanity_check &actualDateTime
                     &actualDate &actualTime &myChomp &getAddresses &timezone
                     &enqRMC &delRMC &delRMC12 &delRMC24 &enqSPINLOG &delSPINLOG &getDomain
                     &checkRFC2822);
}

use subs @EXPORT_OK;
use POSIX qw(WEXITSTATUS WIFEXITED WTERMSIG WIFSIGNALED);
use Errno qw(ENOENT);

use Opec::Conf qw( :confvars :platform :log :scheduler :const :response :xml);
use Opec::Log qw( &request_id &writeLog &writeLogXml );
use Opec::Command qw( enqTask delTask delTaskSafe );
use Time::Local 'timelocal_nocheck';


my $IDLOG_SYSTEM        = '0000000.00000000000000.00000.00.1';
my $IDLOG_LEGAL         = '0000000.00000000000000.00000.00.2';
my $IDLOG_XML           = '0000000.00000000000000.00000.00.3';
my $msgid_system        = "$IDLOG_SYSTEM.$idServer\@00000.00";
my $msgid_legal         = "$IDLOG_LEGAL.$idServer\@00000.00";
my $msgid_xml                   = "$IDLOG_XML.$idServer\@00000.00";
my $name_task_loglegal  = $msgid_legal.'@@'.$CONST_CMD_SPINLOG_LEGAL.'@';
my $name_task_logsystem = $msgid_system.'@@'.$CONST_CMD_SPINLOG_SYSTEM.'@';
my $name_task_logxml    = $msgid_xml.'@@'.$CONST_CMD_SPINLOG_XML.'@';

# costanti utilizzate da actualDateTime ()
my @WDAYS = ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
my @MONTHS = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
       'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

#
# Scrive il messaggio di log sul sistema prescelto
# utilizzando Opec::Log::writeLog
#
# IN: 1. intero - livello del messaggio di log
#        (vedi file conf sezione III per i valori disponibili)
#     2. messaggio da loggare
#
# OUT: -
#
sub doLog {
    my($level, $errmsg) = @_;

    $level = 0  unless $daemonize;
    my $msgId = msgId() ? request_id() . msgId() : 0;
    &writeLog($msgId, $level, $errmsg)
        if($level <= $SYSTEM_LOGFILE_LEVEL);
}


#
# Scrive il file xml sul sistema prescelto
# utilizzando Opec::Log::writeLogXml
#
# IN:
#       1. arr di Opec::Message
#       2. rif hash con
#       key:   path tag/attributo da aggiungere/modificare
#       value: valore tag/attributo
#
# OUT: -
#
#
# * il formato del path e' del tipo
# tag1#tag1.1
# tag2#tag2.2.attributo
#
# * se il valore e' # il tag/attributo viene cancellato
#
sub doLogXml {
    return unless $XML_DO_LOG;
    my $obj = shift || return;

    $obj = [$obj] if(ref($obj) ne 'ARRAY');

        foreach my $s_obj (@$obj){
                next if $s_obj->name eq $OPEC_HEADER_VALUE_DOCTRANSP_ANOM; # salto le anomalie di messaggo xche' non hanno l'allegato xml
                my $strXml;
            if(%OPEC_XML_ADD_MAILHEADER){
                # * modifico XML in funzione di $h_param
                my $h = $s_obj->get_xml_tree;
                next unless $h; # XXX non ha trovato l'allegato XML - non dovrebbe accadere
                        # ne creo una copia temporanea
                        my $h_copy;
                        {
                                my %h_ = %$h;
                                my %h_copy_tmp = %h_;
                                $h_copy = \%h_copy_tmp;
                        }

                        # aggiungo alla copia i tag/attributi
                        foreach my $xml_path (keys %OPEC_XML_ADD_MAILHEADER){
                                my $nonode;
                                my $header_tmp = $OPEC_XML_ADD_MAILHEADER{$xml_path};
                                next unless defined $header_tmp;
                                my $header_val = $header_tmp eq '#' ? '#' : &myChomp($s_obj->header->get($header_tmp));
                                my ($route,$attr) = split(/\./,$xml_path);
                                my @path = split(/\#/,$route);

                                my $h_last_node = $h_copy->{$XML_ROOTNODE_NAME};
                                foreach my $node (@path){
                                        if( !exists $h_last_node->{$node} ) {$nonode=1;last}
                                        $h_last_node = $h_last_node->{$node};
                                }
                                if($nonode){
                                        # tag da aggiungere
                                        $h_last_node->{pop @path} = {'0' => $header_val};
                                        next}

                                if($header_val eq '#'){
                                        # cancello il tag/attributo
                                        if($attr){delete $h_last_node->{$attr}}else{delete $h_last_node->{0}}
                                        next}

                                if( $attr ){
                                        $h_last_node->{$attr} = $header_val
                                }else{
                                        $h_last_node->{0} = $header_val
                                }
                        }

                        # ottengo la string XML dato l'albero di parsing
                        $strXml = xmlTreeToString($h_copy);

            }else{
                        $strXml = $s_obj->xml_local(); # xml creato localmente
                        if( !$strXml ){
                                # xml remoto
                                my $pathxml = $s_obj->_getXmlFile();
                                next unless $pathxml;
                        {
                                local $/=undef;
                            open FILE, $pathxml or return;
                            $strXml = <FILE>;
                            close FILE;
                        }
                        }
                        $strXml =~ s/(?:\n)|(?:\015\012)//gs;
                        $strXml =~ s/>\s+</></gs if $XML_TRIMWS;        # trim
            }

            # Inserisco, se presenti, i dati relativi alla dimensione del
            # messaggio.
            my $mail_size = $s_obj->mail_size();
            if ($mail_size) {
                my $tag = "<$OPEC_XML_TAG_DIMENSIONE>$mail_size</$OPEC_XML_TAG_DIMENSIONE>";
                $strXml =~ s/<$OPEC_XML_TAG_DATI>/<$OPEC_XML_TAG_DATI>$tag/;
            }

            writeLogXml($strXml);
        }
}


#
# Dato un albero di parsing xml, restituisce la stringa corrispondente
#
# IN: obj xml
#
# OUT: stringa in formato xml
#
sub xmlTreeToString {
    my $h_tree     = shift;
    my $tag_name   = shift;
        my $xml_header = '<?xml version="1.0" encoding="UTF-8"?>';
        my $xml_local;
        my @xml_ext;

        my $tag_value;
        my $tag_attr = {};
        foreach my $local_node (keys %$h_tree){
            if( ref($h_tree->{$local_node}) && ref($h_tree->{$local_node}) eq 'ARRAY' ){
                    foreach my $local_node_elem (@{$h_tree->{$local_node}}){
                if( ref($local_node_elem) && ref($local_node_elem) eq 'HASH' ){
                    # nodo
                    push( @xml_ext,xmlTreeToString($local_node_elem,$local_node));
                    next}
                 if( $local_node eq '0' ){
                    # valore
                    $tag_value = $local_node_elem;
                    $tag_value =~ s/</&lt;/g;
                    $tag_value =~ s/>/&gt;/g;
                    next}
                                 # attributo
                 $tag_attr->{$local_node} = $local_node_elem;
                        }
                }else{
                    if( ref($h_tree->{$local_node}) && ref($h_tree->{$local_node}) eq 'HASH' ){
                            # nodo
                            push(@xml_ext,xmlTreeToString($h_tree->{$local_node},$local_node));
                                next}
                        if( $local_node eq '0' ){
                            # valore
                                $tag_value = $h_tree->{$local_node};
                                $tag_value =~ s/</&lt;/g;
                $tag_value =~ s/>/&gt;/g;
                                next}
                    # attributo
                        $tag_attr->{$local_node} = $h_tree->{$local_node};
                }
    }

    $xml_local = $tag_name ? '<'.$tag_name : $xml_header;
    foreach (keys %$tag_attr){$xml_local .= ' '.$_.'="'.$tag_attr->{$_}.'"'}
        $xml_local .= '>' if $tag_name;
        $xml_local .= $tag_value ? $tag_value : join('',@xml_ext);
        $xml_local .= '</'.$tag_name.'>' if $tag_name;

        return $xml_local
}


#
# Setta o restituisce un id del messaggio
# da utilizzare internamente (non per l'header delle mail!)
#
# IN: 1. opz. - nuovo id del messaggio
#
# OUT: id del messaggio (se precedentemente settato)
#
use vars qw($opecTaskId);  # internal message id (accessible via &msgId)
sub msgId (;$) {
    if (@_) {   # set, if argument present
        $opecTaskId = shift;
        $0 = $opecTaskId ?
             "opecd (child-$opecTaskId)":
             "opecd (child-ready)";
    }
    $opecTaskId || 0;  # return current value
}


#
# Genera il "Message-ID" secondo le specifiche
#
# IN: 1. contatore
# OUT:  message Id
#
sub headerMsgId {
    my $count       = shift;    # id per doc. prodotti localmente
    my $mycount;
    my $strOut;
    my @now         = localtime(time);
    my $datefmtd    = sprintf ("%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",
                                $now[5]+1900,
                                $now[4]+1,
                                $now[3],
                                $now[2],
                                $now[1],
                                $now[0]);

    if( defined $count ){
        $mycount = ref($count)? $$count++ : $count}
    # MessageID: NomeApplicazioneEVersione + TimeStampAlSec +
    #            IdServer + Contatore + -contatore @ Dominio
    # MessageID: NomeApplicazioneEVersione + TimeStampAlSec +
    #            IdServer + Contatore @ Dominio
    defined $mycount?sprintf(   "%s.%s.%s.%2.2d.%s@%s",
                                $myversion,
                               $datefmtd,
                               msgId(),
                               $mycount,
                               $idServer,
                               $myDomain):
                     sprintf(   "%s.%s.%s.%s@%s",
                               $myversion,
                               $datefmtd,
                               msgId(),
                               $idServer,
                               $myDomain)
}


#
# Gestisce il tempo di vita del singolo processo
# (la prima chiamata lo inizializza con $childTimeout
# da file conf)
#
# IN: 1. nome sezione della chiamata
#     2. (secondi dopo i quali viene lanciato SIGALRM
#         - default 60sec.)
#
# OUT: -
#
sub prolongTimer ($;$) {
    my($which_section,$child_remaining_time) = @_;
    if (!defined($child_remaining_time)) {
        $child_remaining_time = alarm(0);  # check how much time is left
    }
    doLog(5,"prolongTimer in $which_section: ".
            "tempo rimanente = $child_remaining_time s");

    # devel
    #$child_remaining_time = 1000  if $child_remaining_time < 1000;

    # production
    $child_remaining_time = 60  if $child_remaining_time < 60;

    alarm($child_remaining_time); # restart/prolong the timer
}


# unquote_rfc2821_local() strips away the quoting from the local part
# of an external (quoted) mailbox address, and returns internal (unquoted)
# mailbox address, as per rfc2821.
#
# Internal (unquoted) form is used internally by AMaViS and other mail sw,
# external (quoted) form is used in SMTP commands and message headers.
#
sub unquoteRfc2821Local ($) {
    my($mailbox) = @_;
    my($taint) = substr($mailbox,0,0);
    # the angle-bracket stripping is not really a duty of this subroutine,
    # as it should have been already done elsewhere, but for the time being
    # we do it here:
    $mailbox = $1.$taint  if $mailbox =~ /^ \s* < ( .* ) > \s* $(?!\n)/xs;
    my($localpart,$domain) = splitAddress($mailbox);
    $localpart =~ s/ " | \\(.) | \\$ /$1/xsg;  # unquote quoted-pairs
    $localpart . $domain;
}

# Splits unquoted fully qualified e-mail address, or an address
# with missing domain part. Returns a pair: (localpart, domain).
# The domain part (if nonemty) includes the '@' as the first character.
# If the syntax is badly broken, everything ends up as the localpart.
# The domain part can be an address literal, as specified by rfc2822.
# Parametri:
#   Stringa - indirizzo/i email
# Return Value:
#   Array - (localpart, domain)
sub splitAddress ($) {
    my($mailbox) = @_;
    my($taint) = substr($mailbox,0,0);
    $mailbox =~ /^ (.*?) ( \@ (?:  \[  (?: \\. | [^\]\\] )*  \]
                |  [^@"<>\[\]\\\s] )*
             ) $(?!\n)/xs ? ($1.$taint, $2.$taint) : ($mailbox,'');
}

# Prende la parte email dell'indirizzo. Esempio:
#     con Luca <luca@luca.it> ritorna luca@luca.it
# Parametri:
#     Stringa - Indirizzo email
#sub getEmailPart ($) {
#    $_ =~ /.*<(.+)>|(.+)/;
#    $1;
#}

# Effettua un controllo rfc822 dell'email
# Parametri:
#   String - Email
# Return value:
#   1 - rfc822 valida
sub checkRFC2822 ($) {
    my @x = splitAddress(shift);
    ($x[0] && $x[1])?1:0;
}

#
# Controlla se la mail e' formalmente corretta.
# Descrizione:
#    Fa un controllo RFC822 dei campi "From" e "To".
#    I sender e recipts dell'envelope devono combaciare con
#    quelli del campo "From" e "To".
# IN: obj Opec::Message
# OUT: 0 ok, stringa descrittiva dell'errore altrimenti
#
sub sanity_check ($) {
    my $objMsg = shift;
    return $SANITY_CHECK_DEFAULT if(!ref($objMsg));
    my $mailHeader = $objMsg->header;

    # check mittente
    # . deve esistere
    # . deve essere RFC 2822 compliant
    # . deve coincidere con quello in envelop
    my ($headerFrom) = @{getAddresses ($mailHeader->get ('From'))};
    return $SANITY_CHECK_NO_FROM if !$headerFrom;
    return $headerFrom.$SANITY_CHECK_NO_RFC822 if !checkRFC2822($headerFrom);

    # faccio passare le mail di servizio del MTA
    # hanno sender (forward path) vuoto e From specifico
    if( !$objMsg->sender->mailAddress &&
        ($OPEC_DEFAULT_EMAIL_SENDER_MTA && grep { lc($headerFrom) eq lc($_) } @$OPEC_DEFAULT_EMAIL_SENDER_MTA) ){

        $objMsg->sender($headerFrom)
    }else{
        return $SANITY_CHECK_NO_RPATH if !$objMsg->sender->mailAddress;
        return $SANITY_CHECK_DIS_RPATH_FROM if ($headerFrom ne $objMsg->sender->mailAddress);
    }

    # check destinatari (To e Cc)
    # . deve esistere almeno un destinatario primario
    # . deve esisterne almeno uno
    # . devono essere RFC 2822 compliant
    # . coerenza con envelop
    my ($headerTo) = @{getAddresses ($mailHeader->get ('To'))};
    return $SANITY_CHECK_NO_TO if !$headerTo;
    return $headerTo.$SANITY_CHECK_NO_RFC822 if !checkRFC2822($headerTo);

    my @headerToCc;
    map push(@headerToCc, @{getAddresses(join(',',$mailHeader->get($_)))}),
        ('To','Cc');
    return $SANITY_CHECK_NO_HEADER_DEST if !@headerToCc;

    my @objRecips;
    return $SANITY_CHECK_NO_FPATH if( !$objMsg->recips() || !(@objRecips = @{$objMsg->recips()}));

    my %hHeaderToCc;
    map $hHeaderToCc{lc($_)}=1,@headerToCc;
    return $SANITY_CHECK_BCC if(@objRecips != keys %hHeaderToCc);

    foreach ( @{$objMsg->recips()} ){
        return $_.$SANITY_CHECK_NO_RFC822 if (!checkRFC2822($_->mailAddress));
        return $SANITY_CHECK_DIS_FPATH_TOCC if(!exists $hHeaderToCc{lc($_->mailAddress)});
    }

    0
}

# confronta una email con un'altra
# param0: string - email
# param1: string - email
# return value: 1 if same
sub _sameEmails ($$) {
    my $email1 = shift;

    $email1 =~ /(.+)\@(.+)/;
    my @email1Parts = ($1, $2);

    return _compareEmail (\@email1Parts, shift);
}

# funzione privata
# param0: array of email parts
# param1: email
sub _compareEmail ($$) {
    my $email1Parts = shift;
    my $email2 = shift;

    $email2 =~ /(.+)\@(.+)/;
    my @email2Parts = ($1, $2);

    if (lc($email1Parts->[1]) ne lc($email2Parts[1])) {
        return 0;
    }

    if ($localpart_is_case_sensitive) {
        if ($email1Parts->[0] eq $email2Parts[0]) {
            return 1;
        }
    } else {
        if (lc($email1Parts->[0]) eq lc($email2Parts[0])) {
            return 1;
        }
    }

    return 0;
}


# Returns actual datetime like:
# Tue, 21 Oct 2003 13:41:19 +0100 (the +0100 is fixed)
sub actualDateTime {
    my $epoch = shift || time;
    my @ts = localtime($epoch);
    sprintf ("%s, %d %s %d %02d:%02d:%02d %s",
        $WDAYS[$ts[6]],
        $ts[3],
        $MONTHS[$ts[4]],
        $ts[5]+1900,
        $ts[2],
        $ts[1],
        $ts[0],
        timezone($epoch));
}

# Returns actual date: DD/MM/AAAA
sub actualDate {
        my $epoch = shift || time;
    my @date = localtime($epoch);
    sprintf ("%02d/%02d/%d",
        $date[3],
        $date[4]+1,
        $date[5]+1900);
}

# Returns actual time: HH:MM:SS (24 hour time)
# Parameters:
#       secondi epocali da cui estrarre l'ora
sub actualTime {
    my $epoch = shift || time;
    my @ora = localtime($epoch);
    sprintf ("%02d:%02d:%02d",
        $ora[2],
        $ora[1],
        $ora[0]);
}

# Better than chomp(). Removes \015, \012 and \n
# parametri:
#   string
sub myChomp ($) {
    $a = shift;
    if (defined $a) {
        $a =~ s/\015|\012|\n//gxs;
    } else {
        #print "\n>>>>>>>> Error in myChomp con valore UNDEF <<<<<<<<<\n";
    }
    $a;
}

# Restituisce un array di indirizzi email estratti
# dai campi dell'header To, Cc e Bcc
#
# Parametri:
#   stringa - una email o un campo estratto dall'header
# Return value:
#   array ref - lista di indirizzi email RFC822 compliant
sub getAddresses ($) {
    my @emails = Mail::Address->parse(shift);
    my @array;
    foreach (@emails)
        {push @array, $_->address}
    \@array;
}

# Restituisce il dominio di un'email
# Parametri:
#   stringa - email
# Return value:
#   stringa - dominio
sub getDomain ($) {
    my $email = Mail::Address->new("x", shift);
    $email -> host;
}

# Calcola il fuso orario attuale
# IN: opz. epocale di riferimento
# OUT: 1. Fuso orario espresso in +xx00
#
sub timezone {
    my $timeref = shift || time;
    my $zonesign = '+';

    my (@l) = localtime($timeref);
    my (@g) = gmtime($timeref);

    my $off;

    $off = $l[0] - $g[0] + ($l[1] - $g[1]) * 60 + ($l[2] - $g[2]) * 3600;

    if ($l[7] == $g[7]) {
        # done
    } elsif ($l[7] == $g[7] + 1) {
        $off += 86400;
    } elsif ($l[7] == $g[7] - 1) {
        $off -= 86400;
    } elsif ($l[7] < $g[7]) {
        # crossed over a year boundry!
        $off += 86400;
    } else {
        $off -= 86400;
    }

    my $fuso = $off / 3600;

    $fuso = $fuso < 10 ? $zonesign.'0'.$fuso."00" : $zonesign.$fuso."00";
}

#
# Crea il task SPINLOG (per la rotazione temporale del log )
#
# paramIN:   false log di sistema, true log legale
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
sub enqSPINLOG {
    my $type    = shift;
    my %tasks;

    # per garantire continuita' non devo sovrascrivere precedenti task spinlog
    # quindi prima di effettuare l'enqueue ne verifico l'esistenza
    chdir($OPEC_QUEUE_PATHNAME.'/'.$idServer) || die "Impossibile accedere alla coda $!\n";

    # creo il nome del file (task) senza scadenza (vengono eseguiti sempre)
    # formato: <msg id>@@<nome cmd>@
    if(!$type){
        # log sistema
        return if -e "$OPEC_QUEUE_DIR_OPT/$name_task_logsystem";
        $tasks{ $name_task_logsystem } = [0]
    }elsif($type == 1){
        # log legale
        return if -e "$OPEC_QUEUE_DIR_OPT/$name_task_loglegal";
        $tasks{ $name_task_loglegal } = [1]
    }elsif($type == 2){
        # log xml
        return if -e "$OPEC_QUEUE_DIR_OPT/$name_task_logxml";
        $tasks{ $name_task_logxml } = [2]
    }

    return enqTask(\%tasks);
}

#
# Elimina il task SPINLOG (per la rotazione temporale del log )
#
# paramIN:   false log di sistema, true log legale
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
sub delSPINLOG {
    my $type    = shift;
    my %tasks;

    chdir($OPEC_QUEUE_PATHNAME.'/'.$idServer) || die "Impossibile accedere alla coda $!\n";

    if(!$type){
        # log sistema
        return delTask($name_task_logsystem, 1, 1)
    }elsif($type ==  1){
        # log legale
        return delTask($name_task_loglegal, 1, 1)
    }elsif($type ==  2){
        # log xml
        return delTask($name_task_logxml, 1, 1)
    }
}

#
# Crea i task RMC12 e RMC24 (per la gestione della ricevuta di mancata consegna - 12 e 24 ore)
#
# paramIN:   1. msg id
#            2. oggetto da salvare nel file
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
sub enqRMC ($$) {
    my $msgid   = shift;
    my $obj     = shift;

    # creo il nome del file (task)
    # formato: <msg id>@@<nome cmd>@<scadenza>
    my %tasks;
    $tasks{ $msgid.'@@'.$CONST_CMD_RMC12.'@'._getTimeOut_RMC12() } = $obj;  # originale
    $tasks{ $msgid.'@@'.$CONST_CMD_RMC24.'@'._getTimeOut_RMC24() } = $obj;  # originale

    return enqTask(\%tasks);
}

#
# Elimina i task RMC (per la gestione della ricevuta di mancata consegna - 12 e 24 ore)
#
# paramIN:   false log di sistema, true log legale
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
sub delRMC {
    my $msgid   = shift;
    my $out;

    $out = delTask($msgid.'@@RMC12@',undef,1);
    $out .= delTaskSafe($msgid.'@@RMC24@',undef,1);

    return $out
}

#
# Elimina i task RMC (per la gestione della ricevuta di mancata consegna - 12 ore)
#
# paramIN:   false log di sistema, true log legale
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
sub delRMC12 {
    my $msgid = shift; return delTaskSafe($msgid.'@@RMC12@',undef,1)}

#
# Elimina i task RMC (per la gestione della ricevuta di mancata consegna - 24 ore)
#
# paramIN:   false log di sistema, true log legale
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
sub delRMC24 {
    my $msgid = shift; return delTaskSafe($msgid.'@@RMC24@',undef,1)}

sub _getTimeOut_RMC12 {
    my $POST_TIME = 43200; # 60 * 60 * 12
    #my $POST_TIME = 60;
    time + $POST_TIME
}

sub _getTimeOut_RMC24 {
    # La normativa vuole tre le 22 e le 24 ore. Noi mettiamo 23!
    my $POST_TIME = 82800; # 60 * 60 * 23
    #my $POST_TIME = 180;
    time + $POST_TIME
}


1;

__END__

=head1 NAME

Opec::Util - funzioni di utilita' generale

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
