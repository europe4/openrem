#
# $Id: Timing.pm,v 1.17 2011/05/02 17:01:49 flazan Exp $
#
# Project       OpenPec
# file name:    Timing.pm
# package:      Opec::Timing
#
# DESCRIZIONE: Riporta i tempi di elaboraziona totali e
#              parziali per singola mail
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# 01/02/2008 - Fanton Flavio
#       . gestione di + mail per sigola connessione
# 06/04/2009 - Fanton Flavio
#       . inserimento del metodo setMailSize per salvare la dimensione
#         della mail in bytes
# 26/10/2010 - Fanton Flavio
#               . aggiunte le info per la risposta verso MTA configurabile
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#
package Opec::Timing;
use strict;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    %EXPORT_TAGS = ();
    @EXPORT = ();
    @EXPORT_OK = qw(&init &sectionTime &extraInfo &report &setMailSize &formatMtaResponse);
}
use subs @EXPORT_OK;

use Time::HiRes qw(time);

use vars qw(@timing $setmailsize %extra_info);
use Opec::Conf qw( $TEMPLATE_MTARESPONSE
                                   $OPEC_MTARESPONSE_TAGS_ID
                                   $OPEC_MTARESPONSE_Q_NAME
                                   $OPEC_MTARESPONSE_Q_TSSTART);
use Opec::Util qw( timezone );

#
#  Dato un epoch hires (diversamente se lo calcola)
#  restituisce una stringa con il seguente formato
#  ddMMAAAAhhmmssmilzzzzz
#
sub formatTimeHiRes {
    my $timehires = sprintf("%.3f",shift||time);
    my ($sec,$msec) = split(/\./,$timehires,2);
    my @ora = localtime($sec);
    sprintf ("%02d%02d%04d%02d%02d%02d%03d%s",
        $ora[3],
        $ora[4]+1,
        $ora[5]+1900,
        $ora[2],
        $ora[1],
        $ora[0],
        $msec,
        timezone($sec));
}

#
# Inizializza le tempistiche
# IN: -
# OUT: scalare - numero di elementi nella lista (sempre 1)
#
sub init {
    @timing = ();
    undef $setmailsize;
    undef %extra_info;
    sectionTime('init');
}

#
# Aggiunge l'istante attuale alle tempistiche associandolo
# alla sezione data in ingresso
# IN: scalare           - nome sezione
#     scalare opz.      - key info aggiuntiva
#     scalare opz.      - velue info aggiuntiva (time se undef)
# OUT: -
#
sub sectionTime {
        my $tmp_time = time;
    push(@timing, shift, $tmp_time);
    if(@_){
        my $k = shift;
        my ($k1,$k2) = split(/\./,$k,2);
        $extra_info{$k} = shift||formatTimeHiRes($tmp_time);
        if( !exists $extra_info{"$k1.$OPEC_MTARESPONSE_Q_NAME"} ){
                # imposto il nome del tag se non già impostato
                $extra_info{"$k1.$OPEC_MTARESPONSE_Q_NAME"} = $k1
        }
        if( !exists $extra_info{"$k1.$OPEC_MTARESPONSE_Q_TSSTART"} ){
                # se l'istante di inizio non è definito lo imposto
                # recuperandolo da @timing
                ###$extra_info{"$k1.$OPEC_MTARESPONSE_Q_TSSTART"} = $timing[3]||0;
                $extra_info{"$k1.$OPEC_MTARESPONSE_Q_TSSTART"} = formatTimeHiRes($timing[3])||0;
        }
    }
}

#
# Aggiunge ulteriori info utili per comporre il ritorno verso MTA
#
# IN: scalare           - nome sezione
#     scalare           - key info aggiuntiva
# OUT: -
#
sub extraInfo
        { my($k)=shift||return; $extra_info{$k}=shift }


#
# Imposta la dimensione della mail in ingresso
# IN: scalare - dimensione della mail in bytes
# OUT: -
#
sub setMailSize {
    $setmailsize = shift;
}

#
# Produce il report finale con il tempo d'esecuzione totale e parziale per
# sezione.
# IN: -
# OUT: scalare - report dei tempo per sezione
#
sub report {
    sectionTime('rundown');
    my($notneeded, $t0) = (shift(@timing), shift(@timing));
    my($total) = $timing[$#timing] - $t0;
    if ($total < 0.0000001) { $total = 0.0000001 }
    my(@sections);
    while (@timing) {
            my($section, $t) = (shift(@timing), shift(@timing));
            push(@sections, sprintf("%s: %.0f (%.0f%%)",
                        $section, ($t-$t0)*1000, ($t-$t0)*100.0/$total ) );
            $t0 = $t;
    }

    # gestione di + mail per sigola connessione
    @timing = ();
    undef %extra_info;
    sectionTime('init');

    if($setmailsize){
        sprintf("TIMING (MailIn Size: %s bytes) [total %.0f ms] - %s",
            $setmailsize,
            $total*1000,
            join(", ", @sections));
    }else{
        sprintf("TIMING [total %.0f ms] - %s",
            $total*1000,
            join(", ", @sections));
    }
}


#
# Formatta la stringa di risposta verso MTA
# (se $TEMPLATE_MTARESPONSE è undef restituisce undef senza fare niente)
#
# - TAG disponibili:
# ID     - id della traccia di openpec
# DT     - busta di trasporto
# AM     - busta di anomalia
# RdAC   - ricevuta di accettazione
# RpC    - ricevuta di presa incarico
# RdNAC  - avviso di non accettazione
# RRV    - avviso di rilevazione per virus informatico
# RdC    - ricevuta di consegna (completa,breve,sintetica), avviso di mancata consegna
# RdEC   - avviso di mancata consegna
# RMC    - preavviso di mancata consegna (12/24 ore), mancata cons
#
# Per ogni TAG sono disponibili i seguenti qualificatori:
# name
# direction
# size
# ts_start
# ts_end
#
# * il TAG ID non ha qualificatori
# ** se il tipo di messaggio non e' previsto nessun valore verrà inserito
#
#
# IN:  scalare - template
# OUT: scalare - stringa formattata
#
sub formatMtaResponse {
        my $id_track = shift||'ND';
        my $response = $TEMPLATE_MTARESPONSE;

        return if !defined $TEMPLATE_MTARESPONSE or !%extra_info;

        $extra_info{$OPEC_MTARESPONSE_TAGS_ID} = $id_track;
        $response =~ s/\[(\w+\.?\w+)\]/$extra_info{$1}/g;
        $response
}


1;


__END__

=head1 NAME

Opec::Timing - Modulo di sviluppo - implementazione profiling

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
