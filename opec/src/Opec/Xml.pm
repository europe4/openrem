#
# $Id: Xml.pm,v 1.35 2012/07/06 10:50:47 ldivizio Exp $
#
# Project       OpenPec
# file name:    Xml.pm
# package:      Opec::Xml
#
# DESCRIPTION
# Modulo di utilita' per generare documenti xml
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   21/10/2004 - Fanton Flavio - segnalazione e soluzione Zuin
#       Aggiornamento timezone
#   20/03/2006 - Umberto Ferrara
#       Aggiunte funzioni per ricevute nuove regole tecniche
#   17/05/2006 - Fanton Flavio
#       Aggiunte funzioni per RMC
#   04/05/2006 - Fanton Flavio
#       Corretto tag msgid
#   15/09/2006 - Flavio Fanton
#     - Revisione avvisi virus
#   24/04/2007 - Flavio Fanton
#     - risolto bug sul TAG "ricezione"
#   01/05/2007 - Flavio Fanton
#     - aggiornata la getCertInfo_doctrasp() in modo che se il msg orig
#       non contiene l'header Reply-To viene utilizzato il contenuto
#       del campo From al suo posto
#   12/05/2008 - Flavio Fanton
#     - aggiornata la getCertInfo_rmc() in modo che imposti l'attributo "errore"
#       del tag "postacert" a "altro"
#   15/12/2008 - Flavio Fanton
#     - bugfix su caratteri speciali con codifica diversa da iso-8859-1
#   10/06/2009 - Flavio Fanton
#     - adattamenti in relazione a cambiamenti in Receipt.pm
#   17/06/2010 - Flavio Fanton
#     - bugfix su subject in caso di caratteri non ASCII
#   20/12/2010 - Flavio Fanton
#     - in getCertInfo_errdelivery_virus spostato il tag
#               errore-esteso alla fine
#         - sincronizzati istanti temporali tra xml e body
#     - bugfix su attributo tipo del tag destinatari
#   22/12/2010 - Fanton Flavio
#         - bugfix su subject con errore nella decodifica
#       04/07/2011 - Fanton Flavio
#         - aggiunta $OPEC_XML_RMC_ERRSTR: errore-esteso nel caso di RMC
#       22/08/2011 - Fanton Flavio
#         - sostituita getCertInfo_rmc con getCertInfo_rmc12 e getCertInfo_rmc24
#       per fornire un errore esteso specifico
#
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

package Opec::Xml;


use Opec::Conf qw( :confvars :platform :template :const );
use Opec::Util qw( &actualDate &actualTime &myChomp &splitAddress &timezone &getDomain);
use strict;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    %EXPORT_TAGS = ();
    @EXPORT = ();
    @EXPORT_OK = qw(&getCertInfo_accept
                    &getCertInfo_getload
                    &getCertInfo_delivery
                    &getCertInfo_doctrasp
                    &getCertInfo_anom
                    &getCertInfo_err_delivery
                    &getCertInfo_notaccept
                    &getCertInfo_notaccept_virus
                    &getCertInfo_virusfound
                    &getCertInfo_errdelivery_virus
                    &getCertInfo_rmc12
                    &getCertInfo_rmc24);
}

# costanti
my $XML_TAB     = '    '; # tabulatore
my $XML_HEADER  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>$eol";
my $certInfoXml;

# ricevuta di accettazione
sub getCertInfo_accept {
        my $ts             = shift;
    my $objMsgOrig = shift;

    $certInfoXml = $XML_HEADER;
    _openCertInfo($OPEC_CERTINFO_TYPE_ACCEPT);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);
            _setOggetto( $objMsgOrig->h_subject_utf84xml );
        _closeIntestazione();
        _openDati();
            _setGestoreEmittente ($adminName);
            _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
            _closeData();
            _setIdentificativo ( $objMsgOrig->msgid_new );
            _setIdentificativo_orig ( $objMsgOrig->msgid_orig );
        _closeDati();
    _closeCertInfo();
    $certInfoXml;
}

# ricevuta di non accettazione
sub getCertInfo_notaccept {
        my $ts             = shift;
    my $objMsgOrig = shift;
    my $errType    = shift;
    my $errString  = shift;
    $certInfoXml = $XML_HEADER;
    $errType ||= $OPEC_CERTINFO_ERR_VALUES_ALTRO;

    _openCertInfo($OPEC_CERTINFO_TYPE_NOTACCEPT, $errType);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);
            _setOggetto( $objMsgOrig->h_subject_utf84xml );
        _closeIntestazione();
        _openDati();
            _setGestoreEmittente ($adminName);
            _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
            _closeData();
            if($objMsgOrig->msgid_new()){
                _setIdentificativo (    $objMsgOrig->msgid_new  )
            }else{
                _setIdentificativo (    $objMsgOrig->msgid_orig ) }
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );
            defined $errString && _setErroreEsteso($errString);
        _closeDati();
    _closeCertInfo();
    $certInfoXml;
}


# ricevuta di non accettazione per virus
sub getCertInfo_notaccept_virus {
        my $ts             = shift;
    my $objMsgOrig = shift;
    my $errType    = shift;
    my $virusDesc  = shift;
    $certInfoXml = $XML_HEADER;

    _openCertInfo($OPEC_CERTINFO_TYPE_NOTACCEPT, $errType);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);
            _setOggetto( $objMsgOrig->h_subject_utf84xml );
        _closeIntestazione();
        _openDati();
            _setGestoreEmittente ($adminName);
            _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
            _closeData();
            if($objMsgOrig->msgid_new()){
                _setIdentificativo (    $objMsgOrig->msgid_new  )
            }else{
                _setIdentificativo (    $objMsgOrig->msgid_orig ) }
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );
            _setErroreEsteso($virusDesc);
        _closeDati();
    _closeCertInfo();
    $certInfoXml;
}


# ricevuta di rilevazione virus
sub getCertInfo_virusfound {
        my $ts             = shift;
    my $objMsgOrig = shift;
    my $errType    = shift;
    my $virusDesc  = shift;
    my $arrTo      = shift;
    my $arrCons    = shift;

    $certInfoXml = $XML_HEADER;
    _openCertInfo($OPEC_HEADER_VALUE_RECEIPT_VIRUSFOUND, $errType);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
            foreach my $elem_rcpt (@$arrTo){ _setDestinatario($elem_rcpt->[0],$elem_rcpt->[1]) }
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);
             _setOggetto( $objMsgOrig->h_subject_utf84xml, $OPEC_HEADER_SUBJECT_DOCTRASP );
        _closeIntestazione();
        _openDati();
            _setGestoreEmittente ($adminName);
            _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
            _closeData();
            _setIdentificativo ( myChomp( $objMsgOrig->header->get('Message-ID') ));
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );

            if( ref($arrCons) && @$arrCons ){
                foreach  ( @{$arrCons} ){
                    my $orig_rcpt_domain = getDomain($_);
                    _setConsegna ($_)
                        if($orig_rcpt_domain && exists $hLocalCertDomains{lc($orig_rcpt_domain)});
                }
            }else{
                foreach  ( @{$arrTo} ){
                    my $orig_rcpt_domain = getDomain($_->[0]);
                    _setConsegna ($_->[0])
                        if($orig_rcpt_domain && exists $hLocalCertDomains{lc($orig_rcpt_domain)});
                }
            }

            _setErroreEsteso($virusDesc);
        _closeDati();
    _closeCertInfo();
    $certInfoXml;
}


# ricevuta di presa in carico
sub getCertInfo_getload {
        my $ts             = shift;
    my $objMsgTransp = shift;
    my $objMsgOrig   = shift;
    my $arrTo      = shift;
    $certInfoXml = $XML_HEADER;

        _openCertInfo ($OPEC_CERTINFO_TYPE_GETLOAD);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);
            _setOggetto( $objMsgOrig->h_subject_utf84xml );

        _closeIntestazione();
        _openDati();
                _setGestoreEmittente ($adminName);
                _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
                _closeData();
            _setIdentificativo ( $objMsgTransp->msgid_orig );
            _setIdentificativo_orig ( myChomp($objMsgTransp->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );

            foreach my $address (@$arrTo){_setRicezione ($address)}
        _closeDati();
        _closeCertInfo();
        $certInfoXml;
}


# ricevuta di avvenuta consegna
sub getCertInfo_delivery {
        my $ts             = shift;
    my $objMsgOrig = shift;
    my $origRcpt   = shift;

    $certInfoXml = $XML_HEADER;

        _openCertInfo ($OPEC_CERTINFO_TYPE_DELIVERY);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                    foreach my $elem_rcpt (@$origRcpt){ _setDestinatario($elem_rcpt->[0],$elem_rcpt->[1]) }
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);

            _setOggetto( $objMsgOrig->h_subject_utf84xml, $OPEC_HEADER_SUBJECT_DOCTRASP);

        _closeIntestazione();
        _openDati();
                _setGestoreEmittente ($adminName);
                _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
                _closeData();

            if($objMsgOrig->msgid_new()){
                _setIdentificativo (    $objMsgOrig->msgid_new  )
            }else{
                _setIdentificativo (    $objMsgOrig->msgid_orig ) }
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );

            _setReceipt( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_RECEIPT_TYPE)) );
            _setConsegna( $objMsgOrig->recips->[0]->mailAddress );
        _closeDati();
        _closeCertInfo();
        $certInfoXml;
}


# ricevuta di errore di consegna
sub getCertInfo_err_delivery {
        my $ts             = shift;
    my $objMsgOrig   = shift;
    my $arrTo        = shift;
    my $errType      = shift;
    my $errString    = shift;

    $certInfoXml   = $XML_HEADER;
    $errType ||= $OPEC_CERTINFO_ERR_VALUES_ALTRO;
    _openCertInfo($OPEC_CERTINFO_TYPE_ERR_DELIVERY, $errType);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);

            foreach my $elem_rcpt (@$arrTo){ _setDestinatario($elem_rcpt->[0],$elem_rcpt->[1]) }
#               foreach my $obj (@{$arrTo->recips}){
#                   _setDestinatario ($obj->mailAddress,$obj->certificate)}

            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);

            _setOggetto( $objMsgOrig->h_subject_utf84xml, $OPEC_HEADER_SUBJECT_DOCTRASP);

        _closeIntestazione();
        _openDati();
            _setGestoreEmittente($adminName);
            _openData(timezone($ts));
                _setGiorno(actualDate($ts));
                _setOra(actualTime($ts));
            _closeData();
            if($objMsgOrig->msgid_new()){
                _setIdentificativo (    $objMsgOrig->msgid_new  )
            }else{
                _setIdentificativo (    $objMsgOrig->msgid_orig ) }
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );
            _setConsegna( $objMsgOrig->recips->[0]->mailAddress );
            _setErroreEsteso($errString);
        _closeDati();
    _closeCertInfo();
    $certInfoXml;
}


# ricevuta di errore di consegna per virus
sub getCertInfo_errdelivery_virus  {
        my $ts             = shift;
    my $objMsgOrig = shift;
    my $errType    = shift;
    my $errString  = shift;
    my $arrTo      = shift;
    my $arrCons    = shift;
    my $arrReplyTo = shift;

    $certInfoXml   = $XML_HEADER;
    _openCertInfo($OPEC_CERTINFO_TYPE_ERR_DELIVERY, $errType);
    _openIntestazione();
    _setMittente( myChomp($objMsgOrig->header->get($OPEC_HEADER_XSENDER)) );
    foreach my $elem_rcpt (@$arrTo){ _setDestinatario($elem_rcpt->[0],$elem_rcpt->[1]) }
#       foreach my $obj (@{$arrTo->recips}){
#           _setDestinatario ($obj->mailAddress,$obj->certificate)}
    if(ref($arrReplyTo)){
        foreach  my $obj (@$arrReplyTo){_setRisposte($obj) }
    }

    _setOggetto( $objMsgOrig->h_subject_utf84xml, $OPEC_HEADER_SUBJECT_RECEIPT_VIRUSFOUND);

    _closeIntestazione();
    _openDati();
    _setGestoreEmittente($adminName);
    _openData(timezone($ts));
            _setGiorno(actualDate($ts));
            _setOra(actualTime($ts));
    _closeData();

    _setIdentificativo ( $objMsgOrig->msgid_new );
    _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );

    if( ref($arrCons) && @$arrCons ){
        foreach  ( @{$arrCons} ){_setConsegna($_) }
    }else{
        foreach  ( @{$arrTo} ){_setConsegna($_->[0]) }
    }
    _setErroreEsteso($errString);
    _closeDati();
    _closeCertInfo();
    $certInfoXml;
}

# RMC12
sub getCertInfo_rmc12 {
        my $ts             = shift;
    my $objMsgOrig = shift;
    $certInfoXml = $XML_HEADER;

        _openCertInfo ($OPEC_CERTINFO_TYPE_RMC,$OPEC_CERTINFO_ERR_VALUES_ALTRO);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);

            _setOggetto( $objMsgOrig->h_subject_utf84xml, $OPEC_HEADER_SUBJECT_DOCTRASP);

        _closeIntestazione();
        _openDati();
                _setGestoreEmittente ($adminName);
                _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
                _closeData();
            if($objMsgOrig->msgid_new()){
                _setIdentificativo (    $objMsgOrig->msgid_new  )
            }else{
                _setIdentificativo (    $objMsgOrig->msgid_orig ) }
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );
            _setConsegna( $objMsgOrig->tmp_consegna() );
            _setErroreEsteso($OPEC_XML_RMC_ERRSTR12);
        _closeDati();
        _closeCertInfo();
        $certInfoXml;
}


# RMC24
sub getCertInfo_rmc24 {
        my $ts             = shift;
    my $objMsgOrig = shift;
    $certInfoXml = $XML_HEADER;

        _openCertInfo ($OPEC_CERTINFO_TYPE_RMC,$OPEC_CERTINFO_ERR_VALUES_ALTRO);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);

            _setOggetto( $objMsgOrig->h_subject_utf84xml, $OPEC_HEADER_SUBJECT_DOCTRASP);

        _closeIntestazione();
        _openDati();
                _setGestoreEmittente ($adminName);
                _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
                _closeData();
            if($objMsgOrig->msgid_new()){
                _setIdentificativo (    $objMsgOrig->msgid_new  )
            }else{
                _setIdentificativo (    $objMsgOrig->msgid_orig ) }
            _setIdentificativo_orig ( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_MSGID_RIF)) );
            _setConsegna( $objMsgOrig->tmp_consegna() );
            _setErroreEsteso($OPEC_XML_RMC_ERRSTR24);
        _closeDati();
        _closeCertInfo();
        $certInfoXml;
}


# documento di trasporto
sub getCertInfo_doctrasp {
        my $ts             = shift;
    my $objMsgOrig = shift;
    $certInfoXml = $XML_HEADER;

        _openCertInfo ($OPEC_CERTINFO_TYPE_TRASP);
        _openIntestazione();
            _setMittente($objMsgOrig->sender->mailAddress);
                foreach my $obj (@{$objMsgOrig->recips}){
                    _setDestinatario ($obj->mailAddress,$obj->certificate)}
            _setRisposte( myChomp($objMsgOrig->header->get('Reply-To') ) ||
                          $objMsgOrig->sender->mailAddress);
            _setOggetto( $objMsgOrig->h_subject_utf84xml );
        _closeIntestazione();
        _openDati();
                _setGestoreEmittente ($adminName);
                _openData (timezone($ts));
                _setGiorno (actualDate($ts));
                _setOra (actualTime($ts));
                _closeData();
            _setIdentificativo ( $objMsgOrig->msgid_new );
            _setIdentificativo_orig ( $objMsgOrig->msgid_orig );
            _setReceipt( myChomp($objMsgOrig->header->get($OPEC_HEADER_KEY_RECEIPT_TYPE)) );
        _closeDati();
        _closeCertInfo();
        $certInfoXml;
}

#
# Imposta il tipo di documento
#
# IN: 1. tipo di ricevuta
#     2. ( tipo di errore )
# OUT: -
#
sub _openCertInfo {
        my $tipo = shift;
        my $errore = shift;

        $certInfoXml.= "<postacert tipo=\"".$tipo."\" ";
        if ($errore) {
                $certInfoXml.= "errore=\"$errore\"";
        } else {
                # nessun errore
                $certInfoXml.= "errore=\"".
                $OPEC_CERTINFO_ERR_VALUES_NESSUNO."\"";
        }
        $certInfoXml.= ">$eol";
}

sub _closeCertInfo
    {$certInfoXml.= "</postacert>$eol"}


# intestazione
sub _openIntestazione
    {$certInfoXml.= $XML_TAB."<intestazione>$eol"}

sub _closeIntestazione
    {$certInfoXml.= $XML_TAB."</intestazione>$eol"}


sub _setMittente
{
        my $sndr = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB."<mittente>".
            _convertToXML($sndr)."</mittente>$eol";
}

#
# Imposta i destinatari sul documento di certificazione (XML)
#
# IN: 1. email del destinatario
#     2. tipo del destinatario:
#        true se certificato
#        false altrimenti
# OUT: rif. obj
#
sub _setDestinatario
{
        my $rcpt = shift;
        my $type = shift;

        $certInfoXml.= $XML_TAB.$XML_TAB."<destinatari";
        $certInfoXml .= $type?" tipo=\"certificato\"":" tipo=\"esterno\""  if(defined $type);
    $certInfoXml .= ">"._convertToXML($rcpt)."</destinatari>$eol";
}

sub _setRisposte
{
        my $risp = shift;
        $risp =~ s/^\<//;
        $risp =~ s/\>$//;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                   "<risposte>"._convertToXML($risp)."</risposte>$eol";
}

sub _setOggetto
{
        my $ogg     = shift;
        my $str2del = shift;

        $ogg = substr($ogg, length($str2del)) if defined $str2del;

        $certInfoXml.= $XML_TAB.$XML_TAB.
        "<oggetto>"._convertToXML($ogg)."</oggetto>$eol";
}


sub _openDati
    {$certInfoXml.= $XML_TAB."<dati>$eol"}

sub _closeDati
    {$certInfoXml.= $XML_TAB."</dati>$eol"}

sub _setGestoreEmittente
{
        my $gest = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB."<gestore-emittente>".
            _convertToXML($gest)."</gestore-emittente>$eol";
}

sub _openData
{
        my $zona = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                                "<data zona=\"$zona\">$eol";
}

sub _closeData
    {$certInfoXml.= $XML_TAB.$XML_TAB."</data>$eol"}


sub _setGiorno
{
        my $giorno = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB.$XML_TAB.
                                "<giorno>$giorno</giorno>$eol";
}

sub _setOra
{
        my $strTime = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB.$XML_TAB.
                                "<ora>$strTime</ora>$eol";
}

sub _setReceipt
{
        my $recType = shift || $OPEC_HEADER_VALUE_RECEIPT_TYPE_COMPLETE;

        return if(  $recType ne $OPEC_HEADER_VALUE_RECEIPT_TYPE_COMPLETE &&
                    $recType ne $OPEC_HEADER_VALUE_RECEIPT_TYPE_SYNTHETIC &&
                    $recType ne $OPEC_HEADER_VALUE_RECEIPT_TYPE_SHORT);

        $certInfoXml.= $XML_TAB.$XML_TAB.
                                "<ricevuta tipo=\"$recType\" />$eol";
}

sub _setIdentificativo
{
        my $msgId = shift;
        $msgId =~ s/^\<//;
        $msgId =~ s/\>$//;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                                '<identificativo>'._convertToXML($msgId)."</identificativo>$eol";
}

sub _setIdentificativo_orig
{
        my $msgId = shift || return;
        $msgId =~ s/^\<//;
        $msgId =~ s/\>$//;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                                "<msgid>&lt;"._convertToXML($msgId)."&gt;</msgid>$eol";
}

sub _setConsegna
{
        my $consegna = shift || return;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                                "<consegna>"._convertToXML($consegna)."</consegna>$eol";
}

sub _setRicezione
{
        my $ricezione   = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                                "<ricezione>"._convertToXML($ricezione)."</ricezione>$eol";
}

sub _setErroreEsteso
{
        my $erroreesteso = shift;
        $certInfoXml.= $XML_TAB.$XML_TAB.
                                "<errore-esteso>"._convertToXML($erroreesteso)."</errore-esteso>$eol";
}

# converte testo sostituendo i caratteri in conflitto con XML
sub _convertToXML
{
    $_ = shift;

        s/&/&amp;/g;
        s/>/&gt;/g;
        s/</&lt;/g;
        s/"/&quot;/g;
        s/'/&apos;/g;

        return $_;
}

sub _convertFromXML
{
        $_ = shift;

        s/&gt;/>/g;
        s/&lt;/</g;
        s/&quot;/"/g;
        s/&apos;/'/g;
        s/&amp;/&/g;

        return $_;
}



1;

__END__

=head1 NAME

Opec::Xml - Generatore di allegati xml

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
