#
# $Id: Bookmarks.pm,v 1.12 2010/06/21 14:52:41 flazan Exp $
#
# Project       OpenPec
# file name:    Bookmarks.pm
# package:      Opec::Bookmarks
#
# DESCRIPTION
# Sistema di fault recovery.
# Crea un file temporaneo dove salvare stati o
# serializzare oggetti da recuperare in un secondo momento.
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 01/07/2004 - Fanton Flavio
# modification:
# 16/01/2008 - Fanton Flavio
#       . corretta la label dell'inizializzazione
# 18/01/2008 - Fanton Flavio
#       . aggiornata la appendBookmark con l'eliminazione della label esistente
#         prima di inserire il nuovo elemento
# 21/01/2008 - Fanton Flavio
#       . aggiunto il metodo getTag
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

#
# BIS: Bookmark Identification String
#


package Opec::Bookmarks;

use strict;
use Storable qw(store retrieve);
use Digest::MD5 qw(md5_hex);

my $BISpath;
my $BIS;
my $errstring = '';
my $lblINIT = 'INIT';


BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    @EXPORT = qw(   &BIS
                    &BISpath
                    &createBookmark
                    &eraseBookmarks
                    &getBookmark
                    &getLastBookmark
                    &appendBookmark
                    &getTag
                    &lastErr);
}

#
# Crea il file di BIS nel caso non esista altrimenti ne restituisce
# l'ultima label.
#
# paramIN:   1. string da utilizzare per creare il BIS
#            2. PATH dove creare il file temporaneo
#
# paramOUT:  ultima LABEL se viene trovato il BIS
#            (nel caso di creazione viene restituita la label di
#            inizializzazione), undef in caso di errore.
#
sub createBookmark {
    my $str4BIS = shift;
    $BISpath = shift;

    return undef if(!$str4BIS || !$BISpath);

    # creo il BIS dando a MD5 la $str4BIS
    $BIS = md5_hex($str4BIS);

    # costruisco il path del file BIS
    if(substr($BISpath,length($BISpath)-1,1) eq '/'){
        $BISpath .=$BIS;
    }else{
        $BISpath .='/'.$BIS;
    }

    # ne verifico l'esistenza
    my $arrRef;
    $arrRef = _retrieve($BISpath) if(-r $BISpath);

    if($arrRef && ref($arrRef) eq 'ARRAY'){
        # recupero l'ultima label e la restituisco
        my $hRef = pop @$arrRef;
        if(ref($hRef) eq 'HASH'){
            return (keys %$hRef)[0];
        }else{
            return undef}
    }else{
        # creo il file BIS con la label di inizializzazione
        my $lblINIT_tmp = $lblINIT .'::'._timeStamp();
        if(_store([{$lblINIT_tmp,''}], $BISpath)){
            return $lblINIT_tmp;
        }else{
            return undef}
    }
}


#
# Restituisce il rif. all'obj relativo al BIS ed alla label dati in ingresso
#
# paramIN:   1. label
#
# paramOUT:  rif. obj se ok, undef altrimenti
#
sub getBookmark {
    my ($label) = @_;

    return undef if(!$label);
    return undef if(!-r $BISpath);

    # recupero il rif. obj. relativo alla label in ingresso
    my $objOut;
    my $arrRef = _retrieve($BISpath);
    if( ref($arrRef) eq 'ARRAY' ){
        foreach (@$arrRef){
            if(ref($_) eq 'HASH'){
                my ($labelTmp, $objOutTmp) = %$_;
                if($labelTmp =~ /^$label\:\:.*/ ){
                    $objOut = $objOutTmp;
                    last}}}}

    return $objOut;
}


#
# Restituisce l'ultimo rif. all'obj relativo all'eventuale BIS in ingresso
#
# paramIN:   -
#
# paramOUT:  rif. obj se ok, undef altrimenti
#
sub getLastBookmark {
    return undef if(!-r $BISpath);

    # recupero il rif. obj. relativo all'ultima label
    my $arrRef = _retrieve($BISpath);
    if( ref($arrRef) eq 'ARRAY' ){
        return pop @$arrRef
    }else{
        return undef}
}


#
# Inserisce l'elemento nella pila eliminando
# il precedente con la stessa label
#
# paramIN:   1. label
#            2. obj da associare a label per recuperare successivamente
#
# paramOUT:  label se ok, undef altrimenti
#
sub appendBookmark {
    my ($label, $objRef) = @_;

    return undef if(!-w $BISpath || !$label);

    # recupero l'array contenente tutti gli elementi
    # cancello la label se presente
    # ed appendo il nuovo
    my $arrRef = _retrieve($BISpath);
    my @arrNew;
    if( ref($arrRef) eq 'ARRAY' ){
        foreach(@$arrRef){  # elimino la label
            if(ref($_) eq 'HASH'){
                my ($labelTmp, $objOutTmp) = %$_;
                if($labelTmp =~ /^$label\:\:.*/ ){
                    next;
                }else{
                    push @arrNew, $_;
                }
            }else{
                next;
            }
        }
        $label .= '::'._timeStamp();
        push @arrNew, {$label, $objRef};
        if(_store(\@arrNew, $BISpath)){
            return $label
        }
    }

    return undef
}


#
# Restituisce il tag del bookmark senza timestamp
#
# paramIN:   -
#
# paramOUT:  tag se ok, undef altrimenti
#
sub getTag {
    my $lbl = shift;

    return undef unless defined $lbl;
    return substr($lbl,0,index($lbl,'::'));
}


#
# Elimina il file relativo al BIS
#
# paramIN:   -
#
# paramOUT:  1 se ok, 0/undef altrimenti
#
sub eraseBookmarks {
    # cancello materialmente il file
    return unlink($BISpath);
}

#
# Imposta/Restituisce l'ultima stringa d'errore
#
sub lastErr {
    @_ ? $errstring = shift : $errstring}

#
# Imposta/Restituisce il BIS
#
sub BIS {
    @_ ? $BIS = shift : $BIS}

#
# Imposta/Restituisce il path del file BIS
#
sub BISpath {
    @_ ? $BISpath = shift : $BISpath}

#
# PRIVATA
# Ritorna l'attuale timestamp nel formato HH:MM:SS-DD/MM/AAAA
#
sub _timeStamp {
    my @ora = localtime(time);
    sprintf ("%02d:%02d:%02d-%02d/%02d/%d",
        $ora[2],
        $ora[1],
        $ora[0],
        $ora[3],
        $ora[4]+1,
        $ora[5]+1900);
}

#
#
#
#
sub _store {
    my $objRef2Store = shift;
    my $filePath = shift;
    my $out;

    eval{$out = store($objRef2Store, $filePath)};
    $errstring = $@?$@:'';

    $out
}

#
#
#
#
sub _retrieve {
    my $filePath = shift;
    my $hRef;

    eval{
        $hRef = retrieve($filePath);
    };
    $errstring = $@?$@:'';

    $hRef
}


1;

__END__

=head1 NAME

Opec::Bookmarks - Gestione bookmark

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
