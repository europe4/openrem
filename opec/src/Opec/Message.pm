#
# $Id: Message.pm,v 1.34 2012/09/25 14:56:28 flazan Exp $
#
# Project       OpenPec
# file name:    Message.pm
# package:      Opec::Message
#
# DESCRIPTION
# Rappresenta un messaggio di posta elettronica.
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
# 05/06/2007 - Fanton Flavio - Modificato il metodo header in modo che
#                                accetti in ingresso anche oggetti MIME::Head
#       18/01/2008 - Fanton Flavio
#       - aggiunto il metodo name che contiene il tipo di messaggio
#
#       05/06/2009 - Fanton Flavio
#       - il metodo recips imposta localCertificate invece che certificate
#       - il metodo checkCert verifica localCertificate al posto di certificate
#       26/10/2010 - Fanton Flavio
#       - aggiunto il metodo direction che restituisce la direzione del messaggio tra
#                 $OPEC_DIRECTION_OUTBOUND: diretto a sistemi esterni
#                 $OPEC_DIRECTION_INBOUND: diretto localmente
#                 $OPEC_DIRECTION_MIXED: con destinatari interni ed esterni
#                 $OPEC_DIRECTION_ND: non definita (inizializzazione)
#       22/12/2010 - Fanton Flavio
#               - bugfix su subject con errore nella decodifica
#   26/05/2011 - Fanton Flavio
#         - aggiunto parsing xml e metodi formattazione ed estrazioni dati
#   17/02/2012 - Fanton Flavio
#         - aggiornata get_xml_data per gestire correttamenti gli attributi dell'elemento root
#    17/08/2012 - Fanton Flavio
#         - bugfix parser XML
#    25/09/2012 - Fanton Flavio
#         - aggiunto metodo mail_size
#
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

package Opec::Message;


use strict;
use Encode qw( decode );
use MIME::Parser;
use MIME::Entity;
use Mail::Header;
use POSIX qw(strftime);
use Errno qw(ENOENT);
use Time::HiRes qw(time);
use XML::Parser;

use Opec::Conf qw( :confvars %hLocalCertDomains :const :xml :log );
use Opec::Util qw( &doLog &myChomp);
use Opec::Message::EnvelopSender;
use Opec::Message::EnvelopRecipt;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
}

# nome del file contenente la mail in ingresso
my $fileName    = 'email.txt';

#
# Metodo di classe
# COSTRUTTORE
#
#  IN: 1. (path temporaneo)
#      2. (nome del messaggio)
#
#  OUT:  rif. oggetto
#
sub new {
        my $proto       = shift;
        my $tempDir     = shift;    # path temporaneo
        my $name        = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

        # eventuale identificativo dell'obj
        # a scopo di debug
        $self->{name}    = $name || '';

        # timestamp (epoch) di ricezione messaggio
        $self->{rx_time}    = time;

        # obj Mail::Header
        $self->{msgid_orig} = '';

        # obj Mail::Header
        $self->{msgid_new}  = '';

        # obj Mail::Header
        $self->{header}     = '';

        # subject utf-8 format
        $self->{h_subject_utf8} = undef;

        # obj MIME::Entity
        $self->{mime_entity}= '';

        # msg in formato obj IO::File o stringa
    $self->{mail_text}  = '';

    # obj Opec::Message::EnvelopSender
    # rappresentante il mittente (envelop)
    $self->{sender}     = '';

    # arr ref a obj Opec::Message::EnvelopRecipt
    # rappresentante l'insieme dei destinatari
    # (envelop)
    $self->{recips}     = '';

        # direzione del messaggio tra
        # $OPEC_DIRECTION_OUTBOUND: diretto a sistemi esterni
        # $OPEC_DIRECTION_INBOUND: diretto localmente
        # $OPEC_DIRECTION_MIXED: con destinatari interni ed esterni
        # $OPEC_DIRECTION_ND: non definita (inizializzazione)
        $self->{direction}     = $OPEC_DIRECTION_ND;

    # hash ref - rappresentazione ad albero del file xml
    # del file dei dati di certificazione del messaggio
    $self->{xml_tree}     = '';
    # xml generato localmente
    $self->{xml_local}    = '';

    $self->{tmp_consegna} = '';;

    # bool: true mantengo il path temp + contenuto
    # (caso di errore)
    # false altrimenti
    $self->{preserve}   = 1;

    # dir temporanea utilizzata per salvare
    # la mail in ingresso ed i file temp. di MIME::Tools
    $self->{tempdir}    = $tempDir || '';

    # bool: se true viene mantenuta la root del path temp
    # false altrimenti
    $self->{tempdir_keep}=0;

    # allegati
    $self->{attachs} = undef;

    # dimensione messaggio
    $self->{mail_size} = undef;

    $self;
}

#
# Metodo di istanza
# Restituisce il valore di rx_time:
# istante di arrivo del messaggio in secondi Epoch
#
# IN: (quanto restituisce localtime)
# OUT: $self->{rx_time}
#
#sub rx_time
#  { my($self)=shift; @_?$self->{rx_time}=shift:$self->{rx_time} }
sub rx_time
  { my($self)=shift; $self->{rx_time} }

#
# Metodo di istanza
# Restituisce il suject in formato utf-8, undef altrimenti:
# istante di arrivo del messaggio in secondi Epoch
#
# IN: -
# OUT: suject in formato utf-8, undef altrimenti
#
sub h_subject_utf8 {
        my($self)=shift;

        return $self->{h_subject_utf8} if defined $self->{h_subject_utf8};

        my $subject = myChomp($self->header->get ('Subject'));
        eval{
                $self->{h_subject_utf8} = decode('MIME-Header', $subject, Encode::FB_QUIET)
        };

        return $self->{h_subject_utf8} unless $@;

        # siamo in errore
        # provo a recuperare il set di caratteri e fissare l'alias manualmente
        my $enc = my_find_encoding($subject);
    if(defined $enc){
        Encode::Alias::define_alias($enc => 'ascii');
        eval{
                $self->{h_subject_utf8} = decode("MIME-Header", $subject, Encode::FB_QUIET)
        };
        return $self->{h_subject_utf8} unless $@
    }

        # mi arrendo
    $self->{h_subject_utf8} = $subject
}

#
# Metodo di istanza
# Adatta h_subject_utf8 in modo da usare solo caratteri accettati da XML.
# Si veda: http://www.w3.org/TR/REC-xml/#charsets
#
# IN: -
# OUT: subject in formato utf-8 per xml, undef altrimenti
#
sub h_subject_utf84xml {
        my($self)=shift;

        return $self->{h_subject_utf84xml} if defined $self->{h_subject_utf84xml};

        $self->{h_subject_utf84xml} = $self->h_subject_utf8();

    $self->{h_subject_utf84xml} =~ s/[^\x{9}\x{A}\x{D}\x{20}-\x{D7FF}\x{E000}-\x{FFFD}-\x{10000}-\x{10FFFF}]//g;

    return $self->{h_subject_utf84xml}
}


#
# Metodo di classe
# Data una stringa codificata restituisce il set di caratteri
# (regexp tratta da Encode::MIME::Header.pm di Dan Kogai)
#
# IN: bool: true per mantenere il path temporaneo
#           false per eliminarlo
# OUT: bool
#
sub my_find_encoding {
    my $str = shift;

    # zap spaces between encoded words
    $str =~ s/\?=\s+=\?/\?==\?/gos;
    $str =~ /=\?
                ([-0-9A-Za-z_]+)
                (?:\*[A-Za-z]{1,8}(?:-[A-Za-z]{1,8})*)?
                \?[QqBb]\?
                .*?\?=/xos;
    return $1
}

#
# Metodo di istanza
# Restituisce o imposta il valore di tempdir_keep:
# (utilizzata da _deleteTmpSystem per riciclare
# il path temporaneo)
#
# IN: bool: true per mantenere il path temporaneo
#           false per eliminarlo
# OUT: bool
#
sub tempdirkeep
  { my($self)=shift; @_?$self->{tempdir_keep}=shift:$self->{tempdir_keep} }

#
# Metodo di istanza
# (sola lettura)
# Restituisce il valore di tempdir: path temporaneo
#
# IN: -
# OUT: path temporaneo
#
sub tempdir
  { my($self)=shift; $self->{tempdir} }

#
# Metodo di istanza
# Restituisce il valore del'header Message-ID originale
#
# IN: -
# OUT: Message-ID originale
#
sub msgid_orig
    { my $self=shift; @_?$self->{msgid_orig}=shift:$self->{msgid_orig} }

#
# Metodo di istanza
# Restituisce il valore del'header Message-ID modificato secondo le specifiche
#
# IN: -
# OUT: Message-ID modificato
#
sub msgid_new
  { my($self)=shift; @_?$self->{msgid_new}=shift:$self->{msgid_new} }


#
# Metodo di istanza
# Restituisce o imposta il parametro xml_local con xml generato localmente
#
# IN: [stringa xml]
#
# OUT: stringa xml
#
sub xml_local
  { my($self)=shift; @_?$self->{xml_local}=shift:$self->{xml_local} }


#
# Metodo di istanza
# Restituisce o imposta il valore di name che
# indica il tipo di messaggio tra i seguenti:
# msg-originale
# accettazione
# presa-in-carico
# avvenuta-consegna
# errore-consegna
# non-accettazione
# rilevazione-virus
# preavviso-errore-consegna
# errore
# posta-certificata
#
# IN: [tipo di messaggio]
#
# OUT: tipo di messaggio
#
sub name
  { my($self)=shift; @_?$self->{name}=shift:$self->{name} }



#
# Metodo di istanza
# (sola lettura)
# Restituisce path del file temporaneo
# (file che mantiene la mail in ingresso inalterata)
#
# IN: -
# OUT: path file temporaneo
#
sub tempfiledir {
    my($self)=shift;
    $self->{tempdir}.'/'.$fileName if $self->{tempdir} }

#
# Metodo di istanza
# Imposta $self->{header} con l'obj Mail::Header
# che rappresenta l'header di Opec::Message.
#
# IN: 1. (obj Mail::Header)
# OUT: Mail::Header se $self->{header} impostato
#      false altrimenti
#
# Origine del Mail::Header restituito:
#
#      in | mail_text | mime_entity | OUT
#      ------------------------------------
#       -       -           -          quanto in $self->{header}
#       *       x           x          quanto in ingresso
#       -       *           x          header recuperato da
#                                      quanto in mail_text
#       -       -           *          header recuperato da
#                                      MIME::Entity
#
# - non fornito
# * fornito
# x qualsiasi
#
sub header {
    my $self        = shift;
    my $objHeader   = shift;

    if($objHeader &&
       (ref($objHeader) eq 'Mail::Header' or ref($objHeader) eq 'MIME::Head') ){
        $self->{header} = $objHeader;
    }elsif($self->{header}){
        $self->{header}
    }elsif( $self->{mail_text} &&
            ref($self->{mail_text}) eq 'IO::File' ){
        $self->{mail_text}->seek(0,0);
        $self->{header} = new Mail::Header $self->{mail_text}}
    elsif($self->{mime_entity}){
        $self->{header} = $self->mime_entity->head}
}

#
# Metodo di istanza
# Imposta $self->{header} a stringa vuota, come da costruttore
#
# IN: -
# OUT: -
#
sub header_clean
  { my($self)=shift; $self->{header}='' }

#
# Metodo di istanza
# Imposta $self->{mime_entity} con l'obj MIME::Entity
# Utilizzato per creare nuovi messaggi (sia ricevute che
# doc./anom. di trasporto) o, solo in caso di effettiva necessita',
# accedere alle componenti del messaggio in ingresso (header,
# allegati...)
# Per accedere solo all'header, utilizzare con preferenza
# il metodo header.
#
# IN: 1. (obj MIME::Entity)
# OUT: MIME::Entity se $self->{mime_entity} impostato
#      false altrimenti
#      undef in caso di errore di eventuale parsing
#
# Origine di MIME::Entity restituito:
#
#      in | mime_entity | mail_text | OUT
#      ------------------------------------
#       -       -             -        new MIME::Entity
#       *       x             x        quanto in ingresso
#       -       *             x        quanto in $self->{mime_entity}
#       -       -             *        MIME::Entity ottenuto dal
#                                      parsing di $self->{mail_text}
#                                      undef in caso di errore nel parsing
#
# - non fornito
# * fornito
# x qualsiasi
#
sub mime_entity {
    my $self        = shift;
    my $objEntity   = shift;

    if($objEntity && ref($objEntity) eq 'MIME::Entity'){
        $self->{mime_entity} = $objEntity;
    }elsif(!$self->{mime_entity}){
        if(ref($self->{mail_text}) eq 'IO::File'){
            my $parser = new MIME::Parser;

            if($self->{tempdir}){
                #$parser->output_under('MSG', $self->{tempdir});
                $parser->output_dir($self->{tempdir});
                $parser->output_to_core(0);
                $parser->tmp_to_core(0);
                $parser->tmp_recycling(1);
            }else{
                # solo per sicurezza
                $parser->output_to_core(1);
                $parser->tmp_to_core(1);
                $parser->use_inner_files(1)
            }
            $self->{mail_text}->seek(0,0);
            eval{
            $self->{mime_entity} = $parser->parse($self->{mail_text})};
            return undef if($@ || !$self->{mime_entity});
        }else{
            $self->{mime_entity} = new MIME::Entity}}

    $self->{mime_entity}
}


#
# Metodo di istanza
# Restituisce una copia dell'oggetto senza il body
# (quindi senza poter usare il metodo mime_entity)
#
# IN: undef
#
# OUT: obj Opec::Message se ok, undef altrimenti
#
sub getCopyLight {
    my $self = shift || return undef;
    my $tmp_consegna = shift;
    my $copy = new Opec::Message;

    foreach (keys %$self){
        next if $_ eq 'mime_entity' || $_ eq 'mail_text';
        $self->header() if $_ eq 'header';
        $copy->{$_} = $self->{$_}}

    $copy->{tmp_consegna} = $tmp_consegna;

    $copy;
}

#
# Metodo di istanza
# Rappresenta l'obj IO::File della mail
#
# IN: undef OUT: IO::File se $self->{mail_text} o
#                $self->{mime_entity} sono avvalorati
# OUT: obj IO::File OUT: obj IO::File
#
sub mail_text
  { my($self)=shift; !@_ ? $self->{mail_text} : ($self->{mail_text}=shift) }

#
# Metodo di istanza
# Rappresenta l'obj IO::File della mail
#
# IN: undef OUT: IO::File se $self->{mail_text} o
#                $self->{mime_entity} sono avvalorati
# OUT: obj IO::File OUT: obj IO::File
#
sub tmp_consegna
  { my($self)=shift; !@_ ? $self->{tmp_consegna} : ($self->{tmp_consegna}=shift) }

#
# Metodo di istanza
# Imposta o recupra il mittente del messaggio (ENVELOP)
# (obj Opec::Message::EnvelopSender)
#
# IN: 1. (email mittente)
#
# OUT: obj Opec::Message::EnvelopSender
#      se mittente presente, false altrimenti
#
sub sender {
    my($self)=shift;
    if(@_) {
        my $emailSender = shift;
        $self->{sender} = Opec::Message::EnvelopSender->new;
            $self->{sender}->mailAddress($emailSender);
            # avvaloro la proprieta' certificate
            # controllando che il sender sia cert o no cert
            # rispetto al dominio locale
            $emailSender =~ /.+\@(.+)/;
            my $sender_domain = $1;
            $self->{sender}->certificate(1)
                if(exists $hLocalCertDomains{lc($sender_domain)} )}

    $self->{sender}
}

#
# Metodo di istanza
# Imposta o recupra i destinatari del messaggio (ENVELOP)
# (obj Opec::Message::EnvelopRecipt)
#
# IN: 1. (arr ref email destinatari)
#
# OUT: obj Opec::Message::EnvelopRecipt
#      se destinatari presenti, false altrimenti
#
sub recips {
    my($self)= shift;

    if(@_){
        $self->{recips} =([ map {
            if(ref($_) ne 'Opec::Message::EnvelopRecipt'){
            my($rcp_obj) = Opec::Message::EnvelopRecipt->new;
            $rcp_obj->mailAddress($_);
            # avvaloro la proprieta' certificate
            # controllando che il recipt sia cert o no cert
            # rispetto al gestore locale
            /.+\@(.+)/;
            my $rcp_domain = $1;
            if( exists $hLocalCertDomains{lc($rcp_domain)} ){
                $rcp_obj->localCertificate(1);
                if( $self->{direction} eq $OPEC_DIRECTION_ND ){
                        $self->{direction} = $OPEC_DIRECTION_INBOUND;
                }elsif( $self->{direction} eq $OPEC_DIRECTION_OUTBOUND ){
                        $self->{direction} = $OPEC_DIRECTION_MIXED;
                }
            }else{
                if( $self->{direction} eq $OPEC_DIRECTION_ND ){
                        $self->{direction} = $OPEC_DIRECTION_OUTBOUND;
                }elsif( $self->{direction} eq $OPEC_DIRECTION_INBOUND ){
                        $self->{direction} = $OPEC_DIRECTION_MIXED;
                }
            }
            $rcp_obj}
        else{
            $_} } @{$_[0]} ])}

    $self->{recips}
}


#
# Metodo di istanza
# Restituisce la direzione di delivery del messaggio tra
#
# $OPEC_DIRECTION_OUTBOUND: diretto a sistemi esterni
# $OPEC_DIRECTION_INBOUND: diretto localmente
# $OPEC_DIRECTION_MIXED: con destinatari interni ed esterni
# $OPEC_DIRECTION_ND: non definita (inizializzazione)
#
# IN: -
# OUT: stringa - direzione
#
sub direction
        { my($self)=shift; $self->{direction}}


#
# Metodo di istanza
# Controlla se mittenti o destinatari locali appartegono
# al dominio certificato
#
# IN: -
# OUT: bool - false mail da non processare
#             true mail da processsare
#
sub checkCert {
    my $self = shift;

    # check mittente
    return 1 if( $self->sender->certificate() );

    # check destinatari
    return 1 if(grep $_->localCertificate() ,@{$self->recips});

    0;
}

#
# Imposta o restituisce $self->{preserve}:
# utilizzata per mantenere il path temporaneo o eliminarlo
#
# IN: bool - true mantengo il path temp + contenuto (caso di errore)
#            false altrimenti
# OUT: bool
#
sub preserve_evidence
    {my($self)=shift; @_?($self->{preserve}=shift):$self->{preserve}}

#
# Restituisce gli allegati
#

sub attachs {
    my $self = shift;

    my $attach_name;
    my $attach_size = 0;
    my $attach_digest;

    foreach my $part ($self->{mime_entity}->parts_DFS) {
      my $attach_name = $part->head->recommended_filename;
      if ($attach_name ne '' && $attach_name ne $OPEC_XML_FILE && $attach_name ne $OPEC_POSTACERT_FILE && $attach_name ne $OPEC_SMIME_FILE) {
        my $attach_name_fullpath = $self->{tempdir} . '/' . $attach_name;
        if (-f $attach_name_fullpath) {
          $attach_size = (stat($attach_name_fullpath))[7];
          if ($ATTACH_CHECKSUM) {
            $attach_digest = Opec::Sign::getHash($attach_name_fullpath);
          }
        }
        $self->{attachs}->{$attach_name} = [$attach_size, $attach_digest];
      }
    }

    $self->{attachs}
}


#
# Metodo di istanza
# Imposta o restituisce la dimensione del messaggio
#
# IN: dimensione in bytes del messaggio
#
# OUT: dimensione in bytes del messaggio, undef se non precedentemente impostata
#
sub mail_size {
        my($self)=shift;

        return $self->{mail_size} if defined $self->{mail_size};

        return unless $self->mail_text;

        $self->{mail_size} = (stat $self->mail_text)[7];
        $self->{mail_size}
}


#
# Metodo di istanza - privato
# Crea un path temporaneo
# (se puo' mantiene quello vecchio:
# sono in caso di + mail per una stessa connessione)
#
# IN: -
# OUT: die in caso d'errore
#
# Ciclo di vita del path temporaneo
# E' costruito su richiesta del costruttore quando il messaggio si
# riferisce alla mail in ingresso.
# Alla distruzione del messaggio tenta di eliminare il path con il
# suo contenuto.
# Durante la distruzione, se la proprieta' preserve_evidence e' impostata
# a true non cancella niente mentre se e' impostata la proprieta' tempdir
# svuota il path temporaneo mantenendo la dir radice.
#
sub createTmpSystem ($) {
    my($self) = @_;
    #--- crea un nome per il path del tipo pathbase opec-timestamp-pid(5)
    #    (se non esiste)
    if (! $self->{tempdir} ) {
        # invent a name for a temporary directory for this child, and create it
        my($now_iso8601) = strftime("%Y%m%dT%H%M%S", localtime);
        $self->{tempdir} = sprintf("%s/opec-%s-%05d",
                                        $TEMPBASE, $now_iso8601, $$);
    }

    #--- se non esiste la creo
    my($errn) = stat($self->{tempdir}) ? 0 : 0+$!;
    if ($errn == ENOENT || ! -d _) {
        doLog(4, "-- creo dir: ".$self->{tempdir});
        mkdir($self->{tempdir}, 0750)
            or die "Impossibile creare la direttory $self->{tempdir}: $!";
        $self->{tempdir_empty} = 1;
    }

    #--- preparo il file temporaneo:
    #    (se c'e' cerco di riutillizzarlo troncandolo)
    my($fname) = $self->{tempdir} . '/' . $fileName;
    $errn = stat($fname) ? 0 : 0+$!;
    if ($self->{mail_text} && !$errn && -f _) {
        $self->{mail_text}->seek(0,0)
            or die "Impossibile spostarsi all'inizio del file temporaneo: $!";
        $self->{mail_text}->truncate(0)
            or die "Impossibile troncare il file temporaneo: $!";
    } else {
        doLog(4, "-- creo file: ".$fname);
        $self->{mail_text} = IO::File->new($fname, 'w+', 0640)
            or die "Impossibile creare il file temporaneo $fname: $!";
    }
}

#
#
# Metodo di istanza
# Salva l'oggetto (mail) su path e con nome dati
# ad esclusione che si sia verificato un errore
# (segnalato da opec quando si esce dall'eval
# erroneamente)
#
# IN: - path file
#     - nome file (deve essere univoco universalmente)
# OUT: undef se ok, stringa d'errore altrimenti
#
sub save {
    my $self       = shift;
    my $file_path  = shift;
    my $file_name  = shift;
    my $file;

    ($file_path && substr($file_path, -1, 1) eq '/') ?
        $file = $file_path . $file_name :
        $file = $file_path . '/' . $file_name;

    $self->mail_text->seek(0,0);
    open(MAIL, ">$file") or return $!;

    my $block;
    while($self->mail_text->read($block, 4096)){
        print MAIL $block}
    close(MAIL);

    undef;
}


#
#
# Metodo di istanza
# Restituisce un riferimento hash contenente
# il file xml daticert.xml opportunamente incapsulato
#
# IN: -
#
# OUT: rif hash se ok, undef altrimenti
#
sub get_xml_tree {
    my $self       = shift;

        if(!$self->{xml_tree}){
                my $xp = XML::Parser->new(Style => 'Tree');
                my $tree;
                if( $self->{xml_local} ){
                        # messaggio locale
                        eval{ $tree = $xp->parse($self->{xml_local}) };
                }else{
                        # messaggio remoto
                        eval{ $tree = $xp->parsefile($self->_getXmlFile()) };
                }
                return if $@;
                $self->{xml_tree} = {};
                _xml_formatted($XML_ROOTNODE_NAME, $tree->[1], $self->{xml_tree})
        }

        return $self->{xml_tree};
}


#
#
# Metodo di istanza
# Restituisce il valore di elemento o di un suo attributo
# specificando il percorso nella forma
#
# 'intestazione#mittente' > restituisce il test dell'attrib mittente
# 'intestazione#destinatari.tipo' > restituisce l'attributo tipo dell'elemento destinatari
#
# * se esistono + elementi restituisce un array
#
# IN: path elemento richiesto
#
# OUT: stringa/ref array, undef in caso di elemento o attributo inesistente
#
sub get_xml_data {
        my $self                 = shift;
        my $data_request = shift;
        my $h = $self->get_xml_tree;

        my ($route,$attr) = split(/\./,$data_request);
        my @path = split(/\#/,$route);
        my $ref = $h->{$XML_ROOTNODE_NAME};
        my $wrongdone;
        foreach my $node (@path){
                if( !exists $ref->{$node} ) {$wrongdone=1;last}
                $ref = $ref->{$node};
        }

        return if $wrongdone;
        return $ref if ref($ref) eq 'ARRAY';
        return $ref->{0} if !$attr;
        return exists $ref->{$attr} ? $ref->{$attr} : undef;
}


#
# Metodo di classe
#
# Formatta la struttura ritornata da XML::Parser style tree, in particolare
# la struttura in INGRESSO e' del tipo
#
# <nome elemento>, [ { <href attributi> }, <nome elemento>, [ .. ], .. ]
# * L'elemento con nome '0' contiene il testo
#
# la struttura di USCITA e' del tipo
#
# { attr -> valore, elem1 -> {..}, elem2 -> [ {..}, {..}, .. ], .. }
#
# IN:   - nome attributo
#               - arr ref contenuto elem
#               - h ref struttura finale
#
# OUT:  -
#
sub _xml_formatted {
    my $attr_name = shift;
    my $data_in   = shift;
    my $h_out     = shift;

    # imposto il nuovo nodo
    my $h;
    if( !exists $h_out->{$attr_name} ){
        $h = $h_out->{$attr_name} = {};
    }else{
        if( ref($h_out->{$attr_name}) ne 'ARRAY' ){
            $h_out->{$attr_name} = [$h_out->{$attr_name}]
        }
        $h = $h_out->{$attr_name}->[push(@{$h_out->{$attr_name}}, {})-1];
    }

    # imposto gli attributi
    # XXX in caso di + attributi con lo stesso nome si prende solo l'ultimo
    my $attributi = shift @$data_in;
    @$h{keys %$attributi} = values %$attributi;

    # imposto gli altri elementi
    while(@$data_in){
        my ($k, $v) = (shift @$data_in,shift @$data_in);
        if( ref($v) ){
            _xml_formatted($k,$v,$h)
        }else{
            # solo nodo testo
            $v =~ s/^\s+//s;
            $v =~ s/\s+$//s;
            $h->{$k} = $v;
        }
    }
}

#
# funzione privata
# Dato un oggetto Opec::Message, restituisce il nome del file xml che contiene i dati
# di certificazione.
# Questo metodo si rende fondamentale dal momento in cui un messaggio puo' contenere
# + allegati con lo stesso nome; dobbiamo quindi prevenire il caso in cui un fornitore
# alleghi oltre al file daticert.xml un altro file con lo stesso nome ma con contenuto
# inaspettato.
#
# IN: 1. Opec::Message
# OUT: restituisce il nome del file che rappresenta l'allegato xml certificato, undef altrimenti
#
sub _getXmlFile {
    my $self = shift;

    chdir($self->tempdir) || return;
    $self->mime_entity;

    # devo verificare che il file $OPEC_XML_FILE sia quello che mi aspetto
    open(XML, $OPEC_XML_FILE) || return;
    my $check;
    while(<XML>){
        if(/\s*\<\s*postacert\s*tipo\s*=/so){
            $check = $OPEC_XML_FILE;
            last}
    }
    close(XML);

    $check
}


# Metodo di istanza - privato
# Elimina il path temporaneo
# ad esclusione che si sia verificato un errore
# (segnalato da opec quando si esce dall'eval
# erroneamente)
#
# IN: -
# OUT: die in caso d'errore
#
sub _deleteTmpSystem {
    my($self)           = shift;

    #--- chiusura file temporaneo contenente la mail
    if( ref($self->{mail_text}) ){
        #doLog(4, "tento di chiudere il file temporaneo");
        $self->{mail_text}->close
            or die "Impossibile chiudere il file temporaneo: $!"}

        #--- gestione path temporaneo
    if (!$self->{preserve}) {
        my($errn) = $self->{tempdir} eq '' ? ENOENT
                    : (stat($self->{tempdir}) ? 0 : 0+$!);
        _rmdirRecursively($self->{tempdir},$self->{tempdir_keep})
            if ($errn != ENOENT)}
}


#
# Metodo di classe - privato
# Elimina una direttory e tutto il suo contenuto
# (funz. ricorsiva)
#
# IN: 1. directory da eliminare
#     2. opz. (false) - se true mantiene la direttory stessa
#        (canc. solo il contenuto) false altrimenti
# OUT: 1 se tutto ok, die altrimenti
#
#sub rmdirRecursively($;$); # predichiaro per evitare il warning
sub _rmdirRecursively ($;$) {
    my($dir, $exclude_itself) = @_;
    my($f);
    my($cnt) = 0;
    local(*DIR);

    opendir(DIR, $dir) or die "Impossibile aprire la direttory $dir: $!";
    while (defined($f = readdir(DIR))) {
        # check path
        my($msg);
        my($errn) = lstat("$dir/$f") ? 0 : 0+$!;
        if ($errn == ENOENT) { $msg = "inesistente" }
        elsif ($errn)        { $msg = "inaccessibile: $!" }
        if (defined $msg) { die "_rmdirRecursively: \"$dir/$f\" $msg" }

        next  if ($f eq '.' || $f eq '..') && -d _;
        $f = $1  if $f =~ /^(.+)\z/s;

        if (-d _) {
            # chiamata ricorsiva
            _rmdirRecursively("$dir/$f",0);
        } else {
            # elimino i file contenuti
            $cnt++;
            doLog(4,"-- cancello file: $dir/$f");
            unlink("$dir/$f") or die "Impossibile rimuovere il file $dir/$f: $!";
        }
    }
    closedir(DIR) or die "Impossibile chiudere la direttory $dir: $!";

    if (!$exclude_itself) {
        doLog(4,"-- cancello dir: $dir");
        rmdir($dir) or die "Impossibile rimuovere la direttory $dir: $!";
    }
    1
}


sub DESTROY {
    my $self = shift;

    #doLog(4, "Opec::Message::DESTROY chiamata".($self->{name}ne''?" (".$self->{name}.")":""));
    $self->_deleteTmpSystem() if(defined $self->{tempdir});
}

1;


__END__

=head1 NAME

Opec::Message - Interfaccia per la gestione del messaggio

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
