#
# $Id: Log.pm,v 1.21 2011/10/13 17:26:56 flazan Exp $
#
# Project       OpenPec
# file name:    Log.pm
# package:      Opec::Log
#
# DESCRIPTION
# Gestione del log
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   05/07/2004 - Fanton Flavio - scorporamento dei log in
#                                log di sistema e log legale
#   17/05/2005 - Fanton Flavio - aggiunta la rotazione dei log in base al size
#   19/05/2005 - Fanton Flavio - modificata la init per la stampa dei log
#   07/06/2007 - Fanton Flavio - aggiunto l'anno nel tracciato log
#   26/05/2011 - Fanton Flavio - aggiunta compatibilita' con log xml
#   26/08/2011 - Fanton Flavio - aggiornata rotazione temporale log xml
#
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

package Opec::Log;


use strict;

use Opec::Conf qw( :confvars :platform :log );
use Opec::Lock;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    @ISA = qw(Exporter);
    %EXPORT_TAGS = ();
    @EXPORT = ();
    @EXPORT_OK = qw(&init
                    &request_id
                    &writeLog
                    &writeLogXml
                    &rotate);
}
use subs @EXPORT_OK;

use POSIX qw(strftime);
use Unix::Syslog qw(:macros :subs);
use File::Copy;
use IO::File;
use File::Basename;

my $myname;
my $log_to_stderr;

# log di sistema
my $systemLogHandle;  # log file handle
my ($system_syslog_facility, $system_syslog_priority);

# log legale
my $legalLogHandle;  # log file handle
my ($legal_syslog_facility, $legal_syslog_priority);

# log xml
my $xmlLogHandle;  # log file handle
my ($xml_syslog_facility, $xml_syslog_priority);


# Setta o ritorna l'id univoco per la richiesta.
#
# IN: valore booleano. Se true, allora setta l'id univoco.
#     Andrebbe chiamato solo una volta con 1 per ogni richiesta.
#
# OUT: l'id univoco per la richiesta.
#
our $per_request_id;
sub request_id (;$) {
    if (@_) {
        $per_request_id = sprintf("%s.%02d.",
                                  strftime("%Y%m%d%H%M%S", localtime),
                                  $idServer);
    }

    return $per_request_id;
}

#
# Inizializza il sistema di log
#
# IN: 1. identificativo sempre presente nella traccia del log
#     2. booleano - true se tutto il log e' rediretto su STDERR
#       false altrimenti (HA LA PRECEDENZA)
#
# OUT: -
#
sub init ($$;$) {
    my $ident = shift;
    $log_to_stderr = shift;
    my $not_start = shift;

#    $log_to_stderr = 1 unless defined $log_to_stderr;

#    $do_syslog,  $syslog_level, $logfile, $log_lvl) = @_;
#    $DO_SYSLOG, $SYSLOG_LEVEL, $LOGFILE, $logLevel

    # Avoid taint bug in some versions of Perl (likely in 5.004, 5.005).
    # The 5.6.1 is fine. To test, run this one-liner:
    #   perl -Te '"$0 $$"; $r=$$; print eval{kill(0,$$);1}?"OK\n":"BUG\n"'

    # . nome del file senza path
    #$myname = $1  if basename($0) =~ /^(.*)$/;
    $myname = $myversion;

    # . dato syslog_level da file conf nel formato
    #   <facility>.<prority> (es.: mail.info)
    #   ne recupera il valore numerico associato in syslog.h
    # . apre, se caso il sistema di log prescelto
    if(!$log_to_stderr){
        # LOG DI SISTEMA
        if ($SYSTEM_DO_LOG) {
            if ($SYSTEM_SYSLOG_LEVEL =~ /^\s*([a-z0-9]+)\.([a-z0-9]+)\s*$/i) {
                $system_syslog_facility = eval("LOG_\U$1");
                $system_syslog_priority = eval("LOG_\U$2");
            }
            $system_syslog_facility = LOG_DAEMON
                if $system_syslog_facility !~ /^\d+$(?!\n)/;
            # priority e' utilizzato in write_log
            $system_syslog_priority = LOG_WARNING
                if $system_syslog_priority !~ /^\d+$(?!\n)/;

                openlog($ident, LOG_PID, $system_syslog_facility);
        } else {
                $systemLogHandle = IO::File->new($SYSTEM_LOGFILE, 'a')
                    or die "Impossibile aprire il file di log ".
                           "di sistema $SYSTEM_LOGFILE: $!";
                chown($daemonUserUid, $daemonUserGid, $SYSTEM_LOGFILE);
                $systemLogHandle->autoflush(1);
        }

        # LOG LEGALE
        if($LEGAL_DO_LOG) {
            # syslog
            if ($LEGAL_SYSLOG_LEVEL =~ /^\s*([a-z0-9]+)\.([a-z0-9]+)\s*$/i) {
                $legal_syslog_facility = eval("LOG_\U$1");
                $legal_syslog_priority = eval("LOG_\U$2");
            }
            $legal_syslog_facility = LOG_DAEMON
                if $legal_syslog_facility !~ /^\d+$(?!\n)/;
            # priority e' utilizzato in write_log
            $legal_syslog_priority = LOG_WARNING
                if $legal_syslog_priority !~ /^\d+$(?!\n)/;

                openlog($ident, LOG_PID, $legal_syslog_facility);
        }else{
            # file
                $legalLogHandle = IO::File->new($LEGAL_LOGFILE, 'a',)
                    or die "Impossibile aprire il file di log ".
                           "legale $LEGAL_LOGFILE: $!";
            chown($daemonUserUid, $daemonUserGid, $LEGAL_LOGFILE);
                $legalLogHandle->autoflush(1)}

#        # LOG XML
#        if($XML_DO_LOG) {
#            # syslog
#            if ($XML_SYSLOG_LEVEL =~ /^\s*([a-z0-9]+)\.([a-z0-9]+)\s*$/i) {
#               $xml_syslog_facility = eval("LOG_\U$1");
#               $xml_syslog_priority = eval("LOG_\U$2");
#            }
#            $xml_syslog_facility = LOG_DAEMON
#                if $xml_syslog_facility !~ /^\d+$(?!\n)/;
#            # priority e' utilizzato in write_log
#            $xml_syslog_priority = LOG_WARNING
#                if $xml_syslog_priority !~ /^\d+$(?!\n)/;
#
#               openlog($ident, LOG_PID, $xml_syslog_facility);
#        }else{
#            # file
#               $xmlLogHandle = IO::File->new($XML_LOGFILE, 'a',)
#                   or die "Impossibile aprire il file di log ".
#                          "xml $XML_LOGFILE: $!";
#            chown($daemonUserUid, $daemonUserGid, $XML_LOGFILE);
#               $xmlLogHandle->autoflush(1)}

    }

    # LOG XML
    if($XML_DO_LOG && $XML_DO_LOG == 1) {
        # syslog
        if ($XML_SYSLOG_LEVEL =~ /^\s*([a-z0-9]+)\.([a-z0-9]+)\s*$/i) {
                $xml_syslog_facility = eval("LOG_\U$1");
                $xml_syslog_priority = eval("LOG_\U$2");
        }
        $xml_syslog_facility = LOG_DAEMON
            if $xml_syslog_facility !~ /^\d+$(?!\n)/;
        # priority e' utilizzato in write_log
        $xml_syslog_priority = LOG_WARNING
            if $xml_syslog_priority !~ /^\d+$(?!\n)/;

                openlog($ident, LOG_PID, $xml_syslog_facility);
    }elsif($XML_DO_LOG && $XML_DO_LOG == 2) {
        # file
                $xmlLogHandle = IO::File->new($XML_LOGFILE, 'a',)
                    or die "Impossibile aprire il file di log ".
                           "xml $XML_LOGFILE: $!";
        chown($daemonUserUid, $daemonUserGid, $XML_LOGFILE);
                $xmlLogHandle->autoflush(1)
    }

    # messaggio di inizializzazione
    unless($not_start){
        my($msg) = "> >>>Starting - $myname at $myhostname - version: $VERSION";
        $msg .= ", eol=\"$eol\""   if $eol ne "\n";
        writeLog(0,0, $msg);
        writeLog(0,1, $msg);
    }
}


#
# Scrive il messaggio di log sul sistema prescelto
# (utilizzare Opec::Util::doLog)
#
# IN: 1. id interno del messaggio
#     2. intero - livello del messaggio di log
#        (vedi file conf sezione III per i valori disponibili)
#     3. messaggio da loggare
#
# OUT: -
#
sub writeLog ($$$) {
    my($msgId, $level, $errmsg) = @_;
     $msgId = $msgId ? "(".$msgId.") " : '';

    my $prefix;
    # se STDERR o
    # se non abbiamo scelto syslog
    if ($log_to_stderr || !$SYSTEM_DO_LOG || !$LEGAL_DO_LOG) {  # create syslog-alike
        $prefix = sprintf("%s %s %s[%s]: ",
                          strftime("%Y %b %e %H:%M:%S", localtime),
                          $myhostname, $myname, $$)}

    if ($log_to_stderr) {
            print STDERR $prefix,$msgId,$errmsg,$eol;
            return}

    if($level){
        # LOG DI SISTEMA
        if ($SYSTEM_DO_LOG) {   # syslog
            my $pre = ''; # dichiaro vuoto solo per evitare i warning: a regime puo' essere tolto
            my($logline_size) = 980;   # less than  1023 - prefix
                while (length($msgId.$pre.$errmsg) > $logline_size) {
                    my($avail) = $logline_size - length($msgId.$pre."...");
                    syslog($system_syslog_priority, "%s",
                        $msgId . $pre . substr($errmsg,0,$avail) . "...");
                    $pre = "..."; $errmsg = substr($errmsg,$avail);
                }
                syslog($system_syslog_priority, "%s", $msgId.$pre.$errmsg);
        } else {    # file
                # verifico che il file relativo all'handle $systemLogHandle esista
                # altrimenti lo ricreo.
                # Lasoluzione che ho trovato e' mediante il confronto
                # fra l'inode del file associato all'handle $systemLogHandle ed il path del file stesso.
                # Questo perche' in Unix se apro un handle e cancello fisicamente il file associato
                # l'handle continua ad esistere per l'intera vita della variabile che contiene
                # l'handle.
            if( ($systemLogHandle->stat)[1] != (stat($SYSTEM_LOGFILE))[1] ){
                $systemLogHandle = IO::File->new($SYSTEM_LOGFILE, 'a')
                    or die "Impossibile aprire il file di log ".
                           "di sistema $SYSTEM_LOGFILE: $!";
                $systemLogHandle->autoflush(1);
            }

                lock($systemLogHandle);
                print $systemLogHandle $prefix,$msgId,$errmsg,$eol;
                unlock($systemLogHandle);

            if(defined $SYSTEM_MAXSIZE){    # verifica condizione per rotazione
                my $dim = $systemLogHandle->tell();
                _rotateFile($SYSTEM_LOGFILE, $systemLogHandle) if($dim && $dim > $SYSTEM_MAXSIZE);
            }
        }
    }else{
        # LOG LEGALE
        if($LEGAL_DO_LOG) { # syslog
            my $pre = ''; # dichiaro vuoto solo per evitare i warning: a regime puo' essere tolto
            my($logline_size) = 980;   # less than  1023 - prefix
                while (length($msgId.$pre.$errmsg) > $logline_size) {
                    my($avail) = $logline_size - length($msgId.$pre."...");
                    syslog($legal_syslog_priority, "%s",
                        $msgId . $pre . substr($errmsg,0,$avail) . "...");
                    $pre = "..."; $errmsg = substr($errmsg,$avail);
                }
                syslog($legal_syslog_priority, "%s", $msgId.$pre.$errmsg);
        }else{  # file
            if( ($legalLogHandle->stat)[1] != (stat($LEGAL_LOGFILE))[1] ){
                $legalLogHandle = IO::File->new($LEGAL_LOGFILE, 'a')
                    or die "Impossibile aprire il file di log ".
                           "di sistema $LEGAL_LOGFILE: $!";
                $legalLogHandle->autoflush(1);
            }

                lock($legalLogHandle);
                print $legalLogHandle $prefix,$msgId,$errmsg,$eol;
                unlock($legalLogHandle);

            if(defined $LEGAL_MAXSIZE){    # verifica condizione per rotazione
                my $dim = $legalLogHandle->tell();
                _rotateFile($LEGAL_LOGFILE, $legalLogHandle) if($dim && $dim > $LEGAL_MAXSIZE);
            }
        }
    }
}


#
# Scrive il tracciato xml sul sistema prescelto
# (utilizzare Opec::Util::doLogXml)
#
# IN: 3. stringa xml
#
# OUT: -
#
sub writeLogXml {
        my $strXml = shift || return;

    # LOG DI SISTEMA
    if($XML_DO_LOG && $XML_DO_LOG == 1) {   # syslog
            my $pre = ''; # dichiaro vuoto solo per evitare i warning: a regime puo' essere tolto
            my($logline_size) = 10000;   # XXX dimensione massima della riga - per sicurezza
                while (length($pre.$strXml) > $logline_size) {
                    my($avail) = $logline_size - length($pre.' ');
                    syslog($xml_syslog_priority, "%s",
                                $pre . substr($strXml,0,$avail) . '\\');
                    $pre = ' ';
                    $strXml = substr($strXml,$avail);
                }
                syslog($xml_syslog_priority, "%s", $pre.$strXml);
    } elsif($XML_DO_LOG && $XML_DO_LOG == 2) {    # file
                my $prefix = $XML_PREFIXOFF ?
                                         '' :
                                         sprintf("%s %s %s[%s]: ",
                                        strftime("%Y %b %e %H:%M:%S", localtime),
                                        $myhostname, $myname, $$);

                # verifico che il file relativo all'handle $xmlLogHandle esista
                # altrimenti lo ricreo.
                # La soluzione che ho trovato e' mediante il confronto
                # fra l'inode del file associato all'handle $xmlLogHandle ed il path del file stesso.
                # Questo perche' in Unix se apro un handle e cancello fisicamente il file associato
                # l'handle continua ad esistere per l'intera vita della variabile che contiene
                # l'handle.
        if( ($xmlLogHandle->stat)[1] != (stat($XML_LOGFILE))[1] ){
                $xmlLogHandle = IO::File->new($XML_LOGFILE, 'a')
                    or die "Impossibile aprire il file di log ".
                           "di sistema $XML_LOGFILE: $!";
                $xmlLogHandle->autoflush(1);
        }

                lock($xmlLogHandle);
                print $xmlLogHandle $prefix,$strXml,$eol;
                unlock($xmlLogHandle);

        if(defined $XML_MAXSIZE){    # verifica condizione per rotazione
            my $dim = $xmlLogHandle->tell();
                _rotateFile($XML_LOGFILE, $xmlLogHandle) if($dim && $dim > $XML_MAXSIZE);
        }
    }
}


#
# Esegue la rotazione del file tramite _rotateFile
#
# PARAM IN:     false rotazione file di sistema
#               1: rotazione log legale
#               2: rotazione log xml
#
# PARAM OUT:    1 se ok, stringa d'errore o undef altrimenti
#
sub rotate {
        my $type = shift;

        return _rotateFile($SYSTEM_LOGFILE, $systemLogHandle) unless $type;
        if($type == 1){
                return _rotateFile($LEGAL_LOGFILE, $legalLogHandle);
        }elsif($type == 2){
                return _rotateFile($XML_LOGFILE, $xmlLogHandle);
        }else{
                return '';
        }
}


#
# Esegue la rotazione del file di log specificato
# Formato:
# <file di log>.<YYYYMMDD_hhmmss>[.<x>] dove x va da 1 a 10
#
# PARAM IN:     1. path file di log
#               2. handle aperto file di log
#
# PARAM OUT:    1 se ok, stringa d'errore o undef altrimenti
#
sub _rotateFile ($$) {
    my $filelog = shift;
    my $handle  = shift;
    my $file_newlog = basename($filelog); $file_newlog .= '.'._now();

    chdir( $LOGPATH ) || return undef;

    # nome file da storicizzare
    my $i;
    my $file_newlog_count = $file_newlog;
    while(-e $LOGPATH_STORY.$file_newlog_count){
        $file_newlog_count = $file_newlog.'.'.++$i;
        return undef if $i == 10}

    # se sorgente e destinazione sono sullo stesso FS
    # mouvo il file di log
    # se sono su un FS diverso copio dopo aver fatto la move
    # per togliere velocemente il lock
    # * la move consiste nel creare un link hard e cancellare la prima voce.
    # * la cancellazione di un file con lock dissolve contestualmente il lock stesso
    # in caso di problemi la rotazione non viene effettuata e si contiua a scrivere
    # nello stesso file
    my $dev_filelog = (stat($filelog))[0];
    my $dev_LOGPATH = (stat($LOGPATH))[0];

    return undef unless( defined $dev_filelog and defined $dev_LOGPATH );

    if( $dev_filelog == $dev_LOGPATH ){    # stesso FS
        lock($handle);
        link($filelog, $LOGPATH_STORY.$file_newlog_count) and unlink($filelog);
        unlock($handle);
    }else{  # FS diverso
        my $filetmp = $filelog.'.tmp';
        lock($handle);
        if( link($filelog, $filetmp) && unlink($filelog) ){
            unlock($handle);
            copy($filetmp, $LOGPATH_STORY.$file_newlog_count) && unlink($filetmp);
        }else{
            unlock($handle);
        }
    }
}

#
# Ritorna un timestamp con il formato
# YYYYMMDD_hhmmss
#
# PARAM IN:     -
#
# PARAM OUT:    timestamp
#
sub _now {
    my @ora = localtime(time);
    sprintf ("%04d%02d%02d_%02d%02d%02d",
        $ora[5]+1900,
        $ora[4]+1,
        $ora[3],
        $ora[2],
        $ora[1],
        $ora[0])
}

1;

__END__

=head1 NAME

Opec::Log - Gestione del log

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
