#
# $Id: Command.pm,v 1.13 2011/11/07 17:00:47 flazan Exp $
#
# Project       OpenPec
# file name:    Command.pm
# package:      Opec::Command
#
# DESCRIPTION
# Crea e cancella task da eseguire in un istante determinato.
# Lo scheduler controlla la scadenza dei task ed in caso positivo li passa ad openpec per l'esecuzione.
# I task sono file con il seguente formato:
# <id>@@<operazione>@<scadenza>
# <id>: obbligatorio - garantisce univocita' al messaggio
# <operazione>: obbligatorio - stringa per lo scheduler che indica l'operazione da svolgere
# <scadenza>: opzionale - se presente indica la data oltre la quale deve essere consumato;
#             se non presente il task deve essere consumato appena possibile
# <id>@@<operazione>: nome task
#
# Vengono salvati su un path condiviso per fare in modo che tutte le istanze di openpec possano eliminarli.
# La struttura condivisa sara' del tipo:
# .../QUEUE/1/tmp
# .../QUEUE/1/opt
# .../QUEUE/1/work
# .../QUEUE/2/tmp
# .../QUEUE/2/opt
# .../QUEUE/2/work
# ...
# Dove 1 e 2 stanno per le istanze di PEC in particolare e' il valore della variabile $idServer.
# Il ruolo delle dir e' simile a quello per le Maildir.
# Operazioni previste per la CREAZIONE del file:
# 1. per la istanza x chdir in .../QUEUE/x
# 2. verificare l'esistenza del file che stiamo andando a creare con stat ( stat(.../QUEUE/x/tmp/<msg id>@@<nome cmd>@<scadenza>) )
# 3. se il file non esiste lo creo, scrivo e chiudo
# 4. creare un link hard del file sotto .../QUEUE/x/opt
# 5. eliminare il file .../QUEUE/x/tmp
#
# Operazioni previste per la LETTURA/CANCELLAZIONE del file:
# 1. creare un link hard del file sotto .../QUEUE/x/work
# 2. apro il file sotto .../QUEUE/x/work, recupero il contenuto, eseguo le operazioni previste
# 3. elimino il file sotto opt e work
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 07/05/2006 - Fanton Flavio
# modification:
#   09/06/2011 - Fanton Flavio
#               - aggiunti i metodi delTaskSafe e getTaskNum per problema RC + RMC
#               - delTaskScheduler e' la vecchia delTask e delTask e' modificata
#                 per non eliminare i task rem
#   07/11/2011 - Fanton Flavio
#               - aggiunto log di sistema della gestione dei task
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Roma 43 - 57126 Livorno (LI) - Italy
#   via Giuntini, 25 / int. 9 - 56023 Navacchio (PI) - Italy
#   tel. +39 050 754 703 - fax +39 050 754 707
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#



package Opec::Command;


use strict;
use Opec::Conf  qw ( :confvars :scheduler :const :log );
use Opec::Util qw( &doLog );
use Storable qw(store retrieve);

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    @EXPORT = ();
    @EXPORT_OK = qw(    &enqTask
                        &deqTask
                        &delTask
                        &delTaskScheduler
                        &delTaskSafe
                        &getTaskNum);
}


#
# Restituisce il task - non fa alcuna cancellazione
#
# paramIN:   1. nome completo task
#
# paramOUT:   ref obj se ok, stringa d'errore altrimenti
#
sub deqTask ($) {
    my $taskname   = shift;

    doLog(4,"deqTask - 1/3: path: $OPEC_QUEUE_PATHNAME/$idServer") if $SYSTEM_DO_LOG_TASK;
    chdir($OPEC_QUEUE_PATHNAME.'/'.$idServer) || return $!;
    my $work_path = $OPEC_QUEUE_DIR_WORK.'/'.$taskname;

    # non faccio alcuna verifica sulla preesistenza del task sotto work
    # visto che se e' presente significa che e' una situazione di ripristino
    # comtemplata

    # sposto il file sotto la dir di lavoro
    if($SYSTEM_DO_LOG_TASK){
        my $out_link = link "$OPEC_QUEUE_DIR_OPT/$taskname", $work_path;
        if(!$out_link){
                doLog(4,"deqTask - 2/3: copia di $taskname in $OPEC_QUEUE_DIR_WORK: errore ($!)");
                return $!}
        doLog(4,"deqTask - 2/3: copia di $taskname in $OPEC_QUEUE_DIR_WORK: Ok");

        my $out_retrive = _retrieve($work_path);
        if(ref($out_retrive)){
                doLog(4,"deqTask - 3/3: recupero del task: Ok");
        }else{
                doLog(4,"deqTask - 3/3: recupero del task: errore ($out_retrive)");
        }
        return $out_retrive;
    }else{
        link "$OPEC_QUEUE_DIR_OPT/$taskname", $work_path;
        return _retrieve($work_path);
    }

}


#
# Elimina i task dal path opt dell'appropriata istanza di openpec
#
# paramIN:   1. <msg id> del task
#           (2. boolean - true se la cancellazione e' relativa all'istanza di openpec in oggetto, false altrimenti)
#           (3. boolean - true elimina il task sotto opt e work, false solo sotto work)
#
# paramOUT:   undef se ok, stringa d'errore altrimenti
#
sub delTask ($;$$) {
    my $taskid    = shift;
    my $istanza   = shift;
    my $whatDel   = shift;

    # check formato di taskid (nome task)
    # <id>@@<operazione>: nome task
    # <id>: xxx@dominio
    # <operazione>: xxx
    my ($tmp_id, $tmp_operazione) = split /\@\@/, $taskid;
    unless(defined $tmp_id && defined $tmp_operazione){
        doLog(4,"delTask: Formato nome task non valido: $taskid") if $SYSTEM_DO_LOG_TASK;
        return "Formato nome task non valido: $taskid";
    }

    $istanza = $istanza ? $idServer : _getIstanza($taskid);
    doLog(4,"delTask: path: $OPEC_QUEUE_PATHNAME/$istanza/") if $SYSTEM_DO_LOG_TASK;
    chdir($OPEC_QUEUE_PATHNAME.'/'.$istanza.'/') || return $!;

        # se il task rem esiste gia' non faccio niente,
        # altrimenti rischio di cancellare solo il rem
        # e lasciare il task
        # a pareggiare i conti ci pensara' lo scheduler
        my $taskid_rem = $taskid.'_';
        if(-e $OPEC_QUEUE_DIR_OPT.'/'.$taskid_rem){
                doLog(4,"delTask: task rem ($OPEC_QUEUE_DIR_OPT/$taskid_rem) esistente, non faccio niente") if $SYSTEM_DO_LOG_TASK;
                return;
        }

    $taskid =~ s/[@\$\-\%\(\)\\\/\<\>]/\\$&/go;

    my $tmp_opt  = $OPEC_QUEUE_DIR_OPT.'/'.$taskid;
    my $tmp_work = $OPEC_QUEUE_DIR_WORK.'/'.$taskid;
#    if($whatDel && !unlink <$tmp_opt*>){
    if($whatDel){
        my $out_unlink_opt = unlink <$tmp_opt*>;
        if($out_unlink_opt){
                doLog(4,"delTask: cancellazione di $tmp_opt*: $out_unlink_opt files")
                        if $SYSTEM_DO_LOG_TASK;
        }else{
                doLog(4,"delTask: $tmp_opt*: nessun file da cancellare") if $SYSTEM_DO_LOG_TASK;
                return "Impossibile eliminare ".$OPEC_QUEUE_DIR_OPT.'/'.$taskid
        }
    }

    my $out_unlink = unlink <$tmp_work*>;
    doLog(4,"delTask: pulizia di $tmp_work* ($out_unlink task cancellati)") if $SYSTEM_DO_LOG_TASK;
    undef
}


#
# Elimina i task dal path opt dell'appropriata istanza di openpec
#
# paramIN:   1. <msg id> del task
#           (2. boolean - true se la cancellazione e' relativa all'istanza di openpec in oggetto, false altrimenti)
#           (3. boolean - true elimina il task sotto opt e work, false solo sotto work)
#
# paramOUT:   undef se ok, stringa d'errore altrimenti
#
sub delTaskScheduler ($;$$) {
    my $taskid    = shift;
    my $istanza   = shift;
    my $whatDel   = shift;

    # check formato di taskid (nome task)
    # <id>@@<operazione>: nome task
    # <id>: xxx@dominio
    # <operazione>: xxx
    my ($tmp_id, $tmp_operazione) = split /\@\@/, $taskid;
    unless(defined $tmp_id && defined $tmp_operazione){
        doLog(4,"delTaskScheduler: Formato nome task non valido: $taskid") if $SYSTEM_DO_LOG_TASK;
        return "Formato nome task non valido: $taskid";
    }


    $istanza = $istanza ? $idServer : _getIstanza($taskid);
    doLog(4,"delTaskScheduler: path: $OPEC_QUEUE_PATHNAME/$istanza/") if $SYSTEM_DO_LOG_TASK;
    chdir($OPEC_QUEUE_PATHNAME.'/'.$istanza.'/') || return $!;

    $taskid =~ s/[@\$\-\%\(\)\\\/\<\>]/\\$&/go;

    my $tmp_opt  = $OPEC_QUEUE_DIR_OPT.'/'.$taskid;
    my $tmp_work = $OPEC_QUEUE_DIR_WORK.'/'.$taskid;
    if($whatDel && !unlink <$tmp_opt*>){
        doLog(4,"delTaskScheduler: impossibile eliminare $OPEC_QUEUE_DIR_OPT/$taskid ($!)") if $SYSTEM_DO_LOG_TASK;
        return "Impossibile eliminare ".$OPEC_QUEUE_DIR_OPT.'/'.$taskid
    }


    my $out_unlink = unlink <$tmp_work*>;
    doLog(4,"delTaskSafe: pulizia di $tmp_work* ($out_unlink task cancellati)") if $SYSTEM_DO_LOG_TASK;
    undef
}

#
# Elimina i task dal path opt dell'appropriata istanza di openpec
# se non ci riesce perche' il task non e' presente, crea un task
# DELTASK_REMAINDER per ritentare la cancellazione successivamente
#
# paramIN:   1. <msg id> del task
#                        2. contatore univoco rispetto al processso
#           (3. boolean - true se la cancellazione e' relativa all'istanza di openpec in oggetto, false altrimenti)
#           (4. boolean - true elimina il task sotto opt e work, false solo sotto work)
#
# paramOUT:   undef se ok, stringa d'errore altrimenti
#
sub delTaskSafe ($$;$) {
    my ($taskid, $istanza, $whatDel) = @_;

    # qui dovremmo essere sempre nel caso $whatDel = 1

    # check formato di taskid (nome task)
    # <id>@@<operazione>: nome task
    # <id>: xxx@dominio
    # <operazione>: xxx
    my ($tmp_id, $tmp_operazione) = split /\@\@/, $taskid;
    unless(defined $tmp_id && defined $tmp_operazione){
        doLog(4,"delTaskSafe: Formato nome task non valido: $taskid") if $SYSTEM_DO_LOG_TASK;
        return "Formato nome task non valido: $taskid";
    }

    $istanza = $istanza ? $idServer : _getIstanza($taskid);
    doLog(4,"delTaskSafe: path: $OPEC_QUEUE_PATHNAME/$istanza/") if $SYSTEM_DO_LOG_TASK;
    chdir($OPEC_QUEUE_PATHNAME.'/'.$istanza.'/') || return $!;

        # se il task rem esiste gia' non faccio niente,
        # altrimenti rischio di cancellare solo il rem
        # e lasciare il task
        # a pareggiare i conti ci pensara' lo scheduler
        my $taskid_rem = $taskid.'_';
        return if(-e $OPEC_QUEUE_DIR_OPT.'/'.$taskid_rem);

    $taskid =~ s/[@\$\-\%\(\)\\\/\<\>]/\\$&/go;

    my $tmp_opt  = $OPEC_QUEUE_DIR_OPT.'/'.$taskid;
    my $tmp_work = $OPEC_QUEUE_DIR_WORK.'/'.$taskid;
    my $outenq;
    #if($whatDel && !unlink <$tmp_opt*>){       # provo a cancellare
    if($whatDel){       # provo a cancellare
        my $out_unlink_opt = unlink <$tmp_opt*>;
        if($out_unlink_opt){
                doLog(4,"delTaskSafe: cancellati task $tmp_opt* ($out_unlink_opt files)")
                        if $SYSTEM_DO_LOG_TASK;
        }else{
                # non sono riuscito, aggiungo il task che posticipa la cancellazione
                doLog(4,"delTaskSafe: $tmp_opt*: nessun file da cancellare, accodo il rem") if $SYSTEM_DO_LOG_TASK;
                    my %tasks;
                    $tasks{ $taskid_rem } = [1,\@_];
                    $outenq = enqTask( \%tasks, $istanza );
                    if($SYSTEM_DO_LOG_TASK){
                        $outenq ? doLog(4,"delTaskSafe: enq rem: errore ($outenq)")
                                : doLog(4,"delTaskSafe: enq rem: Ok")
                    }
        }
    }

    my $out_unlink = unlink <$tmp_work*>;
    doLog(4,"delTaskSafe: pulizia di $tmp_work* ($out_unlink task cancellati)") if $SYSTEM_DO_LOG_TASK;
    return $outenq;
}


#
# Restituisce il numero di task presenti
# dato <id>@@<operazione>
#
# paramIN:   1. <msg id> del task
#                        2. istanza
#
# paramOUT:   numero di task presenti, undef in caso d'errore
#
sub getTaskNum ($$) {
    my ($taskid, $istanza) = @_;

    # check formato di taskid (nome task)
    # <id>@@<operazione>: nome task
    # <id>: xxx@dominio
    # <operazione>: xxx
    my ($tmp_id, $tmp_operazione) = split /\@\@/, $taskid;
    return unless(defined $tmp_id && defined $tmp_operazione);

    $istanza = $istanza ? $idServer : _getIstanza($taskid);
    chdir($OPEC_QUEUE_PATHNAME.'/'.$istanza.'/') || return;

    $taskid =~ s/[@\$\-\%\(\)\\\/\<\>]/\\$&/go;

    my $tmp_opt  = $OPEC_QUEUE_DIR_OPT.'/'.$taskid;
    my @files = <$tmp_opt*>;

    return scalar @files;
}


#
# Generatore generico di task
#
#
# paramIN:   hash ref. (nome task -> obj da storare) degli elementi da aggiungere cone task
#
# paramOUT:  undef se ok, stringa d'errore altrimenti
#
#
#
sub enqTask {
    my $href = shift || return;
    my $idServerTmp = shift;
    local $idServer = $idServerTmp if defined $idServerTmp;

        doLog(4,"enqTask - 1/4: path: $OPEC_QUEUE_PATHNAME/$idServer") if $SYSTEM_DO_LOG_TASK;
    chdir($OPEC_QUEUE_PATHNAME.'/'.$idServer) || return $!;

        my @out;
        if($SYSTEM_DO_LOG_TASK){
                foreach my $elem (keys %$href){
                        # store task in tmp
                        doLog(4,"enqTask: lavorazione del task $elem");
                        my $out_store = _store($href->{$elem}, $OPEC_QUEUE_DIR_TMP.'/'.$elem);
                        if($out_store){
                                # errore
                                doLog(4,"enqTask - 2/4: store task in $OPEC_QUEUE_DIR_TMP: $out_store");
                                push @out,$elem;
                                last}
                        doLog(4,"enqTask - 2/4: store task in $OPEC_QUEUE_DIR_TMP: Ok");

                        # copia task in opt
                        my $out_link = link $OPEC_QUEUE_DIR_TMP.'/'.$elem, $OPEC_QUEUE_DIR_OPT.'/'.$elem;
                        if(!$out_link){
                                # errore
                                doLog(4,"enqTask - 3/4: copia task in $OPEC_QUEUE_DIR_OPT: errore ($!)");
                                push @out,$elem;
                                last}
                        doLog(4,"enqTask - 3/4: copia task in $OPEC_QUEUE_DIR_OPT: Ok");

                        # cancellazione task in tmp
                        my $out_unlink = unlink $OPEC_QUEUE_DIR_TMP.'/'.$elem;
                        if(!$out_unlink){
                                # errore
                                doLog(4,"enqTask - 4/4: cancellazione task in $OPEC_QUEUE_DIR_TMP: errore ($!)");
                                push @out,$elem;
                                last}
                        doLog(4,"enqTask - 4/4: cancellazione task in $OPEC_QUEUE_DIR_TMP: Ok");
                }
        }else{
            @out = grep {!(!_store($href->{$_}, $OPEC_QUEUE_DIR_TMP.'/'.$_) and
                               link $OPEC_QUEUE_DIR_TMP.'/'.$_, $OPEC_QUEUE_DIR_OPT.'/'.$_ and
                               unlink $OPEC_QUEUE_DIR_TMP.'/'.$_)} keys %$href;
        }

    @out > 0 ? "Errore durante la creazione dei task RMC" : undef;
}


#
# paramIN:  1. rif obj della struttura da salvare
#           2. path file
#
# paramOUT: undef se ok, stringa d'errore altrimenti
#
sub _store {
    my $objRef2Store = shift;
    my $filePath = shift || return undef;
    my $out;

    eval{$out = store($objRef2Store, $filePath)};
    $@ ? $@ : $out ? undef : "Errore durante l'operazione di store"
}

#
# paramIN:  1. path file
#
# paramOUT: rif. obj se ok, stringa d'errore altrimenti
#
sub _retrieve {
    my $filePath = shift;
    my $hRef;

    eval{$hRef = retrieve($filePath)};
    $@ ? $@ : ref $hRef ? $hRef : "Errore durante l'operazione di retrieve"
}

sub _getIstanza ($){
    my $msgid = shift;

    # mi aspetto che il msgid sia del tipo
    # xxxx.id@dominio
    if( index($msgid, ',') != -1 ){
        ($msgid) = (split /,/, $msgid)[1];
    }
    my @arr = split /\./, substr($msgid, 0, index($msgid, '@'));
    pop @arr
}

1;

__END__

=head1 NAME

Opec::Command - Gestione code

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
