#
# $Id: Transport.pm,v 1.34 2012/04/26 16:20:38 flazan Exp $
#
# Project       OpenPec
# file name:    Transport.pm
# package:      Opec::Transport
#
# DESCRIPTION
# Generatore di documenti/anomalie di trasporto
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   21/10/2004 - Fanton Flavio - segnalato da Zanarini
#       Corretta la costante Replay in Reply
#   21/10/2004 - Fanton Flavio - segnalazione e soluzione Zuin
#       Aggiornamento timezone
#   04/01/2005 - Fanton Flavio - segnalazione e sentiti
#                ringraziamenti a Stefano Brandimarte [stevens@ced.it]
#       Aggiornata _getDocument per getire correttamente
#       il Message-ID della mail originale.
#   24/03/2006 - Umberto Ferrara - aggiornamento per gestione ricevute
#                                  breve e sintetica
#   23/07/2007 - Flavio Fanton
#     - forzata codifica a 7-bit per il body del DT/ANOM TRASP (_getDocument)
#   15/12/2008 - Flavio Fanton
#     - bugfix su caratteri speciali con codifica diversa da iso-8859-1
#   12/05/2009 - Flavio Fanton
#     - aggiunta lista di campi da copiare dal msg-originale al DT
#   29/05/2009 - Luca Di Vizio
#     - bugfix per caratteri che utilizzano code page
#   12/10/2010 - Fanton Flavio
#         - spostata la routine per l'aggiunta di header extra dal msg-originale
#               sul DT per evitare il limite di MIME::Entity->build
#   20/12/2010 - Fanton Flavio
#         - sincronizzati istanti temporali tra xml e body
#   22/12/2010 - Fanton Flavio
#         - bugfix su subject con errore nella decodifica
#       28/03/2011 - Fanton Flavio
#               - aggiunta gestione di $KEY_TEMPLATE_RCPTTO_SHORT che si riferisce ai
#         destinatari senza il dettaglio certificato/non certificato
#       28/03/2011 - Fanton Flavio
#               - aggiunta gestione di $KEY_TEMPLATE_RCPTTO_SHORT che si riferisce ai
#         destinatari senza il dettaglio certificato/non certificato
#       30/05/2011 - Fanton Flavio
#               - aggiunto template html
#       31/08/2011 - Fanton Flavio
#               - bugfix su tag rcpttoshort nella busta di trasporto
#       13/10/2011 - Fanton Flavio
#     - gestione di TS header = TS body = TS xml = istante di creazione dell'oggetto che rappresenta il messaggio
#       oppure  TS header = istante di ricezione del messaggio originale e
#               TS body = TS xml = istante di creazione dell'oggetto che rappresenta il messaggio
#       26/04/2012 - Fanton Flavio
#     - fix coerenza TS Header, TS Body, TS XML
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#
package Opec::Transport;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
    %EXPORT_TAGS = ();
    @EXPORT = ();
    @EXPORT_OK = qw( &generateTransport &getTransportFault );
}


use strict;
use MIME::Words qw( encode_mimeword );
use Encode qw( encode );
use Opec::Conf qw ( :confvars :const :template $OPEC_PREFROM_DOCTRANSP);
use Opec::Util qw ( &actualDateTime &actualTime &actualDate &myChomp
                    &timezone);
use Opec::Xml qw ( &getCertInfo_doctrasp &getCertInfo_anom );

my @OPEC_HEADER_MIME_VERSION = ('MIME-Version', '1.0');

# obj contenente i dati da fondere con i template
my %myFields;
    $myFields{$KEY_TEMPLATE_DATA}       = '';
    $myFields{$KEY_TEMPLATE_ORA}        = '';
    $myFields{$KEY_TEMPLATE_ZONA}       = '';
    $myFields{$KEY_TEMPLATE_SUBJECT}    = '';
    $myFields{$KEY_TEMPLATE_MAILFROM}   = '';
    $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} = '';
    $myFields{$KEY_TEMPLATE_RCPTTO}     = '';
    $myFields{$KEY_TEMPLATE_MSGID}      = '';

#
# Genera il documento di trasporto
#
# IN: 1. Opec::Message - messaggio originale
# OUT: MIME::Entity - Documento di trasporto
#
sub generateTransport ($)
    {_getDocument(shift,0,undef,$OPEC_HEADER_VALUE_DOCTRANSP_TRANSP)}

#
# crea la ricevuta di anomalia di trasporto
#
# IN: 1. ref Opec::Message - Messaggio originario
#     2. error message
# OUT: ref Opec::Message - Ricevuta
#
sub getTransportFault ($$)
    {_getDocument(shift,1,shift,$OPEC_HEADER_VALUE_DOCTRANSP_ANOM)}

#
# generatore di documenti/aomalie di trasporto generico
#
# IN: 1. messagio originale (Opec::Message)
#     2. tipo di documento:
#        0 - doc. di trasporto
#        1 - anomalia di trasporto
#     3. (stringa di errore - solo anomalia di trasporto)
# OUT: nuovo messaggio (Opec::Message)
#
sub _getDocument ($$;$$){
    my $originalMsg     = shift;
    my $docType         = shift;
    my $strErrAnom      = shift;
    my $docName         = shift;
        my $originalMsgHead = $originalMsg->header;
        my $originalMsgEmail= $originalMsg->mail_text;
    # nuovo messaggio
        my $myTransportMsg      = new Opec::Message (
                                  $originalMsg->tempdir(),$docName);
        my $myTransportMsgMime  = $myTransportMsg->mime_entity;
        my $myTransportMsgHead  = new MIME::Head;

    #--- creo l'header
        # aggiungo i campi del messaggio originale
        # che devono rimanere inalterati
        foreach my $elem('Received',
                         'To',
                         'Cc',
                                 'Return-Path',
                                 $OPEC_HEADER_KEY_MSGID,
                                 @OPEC_HEADERTOFLOAT){
            next unless defined myChomp($originalMsgHead->get ($elem));
            map($myTransportMsgHead->add ($elem, $_),
                $originalMsgHead->get ($elem))}

    $myTransportMsg->msgid_new($originalMsg->msgid_new());
    $myTransportMsg->msgid_orig($originalMsg->msgid_orig());

        $myTransportMsgHead->add ($OPEC_HEADER_MIME_VERSION[0], $OPEC_HEADER_MIME_VERSION[1]);

        # TS Header = TS Body = TS XML = istante di ricezione messaggio originale
        $myTransportMsgHead->add ('Date', actualDateTime($originalMsg->rx_time));

        my $orig_sndr = $originalMsg->sender->mailAddress;
    $myTransportMsgHead->add ('From',
                           "\"". $OPEC_PREFROM_DOCTRANSP . $orig_sndr."\"".
                           " <$adminPecEmail>");
    if( $originalMsgHead->get('Reply-To') ){
        $myTransportMsgHead->add ('Reply-To', myChomp($originalMsgHead->get('Reply-To')))
    }else{
        $myTransportMsgHead->add ('Reply-To', $orig_sndr)}

    #--- specializzazione documento/anomalia
        my $originalMsgHeadSubject = $originalMsg->h_subject_utf8;
    my @orig_recips = @{$originalMsg->recips};
        my @recips_mailAddress = map($_->mailAddress, @orig_recips);
        my $body;
        my $body_html;
    if($docType == 0){
        # DOCUMENTO DI TRASPORTO
        # header
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $myTransportMsgHead->add ($OPEC_HEADER_KEY_MSGID_RIF,
                                      $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        }
            $myTransportMsgHead->add ($OPEC_HEADER_KEY_DOCTRASP,
                                      $OPEC_HEADER_VALUE_DOCTRANSP_TRANSP);

        my $xtype_receipt = myChomp($originalMsgHead->get($OPEC_HEADER_KEY_RECEIPT_TYPE));
        if( defined $xtype_receipt ){
            my $xtype_receipt_lc = lc($xtype_receipt);
            if( $xtype_receipt_lc eq $OPEC_HEADER_VALUE_RECEIPT_TYPE_SHORT or
                $xtype_receipt_lc eq $OPEC_HEADER_VALUE_RECEIPT_TYPE_SYNTHETIC or
                $xtype_receipt_lc eq $OPEC_HEADER_VALUE_RECEIPT_TYPE_COMPLETE ){

                $myTransportMsgHead->add( $OPEC_HEADER_KEY_RECEIPT_TYPE, $xtype_receipt);
            }
        }
        $myTransportMsgHead->add (  'Subject',
                                    ($originalMsgHeadSubject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_DOCTRASP.$originalMsgHeadSubject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_DOCTRASP.$originalMsgHeadSubject);
        # body
        $myFields{$KEY_TEMPLATE_RCPTTO}     = join ("\n",
            map( $_->certificate?$_->mailAddress.' "posta certificata"':
                 $_->mailAddress.' "posta ordinaria"',
                    @orig_recips));
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT}= join ("\n",
            map( $_->mailAddress,
                 @orig_recips));
        $body = $TEMPLATE_DOCTRASP;
        $body_html = $TEMPLATE_DOCTRASP_HTML;
    }elsif($docType == 1){
        # ANOMALIA DI TRASPORTO
        # header
        if( myChomp($originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF)) ){
            $myTransportMsgHead->replace ($OPEC_HEADER_KEY_MSGID,
                                          $originalMsgHead->get($OPEC_HEADER_KEY_MSGID_RIF));
        }

            $myTransportMsgHead->add ($OPEC_HEADER_KEY_DOCTRASP,
                                   $OPEC_HEADER_VALUE_DOCTRANSP_ANOM);
        $myTransportMsgHead->add (  'Subject',
                                    ($originalMsgHeadSubject =~ /\P{IsASCII}/o ) ?
                                        encode_mimeword(encode("iso-8859-1",
                                                               $OPEC_HEADER_SUBJECT_ANOM.$originalMsgHeadSubject),
                                                        "B",
                                                        "iso-8859-1") :
                                        $OPEC_HEADER_SUBJECT_ANOM.$originalMsgHeadSubject);
        # body
        $myFields{$KEY_TEMPLATE_ERR} = $strErrAnom;
        $myFields{$KEY_TEMPLATE_RCPTTO_SHORT} =
        $myFields{$KEY_TEMPLATE_RCPTTO} =
            join ("\n", map( $_->mailAddress, @orig_recips));
        $body = $TEMPLATE_DOCTRANSP_ANOM;
        $body_html = $TEMPLATE_DOCTRANSP_ANOM_HTML;
    }

    #--- genero l'envelop
#       $myTransportMsg->sender($orig_sndr);
#       $myTransportMsg->recips( \@recips_mailAddress );
        $myTransportMsg->{sender} = $originalMsg->sender();
        $myTransportMsg->{recips} = $originalMsg->recips();
        $myTransportMsg->{direction} = $originalMsg->direction();

    #--- genero il BODY e lo allego
    $myFields{$KEY_TEMPLATE_DATA}       = actualDate($originalMsg->rx_time);
    $myFields{$KEY_TEMPLATE_ORA}        = actualTime($originalMsg->rx_time);
    $myFields{$KEY_TEMPLATE_ZONA}       = timezone($originalMsg->rx_time);
    $myFields{$KEY_TEMPLATE_SUBJECT}    = encode("iso-8859-1",$originalMsgHeadSubject);
    $myFields{$KEY_TEMPLATE_MAILFROM}   = $orig_sndr;
    $myFields{$KEY_TEMPLATE_MSGID}      = $originalMsg->msgid_new();

        $body =~ s/\[(\w+)\]/$myFields{lc($1)}/gm;
        $body_html =~ s/\[(\w+)\]/$myFields{lc($1)}/gm if $body_html;
        $myTransportMsgHead->replace('X-Mailer', undef);
        if($body_html){
            $myTransportMsgMime->build( %{$myTransportMsgHead->header_hashref()},
                                        Type => 'multipart/mixed');

            my $top_alternative = $myTransportMsgMime->attach(Type => 'multipart/alternative');
            $top_alternative->preamble([]);

            $top_alternative->attach(Type     => 'text/plain',
                                     Encoding => 'quoted-printable',
                                     Data     => $body,
                                     Charset  => 'iso-8859-1');

            $top_alternative->attach(Type     => 'text/html',
                                     Encoding => 'quoted-printable',
                                     Data     => $body_html,
                                     Charset  => 'iso-8859-1');
        }else{
            $myTransportMsgMime->build( %{$myTransportMsgHead->header_hashref()},
                                        Type     => 'text/plain',
                                        Encoding => 'quoted-printable',
                                        Data     => $body,
                                        Charset  => 'iso-8859-1');
        }
        $myTransportMsgMime->preamble([]);

    #--- porto i campi dell'header del msg-originale
    # indicati in conf
        foreach my $elem( @OPEC_MAILHEADER_WHITELIST ){
            next unless defined myChomp($originalMsgHead->get ($elem));
            map($myTransportMsgMime->head->add ($elem, $_),
                $originalMsgHead->get ($elem))}

    if($docType == 0){  # documento di trasporto
        # genero l'allegato XML
        my $xml = getCertInfo_doctrasp($originalMsg->rx_time,$originalMsg);
                utf8::encode($xml);

        $myTransportMsg->xml_local($xml);

        # aggiungo in attach il doc xml
        $myTransportMsgMime->attach (Data => $xml,
                                                  Type => 'application/xml',
                                                  Encoding => 'base64',
                                                  Filename => 'daticert.xml')

    }

        #--- aggiungo in attach il messaggio originale

    # viene parsato solo l'header quindi non abbiamo
    # problemi di memoria
        $myTransportMsgMime->attach (Path => $originalMsg->tempfiledir,
                                     Type => 'message/rfc822',
                                     Encoding => '7bit',
                                     Filename => 'postacert.eml');

    $myTransportMsg
}


1;

__END__

=head1 NAME

Opec::Transport - Generatore documenti di trasporto e anomalie di messaggio

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
