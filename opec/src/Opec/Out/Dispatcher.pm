#
# $Id: Dispatcher.pm,v 1.13 2011/02/14 16:19:52 flazan Exp $
#
# Project       OpenPec
# file name:    Dispatcher.pm
# package:      Opec::Out::Dispatcher
#
# DESCRIPTION
# Livello di astrazione per l'interfaccia di comunicazione
# verso l'esterno
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   20/01/2011 - Fanton Flavio - aggiunta la compatibilita' con le mappe
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

package Opec::Out::Dispatcher;


use strict;

use Opec::Conf qw(  $forward_canonical
                    $forward_pec
                    $localhost_name
                    %hLocalCertDomains);
#use Opec::Out::LMTP;    # delivery certificato


#
# COSTRUTTORE
#  IN:  1. hash da passare al costruttore del modulo
#  OUT:  rif. oggetto se ok, stringa d'errore altrimenti
#
sub new {
        my $proto       = shift;
        my $obj_mapper  = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

    if(         ref($obj_mapper) &&
        ($forward_canonical =~ /^\d$/ or $forward_pec =~ /^\d$/)){

            $self->{mapper}   = $obj_mapper; # riferimento all'oggetto mapper
            $self->{cache_dm} = {};              # cache rif obj delivery module
    }

    if(ref($self->{mapper}) && $forward_canonical =~ /^\d$/){
        # mappa
        return 'Opec::Out::Dispatcher: impossibile caricare la mappa $forward_canonical'
                if !$self->{mapper}->mapExists($forward_canonical);
        $self->{obj_fwd_canonical} = $forward_canonical
    }else{
            # carico il modulo parametrico per il delivery canonico
            my ($method,$param) = split(/:/,$forward_canonical,2);

            my $strGetModule = 'Opec::Out::'.$method;
            eval "require $strGetModule" or
                return "Opec::Out::Dispatcher: impossibile caricare $strGetModule: $@";

            $self->{cache_dm}->{$method} =
            $self->{obj_fwd_canonical}   = $strGetModule->new($param);
                return "Opec::Out::Dispatcher: ". $self->{obj_fwd_canonical}
                    unless(ref($self->{obj_fwd_canonical}));
    }

    if(ref($self->{mapper}) && $forward_pec =~ /^\d$/){
        # mappa
        return 'Opec::Out::Dispatcher: impossibile caricare la mappa $forward_pec'
                if !$self->{mapper}->mapExists($forward_pec);
        $self->{obj_fwd_pec} = $forward_pec
    }else{
                # istanzio il modulo per il deliveri PEC
                my ($method,$param) = split(/:/,$forward_pec,2);

            my $strGetModule = 'Opec::Out::'.$method;
            eval "require $strGetModule" or
                return "Opec::Out::Dispatcher: impossibile caricare $strGetModule: $@";

            $self->{cache_dm}->{$method} =
            $self->{obj_fwd_pec} = Opec::Out::LMTP->new($param);
                return "Opec::Out::Dispatcher: ".$self->{obj_fwd_pec}
                    unless(ref($self->{obj_fwd_pec}));
    }

    $self
}


#
# Interfaccia per il delivery del msg
# (richiama il metodo dispatch del modulo
# caricato su richiesta nella new)
#
# IN: 1. obj Opec::Message o arr ref a obj
#     2. tipo di delivery: false delivery canonico
#                          1 delivery sicuro (PEC)
#                          2 delivery sicuro per i soli destinatari locali
#                            mentre delivery canonico per i dest. esterni
#
# OUT: array di quanto restituito dalla dispatch
#
sub mailDispatch ($$) {
    my $self            = shift;
    my $objRef          = shift;
    my $secureDelivery  = shift;
    my @out;

    $objRef = [$objRef] if(ref($objRef) ne 'ARRAY');

    if($secureDelivery && $secureDelivery == 2){
        # valuto se usare il canale sicuro o non sicuro
        foreach my $objMsg (@{$objRef}){
            my @allRcpt = @{$objMsg->recips()};
            my @localRcpt;
            my @nolocalRcpt;
            foreach (@allRcpt){
                $_->mailAddress =~ /.+\@(.+)/;
                exists $hLocalCertDomains{lc($1)} ?
                    push @localRcpt, $_ :
                    push @nolocalRcpt, $_
            }

            my @rcptTmp;
            if(@localRcpt){
                # dispaccio a dest. locali cert
                if( ref($self->{obj_fwd_pec}) ){
                        # no mappe
                        $objMsg->recips(\@localRcpt);
                        @out = $self->{obj_fwd_pec}->dispatch($objMsg);
                        @rcptTmp = @{$objMsg->recips()}
                }else{
                        # utilizzo delle mappe
                        foreach (@localRcpt){
                                my $map_res = $self->{mapper}->getData($self->{obj_fwd_pec}, $_->mailAddress);
                                die $self->{mapper}->lastErr($self->{obj_fwd_pec}) if $self->{mapper}->lastErr($self->{obj_fwd_pec});
                                next if !$map_res;

                                            # carico il modulo parametrico per il delivery canonico
                                                my ($method,$param) = split(/:/,$map_res,2);

                                                if(!exists $self->{cache_dm}->{$method}){
                                                        # il modulo non e' stato ancora caricato: lo carico
                                                    my $strGetModule = 'Opec::Out::'.$method;
                                                    eval "require $strGetModule" or
                                                        die "Opec::Out::Dispatcher: impossibile caricare $strGetModule: $@";

                                                    $self->{cache_dm}->{$method} = $strGetModule->new($param);
                                                        return "Op$self->{mapper}->lastErr()ec::Out::Dispatcher - $strGetModule: ". $self->{cache_dm}->{$method}
                                                            unless(ref($self->{cache_dm}->{$method}));
                                                }

                                $objMsg->recips([$_]);
                                                push @out, $self->{cache_dm}->{$method}->dispatch($objMsg,$param);
                                                push @rcptTmp, @{$objMsg->recips()};
                        }
                }
            }
            if(@nolocalRcpt){
                # dispaccio a dest. no cert
                if( ref($self->{obj_fwd_canonical}) ){
                        # no mappe
                        $objMsg->recips(\@nolocalRcpt);
                        push @out, $self->{obj_fwd_canonical}->dispatch($objMsg);
                        push @rcptTmp, @{$objMsg->recips()};
                }else{
                        # utilizzo delle mappe
                        foreach (@nolocalRcpt){
                                my $map_res = $self->{mapper}->getData($self->{obj_fwd_canonical}, $_->mailAddress);
                                die $self->{mapper}->lastErr($self->{obj_fwd_canonical}) if $self->{mapper}->lastErr($self->{obj_fwd_canonical});
                                next if !$map_res;

                                            # carico il modulo parametrico per il delivery canonico
                                                my ($method,$param) = split(/:/,$map_res,2);

                                                if(!exists $self->{cache_dm}->{$method}){
                                                        # il modulo non e' stato ancora caricato: lo carico
                                                    my $strGetModule = 'Opec::Out::'.$method;
                                                    eval "require $strGetModule" or
                                                        die "Opec::Out::Dispatcher: impossibile caricare $strGetModule: $@";

                                                    $self->{cache_dm}->{$method} = $strGetModule->new($param);
                                                        return "Opec::Out::Dispatcher - $strGetModule: ". $self->{cache_dm}->{$method}
                                                            unless(ref($self->{cache_dm}->{$method}));
                                                }

                                $objMsg->recips([$_]);
                                                push @out, $self->{cache_dm}->{$method}->dispatch($objMsg,$param);
                                                push @rcptTmp, @{$objMsg->recips()};
                        }
                }
            }
            $objMsg->recips(\@rcptTmp)
        }
    }else{
        my $deliveryRef = $secureDelivery ?
            $self->{obj_fwd_pec} :
            $self->{obj_fwd_canonical};
        # con map si perdono i riferimenti, dalla doc:
        # << ... it can cause bizarre results if the elements of LIST
        #  are not variables.>>
        #@out = map $deliveryRef->dispatch($_), @{$objRef}}
        if( ref($deliveryRef) ){
                        foreach my $objMsg (@{$objRef}){
                                push @out, $deliveryRef->dispatch($objMsg)}
        }else{
                foreach my $objMsg (@{$objRef}){
                        foreach (@{$objMsg->recips()}){
                                my $map_res = $self->{mapper}->getData($deliveryRef, $_->mailAddress);
                                die $self->{mapper}->lastErr($deliveryRef) if $self->{mapper}->lastErr($deliveryRef);
                                next if !$map_res;

                                    # carico il modulo parametrico per il delivery canonico
                                        my ($method,$param) = split(/:/,$map_res,2);

                                        if(!exists $self->{cache_dm}->{$method}){
                                                # il modulo non e' stato ancora caricato: lo carico
                                            my $strGetModule = 'Opec::Out::'.$method;
                                            eval "require $strGetModule" or
                                                die "Opec::Out::Dispatcher: impossibile caricare $strGetModule: $@";

                                            $self->{cache_dm}->{$method} = $strGetModule->new($param);
                                                return "Opec::Out::Dispatcher - $strGetModule: ". $self->{cache_dm}->{$method}
                                                    unless(ref($self->{cache_dm}->{$method}));
                                        }

                                $objMsg->recips([$_->mailAddress]);
                                        push @out, $self->{cache_dm}->{$method}->dispatch($objMsg,$param);
                        }
                }
        }
    }

    @out
}


1;

__END__

=head1 NAME

Opec::Out::Dispatcher - Livello di astrazione per l'interfaccia di comunicazione verso l'esterno

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
