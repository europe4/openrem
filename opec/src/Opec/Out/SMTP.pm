#
# $Id: SMTP.pm,v 1.14 2011/02/14 16:19:52 flazan Exp $
#
# Project       OpenPec
# file name:    SMTP.pm
# package:      Opec::Out::SMTP
#
# DESCRIPTION
# Interfaccia di comunicazione SMTP per il traffico in uscita
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   19/06/2004 - Fanton Flavio - nuovo ritorno di dispatch e _mailViaSmtpSingle
#   20/01/2011 - Fanton Flavio
#               - aggiunte le proprieta' host, port e localhost_name
#               - modifica interfaccia costruttore
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

package Opec::Out::SMTP;

use strict;

use Net::Cmd;
use Net::SMTP 2.24;
use POSIX qw(uname);

use Opec::Util qw(&doLog &msgId &prolongTimer);


#
# COSTRUTTORE
#  IN: -
#  OUT:  rif. oggetto
#
sub new {
        my $proto       = shift;
        my $p           = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

        ($self->{host},$self->{port},$self->{lname}) = split(/:/,$p,3);

    $self->{host}  ||= '127.0.0.1';
    $self->{port}  ||= '10025';
    $self->{lname} ||= (uname)[1]; # host name della macchina

    $self
}

#
# Metodo di istanza
# Restituisce o imposta la proprieta' host;
# se host e' del tipo host[:port][:hostname] viene impostata anche la proprieta' port
#
# IN: opz - host
#
# OUT: host
#
sub host {
        my $self = shift;
        my $host = shift || return $self->{host};
        my $port;
        my $hostname;

        ($host,$port,$hostname) = split(/:/,$host,3);
        $self->{host} = $host;
        $self->port($port);
        $self->localhost_name($hostname);

        $self->{host}
}

#
# Metodo di istanza
# Restituisce o imposta la proprieta' port.
#
# IN: opz port
#
# OUT: port
#
sub port
  { my($self)=shift; @_?$self->{port}=shift:$self->{port} }

#
# Metodo di istanza
# Restituisce o imposta la proprieta' localhost_name
#
# IN: opz nome host con cui il client LMTP si presenta al server
#
# OUT: port
#
sub localhost_name {
    my $self  = shift;
    my $lname = shift || return $self->{lname};

    $self->{lname}=$lname
}

#
# metodo d'istanza
# Dispaccia il messaggio utilizzando _mailViaSmtpSingle
#
# IN: 1. obj Opec::Message
#     2. true SEND, false FWD (modalita' filtro)
# OUT: -
#      (quanto in _mailViaSmtpSingle)
#
sub dispatch (@) {
    my $self = shift;
    my($msginfo,$par) = @_;
    my $filter;
    my @arrOut;
    # $num_recips_undone -> numero di destinatari iniziali
    # $num_recips_undone_after -> numro di destinatari
    #   dopo il dispaccio
    my($num_recips_undone) = scalar(
        grep !$_->reciptDone(), @{$msginfo->recips()});

        $self->host($par);

    # ciclo per gestire il caso di
    # 452 Too many recipients
    # recuoperato da _mailViaSmtpSingle
    while ($num_recips_undone > 0) {
        my ($res_type, $res_string) =
        $self->_mailViaSmtpSingle(@_);  # send what we can in one transaction
        push @arrOut, ($res_type, $res_string);

        my($num_recips_undone_after) = scalar(
            grep !$_->reciptDone(), @{$msginfo->recips()});

        if ($num_recips_undone_after >= $num_recips_undone) {
            # caso anomalo
            doLog(2, "Numero di destinatari ($num_recips_undone_after) ".
                      "non ridotto dalla transazione SMTP, esco");
            last;
        }
        if ($num_recips_undone_after > 0) {
            doLog(2, sprintf(
            "Delivery verso %s destinatari via SMTP, %s ancora da fare",
            $num_recips_undone - $num_recips_undone_after,
                $num_recips_undone_after));
        }
        $num_recips_undone = $num_recips_undone_after;
    }

    @arrOut
}


#
#
# Mail dispaccer via SMTP
#
# IN: 1. obj Opec::Message
#     2. true se la mail viene forwardata (mail nn
#        coinvolta nella certificazione) false
#        altrimenti
# OUT: (modifica il mittente ed i destinatari
#       Open::Message::EnvelopSnder - Open::Message::EnvelopRecipt)
#      undef in caso d'errore
#
sub _mailViaSmtpSingle (@) {
    my $self = shift;
    my($msginfo) = @_;

        # check presenza del messaggio
        return (3, "5.6.0 Message undefined")
        if(!$msginfo->mail_text && !$msginfo->mime_entity);

    my($relayhost,$relayhost_port) = ($self->{host}, $self->{port});

    # cerca di spedire + mail possibile durante la
    # singola sessione
    # ogni elem. di @per_recip_data e' un Opec::Message::EnvelopRecipt
    my(@per_recip_data) = grep !$_->reciptDone(), @{$msginfo->recips()};

    my($logmsg) = sprintf("SEND via SMTP: [%s:%s] <%s>",
            $relayhost, $relayhost_port, $msginfo->sender->mailAddress);

    # esco se mancano i destinatari
    # loggo
    if (!@per_recip_data) {
        doLog(4, "$logmsg, niente da fare");
        return (3, "5.1.3 Destination address invalid")
    }
    doLog(0, $logmsg . " -> " .
            join(",", map {"<".$_->mailAddress.">"} @per_recip_data));
    doLog(2, $logmsg . " -> " .
            join(",", map {"<".$_->mailAddress.">"} @per_recip_data));

    # $smtp_handle: obj ref
    # $smtp_msg: messaggio esteso di risposta SMTP (Net::Cmd::message())
    # $smtp_code: codice di risposta SMTP (Net::Cmd::code())
    # $smtp_resp: "$smtp_code $smtp_msg"
    # $any_valid_recips: numero di destinatari correttamente
    #                    aggiunti all'envelop
    # $any_tempfail_recips: numero di destinatari aggiunti
    #                       all'envelop con errori
    # $any_valid_recips_and_data_sent: 1 aggiunto almeno un dest, 0 altrimenti
    # $in_datasend_mode: 1 inizio trasmissione messaggio, 0 altrimenti
    #
    my($smtp_handle,$smtp_response);
    my($smtp_code, $smtp_msg);
    my($any_valid_recips) = 0; my($any_tempfail_recips) = 0;
    my($any_valid_recips_and_data_sent) = 0; my($in_datasend_mode) = 0;

    # NOTE: Net::SMTP uses alarm to do its own timing.
    #       We need to restart our timer when Net::SMTP is done using it !!!
    my($remaining_time) = alarm(0);  # check how much time is left, stop timer
    eval {
        # Timeout should be more than MTA normally takes to check DNS and RBL,
        # which may take a minute or more in case of unreachable DNS.
        # Specifying shorter timeout will cause alarm to terminate the wait
        # for SMTP status line prematurely, resulting in status code 000.
        # rfc2821 (section 4.5.3.2) requires timeout to be at least 5 minutes

    #--- connessione verso MTA
    die "Impossibile connettersi a $relayhost porta $relayhost_port, $!"
        unless( $smtp_handle = Net::SMTP->new("$relayhost:$relayhost_port",
                    Hello   => $self->{lname}, ExactAddresses => 1,
                    Timeout => (sort {$b<=>$a}(60,
                                sort {$a<=>$b}(5*60,$remaining_time)))[0]
                # LocalAddr => 10.11.12.13,   # (bind) source IP address
        ));
        doLog(0, "Remote host: ".$smtp_handle->domain);
        doLog(2, "Remote host: ".$smtp_handle->domain);

    prolongTimer('fwd-connect', $remaining_time);  # restart timer

        die "Errore impostando MAIL FROM\n" if(!$smtp_handle->mail($msginfo->sender->mailAddress));

    #--- imposta i destinatari
        my($skipping_resp);
        for my $r (@per_recip_data) { # send recipient addresses
            if (defined $skipping_resp) {
                # caso di troppi mittenti
                    $r->reciptRemoteResponse($skipping_resp);
                    next;
            }

            # aggiungo il mittente
            $smtp_handle->recipient($r->mailAddress);
            $smtp_code = $smtp_handle->code;
            $smtp_msg = $smtp_handle->message;
            chomp($smtp_msg);
            my($smtp_resp) = "$smtp_code $smtp_msg";

            # gestisco il ritorno
            if ($smtp_code =~ /^2/) {
                # ok
                    $any_valid_recips++;
            } elsif ($smtp_code =~ /^0/) {
                # timeout, what to do, this is bad
                doLog(4, "risposta a RCPT TO non disponibile, si assume ok");
            } else {  # not ok
                doLog(4, "Risposta a RCPT TO: \"$smtp_resp\"");
                $smtp_resp =~ s/^552/452/;  # compatibility advised by rfc2821
                $r->reciptRemoteHost($relayhost);
                $r->reciptRemoteResponse($smtp_resp);

                # imposto l'enhanced code
                my($resp_code,$resp_enhcode,$resp_msg) = '' x 3;
                if ($smtp_resp =~ /^ \d{3} \s+ ([245] \. \d{1,3} \. \d{1,3})?
                                     \s* .* $(?!\n)/xs) {
                    $resp_enhcode = $1;
                }
                    if ($resp_enhcode eq '' && $smtp_code =~ /^([245])/ ) {
                        my($c) = $1;
                        $resp_enhcode = ($smtp_code eq '452') ? "$c.5.3"
                                            : "$c.1.0";  # insert enhanced code
                        $smtp_resp = "$smtp_code $resp_enhcode $smtp_msg";
                    }

                if ($smtp_resp =~ /^452/) {
                    # caso di troppi destinatari
                    # non setto il destinatario come fatto per
                    # ritentare nella prossima passata
                    doLog(2, sprintf('Solo %d destinatari serviti: "%s"',
                                              $any_valid_recips, $smtp_resp));
                    $skipping_resp = $smtp_resp;
                } else {
                    # errore: setto il destinatario come fatto con errori
                    $any_tempfail_recips++;
                    $smtp_resp =~ /^4/?$r->reciptDone(2):$r->reciptDone(3);
                }

            }
        }
        $smtp_code = $smtp_msg = undef;

    #--- dispaccio
        if ($any_valid_recips) {  # send the message
            $smtp_handle->data or die "comando DATA\n";
            $in_datasend_mode = 1;

            my($smtp_resp) = $smtp_handle->code . " " . $smtp_handle->message;
            chomp($smtp_resp);
            doLog(4, "risposta a DATA: \"$smtp_resp\"");

        if($msginfo->mail_text){
            # la mail e' IO::File
            $msginfo->mail_text->seek(0,0);
                while ( $msginfo->mail_text->read($_,16384) > 0 ) {
                    $smtp_handle->datasend($_)
                            or die "datasend timed out nella spedizione del body\n";}
        }else{
            # c'e' da aggiungere il controllo sulla lunghezza
            # delle linee
            $smtp_handle->datasend($msginfo->mime_entity->stringify);
        }

            # don't check status of dataend here, it may not yet be available
            $smtp_handle->dataend;
            $in_datasend_mode = 0; $any_valid_recips_and_data_sent = 1;

            #--- check risposta MTA
            # figure out the final SMTP response
            $smtp_code = $smtp_handle->code;
            my(@msgs) = $smtp_handle->message;
            # only the 'command()' resets messages list, so now we have both:
            # 'End data with <CR><LF>.<CR><LF>' and 'Ok: queued as...' in @msgs
            # and only the last SMTP response code in $smtp_handle->code
            my($smtp_msg) = $msgs[$#msgs]; chomp($smtp_msg); #take the last one
            $smtp_response = "$smtp_code $smtp_msg";
            doLog(4, "risposta a fine DATA: \"$smtp_response\"");
            for my $r (@per_recip_data) {
                # salto i destinatari in eccesso o con errori
                next if $r->reciptRemoteResponse =~ /^452/ or $r->reciptDone();
                if($smtp_code =~ /^2/){
                    $r->reciptDone(1)
                }elsif($smtp_code =~ /^4/){
                    $r->reciptDone(2)
                }else{
                    $r->reciptDone(3)}
                $r->reciptRemoteHost($relayhost);
                $r->reciptRemoteResponse($smtp_response);
            }
            if ($smtp_code =~ /^[245]/) {
                my($smtp_status) = substr($smtp_code,0,1);
                $smtp_response = sprintf("%s %d.6.0 %s, id=%s, from MTA: %s",
                                      $smtp_code, $smtp_status,
                                      ($smtp_status==2 ? 'Ok' : 'Failed'),
                                      msgId(), $smtp_response);
        }
        }
    }; # fine blocco eval

    #--- formatto l'eventuale stringa d'errore
    my($err) = $@;
    my($saved_section_name) = 'fwd-rundown';

    if ($err ne '') {
        chomp($err);
        $err = ' ' if $err eq ''
    }  # careful chomp

    if ($err ne '') {  # fetch info about failure
            doLog(2, "SMTP: sessione fallita: $err");
            doLog(0, "SMTP: sessione fallita: $err");
            if (!defined($smtp_handle)) {
                $smtp_msg = ''
            }else{
            $smtp_code = $smtp_handle->code;
            $smtp_msg = $smtp_handle->message; chomp($smtp_msg);
            }
    }

    # terminate the SMTP session if still alive
    if (!defined $smtp_handle) {
            # nothing
    } elsif ($in_datasend_mode) {
        # We are aborting SMTP session.  DATA send mode must NOT be normally
        # terminated with a dataend (dot), otherwise recipient will receive
        # a chopped-off mail (and possibly be receiving it over and over again
        # during each MTA retry.
        doLog(2, "SMTP nota: sessione SMTP interrotta, $err");
        doLog(0, "SMTP nota: sessione SMTP interrotta, $err");
        $smtp_handle->close;  # abruptly terminate the SMTP session
    } else {
        $smtp_handle->timeout(15);  # don't wait too long for response
        $smtp_handle->quit;         # send a QUIT regardless of success so far
        if ($err eq '' && $smtp_handle->status != CMD_OK) {
            doLog(2, "SMTP attenzione: comando QUIT fallito: " .
                      $smtp_handle->code . " " . $smtp_handle->message);
            doLog(0, "SMTP attenzione: comando QUIT fallito: " .
                      $smtp_handle->code . " " . $smtp_handle->message);
        }
    }

    # prepare final smtp response and log abnormal events
    if ($err eq '') {    # no errors
        if ($any_valid_recips_and_data_sent && $smtp_response !~ /^[245]/) {
            $smtp_response = sprintf("451 4.6.0 Bad SMTP code, id=%s, from MTA: \"%s\"",
                                     msgId(), $smtp_response);
        }
    } elsif ($err eq "timed out" || $err =~ /: Timeout$/) {
        my($msg) = ($in_datasend_mode && $smtp_code =~ /^354/) ? ''
                        : ", $smtp_code $smtp_msg";
        $smtp_response = sprintf("450 4.4.2 Timed out during %s%s, id=%s",
                                 $saved_section_name, $msg, msgId());
    } elsif ($err =~ /^Can't connect/) {
        $smtp_response = sprintf("450 4.4.1 %s, id=%s", $err, msgId());
    } elsif ($err =~ /^Too many hops/) {
            $smtp_response = sprintf("550 5.4.6 Rejected: %s, id=%s",$err,msgId());
    } elsif ($smtp_code =~ /^5/) {  # 5xx
            $smtp_response = sprintf("%s 5.5.0 Rejected by MTA: %s %s, id=%s",
                    ($smtp_code !~ /^5\d\d$(?!\n)/ ?"550" :$smtp_code),
                        $smtp_code, $smtp_msg, msgId());
    } elsif ($smtp_code =~ /^0/) {  # 000
            $smtp_response = sprintf("450 4.4.2 No response during %s (%s): id=%s",
                    $saved_section_name, $err, msgId());
    } else {
            $smtp_response =
                sprintf("%s 4.5.0 from MTA during %s (%s): %s %s, id=%s",
                        ($smtp_code !~ /^4\d\d$(?!\n)/ ? "451" : $smtp_code),
                        $saved_section_name, $err, $smtp_code,$smtp_msg, msgId());
    }

    doLog(2, "mail_via_smtp: $smtp_response");
    if (!$any_valid_recips || $any_tempfail_recips) {
        doLog(2, "mail_via_smtp: DATA saltato, $any_valid_recips, ".
            "$any_tempfail_recips, $any_valid_recips_and_data_sent");
    }

    # imposto le propri. dei destinatari
    # qualora nn fossero gia' impostate
    # (caso di uscita prematura da eval)
        for my $r (@per_recip_data) {
            if(!defined $r->reciptRemoteResponse()){
                $r->reciptRemoteResponse($smtp_response);
                if($smtp_response =~ /^4/){
                    $r->reciptDone(2)
                }else{
                    $r->reciptDone(3)}
            }
        }

        my $res_type = substr($smtp_response, 0, 3);
        if($res_type =~ /^2/){
            $res_type = 1
        }elsif($res_type =~ /^4/){
            $res_type = 2
        }else{
            $res_type = 3}

        my $res_string = substr($smtp_response, 4);

        ($res_type, $res_string);
}


1;

__END__

=head1 NAME

Opec::Out::LMTP - Gestione interfaccia SMTP outbound

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
