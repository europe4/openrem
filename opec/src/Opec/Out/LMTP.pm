#
# $Id: LMTP.pm,v 1.16 2011/05/02 17:01:48 flazan Exp $
#
# Project       OpenPec
# file name:    LMTP.pm
# package:      Opec::Out::LMTP
#
# DESCRIPTION
# Interfaccia di comunicazione LMTP per il traffico in uscita
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   19/06/2004 - Fanton Flavio - nuovo ritorno di dispatch e _mailViaLmtpSingle
#   20/01/2011 - Fanton Flavio
#               - aggiunte le proprieta' host, port e localhost_name
#               - modifica interfaccia costruttore
#   05/04/2011 - Fanton Flavio
#               - bugfix Can't modify non-lvalue subroutine call at ..
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

package Opec::Out::LMTP;

use strict;

use vars qw(@ISA);

use Opec::Util qw(&doLog &msgId &prolongTimer);

use IO::Socket;
use Net::Cmd;
use POSIX qw(uname);

@ISA = qw(Net::Cmd IO::Socket::INET);

#
# COSTRUTTORE
#  IN: opz - stringa con formato <host>:<port>:<hostname>
#  OUT:  rif. oggetto
#
sub new {
        my $proto       = shift;
        my $p       = shift;
        my $class   = ref($proto) || $proto;
        my $self    = {};
        bless($self,$class);

        ($self->{host},$self->{port},$self->{localhost_name}) = split(/:/,$p,3);

    $self->{host} ||= '127.0.0.1';
    $self->{port} ||= '2003';
    $self->{localhost_name} ||= (uname)[1]; # host name della macchina

    $self
}

#
# Metodo di istanza
# Restituisce o imposta la proprieta' host;
# se host e' del tipo host[:port][:hostname] viene impostata anche la proprieta' port
#
# IN: opz - host
#
# OUT: host
#
sub host {
        my $self = shift;
        my $host = shift || return $self->{host};
        my $port;
        my $hostname;

        ($host,$port,$hostname) = split(/:/,$host,3);
        $self->{host} = $host;
        $self->port($port);
        $self->localhost_name($hostname);

        $self->{host}
}

#
# Metodo di istanza
# Restituisce o imposta la proprieta' port.
#
# IN: opz port
#
# OUT: port
#
sub port
  { my($self)=shift; @_?$self->{port}=shift:$self->{port} }

#
# Metodo di istanza
# Restituisce o imposta la proprieta' localhost_name
#
# IN: opz nome host con cui il client LMTP si presenta al server
#
# OUT: port
#
sub localhost_name {
    my $self  = shift;
    my $lname = shift || return $self->{lname};

    $self->{lname}=$lname
}

#
# metodo d'istanza
# Dispaccia il messaggio utilizzando _mailViaLmtpSingle
#
# IN: 1. obj Opec::Message
#     2. stringa del tipo host[:port][:hostname]
# OUT: -
#      (quanto in _mailViaLmtpSingle)
#
sub dispatch {
    my $self = shift;
    my ($msginfo,$par) = @_;
    my $filter;
    my @arrOut;
    # $num_recips_undone -> numero di destinatari iniziali
    # $num_recips_undone_after -> numro di destinatari
    #   dopo il dispaccio
    my($num_recips_undone) = scalar(
        grep !$_->reciptDone(), @{$msginfo->recips()});

        $self->host($par);

    # ciclo per gestire il caso di
    # 452 Too many recipients
    # recuperato da _mailViaLmtpSingle
    while ($num_recips_undone > 0) {
        push @arrOut, $self->_mailViaLmtpSingle(@_);

        my($num_recips_undone_after) = scalar(
            grep !$_->reciptDone(), @{$msginfo->recips()});

        if ($num_recips_undone_after >= $num_recips_undone) {
            # caso anomalo
            doLog(2, "Numero di destinatari ($num_recips_undone_after) ".
                         "non ridotto dalla transazione LMTP, esco");
            last}
        if ($num_recips_undone_after > 0) {
            doLog(2, sprintf(
            "Delivery verso %s destinatari via LMTP, %s ancora da fare ",
            $num_recips_undone - $num_recips_undone_after,
                $num_recips_undone_after))}

        $num_recips_undone = $num_recips_undone_after;
    }

    # @arrOut non contiene errori generati nella fase di envelop
    # quindi lo aggiorno recuperando le info da tutti gli
    # Opec::Message::EnvelopRecipt con reciptDone() == 0
    if( @arrOut != @{$msginfo->recips()} * 2 ){
        foreach ( @{$msginfo->recips()} ){
            if(!$_->reciptDone()){
                if( $_->reciptRemoteResponse() =~ /^4/ ){
                    $_->reciptDone(2)
                }else{
                    $_->reciptDone(3)}
                    push @arrOut, ( $_->reciptDone(),
                                    substr($_->reciptRemoteResponse(),4) )}
        }
    }

    @arrOut
}


#
# Mail dispaccer via LMTP
#
# IN: obj Opec::Message
# OUT:arr. ref con due valori
#       1: intero che va da 0 a 3 che rappresenta il tipo di errore
#           0/undef - stato indefinito
#           1 - ok
#           2 - errore temporaneo
#           3 - errore definitivo
#       2: stringa contentente il codice ESMTP + desc.
#
sub _mailViaLmtpSingle (@) {
    my $self = shift;
    my($msginfo) = @_;

        # check presenza del messaggio
        return (3, "5.6.0 Message undefined")
        if(!$msginfo->mail_text && !$msginfo->mime_entity);

    my($relayhost,$relayhost_port) = ($self->{host}, $self->{port});

    # cerca di spedire + mail possibile durante la
    # singola sessione
    # ogni elem. di @per_recip_data e' un Opec::Message::EnvelopRecipt
    my(@per_recip_data) = grep !$_->reciptDone(), @{$msginfo->recips()};

    my($logmsg) = sprintf("SEND via LMTP: [%s:%s] <%s>",
            $relayhost, $relayhost_port, $msginfo->sender->mailAddress);

    # esco se mancano i destinatari
    # loggo
    if (!@per_recip_data) {
        doLog(2, "$logmsg, niente da fare");
        return (3, "5.1.3 Destination address invalid")
    }
    doLog(0, $logmsg . " -> " .
            join(",", map {"<".$_->mailAddress.">"} @per_recip_data));
    doLog(2, $logmsg . " -> " .
            join(",", map {"<".$_->mailAddress.">"} @per_recip_data));

    # $lmtp_handle: obj ref
    # $lmtp_msg: messaggio esteso di risposta LMTP (Net::Cmd::message())
    # $lmtp_code: codice di risposta LMTP (Net::Cmd::code())
    # $lmtp_resp: "$lmtp_code $lmtp_msg"
    # $any_valid_recips: numero di destinatari correttamente
    #                    aggiunti all'envelop
    # $any_tempfail_recips: numero di destinatari aggiunti
    #                       all'envelop con errori
    # $any_valid_recips_and_data_sent: 1 aggiunto almeno un dest, 0 altrimenti
    # $in_datasend_mode: 1 inizio trasmissione messaggio, 0 altrimenti
    #
    my($lmtp_handle,$lmtp_resp);
    my($lmtp_code, $lmtp_msg);
    my($any_valid_recips) = 0; my($any_tempfail_recips) = 0;
    my($any_valid_recips_and_data_sent) = 0; my($in_datasend_mode) = 0;

        # Timeout should be more than MTA normally takes to check DNS and RBL,
        # which may take a minute or more in case of unreachable DNS.
        # Specifying shorter timeout will cause alarm to terminate the wait
        # for LMTP status line prematurely, resulting in status code 000.
        # rfc2821 (section 4.5.3.2) requires timeout to be at least 5 minutes
    my($remaining_time) = alarm(0);  # check how much time is left, stop timer
    eval {
    #--- connessione verso LMTP server
    die "Impossibile connettersi a $relayhost porta $relayhost_port, $!"
    unless( $lmtp_handle = $self->SUPER::new(
        PeerAddr => $relayhost,
        PeerPort => $relayhost_port,
        Proto    => 'tcp',
        Timeout  => (sort {$b<=>$a}(60,
                     sort {$a<=>$b}(5*60,$remaining_time)))[0]));
    $lmtp_handle->autoflush(1);

    unless ($lmtp_handle->response() == CMD_OK){
        $lmtp_handle->close();
        die "Errore: il server LMTP rifiuta la connessione"}

        doLog(0, "Host remoto: ".($lmtp_handle->message =~ /\A\s*(\S+)/)[0]);
        doLog(2, "Host remoto: ".($lmtp_handle->message =~ /\A\s*(\S+)/)[0]);

    $lmtp_handle->_LHLO($self->{localhost_name});
    $lmtp_code = $lmtp_handle->code;
    if ($lmtp_code =~ /^5/) {
        $lmtp_msg = $lmtp_handle->message;
        die "LHLO - $lmtp_msg";
    }

    prolongTimer('fwd-connect', $remaining_time);  # restart timer

    #--- imposto il mittente
        die "Errore in MAIL FROM"
            unless($lmtp_handle->_MAIL($msginfo->sender->mailAddress));

    #--- imposta i destinatari
        my($skipping_resp);
        my @recip_add;
        for my $r (@per_recip_data) { # send recipient addresses
            if (defined $skipping_resp) {
                # caso di troppi mittenti
                    $r->recipResponse($skipping_resp);
                    next;
            }

            # aggiungo il destinatario
            doLog(4, "risposta a RCPT TO");
            $lmtp_handle->_RCPT($r->mailAddress);
            $lmtp_code = $lmtp_handle->code;
            $lmtp_msg = $lmtp_handle->message;
            chomp($lmtp_msg);
            $lmtp_resp = "$lmtp_code $lmtp_msg";

            # gestisco il ritorno
            if ($lmtp_code =~ /^2/) {   # ok
                    $any_valid_recips++;
                    push @recip_add, $r;
            } else {  # not ok
                $lmtp_resp =~ s/^552/452/;  # compatibility advised by rfc2821
                $r->reciptRemoteHost($relayhost);
                $r->reciptRemoteResponse($lmtp_resp);

                # imposto l'enhanced code
                my($resp_code,$resp_enhcode,$resp_msg) = '' x 3;
                if ($lmtp_resp =~ /^ \d{3} \s+ ([245] \. \d{1,3} \. \d{1,3})?
                                     \s* .* $(?!\n)/xs) {
                    $resp_enhcode = $1;
                }
                    if ($resp_enhcode eq '' && $lmtp_code =~ /^([245])/ ) {
                        my($c) = $1;
                        $resp_enhcode = ($lmtp_code eq '452') ? "$c.5.3"
                                            : "$c.1.0";  # insert enhanced code
                        $lmtp_resp = "$lmtp_code $resp_enhcode $lmtp_msg";
                    }

                if ($lmtp_resp =~ /^452/) {
                    # caso di troppi destinatari
                    # non setto il destinatario come fatto per
                    # ritentare nella prossima passata
                    doLog(2, sprintf('Solo %d destinatari serviti: "%s"',
                                      $any_valid_recips, $lmtp_resp));
                    $skipping_resp = $lmtp_resp;
                } else {
                    # errore: setto il destinatario come fatto con errori
                    $any_tempfail_recips++;
                    $lmtp_resp =~ /^4/?$r->reciptDone(2):$r->reciptDone(3);
                }
            }
                doLog(4, $r->mailAddress.": \"$lmtp_resp\"");
        }
        $lmtp_code = $lmtp_msg = undef;

    #--- dispaccio (solo se non ho avuto nessun errore dall'envelop)
        if ($any_valid_recips) {  # send the message
            $lmtp_handle->_DATA() or die "Errore in DATA\n";
            $in_datasend_mode = 1;

            $lmtp_resp = $lmtp_handle->code . " " . $lmtp_handle->message;
            chomp($lmtp_resp);
            doLog(4, "risposta a DATA: \"$lmtp_resp\"");

        if($msginfo->mail_text){
            # la mail e' IO::File
            $msginfo->mail_text->seek(0,0);
                while ( $msginfo->mail_text->read($_,16384) > 0 ) {
                    $lmtp_handle->datasend($_)
                            or die "datasend timed out nella spedizione del body\n";}
        }else{
            # c'e' da aggiungere il controllo sulla lunghezza
            # delle linee
            $lmtp_handle->datasend($msginfo->mime_entity->stringify);
        }

            # don't check status of dataend here, it may not yet be available
            $lmtp_handle->dataend;
            $in_datasend_mode = 0; $any_valid_recips_and_data_sent = 1;

            #--- check risposta MTA per ogni rcpt aggiunto
            #    senza errori all'envelop
            # figure out the final LMTP response
            # only the 'command()' resets messages list, so now we have both:
            # 'End data with <CR><LF>.<CR><LF>' and 'Ok: queued as...' in @msgs
            # and only the last LMTP response code in $lmtp_handle->code

            # check primo dest.
            $lmtp_code = $lmtp_handle->code;
            my @msgs = $lmtp_handle->message;
            chomp($msgs[$#msgs]);
        $lmtp_resp = $lmtp_code." ".$msgs[$#msgs];
            my $recipt_add_first = shift @recip_add;
            doLog(4, "risposta a ".$recipt_add_first->mailAddress.": ".
                sprintf("%s: %s, id=%s",
                                    $lmtp_resp,
                                    ($lmtp_code =~ /^2/ ? 'Ok' : 'Failed'),
                                    msgId()));

                if($lmtp_code =~ /^2/){
                    $recipt_add_first->reciptDone(1)
                }elsif($lmtp_code =~ /^4/){
                    $recipt_add_first->reciptDone(2)
                }else{
                    $recipt_add_first->reciptDone(3)}
                $recipt_add_first->reciptRemoteResponse($lmtp_resp);
                $recipt_add_first->reciptRemoteHost($relayhost);

                # check resto dei dest.
            for my $r (@recip_add) {
                $lmtp_handle->response();
            $lmtp_code = $lmtp_handle->code;
            @msgs = $lmtp_handle->message;
            $lmtp_resp = $lmtp_code." ".chomp($msgs[$#msgs]);

            doLog(4, "risposta a ".$r->mailAddress.": ".
                sprintf("%s: %s, id=%s",
                                    $lmtp_resp,
                                    ($lmtp_code =~ /^2/ ? 'Ok' : 'Failed'),
                                    msgId()));
                if($lmtp_code =~ /^2/){
                    $r->reciptDone(1)
                }elsif($lmtp_code =~ /^4/){
                    $r->reciptDone(2)
                }else{
                    $r->reciptDone(3)}
#               $r->reciptDone($lmtp_code=~/^2/?1:2);
                $r->reciptRemoteResponse($lmtp_resp);
                $r->reciptRemoteHost($relayhost);
            }
        }
    }; # fine blocco eval

    #--- formatto l'eventuale stringa d'errore
    my($err) = $@;
    my($saved_section_name) = 'fwd-rundown';

    if ($err ne '') {
        chomp($err);
        $err = ' ' if $err eq ''}  # careful chomp

    if ($err ne '') {  # fetch info about failure
            doLog(0, "LMTP: session failed: $err");
            doLog(2, "LMTP: session failed: $err");
            if (!defined($lmtp_handle)) {
                $lmtp_msg = ''
            }else{
            $lmtp_code = $lmtp_handle->code;
            $lmtp_msg = $lmtp_handle->message; chomp($lmtp_msg);
            }
    }

    # chiudo la sessione o quello ne e' rimasto
    if (!defined $lmtp_handle) {
            # nothing
    } elsif ($in_datasend_mode) {
        # errore durante la transazione
        # We are aborting LMTP session.  DATA send mode must NOT be normally
        # terminated with a dataend (dot), otherwise recipient will receive
        # a chopped-off mail (and possibly be receiving it over and over again
        # during each MTA retry.
        doLog(2, "LMTP nota: sessione LMTP interrotta, $err");
        doLog(0, "LMTP nota: sessione LMTP interrotta, $err");
        $lmtp_handle->close;  # tento di chiudere il socket
    } else {
        $lmtp_handle->timeout(15);  # don't wait too long for response
        $lmtp_handle->_QUIT;         # send a QUIT regardless of success so far
        if ($err eq '' && $lmtp_handle->status != CMD_OK) {
                my $lmtp_message = $lmtp_handle->message;
            doLog(0, "LMTP attenzione: comando QUIT fallito: " .
                      $lmtp_handle->code . " " . chomp($lmtp_message));
            doLog(2, "LMTP attenzione: comando QUIT fallito: " .
                      $lmtp_handle->code . " " . chomp($lmtp_message));
        }
    }

    # prepare final lmtp response and log abnormal events
    if ($err eq '') {    # no errors
        if ($any_valid_recips_and_data_sent && $lmtp_resp !~ /^[245]/) {
            $lmtp_resp = sprintf("451 4.6.0 Bad LMTP code, id=%s, from MTA: \"%s\"",
                                     msgId(), $lmtp_resp);
        }
    } elsif ($err eq "timed out" || $err =~ /: Timeout$/) {
        my($msg) = ($in_datasend_mode && $lmtp_code =~ /^354/) ? ''
                        : ", $lmtp_code $lmtp_msg";
        $lmtp_resp = sprintf("450 4.4.2 Timed out during %s%s, id=%s",
                                 $saved_section_name, $msg, msgId());
    } elsif ($err =~ /^Can't connect/) {
        $lmtp_resp = sprintf("450 4.4.1 %s, id=%s", $err, msgId());
    } elsif ($err =~ /^Too many hops/) {
            $lmtp_resp = sprintf("550 5.4.6 Rejected: %s, id=%s",$err,msgId());
    } elsif ($lmtp_code =~ /^5/) {  # 5xx
            $lmtp_resp = sprintf("%s 5.5.0 Rejected by MTA: %s %s, id=%s",
                    ($lmtp_code !~ /^5\d\d$(?!\n)/ ?"550" :$lmtp_code),
                        $lmtp_code, $lmtp_msg, msgId());
    } elsif ($lmtp_code =~ /^0/) {  # 000
            $lmtp_resp = sprintf("450 4.4.2 No response during %s (%s): id=%s",
                    $saved_section_name, $err, msgId());
    } else {
            $lmtp_resp =
                sprintf("%s 4.5.0 from MTA during %s (%s): %s %s, id=%s",
                        ($lmtp_code !~ /^4\d\d$(?!\n)/ ? "451" : $lmtp_code),
                        $saved_section_name, $err, $lmtp_code,$lmtp_msg, msgId());
    }

    doLog(2, "mail_via_lmtp: $lmtp_resp");
    if (!$any_valid_recips || $any_tempfail_recips) {
        doLog(3, "mail_via_lmtp: DATA skipped, $any_valid_recips, ".
            "$any_tempfail_recips, $any_valid_recips_and_data_sent");
    }

    # imposto le propri. dei destinatari
    # qualora nn fossero gia' impostate
    # (caso di uscita prematura da eval)
    my @arrOut;
        for my $r (@per_recip_data) {
            if(!defined $r->reciptRemoteResponse()){
                $r->reciptRemoteResponse($lmtp_resp);
                if($lmtp_resp =~ /^4/){
                    $r->reciptDone(2)
                }else{
                    $r->reciptDone(3)}
            }

        # imposto il ritorno
        if($r->reciptDone != 0){
            push @arrOut, ( $r->reciptDone(),
                            substr($r->reciptRemoteResponse(), 4));
        }
        }

    @arrOut;
}


# ###

sub _MAIL
    { shift->command("MAIL", "FROM:"._addr(shift))->response()  == CMD_OK }
sub _RCPT
    { shift->command("RCPT", "TO:"._addr(shift))->response()    == CMD_OK }
sub _DATA
    { shift->command("DATA")->response()        == CMD_MORE }
sub _LHLO
    { shift->command("LHLO", @_)->response()    == CMD_OK }
sub _QUIT
    { shift->command("QUIT")->response()        == CMD_OK }

#
#
#
sub _addr {
 my $addr = shift || "";

 return $1
    if $addr =~ /(<[^>]+>)/so;

 $addr =~ s/\n/ /sog;
 $addr =~ s/(\A\s+|\s+\Z)//sog;

 return "<" . $addr . ">";
}

1;

__END__

=head1 NAME

Opec::Out::LMTP - Gestione interfaccia LMTP outboud

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
