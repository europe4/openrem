#
# $Id: EnvelopSender.pm,v 1.13 2010/06/21 14:52:41 flazan Exp $
#
# Project       OpenPec
# file name:    EnvelopSender.pm
# package:      Opec::Message::EnvelopSender
#
# DESCRIPTION
# Rappresenta il mittente dell'envelop
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

package Opec::Message::EnvelopSender;
# the main purpose of this class is to contain information
# about the sender into envelop

use strict;

sub new
  { my($class) = @_; bless {}, $class }

sub mailAddress         # set or get name value
  { my($self)=shift; !@_ ? $self->{emailaddress}    : ($self->{emailaddress}=shift) }

sub certificate   # set or get certificate value (boolean: 1 cert, 0 no cert)
{   my($self)=shift;

    if(@_){
        $self->{certificate}=shift;
    }else{
        $self->{certificate}=0 if(!defined $self->{certificate});
    }
    $self->{certificate};
}


1;


__END__

=head1 NAME

Opec::Message::EnvelopSender - Interfaccia del mittente dell'envelop

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
