#
# $Id: SMTP.pm,v 1.4 2010/12/24 10:02:46 flazan Exp $
#
# Project       OpenPec
# file name:    SMTP.pm
# package:      Opec::In::SMTP
#
# DESCRIPTION
# Interfaccia di comunicazione SMTP per il traffico in ingresso
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
#   19/06/2004 - Fanton Flavio
#               - adattamento nuovo ritorno di corePec
#       - gestione conservativa del ritorno di corePec
#   15/05/2006 - Fanton Flavio
#               - modifica formato del msgID (id interno) per
#         uniformare Message-ID
#   13/06/2006 - Fanton Flavio
#               - Aggiunta gestione del header Message-ID
#         durante la ricezione del messaggio
#   23/07/2007 - Fanton Flavio
#               - Spostato in opec.pl il controllo sulla dimensione
#         max del msg in ingresso per la generazione della
#         ric. di non accettazione relativa
#   25/01/2008 - Fanton Flavio
#               - eliminata la gestione dei timer
#   26/10/2010 - Fanton Flavio
#               - aggiunte le info per la risposta verso MTA
#                 configurabile
#       22/12/2010 - Fanton Flavio
#               - bugfix su subject con errore nella decodifica
#
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#

package Opec::In::SMTP;

use strict;

BEGIN {
    use Exporter ();
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);
}

use Opec::Conf qw(:platform :confvars $OPEC_HEADER_KEY_MSGID $OPEC_HEADER_KEY_RECEIPT
                  $OPEC_HEADER_KEY_DOCTRASP $OPEC_HEADER_KEY_MSGID_RIF $OPEC_HEADER_KEY_RECEIPT_TYPE );
use Opec::Util qw(&doLog &msgId &unquoteRfc2821Local &myChomp &headerMsgId);
use Opec::Message;
use Opec::In::Connection;
use Opec::Timing qw(&formatMtaResponse);

#
# Metodo di classe
# COSTRUTTORE
#
#  IN: -
#  OUT: rif. oggetto
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $self        = {};
        bless($self,$class);

    # servizi ESMTP
    $self->{proto}      = '';  # SMTP/ESMTP
    $self->{pipelining} = 0;   # bool
    $self->{smtp_outbuf}= [];  # buffer per PIPELINING
    $self->{msg_size}   = 0;   # supporto ESMTP - SIZE

    $self->{objmsg} = undef;

    # chiusura corretta con il comando QUIT
    $self->{session_closed_normally} = undef;
    $self;
}

#
# Metodo di istanza
# Restituisce il valore di objmsg:
# obj Opec::Message che rappresenta la mail relativa
# alla richiesta
#
# IN: -
# OUT: Opec::Message se inizializzato (process_request)
#      undef altrimenti
#
sub objmsg
  { my($self)=shift; $self->{objmsg} }

#
# Processa le richieste E/SMTP in ingresso
#
# IN: 1. socket da Net::Server
#     2. obj Opec::In::Connection: info sulla connessione client
#     3. rif. alla sub chiamata alla completa ricezione della mail
#     4. (path temporaneo - passato al costruttore di Opec::Message)
# OUT: die in caso di errore
#
sub process_request ($$$$;$$) {
    my($self, $sock, $conn, $corePec, $tempDir, $report) = @_;

    #--- inizio transazione
    doLog(2, "Gestione richiesta SMTP");

    my( $sender,    # il sender (uno solo) dell'envelop
        @recips,    # destinatari dell'envelop
        $got_rcpt); # numero destinatari
    my $msgInfo =
        $self->{objmsg} =
        Opec::Message->new($tempDir, "Msg in Ingresso");
    $self->{pipelining} = 0;
    $self->{smtp_outbuf}= [];
    $self->{proto}      = 'SMTP';
    my $myHeloName = '[' . $conn->socket_ip . ']';

    $self->smtp_resp(1, "220 $myHeloName ".
        $self->{proto} ." Opec service ready");

    #--- ciclo per la gestione della transazione
    my( $terminating,       # bool: 1 se ricevuto QUIT
        $aborting,
        $eof,
        $voluntary_exit);
    my($seq) = 0;   # contatore di mail per connessione
    while(<$sock>) {
        # blocco usato come switch fra i comandi in ricezione
        {
            # controllo e recupero di comando e argomenti
            my($cmd) = $_;
            my($taint) = substr($cmd,0,0);
            doLog(4, $self->{proto}."< ".&myChomp($cmd));
            !/^ \s* ([A-Za-z]+) (?: \s+ (.*?) )? \s* \015\012 $(?!\n)/xs && do {
                $self->smtp_resp(1,"500 5.5.2 Error: bad syntax"); last;
            };
            $_ = uc($1).$taint;
            my($args) = $2.$taint;

            /^RSET|DATA|QUIT$/ && $args ne '' && do {
                $self->smtp_resp(1,"501 5.5.4 Error: $_ does not accept arguments");
            doLog(2, "comando errato: $cmd");
                last;
            };

            /^RSET$/ && do {
                # sbianco tutto
            $sender=undef; @recips=(); $got_rcpt=0;$self->{smtp_outbuf}= []; $self->{msg_size} = 0;
            $msgInfo = $self->{objmsg} = Opec::Message->new($tempDir, "Msg in Ingresso"); # ###

                $self->smtp_resp(0,"250 2.0.0 Ok $_");
                doLog(5,&$report()); # report in caso di + mail per sigola connessione
                last
                };

            /^NOOP$/ && do {
            $self->smtp_resp(1,"250 2.0.0 Ok $_");
            last
            };

            /^QUIT$/ && do {
                $self->smtp_resp(1,
                    "221 2.0.0 $myHeloName (opec) closing transmission channel");
                $terminating=1;
                last;
            };

            /^HELO$/ && do {
                # sbianco tutto
            $sender=undef; @recips=(); $got_rcpt=0;$self->{smtp_outbuf}= []; $self->{msg_size} = 0;
            $msgInfo = $self->{objmsg} = Opec::Message->new($tempDir, "Msg in Ingresso"); # ###

                $self->smtp_resp(0,"250 $myHeloName");
                doLog(2, "HELO: $args");
                $conn->smtp_helo($args);
                last;
            };

            /^EHLO$/ && do {
                # sbianco tutto
            $sender=undef; @recips=(); $got_rcpt=0;$self->{smtp_outbuf}= []; $self->{msg_size} = 0;
            $msgInfo = $self->{objmsg} = Opec::Message->new($tempDir, "Msg in Ingresso"); # ###

                $self->{proto} = 'ESMTP';
                doLog(2, "EHLO: $args");

#               my $strTmp = $esmtpMaxMsgSize?"SIZE $esmtpMaxMsgSize":"SIZE";
#               $self->smtp_resp(1,"250 $myHeloName\n" .
#                                  "PIPELINING\n".
#                                  $strTmp."\n".
#                                  "ENHANCEDSTATUSCODES\n");
                $self->smtp_resp(1,"250 $myHeloName\n" .
                                   "PIPELINING\n".
                                   "ENHANCEDSTATUSCODES\n");
                $conn->smtp_helo($args);
                last;
            };

            /^LHLO$/ && do {
                $self->smtp_resp("502 5.5.1 Command $_ not implemented");
            doLog(2, "comando $cmd non implementato");
                last;
            };

            /^VRFY$/ && do {
                $self->smtp_resp(1,"502 5.5.1 Command $_ not implemented");
            doLog(2, "comando $cmd non implementato");
                last;
            };

            /^HELP$/ && do {
                $self->smtp_resp(1,
                    "214 2.0.0 See openpec home page at: http://www.openpec.org");
                last;
            };

            /^MAIL$/ && do {
                # inizio transazione SMTP
                if (defined($sender)) {
                $self->smtp_resp(0,"503 5.5.1 Error: nested MAIL command");
                    doLog(2, "Errore: comando Mail nidificato");
                        last;
                }

            $sender=undef; @recips=(); $got_rcpt=0;$self->{smtp_outbuf}= []; $self->{msg_size} = 0;
            $msgInfo = $self->{objmsg} = Opec::Message->new($tempDir, "Msg in Ingresso"); # ###

                $seq++;

            doLog(2, "mail $seq - richiesta $Opec::childInvocationCount");
            # setting id interno del messaggio
                msgId(sprintf(  "%05d.%02d%s",
                                $$,
                                $Opec::childInvocationCount,
                                ".$seq"));

            # creo la struttura temporanea
            # (o la riciclo se $msgInfo->tempdir impostato)
            $msgInfo->createTmpSystem();

                # check argomenti del comando MAIL
                if ($args !~ /^FROM: \s*
                              ( < (?: " (?: \\. | [^\\"] )* " | [^"@] )*
                                  (?: @ (?: \[ (?: \\. | [^\]\\] )* \] |
                                            [^\[\]\\>] )* )?
                                > |
                                [^<\s] (?: " (?: \\. | [^\\"] )* " | [^"\s] )*
                              ) (?: \s+ ([\040-\176]+) )? $(?!\n)/isx ) {
                    $self->smtp_resp(0,"501 5.5.2 Syntax: MAIL FROM: <address>");
                    doLog(2, "Errore di sintassi: $cmd");
                    last;
                }

                # gestione argomenti di MAIL (ESMTP)
                my($addr,$opt) = ($1.$taint, $2.$taint);
                my($bad);
                for (split(' ',$opt)) {
                    if (!/^ ( [A-Za-z0-9] [A-Za-z0-9-]*  ) =
                        ( [\041-\074\076-\176]+ ) $(?!\n)/x) {#printable, no =,SP
                                $self->smtp_resp(0,
                                    "501 5.5.4 Syntax error in MAIL FROM parameters");
                    doLog(2,
                    "Errore di sintassi nel parametro del comando MAIL FROM");
                                $bad = 1;
                                last;
                    } else {
                                my($name,$val) = (uc($1).$taint, $2.$taint);
                                if ($name eq 'SIZE' && $val=~/^\d{1,20}$/) {
                                    $self->{msg_size} = $val+0;
                                } else {
                                    $self->smtp_resp(0,
                                    "504 5.5.4 MAIL command parameter error: $name=$val");
                            doLog(2,
                                "Errore parametro del comando MAIL: $name=$val");
                                    $bad = 1;
                                    last;
                                }
                    }
                }
                if (!$bad) {
                   $addr = ($addr =~ /^<(.*)>$/s) ? $1.$taint : $addr;
                   $self->smtp_resp(0,"250 2.1.0 Sender $addr OK");
                   $sender = unquoteRfc2821Local($addr);
                };
                last;
            };

            /^RCPT$/ && do {
                if (!defined($sender)) {
                    $self->smtp_resp(0,"503 5.5.1 Need MAIL command before RCPT");
                    $sender = undef; @recips = (); $got_rcpt = 0;
                doLog(2, "Errore: comando RCPT senza MAIL precedente");
                    last;
                }
                $got_rcpt++;    # contatore destinatari
                # permit some sloppy syntax without angle brackets
                if ($args !~ /^TO: \s*
                              ( < (?: " (?: \\. | [^\\"] )* " | [^"@] )*
                                  (?: @ (?: \[ (?: \\. | [^\]\\] )* \] |
                                            [^\[\]\\>] )* )?
                                > |
                                [^<\s] (?: " (?: \\. | [^\\"] )* " | [^"\s] )*
                              ) (?: \s+ ([\040-\176]+) )? $(?!\n)/isx ) {
                    $self->smtp_resp(0,"501 5.5.2 Syntax: RCPT TO: <address>");
                doLog(2, "Errore di sintassi: $cmd");
                    last;
                }
                if ($2 ne '') {
                    $self->smtp_resp(0,
                        "504 5.5.4 RCPT command parameter not implemented: $2");
                doLog(2,
                "Errore: parametro del comando RCPT non implementato: $2");
                } else {
                    my $addr = $1.$taint;
                    $self->smtp_resp(2,"250 2.1.5 Recipient $addr OK");
                    push(@recips, unquoteRfc2821Local($addr));
                };
                last;
            };

            /^DATA$/ && !@recips && do {
                if (!defined($sender)) {
                    $self->smtp_resp(1,"503 5.5.1 Need MAIL command before DATA");
                doLog(2, "Errore: comando Mail necessario prima di DATA");
                } elsif (!$got_rcpt) {
                    $self->smtp_resp(1,"503 5.5.1 Need RCPT command before DATA");
                doLog(2, "Errore: comando RCPT necessario prima di DATA");
                } else {
                    $self->smtp_resp(1,
                    "554 5.1.1 Error (DATA): no valid recipients");
                doLog(2, "Errore in DATA: mittente non valido")}
                last;
            };

            /^DATA$/ && do {
                # set timer to the initial value, MTA timer starts here
                my( $within_data_transfer,  # 0 il sender non ha iniz. a trasf.
                                            # 1 altrimenti
                    $complete);             # 0 mail non complet. trasferita
                                            # 1 mail trasferita
                eval {
                    # impostazione evelop
                    $msgInfo->sender($sender);
                    $msgInfo->recips(\@recips);
                    # stampa riassuntiva
                    doLog(1, sprintf("%s:%s %s %s -> %s",
                              $conn->socket_ip eq $inetSocketBind ?
                                '' : '['.$conn->socket_ip.']',
                              $conn->socket_port,
                              $msgInfo->tempdir,
                              $sender,
                              join(',', map{"<$_>"}@recips)
                              ) );
                    $self->smtp_resp(1,"354 End data with <CR><LF>.<CR><LF>");
                    $within_data_transfer = 1;
                    # recupero e salvo su file la mail vera e propria
                    do{ local($/) = "\015\012"; #set in.line terminator to CRLF
                            my $header_process  = 1;
                        my $msgid_orig;
                        my $msg_orig        = 1;
                                while(<$sock>) {    # use native I/O for speed
                                    # doLog(4, $self->{proto} . "< $_");
                                    if (/^\./) {
                                        if ($_ eq ".\015\012") {
                                            $complete = 1; $within_data_transfer = 0;
                                            last;
                                            }
                                        # rfc 2821 by the letter
                                        s/^\.(.+\015\012)$(?!\n)/$1/s;
                                    }

                                    # Gestione Message-ID
                                    # * SOLO PER MESSAGGI ORIGINALI, inalterati gli altri
                                    # devo individuare la fine del header per
                                    # . salvare il Message-ID originale,
                                    # . modificarlo con uno nuovo aderente alle specifiche
                                    # . aggiungerlo qualora mancasse
                                    if($header_process){
                                        if($_ eq $/){   # fine header
                                        $header_process = undef;
                                            $msgInfo->msgid_orig($msgid_orig);

                                if($msg_orig){
                                    # messaggio originale: sostituisco
                                            $msgInfo->msgid_new(headerMsgId());
                                            print {$msgInfo->{mail_text}} $OPEC_HEADER_KEY_MSGID, ': <', $msgInfo->msgid_new(), '>', $eol
                                                or die "Impossibile scrivere nel file temporaneo: $!";
                                    if( defined $msgid_orig ){
                                                    print {$msgInfo->{mail_text}} $OPEC_HEADER_KEY_MSGID_RIF, ': <', $msgid_orig, '>', $eol
                                                        or die "Impossibile scrivere nel file temporaneo: $!"}
                                }else{
                                    # ricevute, avvisi, buste di trasp. o altro
                                    if( defined $msgid_orig ){
                                                    print {$msgInfo->{mail_text}} $OPEC_HEADER_KEY_MSGID, ': <', $msgid_orig, '>', $eol
                                                        or die "Impossibile scrivere nel file temporaneo: $!"}
                                }
                                        }elsif(/^$OPEC_HEADER_KEY_MSGID:\s*(.*)/i){
                                            $msgid_orig = $1;
                                            $msgid_orig =~ s/\s|<|>//g;
                                            chomp($msgid_orig);
                                            next;
                                        }elsif(/$OPEC_HEADER_KEY_RECEIPT|$OPEC_HEADER_KEY_DOCTRASP/){
                                            undef $msg_orig;
                                        }
                                    }

                                    chomp; # remove \015\012 (=$/), faster than s///
                                    print {$msgInfo->{mail_text}} $_,$eol
                                            or die "Impossibile scrivere nel file temporaneo: $!";
                                }

                            $eof = 1  if !$complete;
                    }; # restores line terminator
                    # normal data termination, or eof on socket, or fatal error
                    doLog(4, $self->{proto} . "< .\015\012")  if $complete;
                    $msgInfo->mail_text->flush
                        or die "Impossibile flush file temporaneo: $!";

                }; # FINE EVAL

            # check blocco eval
                if ($@ ne '' || !$complete) {  # error or connection broken
                    # errore durante la transazione
                    # o transazione non completata
                    chomp($@);
                    # either send: '421 Shutting down', or alternatively:
                    #   '451 Aborted, error in processing' and NOT shut down!
                    if ($within_data_transfer) {
                        # transazione non terminata
                                $aborting = "interruzione della connessione dal client ".
                                            "durante il trasferimento"  if !$complete;
                                $aborting .= ', '  if $aborting ne '' && $@ ne '';
                                $aborting .= $@;
                                $aborting = '???'  if $aborting eq '';
                                doLog($@ ne '' ? 0 : 3,
                                       $self->{proto}." ATTENZIONE, ABORTING: $aborting");
                    } else {
                        # tranzazione terminata
                                my($msg) = "Error in processing: " .
                                           !$complete && $@ eq '' ? 'incomplete' : $@;
                                doLog(2, $self->{proto}." ATTENZIONE: 451 4.5.0 $msg");
                                $self->smtp_resp(1,"451 4.5.0 $msg");
                            ### $aborting = $msg;
                    }
                } else {
                        # > CORE<<<

                    # report secondo spefifiche
                    doLog(2, "Estremi del messaggio in ingresso:");
                    doLog(2, "Sender: ".$msgInfo->sender->mailAddress);
                    map {doLog(2, "Rcpt: ".$_->mailAddress)}
                    @{$msgInfo->recips()};
                    # stampo l'oggetto in UTF-8
                doLog(2, 'Subject: '.$msgInfo->h_subject_utf8);
                doLog(2, 'Message-ID: '.$msgInfo->msgid_orig );
                doLog(2, 'Message-ID-new: '.$msgInfo->msgid_new )
                    if($msgInfo->msgid_new);

                doLog(0, "*** NEW MAIL - START");
#                           &sectionTime('IN::SMTP');
#                           my ($res_type, $res_string) =
#                               @{&$corePec($conn, $msgInfo)};
                    my @res_arr = &$corePec($conn, $msgInfo);
                doLog(0, "*** NEW MAIL - END");

#                   corePec ritorna un'arr. da valutarsi a due a due
#                   per quanti sono i destinatari:
#                   il primo valore e' un intero che va da 0/undef a 3
#                   (1 per ok) mentre il secondo una stringa con il
#                   codice ESMTP + desc. Visto che la trans. SMTP prevede
#                   una sola risposta ragiono come segue:
#                   a) se anche un solo dest. e' ok allora ok
#                   b) ha la precedenza l'errore con gravita' maggiore (3)

                my $res_type;
                my $res_string;
                for (my $i=0; $i<@res_arr; $i++){
                    next if($i%2);
                    last if($res_arr[$i] !~ /^[0-3]{1}$/);
                    if($res_arr[$i] == 1){
                        $res_type = 1;
                        $res_string = $res_arr[$i+1]
                            if(exists $res_arr[$i+1]);
                        last}
                    if($res_arr[$i] > $res_type){
                        $res_type = $res_arr[$i];
                        $res_string = $res_arr[$i+1]
                            if(exists $res_arr[$i+1]);
                    }
                }
                if(!defined $res_type){
                    $res_type = 3;
                    $res_string = "Requested action aborted: error in processing"}

#                           &sectionTime('corePec');
                doLog(4, "Risposta corePec: \"$res_type $res_string\"");

                # la transazione SMTP si aspetta una risposta
                # per tutti i destinatari
                #my $rcpt = shift $msgInfo->recips;
                my $msgId_disp = formatMtaResponse(msgId());
                $msgId_disp = " ($msgId_disp)" if $msgId_disp;
                if(!$res_type){
                    # mail non spedita
                    # problema generale, meglio riprovare
                    $self->smtp_resp(0,"451 ".
                    "4.4.3 Requested action aborted: error in processing".$msgId_disp);
                }elsif($res_type == 1){
                    # OK
                    $self->smtp_resp(0,"250 2.7.1 Ok".$msgId_disp);
                }elsif($res_type == 2){
                    # errore temporaneo
                    # (l'MTA riprovera' + tardi)
                    $self->smtp_resp(0,"451 ".$res_string.$msgId_disp);
#                        "4.4.3 ".
#                        "Requested action aborted: error in processing");
                }elsif($res_type == 3){
                    # errore permanente
                    # (l'MTA non riprovera')
                    $self->smtp_resp(0,"554 ".$res_string.$msgId_disp);
#                        " 5.7.7 ".
#                        "Transaction failed");
                }
                }

                alarm(0);
                doLog(4,"timer azzerato");
                # sbianco
                $sender = undef; @recips = (); $got_rcpt = 0;
                $msgInfo->preserve_evidence(0);
                last;
            };  # fine blocco DATA

        # arrivo qua se il comando recuperato
        # non corrisponde a nessuno di quelli previsti
            $self->smtp_resp(1,"502 5.5.1 Error: command ($_) not implemented");
        doLog(2, "Errore: comando non implementato: $_");
        }; # fine blocco interno a while

        # $terminating -> true solo se ricevuto cmd QUIT
        # $aborting -> true in caso di errore in eval
        #              o trasmissione non terminata
        $voluntary_exit = 1;
        last  if $terminating || defined $aborting;  # exit SMTP-session loop
    } # fine blocco while

    my($errn,$errs);
    if (!$voluntary_exit) {
        $eof = 1;
        if (!defined($_)) { $errn = 0+$!; $errs = "$!" }
    }
    # we come here when: QUIT is received, eof on socket, or we need to abort
    my($msg) =
            defined $aborting && !$eof ? "ABORTING the session: $aborting" :
            defined $aborting ? $aborting :
            $errn             ? "error reading from client socket: $errs" :
            !$terminating     ? "client broke the connection without a QUIT" : '';

    doLog(2, $self->{proto}.': NOTA: '.$msg)  if $msg ne '';
    if (defined $aborting && !$eof){
        $self->smtp_resp(1,"421 4.3.2 Service shutting down, ".$aborting)}

    # se arrivo qua la chiusura e' cmq regolare
    $self->{session_closed_normally} = 1;
}

#
# Metodo di istanza
# Spedisce una risposta al client
# del tipo 3-digit code ed un msg opzionale
#
# IN: 1. bool: true obbliga al flush del buffer
#              false altrimenti
#     2. msg di risposta
# OUT: -
#
sub smtp_resp ($$$$) {
    my($self, $flush, $resp) = @_;
    my @buffer;
    my $strResp;

    #--- check codice di risposta
    my($taint) = substr($resp,0,0);
    #$resp = sanitize_str($resp,1);
    if ($resp !~ /^ ([1-5]\d\d) (\ |-|$(?!\n))
                    ([245] \. \d{1,3} \. \d{1,3} (?: \ |$(?!\n)) )?
                    (.*) $(?!\n)/xs)
        { die "Errore interno(2): errato codice di risposta SMTP: '$resp'" }

        #--- prepara la risposta suddividendola nel caso il messaggio
        #    superi i 512 caratteri
    my($resp_code,$continuation,$enhanced,$tail) = ($1,$2,$3, $4.$taint);
    my($lead_len) = length($resp_code) + 1 + length($enhanced);
    my($head);
    while (length($tail) > 512-2-$lead_len || $tail =~ /\n/) {
        # rfc2821: The maximum total length of a reply line including the
        # reply code and the <CRLF> is 512 characters.  More information
        # may be conveyed through multiple-line replies.
        $head = substr($tail,0,512-2-$lead_len);
        if ($head =~ /^([^\n]*\n)/) { $head = $1.$taint }
        $tail = substr($tail,length($head)); chomp($head);

        push(@{$self->{smtp_outbuf}}, $resp_code.'-'.$enhanced.$head);
    }
        push(@{$self->{smtp_outbuf}},$resp_code.$continuation.$enhanced.$tail);
    $self->smtp_resp_flush   if $flush || !$self->{pipelining} ||
                                @{$self->{smtp_outbuf}} > 200;
}

#
# Metodo di istanza
# Esegue il flush del buffer
# (supporto ESMTP - PIPELINING)
#
# IN: -
# OUT: -
#
sub smtp_resp_flush ($) {
    my($self) = shift;
    if (@{$self->{smtp_outbuf}}) {
        # log
        map {doLog(4, $self->{proto}."> $_")}
            @{$self->{smtp_outbuf}};
        # scrittura su socket
        print map($_."\015\012", @{$self->{smtp_outbuf}});
        # svuoto il buffer
        @{$self->{smtp_outbuf}} = ();
    }
}


sub DESTROY {
    my($self) = shift;

    if (! $self->{session_closed_normally}) {
        # chiusura con errori
        $self->smtp_resp("421 4.3.2 Service shutting down, closing channel")}
}

1;

__END__

=head1 NAME

Opec::In::SMTP - Interfaccia di comunicazione SMTP per il traffico in ingresso

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
