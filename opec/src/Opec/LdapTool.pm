#
# $Id: LdapTool.pm,v 1.25 2013/03/28 16:59:03 ldivizio Exp $
#
# Project       OpenPec
# file name:    LdapTool.pm
# package:      Opec::LdapTool
#
# DESCRIPTION
# Interfaccia di comunicazione verso LDAP
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 22/01/2004 - Fanton Flavio; Luca Manganelli
# modification:
# 12/04/2004 - Fanton Flavio
#   . se la connessione verso LDAP server locale (mirror) non va a buon
#     fine tenta di connettersi ad uno di riserva se impostato
#     (generalmente quello del CNIPA): dovrebbe facilitare l'aggiornamento
#     del mirror e dare maggiore affidabilita'.
#   . adattamento per bind anonimo
# 24/05/2006 - Fanton Flavio
#   . aggiunta la funzione getHash
# 26/11/2006 - Fanton Flavio
#   . modificata getMailReceipt: chiamata senza parametri restituisce le mail di riferimento del fornitore
# 13/06/2007 - Fanton Flavio
#   . aggiunto metodo getGestore per conformita' log legale
# 25/01/2008 - Fanton Flavio
#   . eliminata la gestione dei timer
# 08/02/2008 - Fanton Flavio
#   . aggiunto un die in getMailReceipt in caso di + record restituiti
# 09/06/2009 - Fanton Flavio
#   . aggiunte cache per i metodi validateRecipt, getMailReceipt, getCertificate, getCertificateFromHash
#   . modificato il controllo sulla validita' della connessione:
#     _checkConnection non effettua piu' ldapsearch ma semplicemente verifica che la connessione
#     sia sul primario altrimenti se trascorso $timeout_connection ritenta la connessione
# 30/09/2009 - Fanton Flavio
#   . aggiunta clausola providerUnit
# 12/07/2010 - Fanton Flavio
#   . bugfix connessioni server ldap
# 20/03/2013 - Fanton Flavio, Lorenzo Gaggini
#   . aggiunta la cache a getGestore
#
#*  Copyright (C) 2004  Ksolutions S.p.A.,  All Rights Reserved.
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#
package Opec::LdapTool;


use strict;

use Net::LDAP;
use Opec::Conf qw(:confvars :const);
use Opec::Message::EnvelopRecipt;
use Opec::Util qw( &doLog );

my $connSecondServer;               # vale 1 se la connessione e' verso
                                    # il server secondario, undef altrim.
my $timeout_connection = 2*3600;    # tempo oltre il quale la connessione
                                    # verso LDAP server viene rieffettuata

# ### CACHE
my %CACHE_DOM;                      # cache dei domini:
                                    # $CACHE_DOM{<dominio in lc>} = 1; # dominio certificato
                                    # $CACHE_DOM{<dominio in lc>} = 0; # dominio non certificato

my %CACHE_MAILREF;                  # cache mail di riferimento del gestore:
                                    # $CACHE_MAILREF{<dominio in lc>} = <mail>; # dominio certificato

my %CACHE_CERT;                     # cache certificati:
                                    # $CACHE_CERT{<dominio in lc>} = <certificato>; # dominio certificato

my %CACHE_CERT_HASH;                # cache certificati in base a hash del certificato:
                                    # $CACHE_CERT{<hash certif>} = <certificato>;

my %CACHE_GESTORE;                  # cache email riferimento gestore:
                                    # $CACHE_GESTORE{<dominio in lc>} = <indirizzo email>;

my $TIMEOUT_CACHE = 3600;
# ###

# metodo di classe
# COSTRUTTORE
#  IN:   opz. boolean - true nessun log, false altrimenti
#  OUT:  rif. oggetto se ok, stringa d'errore
#        altrimenti
#
sub new {
        my $proto       = shift;
        my $class       = ref($proto) || $proto;
        my $silent      = shift;
        my $self        = {};
        bless($self,$class);

    $self->{silent} = $silent;
    $self->{_host} = '';
    $self->{_port} = '';
    $self->{_user} = '';

    $self->_connection();
}

#
# metodo d'istanza privato
# VALIDA LA CONNESSIONE
# SE CASO LA RICREA CHIAMANDO  _connection
# La validazione della connessione consiste semplicemente nel:
# . caso di connessione verso LDAP mirror:
#   rieffettuarla nel caso non siano state effettuate operazioni entro
#   $timeout_connection
# . caso di connessione verso LDAP secondario
#   rieffettuarla cmq se + vecchia di $timeout_connection
#
# paramIN:   -
#
# paramOUT:  rif. obj se ok, die + stringa d'errore altrimenti
#
sub _checkConnection {
    my $self    = shift;

        # . se la connessione non esiste la creo
    # . se la connessione e' sul secondario
    #     ritento la connessione sul primario dopo $timeout_connection
    return $self->_connection()
        if( !$self->{'_objLDAP'} or ($connSecondServer &&
            (time - $self->{'_lastconnection'} > $timeout_connection)) );


        # se arrivo qua la connessione esiste
        # ma non sappiamo se è sempre valida
        return $self if( $self->_checkPostacert() );

        $self->_connection();
}

#
# metodo d'istanza privato
# Effettua la connessione
# IN: - (prende tutto da configurazione)
# OUT: rif. Obj se ok, die + stringa d'errore altrimenti
#
sub _connection {
    my $self            = shift;
    $self->{'_objLDAP'}->unbind() if($self->{'_objLDAP'});

    my $objLdapMsg;
    my $user = $ldap_user;
    my $psw = $ldap_psw;
    undef $connSecondServer;

    # connessione LDAP mirror
        $self->{'_objLDAP'} = Net::LDAP->new($ldap_host,
                                             port => $ldap_port,
                                             timeout => 5);

    # eventuale connessione LDAP di riserva
    if(!defined $self->{'_objLDAP'}){
        if($ldap2_host){
            $self->{silent} or doLog(1, "Errore durante la connessione LDAP primario: ".
                                         $ldap_host);
                $self->{'_objLDAP'} = Net::LDAP->new($ldap2_host,
                                                     port => $ldap2_port,
                                                     timeout => 5);

            if(!defined $self->{'_objLDAP'}){
                $self->{silent} or doLog(1, "Errore durante la connessione LDAP secondario: ".
                                             $ldap2_host);
                die "Errore durante la connessione LDAP\n"}
            $user = $ldap2_user;
            $psw = $ldap2_psw;
            $connSecondServer = 1;
        }else{
            die "Errore durante la connessione LDAP: $ldap_host\n";
        }
    }

    $self->{silent} or  $connSecondServer?doLog(2, "Nuova connessione LDAP verso $ldap2_host"):
                                          doLog(2, "Nuova connessione LDAP verso $ldap_host");

        # se l'utente nn esiste si effettua il bind anonimo
        $objLdapMsg = $user?
                      $self->{'_objLDAP'}->bind( $user,
                                              password => $psw,
                                              version => 3):
                  $self->{'_objLDAP'}->bind();

    die "Errore durante bind LDAP: ".$objLdapMsg->code .
        "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n"
        if($objLdapMsg->code);

    if($connSecondServer){
        $self->{_host} = $ldap2_host;
        $self->{_port} = $ldap2_port;
    }else{
        $self->{_host} = $ldap_host;
        $self->{_port} = $ldap_port;
    }

    # ping ldap
    die 'Errore durante il ping del server LDAP (' .
        $self->{_host} . ':' . $self->{_port} .
        "): server corretto?\n"
        unless($self->_checkPostacert());

    $self->{'_lastconnection'} = time;

    $self;
}


#
# metodo d'istanza privato
# Effettua la ricerca su LDAP gestendo il caso di connessioni
# connessioni invalide
#
# IN: parametri da passare a metodo search di Net::LDAP
# OUT: rif. Obj Net::LDAP::Search se ok, die + stringa d'errore altrimenti
#
sub _mySearch {
        my $self = shift;
        my $objLdapMsg;
        my $boolerr;

        while(1){
                $objLdapMsg = $self->{'_objLDAP'}->search( @_ )
                        if $self->{'_objLDAP'};

        if(!$objLdapMsg or $objLdapMsg->code){
                # ERRORE
                if( $boolerr ){
                    # devo uscire
                    die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
                        "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
                }else{
                        # primo tentativo, provo a riconnettermi
                        $self->_checkConnection();
                }
                        $boolerr=1;
                        next;
        }

        last;
        }

        return $objLdapMsg;
}


#
# Metodo di istanza
# (sola lettura)
# Restituisce l'host LDAP a cui si e' connessi
#
# IN: -
# OUT: host server LDAP
#
sub host
  { my($self)=shift; $self->{_host} }

#
# Metodo di istanza
# (sola lettura)
# Restituisce la porta dell'host LDAP a cui si e' connessi
#
# IN: -
# OUT: porta del server LDAP
#
sub port
  { my($self)=shift; $self->{_port} }

#
# metodo di istanza
# Verifica la certificazione dei destinatari
#
# IN: rif. array o rif. singolo ad
#     oggetti Opec::Message::EnvelopRecipt
# OUT: modifica la proprieta' certificate dei
#      ref. obj in ingresso, die+stringa in caso
#      di errore
#
sub validateRecipt {
    my $self = shift;
    my $recips = shift;

    $recips = [$recips] if(!ref($recips));

    my $objLdapMsg;
    my $rcpDomain;
    foreach (@{$recips}){
        # salta i destinatari considerati
        # certificati (usualmente quelli dei
        # dei domini locali certificati)
        if($_->certificate){
            $self->{silent} or doLog(4, "LDAP: ".$_->mailAddress." certificato (locale)");
            next}

        $_->mailAddress() =~ /.+\@(.+)/;
        $rcpDomain = $1;

        # check cache
        if($CACHE_ldap){
            if( exists $CACHE_DOM{lc($rcpDomain)} ){
                my ($c,$t) = @{$CACHE_DOM{lc($rcpDomain)}};
                if( (time - $t) < $TIMEOUT_CACHE ){
                    $_->certificate( $c );
                    $self->{silent} or doLog(4, "LDAP: $rcpDomain - data gets from cache");
                    next}
                $self->{silent} or doLog(4, "LDAP: $rcpDomain - cache data expired");
            }
        }

        $objLdapMsg = $self->_mySearch(
            base    => $ldap_basedn,
            scope   => 'sub',
            filter  => $OPEC_LDAP_SEARCH_DOMAIN.
                       '='.$rcpDomain,
            attrs => ['1.1']);  # recupera solo il dn

        if($objLdapMsg->code){
            # errore
            die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
                "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
        }else{
            # ok verificare
            #if($objLdapMsg->count >= 1)
            if($objLdapMsg->entries){
                $_->certificate(1);
                $CACHE_DOM{lc($rcpDomain)} = [1,time] if($CACHE_ldap);
                $self->{silent} or doLog(4, "LDAP: ".$_->mailAddress." certificato");
            }else{
                $CACHE_DOM{lc($rcpDomain)} = [0,time] if($CACHE_ldap);
                $self->{silent} or doLog(4, "LDAP: ".$_->mailAddress." non certificato")}
        }
    }
}

#
# metodo di classe
# Verifica la presenza dell'organization o=postacert, nodo
# per la posta certificata dell'indice dei gestori.
# Puo' essere anche utilizzato come ping del server LDAP locale
#
# IN: -
#
# OUT: true se l'organization esiste, false altrimenti
#
sub _checkPostacert {
    my $self    = shift;

    my $objLdapMsg = $self->{'_objLDAP'}->search(
        base    => $ldap_basedn,
        scope   => 'base',
        filter  => $ldap_basedn,
        attrs => ['1.1']);  # recupera solo il dn

    if(!$objLdapMsg || $objLdapMsg->code){
        # errore
        $self->{silent} or doLog(4, "PING LDAP $self->{_host} Ko");
        return 0;
    }

    my $out;
    if($objLdapMsg->entries){
        $out = 1;
        $self->{silent} or doLog(4, "PING LDAP $self->{_host} Ok");
    }else{
        $out = 0;
        $self->{silent} or doLog(4, "PING LDAP $self->{_host} Ko")}

    return $out;
}


#
# metodo di classe
# Verifica che 2 domini appartengano allo stesso gestore
#
# IN: 1. email o dominio da verificare
#     2. email o dominio da verificare
#
# OUT: 1 - successo / 0 altrimenti - die+stringa in caso di errore
#
sub checkDomainProvider {
    my $self     = shift;
    my $paramIn_1= shift || return 0;
    #XXX il valore providername deve essere bonificato!!
    my $paramIn_2= shift || return 0;

    my $objLdapMsg;
    my $domain2check_1;
    my $domain2check_2;
    if($paramIn_1 =~ /.+\@(.+)/){
        $domain2check_1 = $1;
    }else{
        $domain2check_1 = $paramIn_1;
    }
    if($paramIn_2 =~ /.+\@(.+)/){
        $domain2check_2 = $1;
    }else{
        $domain2check_2 = $paramIn_2;
    }
    return 1 if($domain2check_1 eq $domain2check_2);

    $objLdapMsg = $self->_mySearch(
        base    => $ldap_basedn,
        scope   => 'sub',
        filter  => "(&($OPEC_LDAP_SEARCH_DOMAIN=$domain2check_1) ".
                   "($OPEC_LDAP_SEARCH_DOMAIN=$domain2check_2))",
        attrs => ['1.1']);  # recupera solo il dn

    if($objLdapMsg->code){
        # errore
        die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
            "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
    }

    my $out;
    if($objLdapMsg->entries){
        $out = 1;
        $self->{silent} or doLog(4, "LDAP: $domain2check_1 e $domain2check_2".
                                    " sono dello stesso provider");
    }else{
        $out = 0;
        $self->{silent} or doLog(4, "LDAP: $domain2check_1 e $domain2check_2".
                                    " NON sono dello stesso provider")
    }

    return $out;
}


#
# metodo di classe
# Restituisce tutti i domini locali
#
# IN: -
# OUT: arr. ref. dei domini locali
#
sub getLocalDomain {
    my $self    = shift;
    my $objLdapMsg;
    my $domain2check;
    my $lFilter;

    $objLdapMsg = $self->_mySearch(
        base    => $ldap_basedn,
        scope   => 'sub',
        filter  => $ldap_localfilter,
        attrs => ['managedDomains']);
        #filter  => "providerName=$providerName",

    die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
        "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n"
        if($objLdapMsg->code);
    die "Errore durante il recupero dei domini","\n"
        if($objLdapMsg->count != 1);

    my $entry = $objLdapMsg->shift_entry();
    return $entry->get_value ( 'managedDomains', asref => 1 );
}


#
# metodo d'istanza
# Recupera l'indirizzo di posta di riferimento
# dell'ente che mantiene il dominio certificato
# (utente a cui inviare le ricevute di presa in
# carico)
#
# IN: dominio/email o rif. obj Opec::Message::EnvelopRecipt
#     se null ritorna l'indirizzo email del fornitore
# OUT: indirizzo email, die+stringa in caso d'errore
#      (undef in  caso di inconsistenza)
#
sub getMailReceipt {
    my $self = shift;
    my $rcp  = shift;
    my $out;
    my $objLdapMsg;
    my $rcpDomain;
    if($rcp){
        my $mailRcp = ref($rcp)?$rcp->mailAddress():$rcp;
        $rcpDomain = index($mailRcp, '@') == -1 ?
                     $mailRcp :
                     (split(/@/,$mailRcp))[1];

        # check cache
        # XXX non c'è nessuna logica per rinfrescare i dati
        # visto che dovrebbe essere un dato che cambia raramente
        # inoltre il processo ha una durata limitata
        if($CACHE_ldap){
            if( exists $CACHE_MAILREF{lc($rcpDomain)} ){
                $self->{silent} or doLog(4, "LDAP: $rcpDomain - data gets from cache");
                return $CACHE_MAILREF{lc($rcpDomain)} ;
            }
        }

        $self->{silent} or doLog(4, "LDAP: recupero email gestore di $rcpDomain");
        $objLdapMsg = $self->_mySearch(
            base    => $ldap_basedn,
            scope   => 'sub',
            filter  => $OPEC_LDAP_SEARCH_DOMAIN.'='.$rcpDomain,
            attrs => [$OPEC_LDAP_SEARCH_MAILRECEIPT]);
    }else{
        $self->{silent} or doLog(4, "LDAP: recupero email fornitore");
        $objLdapMsg = $self->_mySearch(
            base    => $ldap_basedn,
            scope   => 'sub',
            filter  => $ldap_localfilter,
            attrs => [$OPEC_LDAP_SEARCH_MAILRECEIPT]);

    }

    if($objLdapMsg->code){
        # errore
        die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
            "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
    }else{
        if($objLdapMsg->count == 1 ){
            # ok
            $out = $objLdapMsg->shift_entry()->get_value(
                $OPEC_LDAP_SEARCH_MAILRECEIPT);
            if($CACHE_ldap){
                $CACHE_MAILREF{lc($rcpDomain)} = $out if($rcpDomain);
            }
        }elsif($objLdapMsg->count == 0){
            # errore
            # nessun elemento trovato
            $self->{silent} or doLog(4, "LDAP - getMailReceipt: nessun elemento trovato");
        }else{
            # errore (raro)
            # trovato + di un elemento
            $self->{silent} or doLog(4, "LDAP - getMailReceipt: trovati + elementi");
            die "errore nell'indice dei gestori: trovati + elementi di $OPEC_LDAP_SEARCH_MAILRECEIPT","\n";
        }
    }

    $out;
}

#
# metodo d'istanza
# Recupera il certificato
#
# IN: dominio o rif. obj Opec::Message::EnvelopRecipt
# OUT: certificato come stringa, die+stringa in caso d'errore
#      (undef in  caso di inconsistenza)
#
sub getCertificate {
    my $self = shift;
    my $rcp = shift;
    my $out;

    my $mailRcp = ref($rcp)?$rcp->mailAddress():$rcp;

    $mailRcp =~ /.+\@(.+)/;
    my $rcpDomain = $1;

    # check cache
    if($CACHE_ldap){
        if( exists $CACHE_CERT{lc($rcpDomain)} ){
            my ($c,$t) = @{$CACHE_CERT{lc($rcpDomain)}};
            if( (time - $t) < $TIMEOUT_CACHE ){
                $self->{silent} or doLog(4, "LDAP: $rcpDomain - data gets from cache");
                return $c}
            $self->{silent} or doLog(4, "LDAP: $rcpDomain - cache data expired");
        }
    }

    $self->{silent} or doLog(4, "LDAP: recupero il certificato per $rcpDomain");
    my $objLdapMsg = $self->_mySearch(
        base    => $ldap_basedn,
        scope   => 'sub',
        filter  => $OPEC_LDAP_SEARCH_DOMAIN.'='.$rcpDomain,
        attrs => [$OPEC_LDAP_SEARCH_CERTIFICATE]);  # recupera solo il dn

    if($objLdapMsg->code){
        # errore
        die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
            "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
    }else{
        if($objLdapMsg->count == 1 ){
            # ok
            my $href = $objLdapMsg->shift_entry()->get_value(
                $OPEC_LDAP_SEARCH_CERTIFICATE, alloptions => 1);
            $out = (each %$href)[1];
            $CACHE_CERT{lc($rcpDomain)} = [$out,time] if($CACHE_ldap);
        }elsif($objLdapMsg->count == 0){
            # errore
            # nessun elemento trovato
            $self->{silent} or doLog(4, "LDAP - getCertificate: nessun elemento trovato");
        }else{
            # errore (raro)
            # trovato + di un elemento
            $self->{silent} or doLog(4, "LDAP - getCertificate: trovati + elementi");
        }
    }

    $out;
}



#
# metodo d'istanza
# Recupera il/i certificato/i dato l'hash del certificato
#
# IN:
#   1. hash del certificato allegato alla mail
#
# OUT:
#   true se l'email appartiene ad un dominio locale, false altrimenti,
#      die+stringa in caso di errore
#
sub getCertificateFromHash {
    my $self    = shift;
    my $inHash  = shift or return undef;
    my $out;

    # check cache
    if($CACHE_ldap){
        if( exists $CACHE_CERT_HASH{lc($inHash)} ){
            my ($c,$t) = @{$CACHE_CERT_HASH{lc($inHash)}};
            if( (time - $t) < $TIMEOUT_CACHE ){
                $self->{silent} or doLog(4, "LDAP: $inHash - data gets from cache");
                return $c}
            $self->{silent} or doLog(4, "LDAP: $inHash - cache data expired");
        }
    }

    $self->{silent} or doLog(4, "LDAP: verifico la stringa HASH");
    my $objLdapMsg = $self->_mySearch(
        base    => $ldap_basedn,
        scope   => 'sub',
        filter  => $OPEC_LDAP_SEARCH_CERTIFICATEHASH.'='.$inHash,
        attrs => [$OPEC_LDAP_SEARCH_CERTIFICATE]);

    if($objLdapMsg->code){
        # errore
        die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
            "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
    }else{
        if($objLdapMsg->count > 0 ){
            # ok
            my $href = $objLdapMsg->shift_entry()->get_value(
                $OPEC_LDAP_SEARCH_CERTIFICATE, alloptions => 1);
            $out = (each %$href)[1];
            $CACHE_CERT_HASH{lc($inHash)} = [$out,time] if($CACHE_ldap);
        }else{
            # errore
            # nessun elemento trovato
            $self->{silent} or doLog(4, "LDAP - nessun certificato trovato");
        }
    }

    $out;
}


#
# metodo d'istanza
# Recupera il gestore (valore dell'attributo providername)
# dato il dominio (valore dell'attributo managedDomains)
#
# IN:
#   1. string - dominio
#
# OUT:
#   valore dell'attributo providername corrispondente all'entry in cui si trova
#   il dominio fornito in ingresso se ok, dominio se il gestore non viene trovato,
#   die+stringa in caso di errore
#
sub getGestore {
    my $self    = shift;
    my $dom     = shift or return undef;
    my $out;

    # check gestore locale
    return $adminName if( exists $hLocalCertDomains{lc($dom)} );

    # check cache
    if($CACHE_ldap){
        if( exists $CACHE_GESTORE{lc($dom)} ){
            my ($c,$t) = @{$CACHE_GESTORE{lc($dom)}};
            if( (time - $t) < $TIMEOUT_CACHE ){
                $self->{silent} or doLog(4, "LDAP: $dom - data gets from cache");
                return $c}
            $self->{silent} or doLog(4, "LDAP: $dom - cache data expired");
        }
    }

    my $objLdapMsg = $self->_mySearch(
        base    => $ldap_basedn,
        scope   => 'sub',
        filter  => $OPEC_LDAP_SEARCH_DOMAIN.'='.$dom,
        attrs => [$OPEC_LDAP_SEARCH_PROVIDERNAME]);

    if($objLdapMsg->code){
        # errore
        die "Errore nell'interrogazione LDAP: ".$objLdapMsg->code .
            "-".$objLdapMsg->error_name."-".$objLdapMsg->error_text,"\n";
    }else{
        if($objLdapMsg->count > 0 ){
            # ok
            $out = $objLdapMsg->shift_entry()->get_value(
                $OPEC_LDAP_SEARCH_PROVIDERNAME );
            $CACHE_GESTORE{lc($dom)} = [$out,time] if($CACHE_ldap);
        }else{
            # errore
            # nessun elemento trovato
            $self->{silent} or doLog(4, "LDAP - Gestore del dominio $dom non trovato");
        }
    }

    $out || $dom;
}



# necessaria??
sub DESTROY {
    my $self = shift;
    if($self->{'_objLDAP'}){
        eval{
        $self->{'_objLDAP'}->unbind();
        $self->{'_objLDAP'}->disconnect();};
    }
}
1;

__END__

=head1 NAME

Opec::LdapTool - Interfaccia di comunicazione verso LDAP

=head2 Modulo del progetto OpenPEC

=cut

=head1 AUTHOR

Fanton Flavio <flazan@users.sourceforge.net>
Manganelli Luca <luca76@users.sourceforge.net>
Ferrara Umberto <uferrara@users.sourceforge.net>
Battaglia Andrea <andreabat@users.sourceforge.net>

=cut
