#!/usr/bin/perl
#
# $Id: scheduler.pl,v 1.12 2011/11/10 18:48:24 flazan Exp $
#
# Project       OpenPec
# file name:    scheduler.pl
# package:      -
#
# DESCRIPTION
# Scheduler
# Formato messaggio
# <id>@@<operazione>@<scadenza>
# <id>: obbligatorio - garantisce univocita' al messaggio
# <operazione>: obbligatorio - stringa per lo scheduler che indica l'operazione da svolgere
# <scadenza>: opzionale - se presenza indica la data oltre la quale deve essere consumato altrimenti
#             deve essere consumato appena possibile
# <id>@@<operazione>: nome task
#
# INSTALLAZIONE
# Ogni scheduler deve poter contare su una struttura condivisa da tutte le istanze;
# ci pensa lui a creare le sottocartelle.
# Ad esempio in caso di 2 istanze di opec e quindi di 2 scheduler sara' creata la struttura:
# .../QUEUE/1/tmp
# .../QUEUE/1/opt
# .../QUEUE/1/work
# .../QUEUE/2/tmp
# .../QUEUE/2/opt
# .../QUEUE/2/work
# ...
#
#
# Autore: Flavio Fanton <flazan@gmail.com>
# Idee, suggerimenti e segnalazioni sono benvenuti.
#
# L'ultima versione di OpenPEC e' disponibile sul sito:
#   http://www.openpec.org/download.shtml
#
# History [++date++ - ++author++]:
# creation: 19/04/2006 - Fanton Flavio - fanton@exentrica.it
# modification:
#   09/06/2011 - Fanton Flavio
#               - problema: ric di cons + mancata cons
#         causa: rallentamento filesystem code
#                 soluzione: introduzione dei task remainder per posticipare
#                    la cancellazione dei task
#   10/06/2011 - Fanton Flavio
#               - aggiunta rimozione task rem orfani + vecchi di $CONST_REM_TIMEOUT
#               - sostituisco le chiamate delTask con delTaskScheduler
#         (cambia solo il nome)
#
#*  Copyright (C) 2010  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Giuntini, 13 / int. L1 - 56023 Navacchio (PI) - Italy
#   tel. +39 0575 193 98 60 - fax +39 0575 86 20 24
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.exentrica.it - info@exentrica.it
#

use strict;

use POSIX       qw ( setsid );
use Sys::SigAction qw( set_sig_handler );
use Socket;
use Opec::Command qw( getTaskNum delTaskScheduler );
use Opec::Conf  qw ( :scheduler :const :confvars);
use Opec::Util  qw ( &doLog );
use vars qw( $VERSION );
use warnings;

$VERSION = '2.03';

my $CONST_EOL               = "\015\012";
my $CONST_POLLING_WAIT      = 300;           # da impostare esternamente
# !!!VERIFICARE CHE L'ALLARME DELLO SCHEDULER + TEMPO DI ATTESA FRA 2 RICHIESTE
# SIA TALE DA SUPERARE QUESTO VALORE - in caso contrario lo scheduler rischia
# di saturare i processi di opec!!!
my $CONST_TIMEOUT           = 600;           # da impostare esternamente
my $CONST_USER_AGENT        = 'OpenPEC - Scheduler ver. '.$VERSION;


my $CONST_COMPATIBILITY_OPEC= '2.02';

my $configFile              = '%%CONFFILE%%';# path del file di configurazione

$SIG{INT} = $SIG{TERM} = $SIG{HUP} = $SIG{PIPE} = \&signal_handler;
sub signal_handler {
    unlink $pidFileScheduler;
    exit(1)}

Opec::Conf::read_config($configFile);

#--- parametri linea di comando
my $daemonize = 1;
my($cmd) = lc($ARGV[0]);
if ($cmd =~ /^(start|debug)$/) {
    if(-e $pidFileScheduler){
        print "Attenzione: $0 in esecuzione (pid file: $pidFileScheduler)\n";
        exit 1}
    unless(-S $scheduler_sck){
        print "Errore: opec non avviato o socket ($scheduler_sck) non accessibile\n";
        exit 1}
    print STDOUT "> >>>Starting $0 - version: $VERSION\n";
    $daemonize = '' if($cmd eq 'debug');
} elsif ($cmd =~ /^stop$/) {
    # recupero il pid
    my($pid);
    if(!open(PID_FILE, $pidFileScheduler)){
        print "Impossibile leggere il file $pidFileScheduler: $!\n";
        exit(1)}
    while (<PID_FILE>) { chomp; $pid = $1 if /^(\d+)$/ }
    if(!close(PID_FILE)){
        print "Impossibile chiudere il file $pidFileScheduler: $!\n";
        exit(1)}
    # signal
    if(!defined($pid)){
        print "PID invalido in $pidFileScheduler, impossibile $cmd\n";
        exit(1)}
    if(!kill('HUP',$pid)){
        print "Impossibile HUP $0[$pid]: $!\n";
        exit(1)}
    print STDOUT "> >>>Shutting down - $0 - version: $VERSION\n";
    exit 0;
} else {
    print "Argomento sconosciuto. Sintassi:\n  ".
          "$0 ( start | stop | debug)\n";
    exit 1;
}

# inizializzazione log openpec
Opec::Log::init("scheduler", !$daemonize, 1);
-d $OPEC_QUEUE_PATHNAME || die "Impossibile cambiare il path di lavoro: $!\n";
$0 = $CONST_USER_AGENT;

if($daemonize){
        my $pid;
        unless($pid = fork){
            # process child
            setsid;
            umask 0;
            chdir($OPEC_QUEUE_PATHNAME.'/'.$idServer) || die "Impossibile accedere alla coda: $!\n";

            while(1){
                &child();
                sleep($CONST_POLLING_WAIT)
            }
                exit(1)
        }
        die "Couldn't fork: $!" unless defined($pid);
        if(open(PID, ">$pidFileScheduler")){
            print PID $pid;
            close(PID)}
}else{
    chdir($OPEC_QUEUE_PATHNAME.'/'.$idServer) || die "Impossibile accedere alla coda $!\n";
    &child()}

# ###########################################
# ###########################################

#
# Esegue le seguenti operazioni:
# - recupero solo i miei elementi scaduti della coda
#
sub child {
    # recupero gli elementi scaduti della coda
    my $now = time;
    my %task_to;        # task scaduti o immediati
    my %task_to_noexp;  # task immediati
    my %task_rem;
    my %task_to_rem;

    # leggo all'interno del path predefinito
    opendir(DIR, $OPEC_QUEUE_DIR_OPT) || return undef;
    while(my $file = readdir(DIR)){
        next if $file !~ /^(.+)@@(.+)@(\d+)?_?$/o;
        if(substr($file,-1,1) eq '_'){
                $task_rem{substr($file,0,length($file)-1)} = $file;
                next;
        }
        $task_to{$file}=[$2, $1] unless (defined $3 && $3 >= $now);
        $task_to_noexp{$file}='' if !defined $3;
        $task_to_rem{$1.'@@'.$2.'@'} = $file;
    }
    closedir DIR;

    # cancello i task i rem ed i loro corrispondenti
    foreach my $k (keys %task_rem){
        # verifico che ci siano sia rem che task
        # entrambi sono presenti sempre sullo stesso idserver
        if( getTaskNum($k,1) > 1 ){
                delTaskScheduler($k, 1, 1);
                delete $task_to{$task_to_rem{$k}} if(exists $task_to_rem{$k}); # XXX in caso di stop prolungato il task potrebbe rischiare di essere elaborato
                next}

        # elimino i task orfani piu' vecchi di mtime + $CONST_REM_TIMEOUT
        my @s = stat $OPEC_QUEUE_DIR_OPT.'/'.$task_rem{$k};
        unlink $OPEC_QUEUE_DIR_OPT.'/'.$task_rem{$k}
                if(@s && $s[9]+$CONST_REM_TIMEOUT < $now);
    }

    return unless(%task_to);

    #
    # SPEDISCO I COMANDI
    #
    eval{
        my $h = set_sig_handler( 'ALRM' ,sub { die "timed out\n"; } );
        alarm($CONST_TIMEOUT);

        _sendCommand(\%task_to,\%task_to_noexp);

        alarm(0);
    };
    alarm(0);
#    if($@ and $@ eq 'time out'){
#
#    }

}

#
# trasmette i messaggi a opec e ne riceve l'esito (positivo o negativo)
# per ogni messaggio trasmesso aspetta l'esito prima di passare al successivo
# Mi aspetto che appena connesso opec mandi una stringa con il seguente formato
# opec-<x.y.z>  dove x, y e z sono interi che indicano la versione
#
# PARAM IN:     rif hash di tutti i msg da spedire
#
# PARAM OUT:    -
#
sub _sendCommand {
    my $cmd2send        = shift;
    my $cmd2send_noexp  = shift;
    ref $cmd2send eq 'HASH' or die "Argomento non valido";

    socket(SCK, PF_UNIX, SOCK_STREAM, 0)        || die "Impossibile creare il socket: $!";
    connect(SCK, sockaddr_un($scheduler_sck))   || die "Impossibile connettersi a $scheduler_sck: $!";

    # check versione
    my $res;
        defined( recv(SCK, $res, 4096, 0) ) || die "Errore di trasmissine $!";
        send(SCK, $VERSION, 0);
        defined( recv(SCK, $res, 4096, 0) ) || die "Errore di trasmissine $!";
        _myChomp($res) || die "Errore: incompatibilita' di versioni\n";

    # trasmissione comandi
    # formato: <nome comando>\015\012<nome task>\015\012
    foreach (keys %$cmd2send){
        (  defined(send(SCK, "$cmd2send->{$_}->[0]$CONST_EOL$_$CONST_EOL", 0)) &&
           defined(recv(SCK, $res, 4096, 0)) ) or die "Errore durante la trasmissione del comando";

        $res = _myChomp($res);
        if( $res && ($res =~ /^engine|1/) ){
            #ok
            # cancello i task (se il task e' senza scadenza lo elimino solo dal path di lavoro)
            (exists $cmd2send_noexp->{$_}) ? delTaskScheduler($_, 1) :
                                             delTaskScheduler($_, 1, 1);
        }else{
            # errore nell'esecuzione del task
            # lo elimino solo dal path di lavoro
            delTaskScheduler($_, 1)
        }
    }
    # chiusura
    send(SCK, "$CONST_EOL.$CONST_EOL", 0);
#    sleep 2;
    close(SCK);
}


sub _myChomp ($) {
    my $a = shift;
    defined $a && $a =~ s/\015|\012|\n//gxs;
    $a
}
