#!/usr/bin/perl
#
# Project       OpenPec
# file name:    ldap_db.pl
# Version:      0.0.2
#
# DESCRIPTION
# Genera il file localdomain.db contenente i domini locali al fornitore.
# Recupera i domini o dall'indice di PEC o da file di testo.
#
# Developer:
# Fanton Flavio
#
# History [++date++ - ++author++]:
# creation: 07/08/2006 - Fanton Flavio
# modification:
#   05/09/2006 - Fanton Flavio
#       - Tolta la cancellazione diretta del file cache
#       15/02/2011 - Fanton Flavio
#               - aggiunta la possibilita' di impostare la providerunit
#
#*  Copyright (C) 2006  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Roma 43 - 57126 Livorno (LI) - Italy
#   via Giuntini, 25 / int. 9 - 56023 Navacchio (PI) - Italy
#   tel. +39 050 754 703 - fax +39 050 754 707
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

use Fcntl qw(:flock);
use Net::LDAP;
use FileHandle;
use DB_File;
use Getopt::Long;

use strict;
use vars qw( $VERSION );
$VERSION = '0.02';

#*** CONFIG
my $filename = 'localdomain.db';
my($opt_help, $opt_file, $opt_print, $opt_out);
my($opt_ldap_host, $opt_ldap_user, $opt_ldap_psw, $opt_ldap_pname, $opt_ldap_punit);
my $OPEC_LDAP_SEARCH_PROVIDERNAME          = 'providerName';
my $OPEC_LDAP_SEARCH_PROVIDERUNIT          = 'providerUnit';
#***

sub sintax {
print "*** $0 - ver. $VERSION - OpenPEC project
Genera il file localdomain.db contenente i domini locali al fornitore.
$0 [options]
options:
    -help       questa schermata
    -file       file testo contenente la lista dei domini (uno per riga)
    -out        path di salvataggio (default ~/MYHOME/cachedb)
    -print      stampa a schermo i domini da inserire in LDAP
    -ldap_host  host che ospita LDAP (host:porta)
    -ldap_user  utente LDAP
    -ldap_psw   password LDAP
    -ldap_pname providerName LDAP (nome del provider)
    -ldap_punit eventuale providerUnit LDAP (subunit del provider)

Es:
./ldap_db.pl -ldap_host 111.111.111.111 -ldap_user \"cn=Manager, o=postacert\" -ldap_psw pswpec -ldap_pname fornitorepec

";

    exit;
}

# ### MAIN ###

#--- check parametri
GetOptions (    'help'            => \$opt_help,
                'print'           => \$opt_print,
                'file=s'          => \$opt_file,
                'ldap_host=s'     => \$opt_ldap_host,
                'ldap_user=s'     => \$opt_ldap_user,
                'ldap_psw=s'      => \$opt_ldap_psw,
                'ldap_pname=s'    => \$opt_ldap_pname,
                'ldap_punit=s'    => \$opt_ldap_punit,
                'out=s'           => \$opt_out);
sintax() if $opt_help;
sintax() unless( $opt_file or ($opt_ldap_host && $opt_ldap_user && $opt_ldap_psw && $opt_ldap_pname) );
sintax() if( $opt_file and ($opt_ldap_host && $opt_ldap_user && $opt_ldap_psw && $opt_ldap_pname) );

$| = 1;

#--- default
if( $opt_out ){
    $opt_out .= '/' if substr(  $opt_out, -1, 1) ne '/';
}else{
    $opt_out = 'MYHOME/cachedb/';
}

#--- cancello e ricreo
#unlink($filename);
my %hash;
tie %hash, 'DB_File', $opt_out.$filename;

if($opt_file){   #--- recupero domini da file
    print "***Recupero dati da file ($opt_file)\n";

    open(FILE, $opt_file) or die "$!\n";

    # lock file
    print "Get lock...";
    my $fh = new FileHandle($opt_out.$filename, O_CREAT|O_RDWR);
    die "$!\n" if(!defined $fh);
    lock($fh); # lock esclusivo
    print "Ok\n";

    # caricamento su dbm
    print "Caricamento dbm ($opt_out$filename)...";
    undef %hash;
    if($opt_print){
        print "\n";
        while(<FILE>){
            chomp;
            print $_,"\n";
            $hash{$_} = ''}

    }else{
        while(<FILE>){
            chomp;
            $hash{$_} = ''}
    }

    # rlascio risorse
    close(FILE);
    unlock($fh);
    $fh->close();
    print "Ok\n";
    print "Caricamento effettuato\n";
}else{  # recupero domini da ldap
    print "***Recupero dati da LDAP ($opt_ldap_host)\n";

    print "Connessione LDAP...";
    # connessione LDAP
    my $ldap_port = (split /:/, $opt_ldap_host)[1];
    my $objldap = Net::LDAP->new(   $opt_ldap_host,
                                    port => $ldap_port || 389,
                                    timeout => 10);
    $objldap->bind( $opt_ldap_user,
                    password => $opt_ldap_psw,
                    version => 3);
    print "Ok\n";

    # lock file
    print "Get lock...";
    my $fh = new FileHandle($opt_out.$filename, O_CREAT|O_RDWR);
    die "$!\n" if(!defined $fh);
    lock($fh); # lock esclusivo
    print "Ok\n";

    # LDAP search
    print "Recupero domini di $opt_ldap_pname...";

        my $ldap_localfilter;
    if(defined $opt_ldap_punit){
        $ldap_localfilter = "(&($OPEC_LDAP_SEARCH_PROVIDERNAME=$opt_ldap_pname)($OPEC_LDAP_SEARCH_PROVIDERUNIT=$opt_ldap_punit)";
    }else{
        $ldap_localfilter = "(&($OPEC_LDAP_SEARCH_PROVIDERNAME=$opt_ldap_pname)(!($OPEC_LDAP_SEARCH_PROVIDERUNIT=*)))";
    }
    my $objLdapMsg = $objldap->search( base    => 'o=postacert',
                                       scope   => 'sub',
                                       filter  => $ldap_localfilter,
                                       attrs  => ['managedDomains']);
    die "Errore durante il recupero dei domini" if($objLdapMsg->count != 1);
    my $entry = $objLdapMsg->shift_entry();
    my $ref = $entry->get_value ( 'managedDomains', asref => 1 );
    print "Ok\n";

    # caricamento su dbm
    print "Caricamento dbm ($opt_out$filename)...";
    my $i = 0;
    undef %hash;
    while($ref->[$i]){
        $hash{$ref->[$i]} = ''; $i++}

    # rlascio risorse
    $objldap->unbind;
    unlock($fh);
    $fh->close();
    print "Ok\n";
    print "Caricamento effettuato\n";
}



# ###
# ###
sub lock ($) {
    my $file = shift;
    flock($file, LOCK_EX) or die "Can't lock $file: $!";
    seek($file, 0, 2) or die "Can't position $file to its tail: $!";
}

sub unlock ($) {
    my $file = shift;
    flock($file, LOCK_UN) or die "Can't unlock $file: $!";
}
