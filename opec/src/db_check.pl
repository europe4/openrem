#!/usr/bin/perl
#
# Project       OpenPec
# file name:    db_checl.pl
# Version:      0.0.1
#
# DESCRIPTION
# Visualizza il contento del file localdomain.db che mantiene
# tutti i domini locali al fornitore
#
# Developer:
# Fanton Flavio
#
# History [++date++ - ++author++]:
# creation: 07/08/2006 - Fanton Flavio
# modification:
#
#*  Copyright (C) 2006  EXENTRICA s.r.l.,  All Rights Reserved.
#   via Roma 43 - 57126 Livorno (LI) - Italy
#   via Giuntini, 25 / int. 9 - 56023 Navacchio (PI) - Italy
#   tel. +39 050 754 703 - fax +39 050 754 707
#   www.exentrica.it   info@exentrica.it
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   www.openpec.org - info@openpec.org
#   www.exentrica.it - info@exentrica.it
#

use Fcntl qw(:flock);
use FileHandle;
use DB_File;

use strict;
use vars qw( $VERSION );
$VERSION = '0.01';

my $db_file = shift or sintax();
my $domain  = shift;
# ###

$| = 1;

my %hash;
tie %hash, 'DB_File', $db_file;

# lock file
my $fh = new FileHandle($db_file, O_CREAT|O_RDWR);
locksh($fh); # lock esclusivo

if($domain){
    exists $hash{$domain} ? print "$domain Ok\n" : print "$domain ko\n";
    exit}

foreach (keys %hash){print $_,"\n";}

untie %hash;
unlock($fh);
$fh->close;

# ###
# ###
sub sintax {
    # sintassi
    print "(Vers. $VERSION) Sintax:\n";
    print "$0 <db file> [domain]\n\n";
    exit;
}

sub locksh($) {
    my $file = shift;
    flock($file, LOCK_SH) or die "Can't lock $file: $!";
}
sub unlock ($) {
    my $file = shift;
    flock($file, LOCK_UN) or die "Can't unlock $file: $!";
}
