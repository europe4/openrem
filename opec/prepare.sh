#!/bin/sh
VERSION=$(cat VERSION)
TOKEN_VERSION='%%VERSION%%'

rm -rf blib
rm -rf lib

mkdir blib lib

cp -r src/Opec lib
for file in src/*pl
do
    cp $file blib/$(basename $file '.pl')
done

perl Makefile.PL
