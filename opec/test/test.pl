use Net::POP3;
use Net::SMTP;
use MIME::Lite;
use MIME::Entity;
use MIME::Parser;
use Opec::Conf qw ( :const :template );
use Mail::Address;

### costanti
my $TIME_SLEEP = 5;

my $FROM_HEADER = 'From';
my $TO_HEADER =  'To';
my $CC_HEADER =  'Cc';
my $SUBJECT_HEADER =  'Subject';

my $MIME_PLAINTEXT = 'text/plain';
my $MIME_XML       = 'application/xml';
my $MIME_MSG       = 'message/rfc822';

# messaggi di errore
my $ERR_NOTYPE = 'valore campo X-Ricevuta errato!';
my $ERR_NODOC  = 'valore campo X-Trasporto errato!';
my $ERR_NOSUBJ = 'valore campo Subject errato!"';
my $ERR_NOPECFROM = 'From $Rfrom != posta-certificata@...';
my $ERR_NOSAMESUBJ = "I campi SUBJECT sono diversi!";
my $ERR_NOSAMEFROM = "I campi SENDER sono diversi!";
my $ERR_RCPTNOTSAME = "I destinatari sono diversi!";
my $ERR_ATTACHMAIL  = 'Il messaggio allegato nel documento di trasporto non e\' uguale al messaggio originario';

my $OPEC_DOCUMENTO_TRASPORTO = 'documento di trasporto';

# help output
my $help = <<LAB

Descrizione:

        Effettua i test di Posta Certificata

Uso:

        perl test.pl [-c nomefile] [-h]

LAB
;

$| = 1;

my $configFile = '../etc/opec.conf';
Opec::Conf::read_config($configFile);

### Lettura parametri

$configFile = 'test.cfg';
if ($#ARGV != -1) {
	if ($ARGV[0] eq '-c') {
		$configFile = $ARGV[1] if $ARGV[1];
	} elsif ($ARGV[0] eq '-h' || $ARGV[0] eq '--help') {
		print $help;
		exit;
	}
}

print "Reading $configFile\n";

do $configFile;

# Inizio Test
doTestA();

exit;

# Better than chomp(). Removes \015, \012 and \n
# param0: string
sub myChomp ($) {
    $a = shift;
	$a =~ s/\015|\012|\n//gxs;
	$a;
}

# Manda una mail via SMTP
# Parametri:
#   string:     SMTP server
#   array ref:  Autenticazione, se necessaria
#           [auth=0/1, string:type, string:user, string:pwd]
#   string:     MAIL FROM
#   string:     RCPT TO
#   MIME::Lite: Mail da spedire

sub sendMail ($$$$$) {
    my $SMTPServer = shift;
    my $AuthArray  = shift;
    my $MailFrom   = shift;
    my $RcptTo     = shift;
    my $eMail      = shift;
    
    print "Sending mail...";

    my $smtp = Net::SMTP->new($SMTPServer);
    if ($AuthArray -> [0]) {
	    $smtp->auth($AuthArray -> [1], $AuthArray -> [2], $AuthArray -> [3]);
    }
    $smtp->mail($MailFrom);
    $smtp->to ($RcptTo);

    my $msg_string = $eMail -> as_string();
    my @rows = split (/\n/, $msg_string);

    $smtp->data();

    foreach (@rows) {
    	$smtp->datasend ("$_\n");
    }
    
    $smtp->dataend();
    
    $smtp->quit;
    
    print "done.\n";
}

# restituisce un arrayref di emails a partire da una stringa di email
# parametri:
#   String: stringa di emails del formato email1, email2, email2 etc
# restituisce:
#   Array ref: array di indirizzi email
sub getEmails ($) {
    my @resolved;
    foreach (Mail::Address->parse (shift)) {
        push @resolved, $_->address;
    }
    
    return \@resolved;
}

# controlla la posta via POP3
# Paramtri:
#   String:       Host pop3
#   String:       user
#   String:       pass
# Restituisce:
#   String:       email
sub checkPOP3 ($$$) {
    my $POP3host = shift;
    my $user = shift;
    my $pass = shift;
    
    my $retval = '';
    my $time_slept = 10;

    print "Checking POP3...";
    
    while (1 == 1) {
    	my $pop = Net::POP3->new($POP3host);
    	if (defined $pop->login($user, $pass)) {
    		my $msgnums = $pop->list; # hashref of msgnum => size
      		my @a = keys %$msgnums;
      		print "num msg: ".($#a+1)." ";
      		if ($#a > -1) {
        	    my $msg = $pop->get($a[0]);
        		$retval = (join '', @$msg);
        		$pop->delete($a[0]);
    		    $pop->quit;
			    return $retval;
    		}
    		$pop->quit;
    		print '.';
    		sleep ($TIME_SLEEP);
    		if ($time_slept -- == 0) {
    		    print "Nessun messaggio nel POP3\n";
    		    return -1;
    		}
        } else {
    		print "Errore nell'autenticazione POP3\n";
    		$pop->quit;
    		return -1;
    	}
    	$pop->quit;
    }
    
    
    print "OK.\n";
    
    return $retval;
}

# svuota la posta via POP3
# Paramtri:
#   - String:       Host pop3
#   - String:       user
#   - String:       pass
sub flushPOP3 ($$$) {
    my $POP3host = shift;
    my $user = shift;
    my $pass = shift;
    
    my @retval = ();
 
   	my $pop = Net::POP3->new($POP3host);
   	if (defined $pop->login($user, $pass)) {
   		my $msgnums = $pop->list; # hashref of msgnum => size
    	foreach my $msgnum (keys %$msgnums) {
        	$pop->delete($msgnum);
    	}
    	$pop->quit;
    }
    
    return \@retval;
}

# Estrapola una stringa da un template
# Parametri:
#   Stringa - Stringa da estrapolare
#   Stringa - Template
#   Stringa - Campo del template
sub searchString ($$$) {
    
    my $stringa = shift;
    my $mask = shift;
    my $field = shift;
    
    $mask =~ s/(\"|\(|\))/\\$1/g;
    
    $mask =~ s/\[$field\]/(.+)/;
    $mask =~ s/\[\w+\]/\.+/g;
    
    $stringa =~ /$mask/s;
    
    $1;
}


# controlla la ricevuta
#   string:     messaggio originale
#   string:     ricevuta
#   string:     tipo di ricevuta
sub checkReceipt ($$$) {
    my $originalMsg = shift;
    my $receipt = shift;
    my $type = shift; 
    
    print "Controllo ricevuta $type\n";
    
    my $parser1 = new MIME::Parser;
    $parser1->output_to_core(1);
    my $parser2 = new MIME::Parser;
    $parser2->output_to_core(1);
    my $originalEntity = $parser1 -> parse_data ($originalMsg);
    my $receiptEntity = $parser2 -> parse_data ($receipt);
    
    # controlli formali della ricevuta
    my $originalHeader = $originalEntity -> head;
    my $receiptHeader = $receiptEntity -> head;
    
    if ($type ne $OPEC_DOCUMENTO_TRASPORTO) {
        my $Rtipo = $receiptHeader -> get ($OPEC_HEADER_KEY_RECEIPT);
        return $ERR_NOTYPE if $Rtipo !~ /$type/;
    } else {
        my $Rtipo = $receiptHeader -> get ($OPEC_HEADER_KEY_DOCTRASP);
        print "$Rtipo\n";
        return $ERR_NODOC if $Rtipo !~ /$OPEC_CERTINFO_TYPE_TRASP/;
    }

    my $Rsubj = $receiptHeader -> get ($SUBJECT_HEADER);
    my $Tsubj;
    
    my %Tsubjs = (
                $OPEC_HEADER_VALUE_RECEIPT_ACCEPT => $OPEC_HEADER_SUBJECT_RECEIPT_ACCEPT,
                $OPEC_HEADER_VALUE_RECEIPT_GETLOAD => $OPEC_HEADER_SUBJECT_RECEIPT_GETLOAD,
                $OPEC_DOCUMENTO_TRASPORTO => $OPEC_HEADER_SUBJECT_DOCTRASP,
                $OPEC_HEADER_VALUE_RECEIPT_DELIVERY => $OPEC_HEADER_SUBJECT_RECEIPT_DELIVERY
              );
        
    $Tsubj = $Tsubjs{$type};
    
    return $ERR_NOSUBJ if $Rsubj !~ /^$Tsubj/;
    
    if ($type ne $OPEC_DOCUMENTO_TRASPORTO) {
        my $Rfrom = $receiptHeader -> get ($FROM_HEADER);
        return $ERR_NOPECFROM if $Rfrom !~ /posta-certificata\@/;
    }
        
    my $Rto = $receiptHeader -> get ($TO_HEADER);

    # prendo il corpo e l'allegato
    my @Rparts = $receiptEntity -> parts;
    
    my $RBodyPart;
    my $RXMLPart;
    my $RMessagePart;
    
    # cerco il testo
    foreach (@Rparts) {
        if ($_ -> mime_type eq $MIME_PLAINTEXT) {
            $RBodyPart = $_;
        }
        if ($_ -> mime_type eq $MIME_XML) {
            $RXMLPart = $_;
        }
        if ($_ -> mime_type eq $MIME_MSG) {
            $RMessagePart = $_;
        }
    }

    my %templates = (
                      $OPEC_HEADER_VALUE_RECEIPT_ACCEPT => $TEMPLATE_RECEIPT_ACCEPT,
                      $OPEC_HEADER_VALUE_RECEIPT_GETLOAD => $TEMPLATE_RECEIPT_GETLOAD,
                      $OPEC_DOCUMENTO_TRASPORTO => $TEMPLATE_DOCTRASP,
                      $OPEC_HEADER_VALUE_RECEIPT_DELIVERY => $TEMPLATE_RECEIPT_DELIVERY
                    );

    my $template = $templates{$type};
    
    my $RBodySubject = searchString ($RBodyPart -> as_string, $template, $KEY_TEMPLATE_SUBJECT);
    my $RMittente    = searchString ($RBodyPart -> as_string, $template, $KEY_TEMPLATE_MAILFROM);
    my $Rdestinatari = searchString ($RBodyPart -> as_string, $template, $KEY_TEMPLATE_RCPTTO);
    
    # Controlli con il messaggio originale
    return $ERR_NOSAMESUBJ if ($originalEntity -> get ($SUBJECT_HEADER) !~ /$RBodySubject/);
    return $ERR_NOSAMEFROM if ($originalEntity -> get ($FROM_HEADER) !~ /$RMittente/);
    
    
    # confronto i destinatari
    my @Odest;
    
    # prendo i destinatari del messaggio originale
    foreach ($originalEntity -> get ($TO_HEADER)) {
        push @Odest, @{getEmails ($_)};
    }
    foreach ($originalEntity -> get ($CC_HEADER)) {
        push @Odest, @{getEmails ($_)};
    }
    
    $Rdestinatari =~ s/\"posta ordinaria\"|\"posta certificata\"//gi;
    my $Rdest = getEmails ($Rdestinatari);
    
    my $trovato;
    foreach my $email (@{$Rdest}) {
        $trovato = 0;
        foreach (@Odest) {
            if ($email eq $_) {
                $trovato = 1;
                last;
            }
        }
        if (!$trovato) {
            last;
        }
    }
    return $ERR_RCPTNOTSAME if (!$trovato);
    
    # Documento di trasporto: controllo l'allegato
    if ($type eq $OPEC_DOCUMENTO_TRASPORTO) {
        my $parser3 = new MIME::Parser;
        $parser3->output_to_core(1);
        my $attachedMsg = $parser3 -> parse_data (join '', @{$RMessagePart->body});

        foreach ('Received', 'Content-Type', 'Content-Transfer-Encoding', 'Message-ID', 'Content-Disposition', 'Content-Length') {
            $attachedMsg -> head -> delete ($_);
            $originalEntity -> head -> delete ($_);
        }
        
        my $attMsg = $attachedMsg -> as_string;
        my $orgMsg = $originalEntity -> as_string;
        return $ERR_ATTACHMAIL if ($attMsg =~ /$$orgMsg/);
    }
    
    return 1;
}

# Controlla un messaggio da HOST1 a HOST2
# Parametri:
#   Stringa  -  Messaggio
sub sendAndCheck ($) {
    #my ($POP3host1, $sndr_user, $sndr_pass, $sndr_email,
    #    $POP3host2, $rcpt_user, $rcpt_pass, $rcpt_email,
    #    $SMTPhost1, $SMTPhost1AuthType, $SMTPhost1User, $SMTPhost1Pass,
    $msg = shift;

    flushPOP3 ($POP3host1, $sndr_user, $sndr_pass);
    flushPOP3 ($POP3host2, $rcpt_user, $rcpt_pass);
   
    sendMail ($SMTPhost1,
              [1, $SMTPhost1AuthType, $SMTPhost1User, $SMTPhost1Pass],
              $sndr_email, $rcpt_email,
              $msg);

    sleep ($TIME_SLEEP);
    
    my $totalMsg = 0;
                
    my $mail;
    # RICEVUTA DI ACCETTAZIONE
    while (!$mail) {
        $mail = checkPOP3 ($POP3host1, $sndr_user, $sndr_pass);
        die if $mail == -1;
    }
    
    if (($err = checkReceipt ($msg -> as_string, $mail, $OPEC_HEADER_VALUE_RECEIPT_ACCEPT)) == 1) {
        print "controllo OK\n";
    } else {
        die "controllo KO: $err \n";
    }

    $mail = undef;
    # RICEVUTA DI PRESA IN CARICO
    while (!$mail) {
        $mail = checkPOP3 ($POP3host1, $PEChost1user, $PEChost1pass);
        die if $mail == -1;
    }
    
    if (($err = checkReceipt ($msg -> as_string, $mail, $OPEC_HEADER_VALUE_RECEIPT_GETLOAD)) == 1) {
        print "controllo OK\n";
    } else {
        die "controllo KO: $err \n";
    }
    
    $mail = undef;    
    # DOCUMENTO DI TRASPORTO    
    while (!$mail) {
        $mail = checkPOP3 ($POP3host2, $rcpt_user, $rcpt_pass);
        die if $mail == -1;
    }
    
    if (($err = checkReceipt ($msg -> as_string, $mail, $OPEC_DOCUMENTO_TRASPORTO)) == 1) {
        print "controllo OK\n";
    } else {
        die "controllo KO: $err \n";
    }
    
    $mail = undef;
    # RICEVUTA DI AVVENUTA CONSEGNA
    while (!$mail) {
        $mail = checkPOP3 ($POP3host1, $sndr_user, $sndr_pass);
        die if $mail == -1;
    }
    
    if (($err = checkReceipt ($msg -> as_string, $mail, $OPEC_HEADER_VALUE_RECEIPT_DELIVERY)) == 1) {
        print "controllo OK\n";
    } else {
        die "controllo KO: $err \n";
    }
}

sub doTestA () {
    for (my $testNumber = 1 ; $testNumber <= 8; $testNumber ++) {
 
        print "Test A.$testNumber\n";
        
        my $msg = MIME::Lite->new(
                         From     =>"$sndr_name <$sndr_email>",
                         To       =>"$rcpt_name <$rcpt_email>",
                         Subject  =>"Test " . localtime(time),
                         Data     =>"Test"
                         ) if ($testNumber != 5 && $testNumber != 6);
        
        next if ($testNumber >= 5);

        $msg -> attach(Type     =>'application/xls',
                       Path     =>'prova.xls',
                       Filename =>'prova.xls')
                if ($testNumber == 2);
        $msg -> attach(Type     =>'application/msword',
                       Path     =>'prova.doc',
                       Filename =>'prova.doc')
                if ($testNumber == 3);
        $msg -> attach(Type     =>'application/exe',
                       Path     =>'prova.exe',
                       Filename =>'prova.exe')
                if ($testNumber == 4);

        sendAndCheck ($msg);
    }
}

sub doTestB () {
    for (my $testNumber = 1 ; $testNumber <= 2; $testNumber ++) {
 
        print "Test B.$testNumber\n";
        
        my $msg = MIME::Lite->new(
                         From     =>"$sndr_name <$sndr_email>",
                         To       =>"$rcpt_name_ne <$rcpt_email_ne>",
                         Subject  =>"Test " . localtime(time),
                         Data     =>"Test"
                         );
        
        $msg -> attach(Type     =>'application/xls',
                       Path     =>'prova.xls',
                       Filename =>'prova.xls')
                if ($testNumber == 2);
                         
        flushPOP3 ($POP3host1, $sndr_user, $sndr_pass);
        flushPOP3 ($POP3host2, $rcpt_user, $rcpt_pass);

        sendMail ($SMTPhost1,
                  [1, $SMTPhost1AuthType, $SMTPhost1User, $SMTPhost1Pass],
                  $sndr_email, $rcpt_email,
                  $msg);

        sleep ($TIME_SLEEP);
        
        my $totalMsg = 0;
                    
        my $mail;
        # RICEVUTA DI ERRORE DI CONSEGNA
        while (!$mail) {
            $mail = checkPOP3 ($POP3host1, $sndr_user, $sndr_pass);
            die if $mail == -1;
        }
        
        if (($err = checkReceipt ($msg -> as_string, $mail, $OPEC_HEADER_VALUE_RECEIPT_ERRDELIVERY)) == 1) {
            print "controllo OK\n";
        } else {
            die "controllo KO: $err \n";
        }
    }
}

sub doTestC () {
    for (my $testNumber = 1 ; $testNumber <= 2; $testNumber ++) {
 
        print "Test C.$testNumber\n";
        
        my $msg = MIME::Lite->new(
                         From     =>"$sndr_name <$sndr_email>",
                         To       =>"$rcpt_name <$rcpt_email>",
                         Data     =>"Test"
                         );
        
        $msg -> attach(Type     =>'application/xls',
                       Path     =>'prova.xls',
                       Filename =>'prova.xls')
                if ($testNumber == 2);
                
        sendAndCheck ($msg);
    }
}

sub doTestD () {
    for (my $testNumber = 1 ; $testNumber <= 3; $testNumber ++) {
 
        print "Test D.$testNumber\n";
        
        my $msg = MIME::Lite->new(
                         From     =>"$sndr_name <$sndr_email>",
                         To       =>"$rcpt_name <$rcpt_email>",
                         Subject  =>"Test " . localtime(time),
                         Data     =>"\n"
                         
                         );
        
        $msg -> attach(Type     =>'application/xls',
                       Path     =>'prova.xls',
                       Filename =>'prova.xls')
                if ($testNumber == 2);
                         
        sendAndCheck ($msg);
    }
}