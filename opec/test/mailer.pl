#!/usr/bin/perl
# mailer a mo' di test
# flazan
# perche' sia un minimo usabile
# dovremmo prima di tutto remdere indipendente da OS
# e quello che mi viene subito in mente e' utilizzare
# File::Basename: non ho avuto voglia...
# inoltre manca interamente la gestione degli errori (good!)
# (vedi 32ln)
# Aggiunta l'autenticazione
# Aggiunta l'opzione di comunicazione tramite SMTP/S
# Aggiunta l'opzione multi destinatario: dest1,dest,

use MIME::Base64;
use IO::Socket;
use POSIX qw(uname);
use strict;

die "Syntax:\n$0 from to[,to2,..] path_or_file_mail num_of_mail server ssl [user] [psw] \n\n"
    if ($#ARGV < 4);

my ($mailFrom,$mailTo,$mailPath,$mail_num,$server,$ssl,$user,$psw) = @ARGV;

require IO::Socket::SSL if $ssl;

my ($host,$port) = split /:/,$server;
my @lstFile;
my $eol = "\015\012";
#my $eol = "\n";
$user = encode_base64($user); chomp($user);
$psw  = encode_base64($psw);  chomp($psw);
my @rcpt = split(/,/,$mailTo);
my $myhostname = (uname)[1];

my @raw_commands;
my $ind = 0;
if($user && $psw){
    $ind = 3;
    @raw_commands=(  "EHLO $myhostname$eol",
                     "AUTH LOGIN$eol",
                     "$user$eol",
                     "$psw$eol",
                     "MAIL FROM:<$mailFrom>$eol",
                     (map{"RCPT TO:<$_>$eol"} @rcpt),
                     "data$eol",
                     "$eol.$eol",
                     "QUIT$eol");
}else{
    @raw_commands=(  "EHLO $myhostname$eol",
                     "MAIL FROM:<$mailFrom>$eol",
                     (map{"RCPT TO:<$_>$eol"} @rcpt),
                     "data$eol",
                     "$eol.$eol",
                     "QUIT$eol");
}

# check $mailPath
# e avvaloro @lstFile
if(-d $mailPath) {
    opendir(PATH, $mailPath)
        || die "Impossibile accedere a $mailPath";
    my $fileName;
    while( defined ($fileName=readdir(PATH)) ) {
        chomp;
        next if($fileName =~ /^\.|\.\.$/);
        push @lstFile, $mailPath.'/'.$fileName;
    }
    closedir PATH;
}elsif(-f $mailPath && -r $mailPath) {
    push @lstFile, $mailPath;
}else{
    die "File o path $mailPath errato";
}

# per ogni file spedisce un msg per $mail_num volte
my $fileCount;
my $mailFile;
my $sck;
my @cp_lstFile = @lstFile;
for(my $i=0; $i<$mail_num; $i++) {
    @lstFile = @cp_lstFile;
    foreach(@lstFile) {
        $mailFile = $_;

        # connessione
        if($ssl){
            $sck = new IO::Socket::SSL
                            (PeerAddr => $host,
                            PeerPort => $port||465,
                            #Proto => 'tcp',
                            #Type => SOCK_STREAM,
                            Timeout => 5);
        }else{
            $sck = new IO::Socket::INET
                           (PeerAddr => $host,
                            PeerPort => $port||25,
                            Proto => 'tcp',
                            Type => SOCK_STREAM,
                            Timeout => 5);
        }
        die $@ unless $sck ;

        # I/O con mailhost
        my $count=0;
        while(<$sck>){
            print;

            if($count==4+$ind+@rcpt-1){
                open(MAILTEXT,$mailFile) || die "Errore: $@";
                while(<MAILTEXT>){ 
                    print $sck $_;
                    #$sck->send($_)
                }
                close MAILTEXT;
            }

            print $sck $raw_commands[$count];
            #print $raw_commands[$count];
            #$sck->send($raw_commands[$count]);
            $count++;

            if($count==5+$ind+@rcpt-1){
                $fileCount++;
                print "$fileCount spedito\n";
            }
        }

        $sck->close();
    }
}
