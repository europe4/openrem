# openrem



## What is it ?

This is a project to modify/fork openpec to the new european standard "REM" (registered electronic mail)

## what is openpec ?

openpec is a software for PEC (posta elettronica certificata), an italian standard for a kind of registered email.

## why this fork ?

Since the 2 standard is very similar, I think is easyer to start from here than start from 0.

## To do list

- edit this readme with other useful info
- translate all code's comments to englis
- do the job

## License

licence of openpec is GPLv2, so it is for this project

## Installation

WIP

## Usage

WIP

## Support

NO support ATM

## Roadmap

WIP

## Contributing

WIP, have to decide about it.

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Copy paste from openpec:

Project Author(s)
	Fanton Flavio <flazan@users.sourceforge.net>
	Manganelli Luca <luca76@users.sourceforge.net>
	Ferrara Umberto <uferrara@users.sourceforge.net>
	Battaglia Andrea <andreabat@users.sourceforge.net>

Project Contributor(s)
    Giovani Faglioni <giova23@users.sourceforge.net>

    Many thanks go to the people of the Amavis-new and OpenSSL 
    projects from where some of the used code comes from. 
    Many thanks to all of them, now and forever.

Project Beta Tester(s)
    Actually None


## Project status
WIP
In search of volunteer, project manager, coders, tester, traslators... Anything needed to conquer globe (or just Europe)...
