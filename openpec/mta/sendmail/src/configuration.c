/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */


/*
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */

#include "general.h"
#include <openssl/conf.h>

#include "support.h"
#include "configuration.h"

/* External imported variables */
extern int verbose;

/* Functions */

CONF *load_config( char *configfile, char **section ) {

	CONF *conf;
	long errorline= -1;

        if (configfile == NULL) configfile = getenv("PEC_CONF");
        if (configfile == NULL) configfile = CONFIG_FILE;
        if (configfile == NULL) return(NULL);

        syslog(LOG_INFO,"Using configuration from %s\n",configfile);
	conf = NCONF_new(NULL);
        if ( NCONF_load(conf, configfile, &errorline) <= 0 )
                {
                if (errorline <= 0)
                        syslog(LOG_ERR,
				"error loading the config file '%s'\n",
                                configfile);
                else
                        syslog(LOG_ERR,
				"error on line %ld of config file '%s'\n"
                                ,errorline, configfile);
                return(NULL);
                }

        if (*section == NULL) {
                *section=NCONF_get_string(conf,BASE_SECTION,ENV_DEFAULT_PECD);
                if (*section == NULL) {
                        lookup_fail(BASE_SECTION,ENV_DEFAULT_PECD);
                        return(NULL);
                }
		setenv( ENV_SECTION, *section, 1);
		if( verbose )
			syslog(LOG_INFO,"section set to %s", *section );
        }

	return(conf);
}

