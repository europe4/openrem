/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/*
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */

#include "general.h"

#ifdef HAVE_ENGINE
#include <openssl/engine.h>
#endif

#include <pwd.h>
#include <grp.h>

#include <time.h>
#include <sys/wait.h>

#include "configuration.h"
#include "support.h"
#include "server.h"
#include "pecd.h"

/* General exported variables */
#ifdef DEBUG
int debug = 1;
#else
int debug = 0;
#endif

int verbose = 0;

static char *pecd_usage[] = {
"PECd - OpenPEC daemon v" PACKAGE_VERSION "\n",
"(c) 2003 by Massimiliano Pala and OpenPEC Team\n",
"GPLv2+ Released Software\n",
"\n",
"   USAGE: pecd args\n",
"\n",
" -d              - Daemon, detach from current console\n",
" -p n            - Start listening on port n [9009]\n",
" -b addr         - Binds to ip <addr> [*]\n",
" -c file         - A config file\n",
" -k pwd          - Password protecting the private key (if any)\n",
" -i passin       - Passin arg\n",
#ifdef HAVE_ENGINE
" -e engine       - use engine e, possibly a hardware device.\n",
#endif
" -v              - Talk alot while doing things\n",
NULL
};

/* Staic variables */
char *prgname = "pecd";
PECD_CONFIG *pecd_conf = NULL;

/* Local functions prototypes */
int writePid ( int pid, char *pidfile );

/* Main */
int main ( int argc, char *argv[] ) {

	struct passwd *pw = NULL;
	struct group *gr = NULL;

	char *usr = NULL;
	char *group = NULL;

	char *port_s  = NULL;
	char *bind_s  = NULL;

	char *child_s = NULL;

	int daemon = 0;
	int badops = 0;
	int ret = 0;
	BIO *keyf = NULL;
	BIO *certf = NULL;

	char *pec_digest_name = NULL;
	char *keyfile = NULL;
	char *configfile = NULL;
	char *cert = NULL;
	char *cacert = NULL;
	char **pp = NULL;
	char *key = NULL;
	char *passargin = NULL;
	char *section = NULL;
	char *pidfile = NULL;
	char *tmp_s = NULL;
	int keyform = FORMAT_PEM;

        /* ADD ENGINE SUPPORT */
#ifdef HAVE_ENGINE
        char *engine=NULL;
        ENGINE *e = NULL;
#endif

	int i = 0;
	pid_t pid = 0;
	pid_t ppid = 0;

	int flags = 0;

	X509V3_add_standard_extensions();
	OpenSSL_add_all_algorithms();

	OpenSSL_add_all_digests();
	OpenSSL_add_all_ciphers();

	argv++;
	argc--;

	openlog( prgname, LOG_PID, LOG_DAEMON);
	while (argc >= 1)
		{
		/* Debuggin options */
#ifdef DEBUG
		syslog(LOG_ERR,"Parsing (%s)\n", *argv);
#endif
                if (strcmp(*argv,"-c") == 0)
                        {
                        if (--argc < 1) goto bad;
                        configfile= *(++argv);
                        }
		else if (strcmp(*argv,"-k") == 0)
			{
			if (--argc < 1) goto bad;
			key= *(++argv);
			}
		else if (strcmp(*argv,"-i") == 0)
			{
			if (--argc < 1) goto bad;
			passargin= *(++argv);
			}
#ifdef HAVE_ENGINE
		else if (strcmp(*argv,"-e") == 0)
			{
			if (--argc < 1) goto bad;
			engine = *(++argv);
			}
#endif
		else if (strcmp(*argv,"-p") == 0)
			{
			if (--argc < 1) goto bad;
			port_s = *(++argv);
			}
		else if (strcmp(*argv,"-b") == 0)
			{
			if (--argc < 1) goto bad;
			bind_s = *(++argv);
			}
		else if (strcmp(*argv,"-v") == 0)
			verbose=1;
		else if (strcmp(*argv,"-d") == 0)
			daemon=1;
		else badops = 1;
		argc--;
		argv++;
		}

bad:
	if (badops) {
		
		for (pp=pecd_usage; (*pp != NULL); pp++)
			printf(*pp);
		goto err;
	}

	ERR_load_crypto_strings();
	if(( pecd_conf = (PECD_CONFIG *) 
			OPENSSL_malloc ( sizeof(PECD_CONFIG))) == NULL ) {
		syslog(LOG_ERR, "Memory Allocation error");
		exit(-1);
	}

	/* load configuration file */
	if( configfile == NULL ) 
		{
		configfile = (char *) OPENSSL_malloc ( 1024 );
		sprintf(configfile, "%s%s", PREFIX, PECD_DEF_CONFIG );
		}

	if( debug )
		syslog( LOG_ERR, "Configuration filename : %s (section %s)",
			configfile, section );

	if( (pecd_conf->conf = load_config( configfile, &section )) == NULL ) {
		syslog( LOG_ERR,"can not load configuration file %s (section %s)",
				configfile, section );
		exit(-1);
	}

	/* Copy the conf filename so that it could be re-loaded on SIGHUP */
	if( (pecd_conf->cnf_filename = 
		(char *) OPENSSL_malloc(strlen(configfile) + 1)) == NULL ) {
		syslog( LOG_ERR,"memory allocation error (%d)", __LINE__ );
		goto err;
	}

	/* Copy the db filename so that it could be re-loaded on SIGHUP */
	strcpy( pecd_conf->cnf_filename, configfile);

        /* ENGINE support added */
#ifdef HAVE_ENGINE
	if (engine != NULL) {
		if((e = ENGINE_by_id(engine)) == NULL) {
			syslog(LOG_ERR,"invalid engine \"%s\"", engine);
			goto err;
		}

		if(!ENGINE_set_default(e, ENGINE_METHOD_ALL)) {
			syslog(LOG_ERR,"can't use that engine");
			goto err;
		}
		syslog(LOG_ERR,"engine \"%s\" set.\n", engine);
		ENGINE_free(e);
	}
#endif

        /* Passin support */
        if(!key && !app_passwd(passargin, NULL, &key, NULL)) {
                syslog(LOG_ERR, "Error getting passwords\n");
                goto err;
        }

	/*****************************************************************/
        /* Reading Private key file */
        if ((keyfile == NULL) && 
			((keyfile = NCONF_get_string(pecd_conf->conf, section,
					ENV_PRIVATE_KEY)) == NULL)) {
                lookup_fail(section,ENV_PRIVATE_KEY);
		goto err;
       	}

        if( verbose )
                syslog(LOG_INFO,"Reading Private Key file %s", keyfile);
        if ((keyf=BIO_new_file( keyfile, "r")) == NULL) {
		syslog( LOG_ERR,"cannot open BIO file, why ?" );
                goto err;
	}

        if ( keyform == FORMAT_ENGINE)
	        {
#ifdef HAVE_ENGINE
		/*
                if(!e) {
			BIO_printf( bio_err,"no engine loaded!\n");
                        goto err;
                }
                pkey = ENGINE_load_private_key( e, keyfile, key );
		*/
                syslog(LOG_ERR,"No engine support yet.\n");
                goto err;
#else
                syslog(LOG_ERR,"No engine support compiled.\n");
                goto err;
#endif
	        } else if ( keyform == FORMAT_PEM ) {
	                pecd_conf->pecd_pkey = 
				PEM_read_bio_PrivateKey(keyf,NULL,NULL,key);
	                if (key) memset(key,0,strlen(key));
	        } else {
                syslog(LOG_ERR,"bad input format specified for key file\n");
		goto err;
        };
        if( keyf ) BIO_free( keyf );

        if( pecd_conf->pecd_pkey == NULL ) {
                syslog(LOG_ERR,"Error loading private key\n");
                goto err;
        };


	/*****************************************************************/
	/* Load the PEC certificate file */
        if ((cert == NULL) && ((cert=NCONF_get_string(pecd_conf->conf,
                section,ENV_CERTIFICATE)) == NULL)) {
                lookup_fail(section,ENV_CERTIFICATE);
        }

	if ( cert ) {
		if( verbose )
	                syslog(LOG_INFO,"reading certificate file.\n");
	        if ((certf=BIO_new_file( cert, "r")) == NULL) {
	                syslog(LOG_ERR,"unable to open certificate file.\n");
	                goto err;
		}
	        if ((pecd_conf->pecd_cert = PEM_read_bio_X509(certf,NULL,NULL,NULL))
				==NULL) {
	                syslog(LOG_ERR,"cannot load certificate.\n");
	                goto err;
		}
	        BIO_free( certf );
	}

        if (pecd_conf->pecd_cert == NULL) {
               	syslog(LOG_ERR,"unable to load PEC certificate\n");
               	goto err;
	}

        if (!X509_check_private_key(pecd_conf->pecd_cert,pecd_conf->pecd_pkey)) {
               	syslog(LOG_ERR, "PEC cert and private key do not match\n");
               	goto err;
	}

	/*****************************************************************/
	/* Load the CA certificate file */
        if ((cacert == NULL) && ((cacert=NCONF_get_string(pecd_conf->conf,
                section,ENV_CA_CERTIFICATE)) == NULL))
                {
                lookup_fail(section,ENV_CA_CERTIFICATE);
                goto err;
                }
	if ( cacert ) {
		if( verbose )
	                syslog(LOG_INFO,"reading CA certificate file.\n");
	        if ((certf=BIO_new_file( cacert, "r")) == NULL) {
	                syslog(LOG_ERR,"unable to open CA certificate file.\n");
	                goto err;
		}
	        if ((pecd_conf->cacert = PEM_read_bio_X509(certf,NULL,NULL,NULL))
				==NULL) {
	                syslog(LOG_ERR,"cannot load CA certificate.\n");
	                goto err;
		}
	        BIO_free( certf );
	}
        if (pecd_conf->cacert == NULL) {
                syslog(LOG_ERR,"unable to load CA certificate\n");
                goto err;
        }

	if(verbose)
		syslog(LOG_INFO,"PEC Daemon setup completed");

        /*****************************************************************/
        /* Let's get the digest */
        if ((pec_digest_name == NULL) &&
	                ((pec_digest_name=NCONF_get_string(pecd_conf->conf,section,
                                                   ENV_PECD_MD)) == NULL)) {
                lookup_fail(section,ENV_PECD_MD);
                goto err;
         }

        pecd_conf->digest = (EVP_MD *) EVP_get_digestbyname(pec_digest_name);
        if( pecd_conf->digest == NULL ) {
                syslog(LOG_ERR, "unsupported digest type %s",
                                                        pec_digest_name);
                goto err;
        }

	/*****************************************************************/
	/* Load other important config keys */

	/* Listen to port_s */
        if ((port_s == NULL) && ((port_s = NCONF_get_string(pecd_conf->conf, section,
					ENV_PECD_PORT)) == NULL)) {
               	if( verbose )
			lookup_fail(section,ENV_PECD_PORT);
		port_s = PEC_DEF_PORT_S;
       	}

	/* Binds to address */
        if ((bind_s == NULL) && ((bind_s = NCONF_get_string(pecd_conf->conf, section,
					ENV_PECD_BIND)) == NULL)) {
               	if( verbose )
			lookup_fail(section,ENV_PECD_BIND);
		bind_s = PEC_DEF_BIND_S;
       	}

	/* Get the user name to set the process to */
        if ((group == NULL) && ((group = NCONF_get_string(pecd_conf->conf, section,
					ENV_PECD_GROUP)) == NULL)) {
               	if( verbose )
			lookup_fail(section,ENV_PECD_GROUP);

		group = PECD_DEF_GROUP;
       	}

	/* Get the user name to set the process to */
        if ((usr == NULL) && ((usr = NCONF_get_string(pecd_conf->conf, section,
					ENV_PECD_USER)) == NULL)) {
               	if( verbose )
			lookup_fail(section,ENV_PECD_USER);

		usr = PECD_DEF_USER;
       	}

	/* Get the pidfile name to write main pid to */
        if ((pidfile == NULL) && ((pidfile = NCONF_get_string(pecd_conf->conf, section,
					ENV_PECD_PIDFILE)) == NULL)) {
               	if( verbose )
			lookup_fail(section,ENV_PECD_PIDFILE);

		pidfile = (char *) OPENSSL_malloc (1024);
		sprintf( pidfile, "%s%s", PREFIX, PECD_DEF_PIDFILE );
       	}
	/*****************************************************************/
	/* PECD crl access */

	/* Get CRL section name */
	/*
        if ((dbms_section = NCONF_get_string(pecd_conf->conf, section,
					ENV_PECD_DBMS)) == NULL) {
               	if( verbose )
			lookup_fail(section, ENV_PECD_DBMS);
		dbms_section = ENV_DEFAULT_PECD;
       	}
	*/

	/* Get the crl URL */
	/*
        if ((crlUrl_s = NCONF_get_string(pecd_conf->conf, dbms_section,
					ENV_PECD_CRL_URL)) == NULL) {
               	if( verbose )
			lookup_fail(dbms_section, ENV_PECD_CRL_URL);
       	} else {
		pecd_conf->crl_data.url = getParsedUrl(crlUrl_s);
		if( ! pecd_conf->crl_data.url ) {
			syslog(LOG_ERR, "ERROR in CRL url specification %s",
					crlUrl_s);
			goto err;
		}
	}
	*/

	/* Get the CRL entry, usually the Base DN of the LDAP */
	/*
	if( pecd_conf->crl_data.url->proto == PEC_CRL_PROTO_LDAP ) {
		if( verbose )
			syslog(LOG_ERR,"Using LDAP protocol for CRL retrivial");
        	if ((crlEntry_s = NCONF_get_string(pecd_conf->conf, dbms_section,
					ENV_PECD_CRL_ENTRY_DN)) == NULL) {
        	       	if( verbose )
				lookup_fail(dbms_section, ENV_PECD_CRL_URL);
       		} else {
			pecd_conf->crl_data.url->dn = (crlEntry_s);
		}
	}
	*/

	/* load CRL data */
	/*
	pecd_conf->crl_data.crl = pecd_get_crl(pecd_conf->crl_data.url);

	if( !pecd_conf->crl_data.crl ) {
		syslog( LOG_ERR, "Cannot access CRL data (%s)", crlUrl_s );
			goto err;
	}
	*/

	/*****************************************************************/
	/* PECD responder specific options */
	// pecd_conf->flags = 0;

	/* Include in the response the other_certs X509 STACK */
	/*
        if ((tmp_s = NCONF_get_string(pecd_conf->conf, resp_section,
				ENV_PECD_RESPONSE_OTHER_CERTS)) == NULL) {
               	if( verbose )
			lookup_fail(resp_section, ENV_PECD_RESPONSE_OTHER_CERTS);
		pecd_conf->flags |= PEC_NOCERTS;
       	} else {
		pecd_conf->other_certs = load_certs( NULL, tmp_s,
			FORMAT_PEM, NULL, "responder other certificates" );
	}
	*/

	/*****************************************************************/
	/* Let's have the program running under username.groupid, somewhere */

	if( (gr = getgrnam( group ) ) == NULL ) {
		syslog( LOG_ERR, "Cannot find group %s", group);
		goto err;
	}
	
	if (setgid (gr->gr_gid) == -1) {
		syslog(LOG_ERR,"Error setting group %d (%s)", gr->gr_gid, group);
		exit (1);
	}

	if( (pw = getpwnam( usr ) ) == NULL ) {
		syslog( LOG_ERR, "Cannot find user %s", usr);
		goto err;
	}

	if (setuid (pw->pw_uid) == -1) {
		syslog(LOG_ERR,"Error setting user %d (%s)", pw->pw_uid, usr );
		exit (1);
	}

	/*****************************************************************/
	/* Main spawn and signal routines */

	if (verbose)
		syslog(LOG_INFO,"Configuration loaded and parsed");

	if( daemon ) {
		pid = fork();
		if( pid == 0 ) {
			/* Nop */
		} else if ( pid > 0 ) {
			/* Main process, we have to save the pid to the
			 * pidfile and then exit */
			ppid = pid;
			writePid( ppid, pidfile );
			goto end;
		} else {
			syslog( LOG_ERR, "Error While spawning child %d", i );
			goto err;
		}
	} else {

		ppid = getpid();
		writePid( ppid, pidfile );
	}

	start_server( bind_s, port_s, pecd_conf );

	goto end;


err:
	ret = -1;

end:
	exit (ret);
}

int writePid ( int pid, char *pidfile ) {
	FILE *fd;

	if( (fd = fopen( pidfile, "w" )) == 0 ) {
		syslog( LOG_ERR, "Cannot open pidfile (%s)",
				pidfile );
		return(0);
	}

	fprintf( fd, "%d", getpid());
	fclose( fd );

	return(1);
}

