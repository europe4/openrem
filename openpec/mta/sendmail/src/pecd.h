/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/*
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */

#ifndef _PECD_HEADER

#ifndef PREFIX
#define PREFIX			"/usr/local"
#endif

#ifdef PECD_USER
#define PECD_DEF_USER		PECD_USER
#else
#define PECD_DEF_USER		"pecd"
#endif

#ifdef PECD_GROUP
#define PECD_DEF_GROUP		PECD_GROUP
#else
#define PECD_DEF_GROUP		"daemon"
#endif

#ifdef PEC_CONFIG
#define PECD_DEF_CONFIG		PEC_CONFIG
#else
#define PECD_DEF_CONFIG		"/etc/pecd.conf"
#endif

#define PECD_DEF_PIDFILE	"/var/run/pecd.pid"

#define _PECD_HEADER

#endif


