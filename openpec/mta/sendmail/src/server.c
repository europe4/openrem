/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/*
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */
 
#include "general.h"

#ifdef __FreeBSD__
#include <netinet/in.h>
#endif

#include <resolv.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <unistd.h>

#include <openssl/err.h>
#include <openssl/ssl.h>

#include <openssl/conf.h>
#include <openssl/txt_db.h>

#include "support.h"
#include "server.h"

/* External General Variables */
extern int debug;
extern int verbose;
extern PECD_CONFIG *pecd_conf;

static int *child_list = NULL;
static int max_child = 1;

int start_server( char *bind_s, char *port_s, PECD_CONFIG *pecd_conf ) {

	BIO *bio_socket, *buf_bio;

	char *buf = NULL;
	char tmp_socket[1024];
	char *addr = NULL;
	int addr_len;
	int max_size = 1024;

	int i = 0;
	long buf_len = 0;

	/* Let's add for later SSL support */
	SSL_load_error_strings();
	OpenSSL_add_ssl_algorithms();

	/* No max size under 1024 */
	// if( max_size < 1024 ) max_size = 1024;
	
	/* Alloc addr for binding to specific address:port */
	addr_len = strlen(bind_s) + strlen(port_s) + 2 ;
	if((addr = (char *) OPENSSL_malloc (addr_len)) == NULL ) {
		syslog(LOG_ERR, "Cannot alloc addr");
		exit( -5 );
	}

	/* We build the "host:port" string to be used */
	sprintf( addr, "%s:%s", bind_s, port_s );

	if( (bio_socket = init_connection( addr )) == NULL ) {
		syslog(LOG_ERR,"Can not setup socket, exit.");
		exit( -6 );
	}
	
	// Register signal handlers
	// signal( SIGCHLD, handle_sigchld );
	// signal( SIGHUP, handle_sighup );

	while ( bio_socket ) {
		handle_connection( &bio_socket, pecd_conf );
	}

	return(0);
}

BIO *init_connection ( char *addr ) {

	BIO *bio_socket, *buf_bio;

	/* Set buffered bio */
	buf_bio = BIO_new(BIO_f_buffer());
	if (!buf_bio) {
		syslog( LOG_ERR, "Can not set bufbio %d", __LINE__ );
		return (NULL);
	}

	/* Bind to socket */
	if((bio_socket = BIO_new_accept( addr )) == NULL ) {
		syslog(LOG_ERR, "Cannot bind to %s", addr);
		return (NULL);
	}

	if (verbose) syslog(LOG_INFO, "successfully binded to %s", addr);

        BIO_set_accept_bios(bio_socket, buf_bio);
	buf_bio = NULL;
	if (BIO_do_accept(bio_socket) <= 0)
	{
		syslog( LOG_ERR, "Error setting up accept BIO");
		BIO_free_all( bio_socket );
		BIO_free( buf_bio );

		return( NULL );
	}

	return bio_socket;
}

int handle_connection( BIO **bio_socket, PECD_CONFIG *pecd_conf ) {

	int pid, i, ret = 0;
	int status = 0;
        BIO *curr_bio = NULL;
	BIO *out = NULL;
	BIO *pbio_socket;

	pbio_socket = *bio_socket;

	if (BIO_do_accept( pbio_socket ) <= 0) {
		return 0;
	}
	curr_bio = BIO_pop( pbio_socket );

	pid = fork();
	if( pid < 0 ) {
		ret = pid;
		goto end;
	} else if ( pid > 0 ) {
		if( verbose ) {
			syslog( LOG_INFO, "Spawned child process [%d]", pid );
		}
		goto end;
	}

	signal(SIGHUP, just_die);
	signal(SIGUSR1, just_die);
	signal(SIGTERM, just_die);

	BIO_printf(curr_bio, "250- OpenPEC Daemon v.0.0.1\n\n");

	/*
	if((pec_req = get_pec_request( curr_bio )) == NULL ) {
		syslog(LOG_ERR, "Can not parse pec request");
		goto err;
	}

	if(( make_pec_response(&pec_resp, pec_req, pecd_conf)) == 0 ) {
		syslog( LOG_ERR, "Error in generating response" );
		goto err;
	}
	*/

	/*
	send_pec_response( curr_bio, pec_resp );
	*/

	goto end;

err:
	/*
	send_pec_response( curr_bio, pec_resp );
	*/

end:
	if( pid == 0 ) {
		BIO_free_all(curr_bio);
		if(out) BIO_free (out);
	}

	return ret;
}

/*
PEC_REQUEST *get_pec_request( BIO *bio ) {

	int r, have_post, req_len;
	char buf[1024];
	char *header = NULL;

	PEC_REQUEST *pec_req = NULL;

	req_len = 0;
	have_post = 0;
	for( ;; ) {
		r = BIO_gets(bio, buf, 1024);
		if( r <= 0 )
			return (NULL);

		if(!have_post) {
			if( strncmp( buf, "POST", 4) ) {
				syslog(LOG_ERR,"Method is not POST, rejecting");
				return (NULL);
			}
			have_post = 1;
		}

		// Here we have seen all the header lines
		if((buf[0] == '\n') || (buf[0] == '\r')) {
			break;
		}
	}

	pec_req = d2i_PEC_REQUEST_bio(bio, NULL);
	if (!pec_req) {
		syslog( LOG_ERR, "Error parsing PEC request\n");
		return(NULL);
	}

	return pec_req;
}
*/

/*
int send_pec_response(BIO *bio, PEC_RESPONSE *resp) {

	char http_resp[] =
		"HTTP/1.0 200 OK\r\nContent-type: application/pec-response\r\n"
 		"Content-Length: %d\r\n\r\n";

	if (!bio) {
		syslog(LOG_ERR, "Error sending response, bio is NULL");
		return 0;
	}

	BIO_printf(bio, http_resp, i2d_PEC_RESPONSE(resp, NULL));
	i2d_PEC_RESPONSE_bio(bio, resp);
	BIO_printf(bio,"\n\n");

	BIO_flush(bio);
	return 1;
}
*/

void just_die ( int code ) {

#ifdef DEBUG
	syslog( LOG_INFO, "Just Die [pid %d: %d]", getpid(), getppid());
#endif
	exit(code);
};

