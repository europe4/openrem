/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/*
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */

extern unsigned long index_serial_hash(char **a);
extern void lookup_fail(char *name, char *tag);
extern char *app_get_pass( char *arg, int keepbio);
extern int app_passwd( char *arg1, char *arg2, char **pass1, char **pass2);
extern int str2fmt(char *s);
extern int strcmp_nocase( char *a, char *b );
int strncmp_nocase( char *a, char *b, int min );

URL *getParsedUrl ( char *url_s );

STACK_OF(X509) *load_certs(BIO *err, const char *file, int format,
	const char *pass, const char *cert_descrip);
