/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/* OpenPEC daemon - (c) 2003 by Massimiliano Pala and OpenPEC Team */

#ifndef _PECD_GENERAL_HEADER

/* External Variables */
#if STDC_HEADERS
# include <string.h>
# include <stdio.h>
# include <stdlib.h>
#else
# ifndef HAVE_STRCHR
#  define strchr index
#  define strrchr rindex
# endif
char *strchr (), *strrchr ();
# ifndef HAVE_MEMCPY
#  define memcpy(d, s, n) bcopy ((s), (d), (n))
#  define memmove(d, s, n) bcopy ((s), (d), (n))
# endif
#endif

#include <syslog.h>
#include <ctype.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <openssl/conf.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/bn.h>
#include <openssl/txt_db.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/objects.h>
#include <openssl/pem.h>

#define PEC_DEF_PORT_S		"9009"
#define PEC_DEF_BIND_S		"*"

#define BASE_SECTION    	"pecd"
#define CONFIG_FILE 		"pecd.conf"
#define ENV_SECTION 		"conf_section"

#define ENV_DEFAULT_PECD	"default_pecd"
#define ENV_PRIVATE_KEY		"pecd_key"
#define ENV_CERTIFICATE		"pecd_certificate"
#define ENV_CA_CERTIFICATE	"ca_certificate"
#define ENV_PECD_PORT		"port"
#define ENV_PECD_BIND		"bind"
#define ENV_PECD_MD		"md"
#define ENV_PECD_CRL_URL	"crl_url"
#define ENV_PECD_CRL_ENTRY_DN	"crl_entry_dn"
#define ENV_PECD_PIDFILE	"pidfile"

#define ENV_PECD_USER		"user"
#define ENV_PECD_GROUP		"group"

#define FORMAT_UNDEF    0
#define FORMAT_ASN1     1
#define FORMAT_TEXT     2
#define FORMAT_PEM      3
#define FORMAT_NETSCAPE 4
#define FORMAT_PKCS12   5
#define FORMAT_SMIME    6
#define FORMAT_ENGINE   7

#define APP_PASS_LEN    1024

typedef struct pw_cb_data
	{
	const void *password;
	const char *prompt_info;
	} PW_CB_DATA;

#define PEC_CRL_PROTO_FILE		0
#define PEC_CRL_PROTO_LDAP		1
#define PEC_CRL_PROTO_HTTP		2

#define PEC_DEFAULT_LDAP_PORT		389
#define PEC_DEFAULT_HTTP_PORT		80

typedef struct url_data
	{
		/* Protocol, currently supported LDAP and FILE */
		int proto;

		/* Address or filename */
		char *addr;

		/* Communication Port (where supported by the protocol) */
		int port;

		/* Authentication (where supported by the protocol) */
		char *usr;
		char *pwd;

		/* LDAP specific */
		char *dn;
	} URL;

typedef struct crl_data
	{
		/* CRL access method PEC_CRL_METHOD_... */
		/* Filename */
		URL *url;

		X509_CRL *crl;
	} CRL_DATA;

typedef struct pecd_config {

	/* Configuration file name */
	char 		*cnf_filename;

	/* CONF strucutre pointer */
	CONF 		*conf;
	EVP_MD		*digest;

	/* CA's  certificate */
	X509		*cacert;

	/* PEC responder certificate and pkey */
	X509 		*pecd_cert;
	EVP_PKEY 	*pecd_pkey;

	/* Other certificates to be added to the response, if any */
	STACK_OF(X509) *other_certs;

	/* CRL data */
	CRL_DATA 	crl_data;

} PECD_CONFIG;

#define __PECD_GENERAL_HEADER
#endif
