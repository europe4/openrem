/*
 * Copyright (C) 2003 by Massimiliano Pala <madwolf@openpec.org>
 * OpenPEC project - http://www.openpec.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/*
 * ====================================================================
 *
 * This product includes cryptographic software written by Eric Young
 * (eay@cryptsoft.com).  This product includes software written by Tim
 * Hudson (tjh@cryptsoft.com).
 *
 */

#include "general.h"

#include <openssl/asn1.h>
#include <openssl/txt_db.h>
#include <openssl/conf.h>
#include <openssl/buffer.h>
#include <openssl/x509.h>

/* Functions */
void lookup_fail(char *name, char *tag) {
        syslog(LOG_ERR,"variable lookup failed for %s::%s\n",name,tag);
	return;
}

int strcmp_nocase( char *a, char *b ) {

	int i = 0, ret = 0;
       
	while( a[i] && b[i]) {
		ret = (short) toupper(a[i]) -  (short) toupper(b[i]);
		if (ret) return (ret);
		i++;
	}
	
	return (ret);
}

int strncmp_nocase( char *a, char *b, int min ) {

	int i = 0, ret = 0;
       
	// syslog( LOG_INFO, "'%s' => '%s'",a,b);

	while( a[i] && b[i]) {
		ret = (short) toupper(a[i]) -  (short) toupper(b[i]);
		if (ret) return (ret);
		i++;
	}

	if( i < min ) return( a[i] || b[i] );
	
	return (ret);
}

int str2fmt(char *s) {
	if 	((*s == 'D') || (*s == 'd'))
		return(FORMAT_ASN1);
	else if ((*s == 'T') || (*s == 't'))
		return(FORMAT_TEXT);
	else if ((*s == 'P') || (*s == 'p'))
		return(FORMAT_PEM);
	else if ((*s == 'N') || (*s == 'n'))
		return(FORMAT_NETSCAPE);
	else if ((*s == 'S') || (*s == 's'))
		return(FORMAT_SMIME);
	else if ((*s == '1')
		|| (strcmp(s,"PKCS12") == 0) || (strcmp(s,"pkcs12") == 0)
		|| (strcmp(s,"P12") == 0) || (strcmp(s,"p12") == 0))
		return(FORMAT_PKCS12);
	else if ((*s == 'E') || (*s == 'e'))
		return(FORMAT_ENGINE);
	else
		return(FORMAT_UNDEF);
	}


char *app_get_pass( char *arg, int keepbio)
{
	char *tmp, tpass[APP_PASS_LEN];
	static BIO *pwdbio = NULL;
	int i;
	if(!strncmp(arg, "pass:", 5)) return BUF_strdup(arg + 5);
	if(!strncmp(arg, "env:", 4)) {
		tmp = getenv(arg + 4);
		if(!tmp) {
			syslog(LOG_ERR,
				"Can't read environment variable %s", arg + 4);
			return NULL;
		}
		return BUF_strdup(tmp);
	}
	if(!keepbio || !pwdbio) {
		if(!strncmp(arg, "file:", 5)) {
			pwdbio = BIO_new_file(arg + 5, "r");
			if(!pwdbio) {
				syslog(LOG_ERR, "Can't open file %s",arg + 5);
				return NULL;
			}
		} else if(!strncmp(arg, "fd:", 3)) {
			BIO *btmp;
			i = atoi(arg + 3);
			if(i >= 0) pwdbio = BIO_new_fd(i, BIO_NOCLOSE);
			if((i < 0) || !pwdbio) {
				syslog(LOG_ERR, "Can't access file descriptor %s", arg + 3);
				return NULL;
			}
			/* Can't do BIO_gets on an fd BIO so add a buffering
			 * BIO */
			btmp = BIO_new(BIO_f_buffer());
			pwdbio = BIO_push(btmp, pwdbio);
		} else if(!strcmp(arg, "stdin")) {
			pwdbio = BIO_new_fp(stdin, BIO_NOCLOSE);
			if(!pwdbio) {
				syslog(LOG_ERR, "Can't open BIO for stdin");
				return NULL;
			}
		} else {
			syslog(LOG_ERR, "Invalid password argument \"%s\"", arg);
			return NULL;
		}
	}
	i = BIO_gets(pwdbio, tpass, APP_PASS_LEN);
	if(keepbio != 1) {
		BIO_free_all(pwdbio);
		pwdbio = NULL;
	}
	if(i <= 0) {
		syslog(LOG_ERR, "Error reading password from BIO");
		return NULL;
	}
	tmp = strchr(tpass, '\n');
	if(tmp) *tmp = 0;
	return BUF_strdup(tpass);
}


int app_passwd( char *arg1, char *arg2, char **pass1, char **pass2)
{
	int same;
	if(!arg2 || !arg1 || strcmp(arg1, arg2)) same = 0;
	else same = 1;
	if(arg1) {
		*pass1 = app_get_pass(arg1, same);
		if(!*pass1) return 0;
	} else if(pass1) *pass1 = NULL;
	if(arg2) {
		*pass2 = app_get_pass(arg2, same ? 2 : 0);
		if(!*pass2) return 0;
	} else if(pass2) *pass2 = NULL;
	return 1;
}

STACK_OF(X509) *load_certs(BIO *err, const char *file, int format,
	const char *pass, const char *cert_descrip)
	{
	BIO *certs;
	int i;
	STACK_OF(X509) *othercerts = NULL;
	STACK_OF(X509_INFO) *allcerts = NULL;
	X509_INFO *xi;
	PW_CB_DATA cb_data;

	cb_data.password = pass;
	cb_data.prompt_info = file;

	if((certs = BIO_new(BIO_s_file())) == NULL)
		{
		ERR_print_errors(err);
		goto end;
		}

	if (file == NULL)
		BIO_set_fp(certs,stdin,BIO_NOCLOSE);
	else
		{
		if (BIO_read_filename(certs,file) <= 0)
			{
			BIO_printf(err, "Error opening %s %s\n",
				cert_descrip, file);
			ERR_print_errors(err);
			goto end;
			}
		}

	if      (format == FORMAT_PEM)
		{
		othercerts = sk_X509_new_null();
		if(!othercerts)
			{
			sk_X509_free(othercerts);
			othercerts = NULL;
			goto end;
			}
		allcerts = PEM_X509_INFO_read_bio(certs, NULL,
				(pem_password_cb *)NULL, &cb_data);
		for(i = 0; i < sk_X509_INFO_num(allcerts); i++)
			{
			xi = sk_X509_INFO_value (allcerts, i);
			if (xi->x509)
				{
				sk_X509_push(othercerts, xi->x509);
				xi->x509 = NULL;
				}
			}
		goto end;
		}
	else	{
		BIO_printf(err,"bad input format specified for %s\n",
			cert_descrip);
		goto end;
		}
end:
	if (othercerts == NULL)
		{
		BIO_printf(err,"unable to load certificates\n");
		ERR_print_errors(err);
		}
	if (allcerts) sk_X509_INFO_pop_free(allcerts, X509_INFO_free);
	if (certs != NULL) BIO_free(certs);
	return(othercerts);
	}

URL *getParsedUrl ( char *url_s ) {

	URL *ret = NULL;
	char *tmp_s = NULL;
	char *tmp_s2 = NULL;
	char *tmp_s3 = NULL;
	int len;

	ret = (URL *) OPENSSL_malloc ( sizeof( URL ));
	if( !ret ) return NULL;

	memset( ret, '\x0', sizeof( URL ));

	if( strncmp("ldap://", url_s, 6 ) == 0)
		{
		ret->proto = PEC_CRL_PROTO_LDAP;
		ret->addr = (char *) OPENSSL_malloc ( 256 );
		tmp_s = &url_s[7];

		if( tmp_s3 = strchr(tmp_s, '@') ) {
			tmp_s2 = strchr( tmp_s, ':' );

			if( !tmp_s2 || (tmp_s3 < tmp_s2)) {
				syslog( LOG_ERR, "Malformed CRL url (usr:pwd)");
				return NULL;
			}
			len = (int) ( (long) tmp_s2 - (long) tmp_s );
			ret->usr = (char *) OPENSSL_malloc (len+1);
			strncpy( ret->usr, tmp_s, len);
			ret->usr[len] = '\x0';
			
			tmp_s = tmp_s2+1;
			tmp_s2 = strchr( tmp_s,'@');

			len = (int) ( (long) tmp_s2 - (long) tmp_s );
			ret->pwd = (char *) OPENSSL_malloc (len+1);
			strncpy( ret->pwd, tmp_s, len);
			ret->pwd[len] = '\x0';

			tmp_s = tmp_s2+1;

		}

		if( strchr( tmp_s, ':' )) {
			tmp_s2 = strchr(tmp_s,':');

			len = (int) ( (long) tmp_s2 - (long) tmp_s );
			ret->addr = (char *) OPENSSL_malloc (len+1);
			strncpy( ret->addr, tmp_s, len);
			ret->addr[len] = '\x0';

			tmp_s = tmp_s2+1;
			ret->port = atoi( tmp_s );
		} else {
			strcpy( ret->addr, &url_s[7]);
			ret->port = PEC_DEFAULT_LDAP_PORT;
			}

		}
	else if (strncmp( "file://", url_s, 6 ) == 0)
		{
		ret->port = -1;
		ret->proto = PEC_CRL_PROTO_FILE;
		ret->addr = (char *) OPENSSL_malloc ( 256 );
		strcpy( ret->addr, &url_s[7]);
		}
	else {
		OPENSSL_free( ret );
		return NULL;
	}

	return ret;
}
